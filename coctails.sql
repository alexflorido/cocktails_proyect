-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: coctails_bd
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cargo_personal`
--

DROP TABLE IF EXISTS `cargo_personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cargo_personal` (
  `id_cargo_per` int(11) NOT NULL AUTO_INCREMENT,
  `nom_cargo` varchar(50) DEFAULT NULL,
  `desc_cargo` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_cargo_per`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cargo_personal`
--

LOCK TABLES `cargo_personal` WRITE;
/*!40000 ALTER TABLE `cargo_personal` DISABLE KEYS */;
INSERT INTO `cargo_personal` VALUES (1,'Generte Producción',NULL),(2,'Bartender',NULL),(3,'Supervisor',NULL),(4,'Ayudante de Barra',NULL),(5,'Jefe de Barra',NULL);
/*!40000 ALTER TABLE `cargo_personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_personal`
--

DROP TABLE IF EXISTS `categoria_personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_personal` (
  `id_cat_per` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cat` varchar(50) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_cat_per`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_personal`
--

LOCK TABLES `categoria_personal` WRITE;
/*!40000 ALTER TABLE `categoria_personal` DISABLE KEYS */;
INSERT INTO `categoria_personal` VALUES (1,'Personal Eventual',NULL),(2,'Personal Fijo',NULL);
/*!40000 ALTER TABLE `categoria_personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `id_cli` int(11) NOT NULL AUTO_INCREMENT,
  `nom_cli` varchar(50) DEFAULT NULL,
  `nit_cli` int(11) DEFAULT NULL,
  `tel_cli` int(11) DEFAULT NULL,
  `email_cli` varchar(50) DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_cli`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'Capresso',1233213,79374484,'Capresso@gmail.com',NULL),(3,'asdsad',123213,234324,'asdasd','2021-12-09 11:10:18'),(4,'No Existe',NULL,NULL,NULL,'null'),(5,'Alexander',5299072,71288123,'alex@gmail.com','2021-12-09 10:59:44'),(6,'Alexander Florido Siles',5299072,71288123,'alex@gmail.com','2022-02-07 07:33:37'),(7,'asdasd',123123,1231312312,'asdas@asda.com','2021-12-09 11:10:13'),(8,'SOCIEDAD AGRO INDUSTRIAL DEL VALLE L.T.D.A.',1024725024,24452123,'saiv@gmail.com',NULL),(9,'PAULA DEHEZA & OSCAR ABASTOFLOR',0,0,'paula@gmail.com',NULL),(10,'LEVENTCORP. S.R.L.',395671026,0,'leventcorp@gmail.com',NULL),(11,'Fabiola Cadima',4444444,1234567,'correo@gmail.com',NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacto_proveedor`
--

DROP TABLE IF EXISTS `contacto_proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacto_proveedor` (
  `id_cont_prov` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_cont` varchar(50) DEFAULT NULL,
  `email_cont` varchar(50) DEFAULT NULL,
  `telf_cont` int(11) DEFAULT NULL,
  `estado` varchar(10) DEFAULT 'Activo',
  `id_prov` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_cont_prov`),
  KEY `id_prov` (`id_prov`),
  CONSTRAINT `contacto_proveedor_ibfk_1` FOREIGN KEY (`id_prov`) REFERENCES `proveedor` (`id_prov`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacto_proveedor`
--

LOCK TABLES `contacto_proveedor` WRITE;
/*!40000 ALTER TABLE `contacto_proveedor` DISABLE KEYS */;
INSERT INTO `contacto_proveedor` VALUES (2,'Andy Velarde','velarde@gmail.com',79374484,'Bolivia',1),(3,'Santiago Aguilar','santi@gmail.com',7351623,'Activo',2);
/*!40000 ALTER TABLE `contacto_proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `det_serv_extra`
--

DROP TABLE IF EXISTS `det_serv_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `det_serv_extra` (
  `id_serv_extra` int(11) NOT NULL AUTO_INCREMENT,
  `id_serv` int(11) DEFAULT NULL,
  `cantidad` float DEFAULT NULL,
  `fecha` varchar(50) DEFAULT NULL,
  `costo_total` float DEFAULT NULL,
  `desc_serv_extra` varchar(300) DEFAULT NULL,
  `id_prof` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_serv_extra`),
  KEY `id_serv` (`id_serv`),
  KEY `id_prof` (`id_prof`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `det_serv_extra`
--

LOCK TABLES `det_serv_extra` WRITE;
/*!40000 ALTER TABLE `det_serv_extra` DISABLE KEYS */;
/*!40000 ALTER TABLE `det_serv_extra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_cliente_pc`
--

DROP TABLE IF EXISTS `detalle_cliente_pc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_cliente_pc` (
  `id_det_cli_pc` int(11) NOT NULL AUTO_INCREMENT,
  `id_cli` int(11) DEFAULT NULL,
  `id_per_cont` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_det_cli_pc`),
  KEY `id_cli` (`id_cli`),
  KEY `id_per_cont` (`id_per_cont`),
  CONSTRAINT `detalle_cliente_pc_ibfk_1` FOREIGN KEY (`id_cli`) REFERENCES `cliente` (`id_cli`),
  CONSTRAINT `detalle_cliente_pc_ibfk_2` FOREIGN KEY (`id_per_cont`) REFERENCES `persona_contacto` (`id_per_cont`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_cliente_pc`
--

LOCK TABLES `detalle_cliente_pc` WRITE;
/*!40000 ALTER TABLE `detalle_cliente_pc` DISABLE KEYS */;
INSERT INTO `detalle_cliente_pc` VALUES (1,1,1),(2,3,2),(3,1,3);
/*!40000 ALTER TABLE `detalle_cliente_pc` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_lista_compras`
--

DROP TABLE IF EXISTS `detalle_lista_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_lista_compras` (
  `id_detalle_list` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `create_time` varchar(200) DEFAULT NULL COMMENT 'create time',
  `update_time` datetime DEFAULT NULL COMMENT 'update time',
  `id_list` int(11) DEFAULT NULL COMMENT 'id from proform',
  `item_list` varchar(200) DEFAULT NULL COMMENT 'item list',
  `description_item` text DEFAULT NULL COMMENT 'description item',
  `cant_item` float DEFAULT NULL,
  `cost_item` float DEFAULT NULL,
  PRIMARY KEY (`id_detalle_list`)
) ENGINE=InnoDB AUTO_INCREMENT=94 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_lista_compras`
--

LOCK TABLES `detalle_lista_compras` WRITE;
/*!40000 ALTER TABLE `detalle_lista_compras` DISABLE KEYS */;
INSERT INTO `detalle_lista_compras` VALUES (1,'2021-11-03',NULL,7,'Clavo de Olor','',10,4.605),(2,'2021-11-03',NULL,12,'Clavo de Olor','aad',10,4.605),(3,'2021-11-03',NULL,12,'Azucar','gfdg',0.34,1.36),(4,'2021-11-03',NULL,12,'Pimienta Dulce','sad',10,5.301),(5,'2021-11-03',NULL,12,'Naranja','sad',1,2),(6,'2021-11-03',NULL,13,'Azucar','',0.34,1.36),(7,'2021-11-03',NULL,13,'Pimienta Dulce','',20,10.602),(8,'2021-11-03',NULL,13,'Naranja','',1,2),(9,'2021-11-03',NULL,13,'Clavo de Olor','',10,4.605),(10,'2021-11-03',NULL,13,'Clavo de Olor','',10,4.605),(11,'2021-11-03',NULL,13,'Naranja','',1,2),(12,'2021-11-03',NULL,13,'Azucar','',0.34,1.36),(13,'2021-11-04',NULL,13,'Vino Rosado Campos de Solana','fdsfsdfsd',1,47),(14,'2021-11-04',NULL,14,'Pimienta Dulce','',10,5.301),(15,'2021-11-04',NULL,14,'Clavo de Olor','',10,4.605),(16,'2021-11-04',NULL,14,'Naranja','',1,2),(17,'2021-11-04',NULL,14,'Azucar','',0.34,1.36),(18,'2021-11-04',NULL,14,'Vino Rosado Campos de Solana','',1,47),(19,'2021-11-05',NULL,15,'Naranja','',1,2),(20,'2021-11-05',NULL,15,'Azucar','',0.34,1.36),(21,'2021-11-05',NULL,15,'Pimienta Dulce','',10,5.301),(22,'2021-11-05',NULL,15,'Clavo de Olor','',10,4.605),(23,'2021-11-05',NULL,16,'Pimienta Dulce','',10,5.301),(24,'2021-11-05',NULL,16,'Clavo de Olor','',10,4.605),(25,'2021-11-05',NULL,16,'Azucar','',0.34,1.36),(26,'2021-11-05',NULL,16,'Naranja','',1,2),(27,'2021-11-30',NULL,16,'Clavo de Olor','',10,4.605),(28,'2021-11-30',NULL,16,'Pimienta Dulce','',10,5.301),(29,'2021-11-30',NULL,16,'Azucar','',0.34,1.36),(30,'2021-11-30',NULL,16,'Naranja','',1,2),(31,'2022-01-13',NULL,17,'Clavo de Olor','',20,9.21),(32,'2022-01-13',NULL,17,'Azucar','',0.68,2.72),(33,'2022-01-13',NULL,17,'Pimienta Dulce','',18,10.602),(34,'2022-01-13',NULL,17,'Naranja','',2,4),(35,'2022-01-17',NULL,18,'Pimienta Dulce','',18,10.602),(36,'2022-01-17',NULL,18,'Azucar','',0.68,2.72),(37,'2022-01-17',NULL,18,'Clavo de Olor','',20,9.21),(38,'2022-01-17',NULL,18,'Naranja','',2,4),(39,'2022-01-18',NULL,19,'Pimienta Dulce','',27,15.903),(40,'2022-01-18',NULL,19,'Vino Rosado Campos de Solana','',3,141),(41,'2022-01-18',NULL,19,'Azucar','',1.02,4.08),(42,'2022-01-18',NULL,19,'Clavo de Olor','',30,13.815),(43,'2022-01-18',NULL,19,'Naranja','',3,6),(44,'2022-01-27',NULL,20,'Azucar','',10,40),(45,'2022-01-27',NULL,20,'Angostura Bitters','',4,7.04),(46,'2022-01-27',NULL,20,'Clavo de Olor','',100,40.5),(47,'2022-01-27',NULL,20,'Pimienta Dulce','',100,53.01),(48,'2022-01-27',NULL,20,'Pepas de Moconchinchi deshidratado','',3,180),(49,'2022-02-02',NULL,20,'Limon','',0.067,0.0389),(50,'2022-02-02',NULL,20,'Singani Casa Real Etiqueta Negra 1Lt','',0.0591,2.605),(57,'2022-02-06',NULL,21,'Pepas de Moconchinchi deshidratado','',30,1800),(58,'2022-02-06',NULL,21,'Singani Casa Real Etiqueta Negra 1Lt','',23.66,104.303),(59,'2022-02-06',NULL,21,'Pepas de Moconchinchi deshidratado','',1.182,70.92),(60,'2022-02-06',NULL,21,'Pimienta Dulce','',94.56,50.1168),(61,'2022-02-06',NULL,21,'Azucar','',7.88,31.52),(62,'2022-02-06',NULL,21,'Clavo de Olor','',94.56,43.4976),(63,'2022-02-06',NULL,21,'Agua Para Jarabes','',15.76,0.0315),(64,'2022-02-07',NULL,22,'Singani Casa Real Etiqueta Negra 1Lt','',2.3656,104.269),(65,'2022-02-07',NULL,22,'Limon','',6.708,3.8906),(66,'2022-02-07',NULL,22,'Gingerale','',4.732,3.4707),(67,'2022-02-07',NULL,22,'Limon','',0.067,0.0389),(68,'2022-02-07',NULL,23,'Agua Para Jarabes','',7.88,0.0158),(69,'2022-02-07',NULL,23,'Singani Casa Real Etiqueta Negra 1Lt','',11.8,51.8882),(70,'2022-02-07',NULL,23,'Pepas de Moconchinchi deshidratado','',0.591,35.46),(71,'2022-02-07',NULL,24,'Singani Casa Real Etiqueta Negra 1Lt','',11.828,521.343),(72,'2022-02-07',NULL,24,'Limon','',33.54,19.4532),(73,'2022-02-07',NULL,24,'Gingerale','',23.66,17.3537),(74,'2022-02-07',NULL,25,'Gingerale','',23.66,17.3537),(75,'2022-02-07',NULL,25,'Limon','',33.54,19.4532),(76,'2022-02-07',NULL,25,'Singani Casa Real Etiqueta Negra 1Lt','',11.828,521.343),(77,'2022-07-10',NULL,26,'Singani Casa Real Etiqueta Negra 1Lt','',5.9,25.9441),(78,'2022-07-10',NULL,27,'Pepas de Moconchinchi deshidratado','',0.0059,0.3546),(79,'2022-07-10',NULL,27,'Agua Para Jarabes','',0.0788,0.0002),(80,'2022-07-10',NULL,27,'Pepas de Moconchinchi deshidratado','',0.591,35.46),(81,'2022-07-10',NULL,27,'Agua Para Jarabes','',7.88,0.0158),(82,'2022-07-10',NULL,30,'Pepas de Moconchinchi deshidratado','',0.591,35.46),(83,'2022-07-10',NULL,30,'Agua Para Jarabes','',7.88,0.0158),(84,'2022-07-10',NULL,31,'Agua Para Jarabes','',0.0788,0.0002),(85,'2022-07-10',NULL,31,'Pepas de Moconchinchi deshidratado','',0.0059,0.3546),(86,'2022-07-10',NULL,32,'Pepas de Moconchinchi deshidratado','',0.0059,0.3546),(87,'2022-07-10',NULL,32,'Agua Para Jarabes','',0.0788,0.0002),(88,'2022-07-10',NULL,33,'Agua Para Jarabes','',0.0788,0.0002),(89,'2022-07-10',NULL,33,'Pepas de Moconchinchi deshidratado','',0.0059,0.3546),(90,'2022-07-10',NULL,34,'Agua Para Jarabes','',0.0788,0.0002),(91,'2022-07-10',NULL,34,'Pepas de Moconchinchi deshidratado','',0.0059,0.3546),(92,'2022-07-10',NULL,35,'Agua Para Jarabes','',7.88,0.0158),(93,'2022-07-10',NULL,35,'Pepas de Moconchinchi deshidratado','',0.591,35.46);
/*!40000 ALTER TABLE `detalle_lista_compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_per_prof`
--

DROP TABLE IF EXISTS `detalle_per_prof`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_per_prof` (
  `id_prod_prof` int(11) NOT NULL,
  `cantidad` float DEFAULT NULL,
  `costo_det` float DEFAULT NULL,
  `total_detalle` float DEFAULT NULL,
  `id_prof` int(11) DEFAULT NULL,
  `tipo_prod_prof` int(11) DEFAULT NULL,
  `id_det_perprof` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_det_perprof`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_per_prof`
--

LOCK TABLES `detalle_per_prof` WRITE;
/*!40000 ALTER TABLE `detalle_per_prof` DISABLE KEYS */;
INSERT INTO `detalle_per_prof` VALUES (1,1,350,350,32,0,1),(2,1,350,350,32,0,2),(20,1,60.266,60.266,32,1,3),(1,1,350,350,33,0,4),(20,1,60.266,60.266,33,1,5),(2,1,350,350,33,0,6),(20,1,60.266,60.266,34,1,7),(1,1,350,350,34,0,8),(1,1,350,350,35,0,9),(20,1,60.266,60.266,35,1,10),(2,1,350,350,36,0,11),(20,1,60.266,60.266,36,1,12),(1,1,350,350,37,0,13),(2,1,350,350,37,0,14),(20,1,60.266,60.266,37,1,15),(20,1,60.266,60.266,38,1,16),(20,1,60.266,60.266,39,1,17),(20,1,60.266,60.266,40,1,18),(20,2,60.266,120.53,41,1,19),(1,1,350,350,41,0,20),(20,1,60.266,60.266,43,1,21),(1,1,350,350,43,0,22),(2,1,350,350,43,0,23),(2,1,350,350,43,0,24),(1,1,350,350,43,0,25),(20,4,60.266,241.06,43,1,26),(1,1,350,350,44,0,27),(1,1,350,350,45,0,28),(1,2,350,700,45,0,29),(2,1,350,350,46,0,30),(1,1,350,350,46,0,31),(20,5,60.266,301.33,46,1,32),(1,1,350,350,48,0,33),(20,1,60.266,60.266,48,1,34),(2,1,350,350,49,0,35),(20,1,60.266,60.266,49,1,36),(1,2,350,700,49,0,37),(1,1,350,350,50,0,38),(20,2,71,142,50,1,39),(1,1,350,350,51,0,40),(2,1,350,350,51,0,41),(20,3,71,213,51,1,42),(1,2,350,700,52,0,43),(30,144,6.3,907.2,52,1,44),(31,144,5.64149,812.37,52,1,45),(30,144,6.3,907.2,53,1,46),(1,2,350,700,53,0,47),(34,144,5.64149,812.37,53,1,48),(30,144,6.3,907.2,54,1,49),(1,1,350,350,54,0,50),(1,1,350,350,55,0,51),(32,200,9,1800,55,1,52),(34,200,14.8787,2975.74,56,1,53),(1,1,350,350,56,0,54),(1,2,350,700,57,0,55),(34,40,6.3,252,57,1,56),(34,100,4.7521,475.21,58,1,57),(1,1,350,350,58,0,58),(1,1,350,350,59,0,59),(30,200,6.3,1260,59,1,60),(1,1,350,350,60,0,61),(30,200,6.3,1260,60,1,62),(28,50,13.52,676,62,1,63),(25,100,22.95,2295,62,1,64),(27,100,24.78,2478,63,1,65),(31,70,7.07,494.9,63,1,66),(29,20,11.3978,227.96,63,1,67),(25,40,22.95,918,64,1,68),(34,50,4.7521,237.61,64,1,69),(5,1,350,350,66,0,70),(7,1,350,350,66,0,71),(30,150,6.3,945,66,1,72),(34,100,4.7521,475.21,70,1,73),(28,150,13.52,2028,70,1,74),(5,1,350,350,71,0,75),(34,100,4.7521,475.21,71,1,76),(5,1,350,350,72,0,77),(34,1,4.7521,4.7521,72,1,78),(5,1,350,350,73,0,79),(34,10,4.7521,47.52,73,1,80),(34,100,4.7521,475.21,74,1,81),(5,1,350,350,74,0,82),(5,1,350,350,75,0,83),(34,100,4.7521,475.21,75,1,84),(5,1,350,350,76,0,85),(34,1,4.7521,4.7521,76,1,86),(5,1,350,350,77,0,87),(34,1,4.7521,4.7521,77,1,88),(34,1,4.7521,4.7521,78,1,89),(5,1,350,350,78,0,90),(5,1,350,350,79,0,91),(34,1,4.7521,4.7521,79,1,92),(34,100,4.7521,475.21,80,1,93),(5,1,350,350,80,0,94);
/*!40000 ALTER TABLE `detalle_per_prof` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle_prod`
--

DROP TABLE IF EXISTS `detalle_prod`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_prod` (
  `id_det_prod` int(11) NOT NULL AUTO_INCREMENT,
  `id_prod` int(11) DEFAULT NULL,
  `id_ins` int(11) DEFAULT NULL,
  `id_rec` int(11) DEFAULT NULL,
  `cantidad_prod` float DEFAULT NULL,
  `costo_recurso_prod` float DEFAULT NULL,
  `costo_total` float DEFAULT NULL,
  `precio_total_detProd` float DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_det_prod`),
  KEY `id_prod` (`id_prod`),
  KEY `id_ins` (`id_ins`),
  KEY `id_rec` (`id_rec`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_prod`
--

LOCK TABLES `detalle_prod` WRITE;
/*!40000 ALTER TABLE `detalle_prod` DISABLE KEYS */;
INSERT INTO `detalle_prod` VALUES (64,25,18,0,0.36,8.3,2.988,2.988,NULL),(65,25,17,0,0.3,0.58,0.174,0.174,NULL),(66,25,22,0,0.0125,38,0.475,0.47503,NULL),(67,25,0,12,1,0.1375,0.1375,0.1375,NULL),(68,25,19,0,0.079,167,13.193,13.193,NULL),(69,26,0,15,1,0.1,0.1,0.1,NULL),(70,26,0,19,0.17,65.2552,11.0934,11.0934,NULL),(71,26,0,14,2,0.225,0.45,0.45,NULL),(72,27,28,0,0.03,7.5,0.225,0.225,NULL),(73,27,0,21,2,0.4,0.8,0.8,NULL),(74,27,26,0,0.015,88,1.32,1.3198,NULL),(75,27,27,0,0.059,287,16.933,16.933,NULL),(76,27,0,20,0.025,4.002,0.10005,0.11,NULL),(77,28,0,20,0.49,4.002,1.96098,1.96098,NULL),(78,28,0,15,1,0.1,0.1,0.1,NULL),(79,28,29,0,0.059,77,4.543,4.543,NULL),(80,28,0,11,0.75,0.3074,0.23055,0.23055,NULL),(81,28,0,22,1,0.4,0.4,0.4,NULL),(82,28,28,0,0.05,7.5,0.375,0.375,NULL),(83,29,0,22,1,0.4,0.4,0.4,NULL),(84,29,0,20,0.049,4.002,0.196098,0.196098,NULL),(85,29,24,0,0.059,74.53,4.39727,4.39727,NULL),(86,29,0,15,1,0.1,0.1,0.1,NULL),(87,29,0,11,0.75,0.3074,0.23055,0.23055,NULL),(88,29,28,0,0.05,7.5,0.375,0.375,NULL),(95,30,24,0,0.05914,74.53,44.077,4.407,NULL),(96,30,0,23,1,0.03886,0.03886,0.03886,NULL),(97,30,0,11,0.19,0.3074,0.058406,0.058406,NULL),(98,30,6,0,0.1183,6.2,0.73346,0.73346,NULL),(99,31,0,24,0.037,16.8,0.6216,0.6216,NULL),(100,31,26,0,0.0003,88,0.0264,0.0264,NULL),(101,31,0,25,1,0.3503,0.3503,0.3503,NULL),(102,31,0,11,0.8,0.3074,0.24592,0.245923,NULL),(103,31,24,0,0.059,74.53,4.39727,4.39727,NULL),(104,32,6,0,0.05,6.2,0.31,0.3005,NULL),(105,32,26,0,0.02,88,1.76,1.76,NULL),(106,32,0,27,0.05,37.311,1.86555,1.86555,NULL),(107,32,24,0,0.05,74.53,3.7265,3.7265,NULL),(108,33,24,0,0.05915,74.53,4.4084,4.4084,NULL),(109,33,0,28,1,9,9,9,NULL),(110,33,6,0,0.05915,6.2,0.3667,0.3667,NULL),(111,33,26,0,0.0014,88,0.1232,0.1232,NULL),(112,33,0,29,0.0394,24.884,0.9804,0.9804,NULL),(113,34,0,30,0.0394,9.004,0.3548,0.3548,NULL),(114,34,24,0,0.059,74.53,4.3973,4.3973,NULL);
/*!40000 ALTER TABLE `detalle_prod` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `actuProd` BEFORE INSERT ON `detalle_prod` FOR EACH ROW UPDATE producto set producto.costo_total_prod = (producto.costo_total_prod + NEW.costo_total), producto.precio_total_prod = (producto.precio_total_prod + NEW.precio_total_detProd)  WHERE producto.id_prod = NEW.id_prod */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `detalle_receta`
--

DROP TABLE IF EXISTS `detalle_receta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle_receta` (
  `id_det_rec` int(11) NOT NULL AUTO_INCREMENT,
  `id_rec` int(11) DEFAULT NULL,
  `id_ins` int(11) DEFAULT NULL,
  `cant_det_rec` float DEFAULT NULL,
  `cost_ins_det_rec` float DEFAULT NULL,
  `prec_final_det_rec` float DEFAULT NULL,
  `fecha_ins_det_rec` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_det_rec`),
  KEY `id_rec` (`id_rec`),
  KEY `id_ins` (`id_ins`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle_receta`
--

LOCK TABLES `detalle_receta` WRITE;
/*!40000 ALTER TABLE `detalle_receta` DISABLE KEYS */;
INSERT INTO `detalle_receta` VALUES (1,1,1,2,1,2,'2020-10-05 20:03:02'),(2,2,3,1,4,5,'2020-10-06 20:23:04'),(3,2,4,1,34,34,'2020-10-06 20:23:04'),(4,2,5,1,2.98,2.98,'2020-10-06 20:23:04'),(5,4,2,20,1,25,'2020-10-12 20:39:00'),(7,5,3,1,4,4,'2020-10-27 21:43:52'),(8,5,10,1,2,2,'2020-10-27 21:43:52'),(9,7,11,9,0.53,5.301,'2021-02-25 15:55:04'),(10,7,12,10,0.46,4.605,'2021-02-25 15:55:04'),(11,7,13,1,2,2,'2021-02-25 15:55:04'),(12,7,3,0.34,4,1.36,'2021-02-25 15:55:04'),(13,10,2,2,1,21,'2021-11-22 15:45:01'),(14,10,6,1,6,6,'2021-11-22 15:45:01'),(15,11,17,0.53,0.58,0.3074,'2022-01-25 16:23:21'),(16,12,20,0.11,1.25,0.1375,'2022-01-25 16:27:40'),(17,14,8,0.025,9,0.225,'2022-01-25 18:09:06'),(18,15,23,0.05,2,0.1,'2022-01-25 18:12:26'),(19,19,3,0.34,4,1.36,'2022-01-25 18:42:36'),(20,19,12,10,0.46,4.605,'2022-01-25 18:42:36'),(21,19,13,1,2,2,'2022-01-25 18:42:36'),(22,19,11,9,0.53,4.775,'2022-01-25 18:42:36'),(23,19,24,0.074,74.53,5.51522,'2022-01-25 18:42:36'),(24,19,15,1,47,47,'2022-01-25 18:42:36'),(25,20,3,1,4,4,'2022-01-25 19:04:39'),(26,20,25,1,0.002,0.002,'2022-01-25 19:04:39'),(27,21,13,0.2,2,0.4,'2022-01-25 20:35:18'),(28,22,23,0.2,2,0.4,'2022-01-25 22:05:12'),(29,23,17,0.067,0.58,0.03886,'2022-01-25 22:26:21'),(30,24,30,1,12.8,12.8,'2022-01-26 08:20:08'),(31,24,3,1,4,4,'2022-01-26 08:20:08'),(32,25,31,0.05,7,0.3503,'2022-01-26 08:28:03'),(33,27,3,1,4,4,'2022-01-27 19:08:17'),(34,27,12,10,0.46,4.05,'2022-01-27 19:08:17'),(35,27,11,10,0.53,5.301,'2022-01-27 19:08:17'),(36,27,5,2,2.98,5.96,'2022-01-27 19:08:17'),(37,27,33,0.3,60,18,'2022-01-27 19:08:18'),(38,28,33,0.15,60,9,'2022-02-06 19:34:55'),(39,29,3,1,4,4,'2022-02-06 22:15:00'),(40,29,33,0.15,60,9,'2022-02-06 22:15:00'),(41,29,11,12,0.53,6.36,'2022-02-06 22:15:00'),(42,29,12,12,0.46,5.52,'2022-02-06 22:15:00'),(43,29,25,2,0.002,0.004,'2022-02-06 22:15:00'),(44,30,33,0.15,60,9,'2022-02-07 14:43:21'),(45,30,25,2,0.002,0.004,'2022-02-07 14:43:21');
/*!40000 ALTER TABLE `detalle_receta` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_VALUE_ON_ZERO' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `actuRec` BEFORE INSERT ON `detalle_receta` FOR EACH ROW UPDATE receta set receta.cost_total_rec = (receta.cost_total_rec + NEW.prec_final_det_rec) WHERE receta.id_rec = NEW.id_rec */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `elemento`
--

DROP TABLE IF EXISTS `elemento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `elemento` (
  `id_ele` int(11) NOT NULL AUTO_INCREMENT,
  `nom_ele` varchar(50) DEFAULT NULL,
  `desc_ele` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_ele`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `elemento`
--

LOCK TABLES `elemento` WRITE;
/*!40000 ALTER TABLE `elemento` DISABLE KEYS */;
INSERT INTO `elemento` VALUES (1,'Receta','Este elemento comprende todos los procesos para ha'),(3,'Personal','Control de Personal'),(4,'Evento','Creacion, modificacion y administracion de proformas'),(5,'Clientes y Proveedores','Registro de Clientes y las personas relacionadas con este como ser personas de contacto');
/*!40000 ALTER TABLE `elemento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `evento`
--

DROP TABLE IF EXISTS `evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `evento` (
  `id_eve` int(11) NOT NULL AUTO_INCREMENT,
  `nom_eve` varchar(200) DEFAULT NULL,
  `fecha_eve` varchar(50) DEFAULT NULL,
  `id_cli` int(11) DEFAULT NULL,
  `cost_eve_total` float DEFAULT NULL,
  `IUE` float DEFAULT NULL,
  `IT` float DEFAULT NULL,
  `IVA` float DEFAULT NULL,
  `total_imp` float DEFAULT NULL,
  `total_facturado` float DEFAULT NULL,
  `utilidad` float DEFAULT NULL,
  `nro_factura` int(11) DEFAULT NULL,
  `id_per_cont` int(11) DEFAULT NULL,
  `evento_terminado` tinyint(1) NOT NULL DEFAULT 0,
  `coordinates_lng` float DEFAULT NULL,
  `coordinates_lat` float DEFAULT NULL,
  `id_usu` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_eve`),
  KEY `id_cli` (`id_cli`),
  KEY `id_per_cont` (`id_per_cont`)
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `evento`
--

LOCK TABLES `evento` WRITE;
/*!40000 ALTER TABLE `evento` DISABLE KEYS */;
INSERT INTO `evento` VALUES (117,'Evento de Prueba 4','2022-01-29',1,1050,170.52,24.36,105.56,300.44,957.24,NULL,NULL,10,1,NULL,NULL,NULL),(121,'Evento SAIV Prueba 1','2022-01-17',8,350,153.47,21.92,95,270.4,957.24,NULL,0,13,1,NULL,NULL,NULL),(123,'PREVIA FRATERNIDADES','2021-04-02',8,2266.61,589.41,84.2,364.87,1038.48,957.24,NULL,455,15,1,NULL,NULL,NULL),(125,'Evento De Prueba 7','2022-02-07',8,3325.74,810.15,115.74,501.52,1427.41,957.24,NULL,0,15,1,NULL,NULL,NULL),(127,'Evento Defenza 3 aaaaaaaaaaaaaaaaaaaaaaaaaaaa','2022-02-08',8,825.21,201.02,28.72,124.44,354.18,957.24,NULL,0,15,1,NULL,NULL,NULL),(131,'Evento Test Reportes','2022-03-11',11,2077.23,723.74,103.39,448.03,1275.15,957.24,NULL,0,16,1,NULL,NULL,NULL),(132,'Evento Prueba Estadistica 2','2022-04-11',8,2560.66,779.73,111.39,482.69,1373.81,957.24,NULL,0,15,1,NULL,NULL,NULL),(133,'Prueba Para Enero 2021','2021-01-17',8,916.31,281.51,40.22,174.27,495.99,957.24,NULL,0,15,1,NULL,NULL,NULL),(135,'Evento Test Personal 2','2022-04-17',8,1485.66,400.72,57.25,248.07,706.03,957.24,NULL,0,13,1,NULL,NULL,NULL),(139,'Evento Test Ubicacion','2022-05-22',10,1616.64,609.78,87.11,377.48,1074.38,957.24,NULL,0,14,1,-63.2017,-17.7671,NULL),(140,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,957.24,NULL,0,13,0,-63.2,-17.7723,NULL),(141,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,957.24,NULL,0,13,0,-63.2058,-17.7701,NULL),(142,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,461.12,NULL,0,13,0,-63.2052,-17.7705,NULL),(143,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,957.24,NULL,0,13,0,-63.203,-17.7714,NULL),(144,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,957.24,NULL,0,13,0,-63.2045,-17.7721,NULL),(145,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,411.51,NULL,0,13,0,-63.2053,-17.7709,NULL),(146,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,411.51,NULL,0,13,0,-63.2025,-17.7705,NULL),(147,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,411.51,NULL,0,13,0,-63.2059,-17.7719,NULL),(148,'Alexander Location Nature','2022-07-10',1,NULL,NULL,NULL,NULL,NULL,411.51,NULL,0,9,0,-63.2051,-17.7701,NULL),(149,'Alexander Location Nature','2022-07-10',8,NULL,NULL,NULL,NULL,NULL,957.24,NULL,0,13,0,-63.1976,-17.7651,NULL);
/*!40000 ALTER TABLE `evento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `evento_view`
--

DROP TABLE IF EXISTS `evento_view`;
/*!50001 DROP VIEW IF EXISTS `evento_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `evento_view` (
  `id_eve` tinyint NOT NULL,
  `nom_eve` tinyint NOT NULL,
  `fecha_eve` tinyint NOT NULL,
  `id_cli` tinyint NOT NULL,
  `cost_eve_total` tinyint NOT NULL,
  `IUE` tinyint NOT NULL,
  `IT` tinyint NOT NULL,
  `IVA` tinyint NOT NULL,
  `total_imp` tinyint NOT NULL,
  `total_facturado` tinyint NOT NULL,
  `utilidad` tinyint NOT NULL,
  `nro_factura` tinyint NOT NULL,
  `nom_cli` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `grupo`
--

DROP TABLE IF EXISTS `grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `grupo` (
  `id_grp` int(11) NOT NULL AUTO_INCREMENT,
  `nom_grp` varchar(50) DEFAULT NULL,
  `desc_grp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_grp`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `grupo`
--

LOCK TABLES `grupo` WRITE;
/*!40000 ALTER TABLE `grupo` DISABLE KEYS */;
INSERT INTO `grupo` VALUES (1,'Administrador','es un super usuario'),(2,'Personal de Administración','Este personal unicamente puede insertar datos');
/*!40000 ALTER TABLE `grupo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `insumo`
--

DROP TABLE IF EXISTS `insumo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `insumo` (
  `id_ins` int(11) NOT NULL AUTO_INCREMENT,
  `nom_ins` varchar(50) DEFAULT NULL,
  `unidad_ins` varchar(20) DEFAULT NULL,
  `cost_ins` float DEFAULT NULL,
  `fecha_actu_ins` varchar(50) DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_ins`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `insumo`
--

LOCK TABLES `insumo` WRITE;
/*!40000 ALTER TABLE `insumo` DISABLE KEYS */;
INSERT INTO `insumo` VALUES (0,'No Existe',NULL,NULL,NULL,'null'),(2,'Limon Sutil','Unidad',1,'2020-10-05','2022-01-25 15:57:36'),(3,'Azucar','Kilogramo',4,'2020-10-06',NULL),(4,'Moconchinchi','Kilogramo',34,'2020-10-06',NULL),(5,'Agua','Lt.',2.98,'2020-10-06',NULL),(6,'Gingerale','Botella de un Litro',6.2,'2022-01-25',NULL),(8,'Frutilla','Kilogramo',9,'2022-01-25',NULL),(9,'Mora','Kilogramo',12,'2020-10-12','2022-01-17 21:36:54'),(10,'Chirimoya','Unidad',2,'2020-10-27','2021-02-25 15:47:39'),(11,'Pimienta Dulce','Gramo',0.53,'2021-02-25',NULL),(12,'Clavo de Olor','Gramo',0.46,'2021-02-25',NULL),(13,'Naranja','Unidad',2,'2021-02-25',NULL),(15,'Vino Rosado Campos de Solana','Lt.',47,'2021-02-25',NULL),(17,'Limon','Unidad',0.58,'2021-02-28',NULL),(18,'Agua Tónica Scheweppes','Lata',8.3,'2021-05-22',NULL),(19,'Gin Beefeater 750 ml','Botella',167,'2021-01-31',NULL),(20,'Pepino','Unidad',1.25,'2021-01-31',NULL),(21,'Champagne María de Codorniu 750 ml','Botella',62.5,'2021-01-31',NULL),(22,'Lavanda - Finca la Vispera 80 flores','Caja',38,'2021-07-24',NULL),(23,'Hierba Buena','Bolsita',2,'2022-01-25',NULL),(24,'Singani Casa Real Etiqueta Negra 1Lt','Botella',74.53,'2022-01-25',NULL),(25,'Agua Para Jarabes','Lt',0.002,'2022-01-25',NULL),(26,'Angostura Bitters','Botella 200ml',88,'',NULL),(27,'Whisky Chivas Regal 12 Años 1Lt.','Botella',287,'2022-01-25',NULL),(28,'Hielo Oriental 3Kg','Bolsa',7.5,'2022-01-25',NULL),(29,'Ron Blanco Havana 3 Años 1Lt','Botella',77,'2022-01-25',NULL),(30,'Nectar Sabor Piña Fruts 2Lt','Botella',12.8,'2022-01-26',NULL),(31,'Piña','Unidad',7,'2022-01-26',NULL),(32,'Insumo de Prueba','asdasd',12,'2021-12-31',NULL),(33,'Pepas de Moconchinchi deshidratado','Kg',60,'2022-01-27',NULL);
/*!40000 ALTER TABLE `insumo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lista_compras`
--

DROP TABLE IF EXISTS `lista_compras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lista_compras` (
  `id_list` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `create_time` varchar(200) DEFAULT NULL COMMENT 'create time',
  `update_time` datetime DEFAULT NULL COMMENT 'update time',
  `created_by` varchar(255) DEFAULT NULL COMMENT 'Quien creo la Lista',
  `date_to_shop` varchar(200) DEFAULT NULL COMMENT 'When have to shop all list',
  `id_prof` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_list`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lista_compras`
--

LOCK TABLES `lista_compras` WRITE;
/*!40000 ALTER TABLE `lista_compras` DISABLE KEYS */;
INSERT INTO `lista_compras` VALUES (1,'2021-10-07 20:31:37',NULL,'MIguel Torrez','2021-10-10 00:00:00',12,1),(17,'2022-01-13',NULL,'Miguel Angel','2022-01-13',41,1),(18,'2022-01-17',NULL,'Miguel Angel','2022-01-17',50,1),(19,'2022-01-18',NULL,'Miguel Angel','2022-01-18',51,1),(20,'2022-01-27',NULL,'Miguel Angel','2022-01-28',55,1),(21,'2022-02-06',NULL,'Miguel Angel','2022-02-06',56,1),(22,'2022-02-07',NULL,'Miguel Angel','2022-02-07',57,1),(23,'2022-02-07',NULL,'Alexander','2022-02-07',58,1),(24,'2022-02-07',NULL,'Alexander','2022-02-07',59,1),(25,'2022-02-07',NULL,'Alexander','2022-02-07',60,1),(26,'2022-07-10',NULL,'Alexander','2022-07-10',71,1),(27,'2022-07-10',NULL,'Alexander','2022-07-10',72,1),(28,'2022-07-10',NULL,'Alexander','2022-07-10',73,1),(29,'2022-07-10',NULL,'Alexander','2022-07-10',74,1),(30,'2022-07-10',NULL,'Alexander','2022-07-10',75,1),(31,'2022-07-10',NULL,'Alexander','2022-07-10',76,1),(32,'2022-07-10',NULL,'Alexander','2022-07-10',77,1),(33,'2022-07-10',NULL,'Alexander','2022-07-10',78,1),(34,'2022-07-10',NULL,'Alexander','2022-07-10',79,1),(35,'2022-07-10',NULL,'Alexander','2022-07-10',80,1);
/*!40000 ALTER TABLE `lista_compras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona_contacto`
--

DROP TABLE IF EXISTS `persona_contacto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `persona_contacto` (
  `id_per_cont` int(11) NOT NULL AUTO_INCREMENT,
  `nom_per_cont` varchar(50) DEFAULT NULL,
  `app_per_cont` varchar(50) DEFAULT NULL,
  `tel_per_cont` int(11) DEFAULT NULL,
  `email_per_cont` varchar(50) DEFAULT NULL,
  `id_cli` int(50) DEFAULT NULL,
  PRIMARY KEY (`id_per_cont`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona_contacto`
--

LOCK TABLES `persona_contacto` WRITE;
/*!40000 ALTER TABLE `persona_contacto` DISABLE KEYS */;
INSERT INTO `persona_contacto` VALUES (1,'Andy','Velarde',7880122,'andy@gmail.com',NULL),(2,'Rime','Siles',7615616,'rimesilea@gmail.com',NULL),(3,'Werner','Kollros',7625122,'werner@gmail.com',NULL),(5,'johanson','florido',1232312,'asdasd@gmail.com',NULL),(6,'johanson','florido',1232312,'asdasd@gmail.com',NULL),(7,'johanson','asdas',545345,'asdasd@gmail.com',NULL),(8,'johanson','Rosco',545345,'asdasd@gmail.com',3),(9,'Rime Elna','Siles Moya',6782123,'rimeelna@gmail.com',1),(10,'Johan Mijail','Florido Siles',76155123,'johan@gmail.com',1),(13,'Jorge','Aranivar',876543,'jorge@gmail.com',8),(14,'Romina','Miserandino',0,'romina@gmail.com',10),(15,'Brand Manager Singani Casa Real ','Stephan Palaez',33433462,'stephan@gmail.com',8),(16,'Fabiola','Cadima',1234567,'fabi@gmail.com',11);
/*!40000 ALTER TABLE `persona_contacto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal`
--

DROP TABLE IF EXISTS `personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal` (
  `id_per` int(11) NOT NULL AUTO_INCREMENT,
  `nom_per` varchar(50) DEFAULT NULL,
  `app_per` varchar(50) DEFAULT NULL,
  `apm_per` varchar(50) DEFAULT NULL,
  `id_cargo_per` int(11) DEFAULT NULL,
  `ci_per` int(11) DEFAULT NULL,
  `id_cat_per` int(11) DEFAULT NULL,
  `unidad_per` varchar(50) DEFAULT NULL,
  `cost_per` float DEFAULT NULL,
  `telf_per` int(11) DEFAULT NULL,
  `email_per` varchar(50) DEFAULT NULL,
  `estado` varchar(50) DEFAULT NULL,
  `ciudad` varchar(50) DEFAULT NULL,
  `img_per` text DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  `fec_ingreso_personal` varchar(100) DEFAULT NULL,
  `token_notification` text DEFAULT NULL,
  `clv_personal` varchar(50) DEFAULT NULL,
  `on_event` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id_per`),
  KEY `id_cargo_per` (`id_cargo_per`),
  KEY `id_cat_per` (`id_cat_per`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal`
--

LOCK TABLES `personal` WRITE;
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` VALUES (1,'Alexander','Florido','Siles',2,5299072,1,'Dia',350,79374484,'elnagas123@gmail.com','Activo','Cochabamba','1647615168_c5ad990554880c9324d6.jpg',NULL,'2020-11-12','dUpOekeuTLW-q2CUvU0yUJ:APA91bEQhpjfLbNwuJxTwp1wpPxT7bB07s6kbMHSiur6uqfaR8G0f6j2aX_KHbUzQ-TqvJAiZakSQR_VOrdwp8HsFVKRttG6nEWUM7LWfrxRpjgUGCP4nDnQLzMrFH12MsqhGaJXZHqS','admin',1),(2,'Johan','Florido','Siles',4,5299068,1,'Dia',200,745516621,'floridosiles@hotmail.com','Activo','Cochabamba','1605216172_6f9515d26cb3e4f1165f.jpg','2022-02-07 10:29:19','2020-11-12',NULL,NULL,0),(3,'Juan Pablo','Caceres','Siles',1,5299072,2,'Mes',4000,712616121,'jp@gmail.com','Activo','Santa Cruz','1639083051_15c23ec90af063b74d51.jpeg','2021-12-09 17:16:11','2021-12-09',NULL,NULL,0),(4,'Reachel','Torrez','Siles',3,5299089,2,'Dia',750,70749648,'reacheltorrez@hotmail.com','Inactivo','Santa Cruz','1643160886_483525918239d154623a.jpg',NULL,'2015-10-25',NULL,NULL,0),(5,'Naber Renato','Figueroa','Vequi',5,8997691,2,'Dia',350,3647712,'naber@gmail.com','Activo','Santa Cruz','1643160997_69d2472e25792d6d3502.jpg',NULL,'2016-01-25',NULL,NULL,0),(6,'Alvaro Geral','Gomez','Flores',2,11376669,2,'Dia',350,3672682,'alvaro@gmail.com','Inactivo','Santa Cruz','1644202496_08ff6cbcfd61b1c7f79f.jpg',NULL,'2016-02-25',NULL,NULL,0),(7,'Samuel ','Rodriguez','Cortes',2,4591195,2,'Dia',350,3456788,'samuel@gmail.com','Activo','Santa Cruz','1643161241_09e9f032c3081471ca72.jpg',NULL,'2018-01-09',NULL,NULL,0),(8,'prueba','prueba2','prueba2',3,5299072,1,'Dia',200,79374484,'prueba@gmail.com','Activo','Cochabamba','1644234201_996c2c9c4dba541bb064.jpg','2022-02-07 10:18:23','2022-02-07',NULL,NULL,0),(17,'Ejempl','Ejemplo','Ejemplo',2,12234567,2,'Dia',200,1234567,'asd@gmail.com','Inactivo','Santa Cruz','1644235171_30ec15f6de65d32d93bd.jpg','2022-02-07 10:29:11','2022-02-07',NULL,'asdasd',0);
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_listacompras`
--

DROP TABLE IF EXISTS `personal_listacompras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_listacompras` (
  `id_plc` int(11) NOT NULL AUTO_INCREMENT,
  `id_list` int(11) DEFAULT NULL,
  `id_per` int(11) DEFAULT NULL,
  `estado_tarea` int(11) DEFAULT 0,
  PRIMARY KEY (`id_plc`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_listacompras`
--

LOCK TABLES `personal_listacompras` WRITE;
/*!40000 ALTER TABLE `personal_listacompras` DISABLE KEYS */;
INSERT INTO `personal_listacompras` VALUES (3,16,1,0),(4,17,1,1),(5,17,1,1),(6,17,1,0),(7,18,1,1),(8,19,1,0),(12,20,1,0),(21,21,1,0),(22,22,1,0),(23,22,1,0),(24,23,1,1),(25,24,1,0),(26,25,1,0),(27,29,1,0),(28,35,1,0),(29,35,1,0);
/*!40000 ALTER TABLE `personal_listacompras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `personal_view`
--

DROP TABLE IF EXISTS `personal_view`;
/*!50001 DROP VIEW IF EXISTS `personal_view`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `personal_view` (
  `id_per` tinyint NOT NULL,
  `nom_per` tinyint NOT NULL,
  `app_per` tinyint NOT NULL,
  `apm_per` tinyint NOT NULL,
  `id_cargo_per` tinyint NOT NULL,
  `ci_per` tinyint NOT NULL,
  `id_cat_per` tinyint NOT NULL,
  `unidad_per` tinyint NOT NULL,
  `cost_per` tinyint NOT NULL,
  `telf_per` tinyint NOT NULL,
  `email_per` tinyint NOT NULL,
  `estado` tinyint NOT NULL,
  `ciudad` tinyint NOT NULL,
  `img_per` tinyint NOT NULL,
  `deleted_at` tinyint NOT NULL,
  `fec_ingreso_personal` tinyint NOT NULL,
  `token_notification` tinyint NOT NULL,
  `clv_personal` tinyint NOT NULL,
  `on_event` tinyint NOT NULL,
  `nom_cargo` tinyint NOT NULL,
  `nombre_cat` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `priv_grp`
--

DROP TABLE IF EXISTS `priv_grp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priv_grp` (
  `id_priv_grp` int(11) NOT NULL AUTO_INCREMENT,
  `crud_cod` varchar(4) DEFAULT NULL,
  `id_grp` int(11) DEFAULT NULL,
  `id_ele` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_priv_grp`),
  KEY `id_grp` (`id_grp`),
  KEY `id_ele` (`id_ele`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `priv_grp`
--

LOCK TABLES `priv_grp` WRITE;
/*!40000 ALTER TABLE `priv_grp` DISABLE KEYS */;
INSERT INTO `priv_grp` VALUES (1,'1111',1,1),(2,'1111',1,3),(3,'1111',1,4),(4,'1111',1,5);
/*!40000 ALTER TABLE `priv_grp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary table structure for view `priv_grp_ele`
--

DROP TABLE IF EXISTS `priv_grp_ele`;
/*!50001 DROP VIEW IF EXISTS `priv_grp_ele`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `priv_grp_ele` (
  `id_priv_grp` tinyint NOT NULL,
  `crud_cod` tinyint NOT NULL,
  `id_grp` tinyint NOT NULL,
  `id_ele` tinyint NOT NULL,
  `nom_grp` tinyint NOT NULL,
  `nom_ele` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `producto` (
  `id_prod` int(11) NOT NULL AUTO_INCREMENT,
  `nom_prod` varchar(50) DEFAULT NULL,
  `fecha_prod` varchar(50) DEFAULT NULL,
  `desc_prod` text DEFAULT NULL,
  `fecha_rec` varchar(50) DEFAULT NULL,
  `precio_total_prod` float DEFAULT NULL,
  `costo_total_prod` float DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_prod`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
INSERT INTO `producto` VALUES (25,'Gin & Tonic Premium','2022-01-25','Coctel elaborado con jugo de limon, agua tonica Schweppes, gin beefeather, sabana de pepino y flor de lavanda',NULL,22.95,16.9675,NULL),(26,'Sangria Rosa','2022-01-25','Coctel de Sangria Rosa By JP',NULL,16.08,11.6434,NULL),(27,'Old Fashioned','2022-01-25','Coctel Premium con Chivas Regal 12 años',NULL,24.78,19.378,NULL),(28,'Mojito','2022-01-25','Coctel mojito con limon,  hierba buena, ron blanco y goma',NULL,13.52,7.60953,NULL),(29,'Mojito Amazónico','2022-01-25','Coctel mojito con limon,  hierba buena, singani casa real y goma',NULL,11.3978,11.3978,NULL),(30,'Chuflay Clásico','2022-01-25','Chuflay clasico con singani casa real etiqueta negra',NULL,6.3,5.23773,NULL),(31,'Piña Punch','2022-01-26','Coctel elaborado a base de jarabe de piña, limon, singani casa real, angostura bitters y piña para garnish',NULL,7.07,5.64149,NULL),(32,'Chuflay de Moconchinchi','2022-01-27','texto de ejemplo',NULL,9,7.66205,'2022-02-06 22:15:33'),(33,'Chuflay de Moconchinchi','2022-02-06','Coctel elaborado a base de jarabe de moconchichi',NULL,14.8787,14.8787,'2022-02-07 10:06:29'),(34,'Chuflay de Moconchinchi','2022-02-07','Coctel elaborado con jugo de limon, agua tonica Schweppes, gin beefeather, sabana de pepino y flor de lavanda',NULL,4.7521,4.7521,NULL);
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proforma`
--

DROP TABLE IF EXISTS `proforma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proforma` (
  `id_prof` int(11) NOT NULL AUTO_INCREMENT,
  `num_prof` int(11) DEFAULT NULL,
  `fecha_prof` varchar(50) DEFAULT NULL,
  `fecha_eve_prof` varchar(50) DEFAULT NULL,
  `sub_total_prof` float DEFAULT 0,
  `iva_prof` float DEFAULT 0,
  `total_general_prof` float DEFAULT 0,
  `id_eve` int(11) DEFAULT NULL,
  `id_per_cont` int(11) DEFAULT NULL,
  `estado_prof` tinyint(1) DEFAULT NULL,
  `pdf_prof` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_prof`),
  KEY `id_eve` (`id_eve`),
  KEY `id_per_cont` (`id_per_cont`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proforma`
--

LOCK TABLES `proforma` WRITE;
/*!40000 ALTER TABLE `proforma` DISABLE KEYS */;
INSERT INTO `proforma` VALUES (1,687,'05/07/2021','2021-07-05',825.21,132.03,957.24,67,9,0,NULL),(28,713,'07/07/2021','2021-07-07',825.21,132.03,957.24,100,9,0,NULL),(29,714,'07/07/2021','2021-07-07',825.21,132.03,957.24,101,9,0,NULL),(30,715,'07/07/2021','2021-07-07',825.21,132.03,957.24,102,9,0,NULL),(31,716,'07/07/2021','2021-07-07',825.21,132.03,957.24,103,9,0,NULL),(32,717,'07/10/2021','2021-10-07',825.21,132.03,957.24,104,9,0,NULL),(33,718,'07/10/2021','2021-10-08',825.21,132.03,957.24,105,9,0,NULL),(34,719,'03/11/2021','2021-11-03',825.21,132.03,957.24,106,10,0,NULL),(35,720,'03/11/2021','2021-11-03',825.21,132.03,957.24,107,10,0,NULL),(36,721,'03/11/2021','2021-11-03',825.21,132.03,957.24,108,10,0,NULL),(37,722,'03/11/2021','2021-11-03',825.21,132.03,957.24,109,9,0,'37-Proforma-2021-11-03.pdf'),(38,723,'04/11/2021','2021-11-04',825.21,132.03,957.24,110,9,0,NULL),(39,724,'05/11/2021','2021-11-05',825.21,132.03,957.24,111,9,0,NULL),(40,725,'05/11/2021','2021-11-05',825.21,132.03,957.24,112,9,0,NULL),(41,726,'13/01/2022','2022-01-14',825.21,132.03,957.24,113,10,0,NULL),(42,727,'14/01/2022','2022-01-22',825.21,132.03,957.24,114,9,0,NULL),(43,728,'14/01/2022','2022-01-14',825.21,132.03,957.24,115,10,0,NULL),(44,729,'14/01/2022','2022-01-14',825.21,132.03,957.24,116,9,0,NULL),(45,730,'14/01/2022','2022-01-29',825.21,132.03,957.24,117,10,0,NULL),(46,731,'14/01/2022','2022-01-14',825.21,132.03,957.24,118,10,0,'46-Proforma-2022-01-14.pdf'),(47,732,'17/01/2022','2022-01-29',825.21,132.03,957.24,119,9,0,NULL),(48,733,'17/01/2022','2022-01-29',825.21,132.03,957.24,117,10,0,NULL),(49,734,'17/01/2022','2022-01-18',825.21,132.03,957.24,120,9,0,NULL),(50,735,'17/01/2022','2022-01-17',825.21,132.03,957.24,121,13,1,'50-Proforma-2022-01-17.pdf'),(51,736,'18/01/2022','2022-01-20',825.21,132.03,957.24,122,13,0,NULL),(52,737,'26/01/2022','2021-04-02',825.21,132.03,957.24,123,15,0,'52-Proforma-2022-01-26.pdf'),(53,738,'26/01/2022','2021-04-02',825.21,132.03,957.24,123,15,1,NULL),(54,739,'26/01/2022','2021-04-02',825.21,132.03,957.24,123,15,0,NULL),(55,740,'27/01/2022','2022-01-27',825.21,132.03,957.24,124,15,0,'55-Proforma-2022-01-27.pdf'),(56,741,'06/02/2022','2022-02-06',825.21,132.03,957.24,125,15,1,'56-Proforma-2022-02-06.pdf'),(57,742,'07/02/2022','2022-02-07',825.21,132.03,957.24,126,15,0,NULL),(58,743,'07/02/2022','2022-02-07',825.21,132.03,957.24,127,15,1,'58-Proforma-2022-02-07.pdf'),(59,744,'07/02/2022','2022-02-07',825.21,132.03,957.24,128,16,0,'59-Proforma-2022-02-07.pdf'),(60,745,'07/02/2022','2022-02-07',825.21,132.03,957.24,129,16,0,NULL),(61,746,'10/02/2022','2022-02-10',825.21,132.03,957.24,130,15,0,NULL),(62,747,'11/03/2022','2022-03-11',825.21,132.03,957.24,131,16,1,NULL),(63,748,'11/03/2022','2022-04-11',825.21,132.03,957.24,132,15,1,NULL),(64,749,'17/03/2022','2021-01-17',825.21,132.03,957.24,133,15,1,NULL),(65,750,'17/03/2022','2022-04-17',825.21,132.03,957.24,134,13,0,NULL),(66,751,'17/03/2022','2022-04-17',825.21,132.03,957.24,135,13,1,NULL),(67,752,'21/03/2022','',825.21,132.03,957.24,136,0,0,NULL),(68,753,'22/03/2022','',825.21,132.03,957.24,137,0,0,NULL),(69,754,'22/03/2022','',825.21,132.03,957.24,138,0,0,NULL),(70,755,'22/03/2022','2022-05-22',825.21,132.03,957.24,139,14,1,NULL),(71,756,'10/07/2022','2022-07-10',825.21,132.03,957.24,140,13,0,NULL),(72,757,'10/07/2022','2022-07-10',825.21,132.03,957.24,141,13,0,NULL),(73,758,'10/07/2022','2022-07-10',397.52,63.6,461.12,142,13,0,NULL),(74,759,'10/07/2022','2022-07-10',825.21,132.03,957.24,143,13,0,NULL),(75,760,'10/07/2022','2022-07-10',825.21,132.03,957.24,144,13,0,NULL),(76,761,'10/07/2022','2022-07-10',354.75,56.76,411.51,145,13,0,NULL),(77,762,'10/07/2022','2022-07-10',354.75,56.76,411.51,146,13,0,NULL),(78,763,'10/07/2022','2022-07-10',354.75,56.76,411.51,147,13,0,NULL),(79,764,'10/07/2022','2022-07-10',354.75,56.76,411.51,148,9,0,NULL),(80,765,'10/07/2022','2022-07-10',825.21,132.03,957.24,149,13,0,NULL);
/*!40000 ALTER TABLE `proforma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `id_prov` int(11) NOT NULL AUTO_INCREMENT,
  `nom_prov` varchar(50) DEFAULT NULL,
  `desc_prov` varchar(200) DEFAULT NULL,
  `nit_prov` int(11) DEFAULT NULL,
  `raz_soc` varchar(50) DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_prov`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'D&M','Venta de destilados como, singani, vodka, etc',1000231232,'Sociedad Agro Industrial del Valle',NULL),(2,'Flor de Lavanda Samaipata','Envia Flor de Lavanda fresca desde Samaipata',12312444,'Samaipata S.R.L.',NULL),(5,'ejemplo','ejemplo',123131,'ejemplo','2021-12-09 11:28:29');
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receta`
--

DROP TABLE IF EXISTS `receta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receta` (
  `id_rec` int(11) NOT NULL AUTO_INCREMENT,
  `nom_rec` varchar(50) DEFAULT NULL,
  `unidad_rec` varchar(50) DEFAULT NULL,
  `desc_rec` varchar(200) DEFAULT NULL,
  `cost_total_rec` float DEFAULT NULL,
  `fecha_rec` varchar(50) DEFAULT NULL,
  `deleted_at` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_rec`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receta`
--

LOCK TABLES `receta` WRITE;
/*!40000 ALTER TABLE `receta` DISABLE KEYS */;
INSERT INTO `receta` VALUES (0,'No Existe',NULL,NULL,NULL,NULL,'null'),(1,'Jarabe de Mokonchinchi',NULL,'Jarabe a base de azucar, agua y mokonchinchi',20,'2020-10-05','2020-10-06 20:21:24'),(4,'Jugo de Limon',NULL,'Extracto del jugo de Limon',25,'2020-10-12','2020-10-27 21:44:24'),(6,'Jarabe de Mokonchinchi',NULL,'Jarabe a base de azucar, agua y mokonchinchi',0,'2020-10-29','2021-12-09 21:52:41'),(7,'Oleo Sacrum','Gramos','Un conjunto de especias, azucar blanca y cascara de naranja para hacer Sangria Rosa',13.266,'2021-02-25',NULL),(10,'Achachayru Royale',NULL,'Ejemplo',27,'2021-11-22','2022-01-26 08:11:20'),(11,'Jugo de Limón 1onz','Onz','Extracto de Limon verde',0.3074,'2022-01-25',NULL),(12,'Sabana de Pepino','Unidad','Se extrae de un pepino mediano, sacando en promedio 9 sabanas por pepino',0.1375,'2022-01-25',NULL),(13,'Garnish Media Frutilla Fresca',NULL,'Frutilla Fresca cortada a la mitad para Garnish',0,'2022-01-25','2022-01-25 18:02:56'),(14,'Garnish Media Frutilla Fresca','Unidad','Frutilla Fresca cortada a la mitad para Garnish',0.225,'2022-01-25',NULL),(15,'Flor de Hierba Buena','Unidad','Flor de hierba buea salen de una bolsita aprox 20',0.1,'2022-01-25',NULL),(16,'Sangria Rosa By JP Caceres',NULL,'El conjunto de Oleo Sacrum, Vino Rosado y Singani',0,'2022-01-25','2022-01-25 18:33:07'),(17,'Sangria Rosa By JP Caceres',NULL,'El conjunto de Oleo Sacrum, Vino Rosado y Singani',0,'2022-01-25','2022-01-25 18:33:15'),(18,'Sangria Rosa By JP Caceres',NULL,'Conjunto de Oleo Sacrum, vino rosado Campos y SIngani',0,'2022-01-25','2022-01-25 18:36:16'),(19,'Sangria Rosa By JP Caceres 1.2Lt','Botella','Conjunto de Oleo Sacrum, vino rosado Campos y SIngani',65.2552,'2022-01-25',NULL),(20,'Jarabe de Goma 1:1 1.2Lt','Botella','Hervor de Agua con Azucar',4.002,'2022-01-25',NULL),(21,'Laminas de Naranja Old Fashioned','Unidad','Laminas de Cascara de Naranja Para Preparado y Garnish Old Fashioned',0.4,'2022-01-25',NULL),(22,'Hoja de Hierba Buena 15 und','Unidad','Sola Hoja de Hierba Buena',0.4,'2022-01-25',NULL),(23,'Limon Fresco en Rodaja 1und','Unidad','Rojada de Limon verde',0.03886,'2022-01-25',NULL),(24,'Jarabe de Piña 1.400 Lt.','Litro','Reduccion de Nectar de piña con Azucar',16.8,'2022-01-26',NULL),(25,'Piña Fresca en Triangulo','Unidad','Piña cortada en triangulo para garnish',0.3503,'2022-01-26',NULL),(26,'Receta de Prueba','sinunidad','asdasdsa',0,'2022-01-27','2022-01-27 15:44:48'),(27,'Jarabe de Moconchinchi 1.5Lt','Litro','Jarabe a base de azucar, agua y mokonchinchi',37.311,'2022-01-27','2022-02-06 19:32:09'),(28,'Pepa de Moconchinchi Hidratada','Unidad','Pepa de moconchinchi hervida con canela y clavo de olor',9,'2022-02-06',NULL),(29,'Jarabe de Moconchinchi 1.5Lt','Botella 1.5Lt','Jarabe a base de azucar, agua y mokonchinchi',24.884,'2022-02-06','2022-02-07 10:06:37'),(30,'Jarabe de Moconchinchi 1.5Lt','Botella 1.5Lt','Jarabe a base de azucar, agua y mokonchinchi',9.004,'2022-02-07',NULL);
/*!40000 ALTER TABLE `receta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reg_almacen`
--

DROP TABLE IF EXISTS `reg_almacen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reg_almacen` (
  `id_alm` int(11) NOT NULL AUTO_INCREMENT,
  `fec_entrada` text DEFAULT NULL,
  `cant_almacen` float DEFAULT NULL,
  `id_rec` int(11) DEFAULT NULL,
  `id_ins` int(11) DEFAULT NULL,
  `id_eve` int(11) DEFAULT NULL,
  `baja_almacen` int(11) DEFAULT NULL,
  `fec_caducidad` text DEFAULT NULL,
  PRIMARY KEY (`id_alm`),
  KEY `id_rec` (`id_rec`),
  KEY `id_ins` (`id_ins`),
  CONSTRAINT `reg_almacen_ibfk_1` FOREIGN KEY (`id_rec`) REFERENCES `receta` (`id_rec`),
  CONSTRAINT `reg_almacen_ibfk_2` FOREIGN KEY (`id_ins`) REFERENCES `insumo` (`id_ins`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reg_almacen`
--

LOCK TABLES `reg_almacen` WRITE;
/*!40000 ALTER TABLE `reg_almacen` DISABLE KEYS */;
INSERT INTO `reg_almacen` VALUES (11,'2022-02-22',1,0,3,117,NULL,'2022-02-23'),(12,'2022-02-21',12,11,0,117,NULL,'2022-02-22'),(13,'2022-02-22',2,0,24,1,NULL,'2022-02-23'),(14,'2022-02-07',1,0,6,127,NULL,'2022-03-21');
/*!40000 ALTER TABLE `reg_almacen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicio_extra`
--

DROP TABLE IF EXISTS `servicio_extra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicio_extra` (
  `id_serv` int(11) NOT NULL AUTO_INCREMENT,
  `nom_serv` varchar(50) DEFAULT NULL,
  `desc_serv` varchar(200) DEFAULT NULL,
  `costo_serv` float DEFAULT NULL,
  `cant_serv` float DEFAULT NULL,
  `unidad_serv` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_serv`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicio_extra`
--

LOCK TABLES `servicio_extra` WRITE;
/*!40000 ALTER TABLE `servicio_extra` DISABLE KEYS */;
/*!40000 ALTER TABLE `servicio_extra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usu` int(11) NOT NULL AUTO_INCREMENT,
  `nom_usu` varchar(50) DEFAULT NULL,
  `app_usu` varchar(50) DEFAULT NULL,
  `tel_usu` int(11) DEFAULT NULL,
  `email_usu` varchar(50) DEFAULT NULL,
  `clv_usu` varchar(20) DEFAULT NULL,
  `id_grp` int(11) DEFAULT NULL,
  `fec_ini` varchar(50) DEFAULT NULL,
  `fec_fin` varchar(50) DEFAULT NULL,
  `ult_con` varchar(50) DEFAULT NULL,
  `foto_usu` text DEFAULT NULL,
  PRIMARY KEY (`id_usu`),
  KEY `id_grp` (`id_grp`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'Alexander','Florido',67410049,'mishaflorido@gmail.com','admin',1,'2022-01-06','2023-04-26','2022-07-10','1643082341_fe5c9e11f669c7d97609.jpg'),(3,'Fresia','Maldonado',7654321,'fresia@gmail.com','admin',2,'2022-01-19','2022-01-07','2022-01-27','1643085617_4fd66c320ede14470aea.jpg');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `evento_view`
--

/*!50001 DROP TABLE IF EXISTS `evento_view`*/;
/*!50001 DROP VIEW IF EXISTS `evento_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `evento_view` AS select `e`.`id_eve` AS `id_eve`,`e`.`nom_eve` AS `nom_eve`,`e`.`fecha_eve` AS `fecha_eve`,`e`.`id_cli` AS `id_cli`,`e`.`cost_eve_total` AS `cost_eve_total`,`e`.`IUE` AS `IUE`,`e`.`IT` AS `IT`,`e`.`IVA` AS `IVA`,`e`.`total_imp` AS `total_imp`,`e`.`total_facturado` AS `total_facturado`,`e`.`utilidad` AS `utilidad`,`e`.`nro_factura` AS `nro_factura`,`c`.`nom_cli` AS `nom_cli` from (`evento` `e` join `cliente` `c` on(`e`.`id_cli` = `c`.`id_cli`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `personal_view`
--

/*!50001 DROP TABLE IF EXISTS `personal_view`*/;
/*!50001 DROP VIEW IF EXISTS `personal_view`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `personal_view` AS select `p`.`id_per` AS `id_per`,`p`.`nom_per` AS `nom_per`,`p`.`app_per` AS `app_per`,`p`.`apm_per` AS `apm_per`,`p`.`id_cargo_per` AS `id_cargo_per`,`p`.`ci_per` AS `ci_per`,`p`.`id_cat_per` AS `id_cat_per`,`p`.`unidad_per` AS `unidad_per`,`p`.`cost_per` AS `cost_per`,`p`.`telf_per` AS `telf_per`,`p`.`email_per` AS `email_per`,`p`.`estado` AS `estado`,`p`.`ciudad` AS `ciudad`,`p`.`img_per` AS `img_per`,`p`.`deleted_at` AS `deleted_at`,`p`.`fec_ingreso_personal` AS `fec_ingreso_personal`,`p`.`token_notification` AS `token_notification`,`p`.`clv_personal` AS `clv_personal`,`p`.`on_event` AS `on_event`,`crg`.`nom_cargo` AS `nom_cargo`,`cat`.`nombre_cat` AS `nombre_cat` from ((`personal` `p` join `cargo_personal` `crg` on(`crg`.`id_cargo_per` = `p`.`id_cargo_per`)) join `categoria_personal` `cat` on(`cat`.`id_cat_per` = `p`.`id_cat_per`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `priv_grp_ele`
--

/*!50001 DROP TABLE IF EXISTS `priv_grp_ele`*/;
/*!50001 DROP VIEW IF EXISTS `priv_grp_ele`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `priv_grp_ele` AS select `p`.`id_priv_grp` AS `id_priv_grp`,`p`.`crud_cod` AS `crud_cod`,`p`.`id_grp` AS `id_grp`,`p`.`id_ele` AS `id_ele`,`g`.`nom_grp` AS `nom_grp`,`e`.`nom_ele` AS `nom_ele` from ((`priv_grp` `p` join `grupo` `g` on(`p`.`id_grp` = `g`.`id_grp`)) join `elemento` `e` on(`p`.`id_ele` = `e`.`id_ele`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-19 12:31:58
