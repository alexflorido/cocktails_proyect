<!-- Tabla de insumos -->

<div class="row mb-3">
  <h1>Registro detalle Producto</h1>
</div>
<!-- <div class="row col-md-10">
  <div class="container offset-md-1 col-md-4">
    Lista Insumos
  </div>
  <div class="container col-md-4 offset-md-1">
    <h3> Lista Insumos Agregados</h3>
  </div>

</div> -->
<div class="row">
  <div class="col-md-3 pe-4">

    <div class="btn-group btn-group-toggle" data-toggle="buttons">
      <label class="btn btn-primary input-radio active">
        <input type="radio" name="options" id="option1" value=' 1' autocomplete="off" checked> Insumo
      </label>
      <label class="btn btn-success input-radio">
        <input type="radio" name="options" id="option2" value='2' autocomplete="off"> Receta
      </label>

    </div>
  </div>

  <div class="container col-md-4 offset-md-2">
    <h3>Detalle del Producto</h3>
  </div>

</div>
<div class="row border p-3 ps-2 m-2">

  <!-- <div class="col-sm-2" style='height: 400px; width: 18%'>
    <div class="row my-2" style='width: 100px'>
      <div class="offset-md-8">


      </div>
    </div> -->
  <div class="col-md-2 ms-2 me-4" style='height: 400px'>
    <div class="row" style='height: 100%;  overflow: auto;'>

      <div class="list-group listado" id="list_ins_prod">
        <li class="list-group-item list-group-item-primary align-content-center text-center">Insumo <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#reg_ins"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
        <div class="input-group">
          <div class="input-group-prepend">
            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
          <input type="text" class="form-control buscar-insumo" placeholder="Buscar insumo" aria-label="" aria-describedby="basic-addon1">
        </div>
      </div>
      <div class="list-group listado d-none" id="list_rec_prod">
        <li class="list-group-item list-group-item-primary align-content-center">Receta</li>
        <div class="input-group">
          <div class="input-group-prepend">
            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
          <input type="text" class="form-control buscar-insumo" placeholder="Buscar Receta" aria-label="" aria-describedby="basic-addon1">
        </div>
      </div>
    </div>

  </div>


  <!-- <div class="col-2">
    
  </div> -->

  <!-- /////////////////////////////// -->
  <!-- Tabla de insumos agregados -->
  <div class="col-md-9 ms-2">

    <div class="row table-responsive">
      <table class="table table-hover" id="tab_det_prod">

        <thead>

          <tr>
            <th class="text-center">
              Cod Producto
            </th>
            <th class="text-center">
              Insumo
            </th>
            <th class="text-center">
              Cantidad
            </th>
            <th class="text-center">
              Unidad
            </th>
            <th class="text-center">
              C/U Insumo Bs.
            </th>
            <th class="text-center">
              Costo Bs.
            </th>
            <th class="text-center">
              Precio Final
            </th>
            <th class="text-center">

            </th>
          </tr>
        </thead>
        <tbody class="tbody-det-prod">

        </tbody>
      </table>
    </div>
    <div class="row">
      <div class="offset-md-5">
        <button type="button" id="btn-det-prod" class="btn btn-success btn-block">Registrar Detalle de Producto</button>
      </div>

    </div>
  </div>

</div>

<!-- /////////////////////////////// -->
<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade" id="reg_ins" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Insumo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <!-- <input type="text" class="form-control" id="id_insumo"> -->
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre Insumo:</label>
            <!-- <input type="text" class="form-control" id="nom_insumo"> -->
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Unidad de Medida</label>
            <!-- <input type="text" class="form-control" id="und_insumo"> -->
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Costo Insumo:</label>
            <!-- <input type="number" class="form-control" id="cost_insumo"> -->
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Fecha Del Precio</label>
            <!-- <input type="date" class="form-control" id="fec_insumo"> -->
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id='nuevo_ins' type="button" class="btn btn-primary" onclick="insert_insumo()">Nuevo Insumo</button>
        <!-- <button id='modificar_ins' type="button" class="btn btn-primary" onclick="modificar_insumo()">Modificar insumo</button> -->
        <button type="button" id="cerrar-ins-det" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ////////// -->