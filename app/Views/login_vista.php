<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="<?= base_url() ?>/assets/css/login_css.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<!------ Include the above in your HEAD tag ---------->
</head>

<body>

	<div class="login-reg-panel">
		<div class="login-info-box">
			<h2>Have an account?</h2>
			<p>Lorem ipsum dolor sit amet</p>
			<label id="label-register" for="log-reg-show">Login</label>
			<input type="radio" name="active-log-panel" id="log-reg-show" checked="checked">
		</div>

		<div class="register-info-box">
			<h2>NEXT GENERATION COCKTAILS</h2>
			<p>La mejor empresa coctelera de Bolivia</p>

		</div>

		<div class="white-panel">
			<?php echo form_open("login"); ?>
			<div class="login-show show-log-panel">
				<h2>INICIAR SESSION</h2>
				<?php if (isset($validation)) { ?>
					<div class="alert alert-danger" role="alert" id="alert_plant_application">
						<?= $validation ?>
					</div>
				<?php } ?>
				<input type="text" name="email" placeholder="Correo Electrónico">
				<input type="password" name="clv" placeholder="Clave de acceso">
				<input type="submit" value="VALIDAR">
				<a href="mailto:alexanderruslan@hotmail.com?Subject=Olvide%20mi%20clave%20del%20sistema%20web%20">Olvido su Contraseña</a>
			</div>
			<?php echo form_close(); ?>
			<div class="register-show">
				<h2>REGISTER</h2>
				<input type="text" placeholder="Email">
				<input type="password" placeholder="Password">
				<input type="password" placeholder="Confirm Password">
				<input type="button" value="Register">
			</div>

		</div>
	</div>
	<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
	<script src="<?= base_url() ?>/assets/js/login.js"></script>

</body>

</html>