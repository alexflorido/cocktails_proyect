<div class="container">
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="radio_pdf" id="radio_pdf1" value="1" checked>
        <label class="form-check-label" for="radio_pdf1">Por año</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="radio_pdf" id="radio_pdf2" value="2">
        <label class="form-check-label" for="radio_pdf2">Por Rango de Años</label>
    </div>
    <div class="form-check form-check-inline">
        <input class="form-check-input" type="radio" name="radio_pdf" id="radio_pdf3" value="3">
        <label class="form-check-label" for="radio_pdf3">Por Evento</label>
    </div>
    <div class="form-check form-check-inline">
        <button id="pdf_button" class="btn btn-primary">PDF</button>
    </div>
    <div class="row">

        <div class="col-md-3 py_filter">
            <!-- <div class="row border">
                <div class="col-md-6">
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="ev_pyear" checked>
                        <label class="form-check-label" for="flexSwitchCheckDefault">Eventos por año</label>
                    </div>
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="ev_ryear">
                        <label class="form-check-label" for="flexSwitchCheckDefault">Eventos por Rango de Años</label>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="form-check form-switch">
                        <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault">
                        <label class="form-check-label" for="flexSwitchCheckDefault">Productos Vendidos</label>
                    </div>
                    <div>
                        <button id="pdf_button">PDF</button>
                    </div>
                </div>
            </div> -->
            <label>Año:</label><br><select class='form-control' name="filt_year" data-component="date" id="filt_year">
                <?php
                for ($year = 2015; $year <= date('Y'); $year++) {
                ?>
                    <option value="<?php echo $year ?>"><?php echo $year ?></option>';
                <?php
                }
                ?>
            </select>
        </div>
        <div class="col-md-5 pr_filter d-none">
            <div class="row">
                <div class="col-md-6 ">
                    <label>Rango de Años de: </label>
                    <select class='form-control year_range' name="filt_year_range1" data-component="date" id="filt_year_range1">
                        <?php
                        for ($year = 2015; $year <= date('Y'); $year++) {
                        ?>
                            <option value="<?php echo $year ?>"><?php echo $year ?></option>';
                        <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="">a: </label>
                    <select class='form-control year_range' name="filt_year_range2" data-component="date" id="filt_year_range2" style="width:10rem">
                        <?php
                        for ($year = 2015; $year <= date('Y'); $year++) {
                        ?>
                            <option value="<?php echo $year ?>"><?php echo $year ?></option>';
                        <?php
                        }
                        ?>
                    </select>

                </div>
            </div>
        </div>
        <div class="col-md-6 mt-4 d-none pe_filter">

            <div class="input-group">
                <input type="text" class="form-control" placeholder="Buscar Evento" aria-label="" aria-describedby="basic-addon1" id="search_events">
                <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
            </div>
        </div>

    </div>
    <hr>
    <div class="pe_gra d-none">
        <div class="row">
            <div class="col-md-6">
                <div style='overflow-y: scroll; height: 28em'>
                    <ol class="list-group list-group-numbered" id="list_eventosRP">

                    </ol>
                </div>
            </div>
            <div class="col-md-6" id="event_details_pdf">
                <input type="hidden" name="index_dataEv">
                <div class="row ms-2">
                    <div class="offset-md-2 col-md-6">
                        <h2>Detalle de Evento</h2>
                    </div>
                    <div class="offset-md-2 col-md-1"><button class="btn btn-primary btn_pdf_eventos_det"><i class="far fa-file-pdf"></i></button></div>
                </div>
                <div class="row my-3">
                    <div class="col-md-6">
                        <div id="nom_evento_pdf">Nombre Evento: </div>
                        <div id="fec_evento_pdf">Fecha Evento: </div>
                    </div>
                    <div class="col-md-6">
                        <div id="nfac_evento_pdf">Numero de Factura: </div>
                        <div id="npro_evento_pdf">Numero de Proforma:
                        </div>
                    </div>
                </div>
                <h4>Personal Asignado</h4>
                <ol class="list-group list-group-numbered mb-3" id="personal_asig">
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                            <div class="fw-bold">Cargo</div>
                            Nombre personal
                        </div>
                        <span class="badge bg-primary rounded-pill">350 Bs</span>
                    </li>
                </ol>

                <h4>Productos Vendidos</h4>
                <ol class="list-group list-group-numbered mb-3" id="prod_asig">
                    <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                            <div class="fw-bold">Nombre Producto</div>
                            <div>Cantidad: </div>
                        </div>
                        <span class="badge bg-warning rounded-pill">100 Bs</span>
                    </li>
                </ol>

                <table class="table mt-2" id="t-detEv">
                    <thead>
                        <tr class="table-info">
                            <th>Costo del Evento</th>
                            <th>IT</th>
                            <th>IVA</th>
                            <th>Total Impuestos</th>
                            <th>Total Facturado</th>
                        </tr>
                    </thead>
                    <tbody id="tb_event_rp">

                    </tbody>

                </table>

            </div>
        </div>
    </div>

    <div class="row mt-2 py_gra">
        <div class="col-md-6 filt_year border">


            <div class="my-2">
                <canvas id="filter_year"></canvas>
            </div>
        </div>



        <div class="col-md-6 border ">
            <div class="list-group list_ev mt-4">

            </div>

        </div>
    </div>
    <div class="row py_gra">
        <div class="offset-md-1 col-md-10">

            <div>
                <canvas id="filter_ryear"></canvas>
            </div>
        </div>


    </div>
    <div class="row py_gra">
        <div class="offset-md-1 col-md-10">

            <div>
                <canvas id="prod_year"></canvas>
            </div>
        </div>

    </div>
    <div class="row py_gra">
        <div class="offset-md-1 col-md-10">
            <div>
                <canvas id="sumprod_year"></canvas>
            </div>
        </div>
    </div>

</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>