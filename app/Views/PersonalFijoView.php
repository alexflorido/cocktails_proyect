<!-- PILL Personal Fijo -->
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/personal.css">

<div class="row">
    <div class="offset-md-5">
        <h1 class="mt-4">Personal Fijo</h1>

    </div>
</div>
<div class="container mt-4">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="cell-border" id="tab_per" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">
                            Nombre
                            <br>
                            CI
                        </th>
                        <th class="text-center">
                            Ciudad
                        </th>
                        <th class="text-center">
                            Cargo
                            <br>
                            Categoria
                        </th>
                        <th class="text-center">
                            Costo
                            <br>
                            Unidad
                        </th>
                        <th class="text-center">
                            Telefono
                        </th>
                        <th class="text-center">
                            Estado
                        </th>
                    </tr>
                </thead>
                <tbody class="tbody-per">

                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- ////////////////////// -->



<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade" id="ModalPersonal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <div class=" border border-dark rounded float-none p-3">

                    <div class="row">
                        <div class="col align-self-center">
                            <h3><span class="badge bg-primary">Datos Personales</span></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="container">
                            <img id='img_per_show' alt="Avatar" class="imagen mx-auto d-block">
                        </div>

                        <form id="modif_imagen" action="" method="post">
                            <label id="mod_img_per" for="img_per_mod" class=" btn btn-mdb-color btn-rounded float-left back_icon edit_icon">
                                <svg width="1.6em" height="1.6em" viewBox="0 0 16 16" class="bi bi-pencil-fill icon " fill="White" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                </svg>
                                <input type="file" style="display: none;" id="img_per_mod" name='img_per_mod' enctype="multipart/form-data">

                            </label>
                        </form>


                    </div>
                    <form role="form" method="POST" id="form_personal">
                        <div class="row">
                            <input type="hidden" class="form-control" id="id_per" name="id_per">
                            <div class="offset-col-1 col-8 col-sm-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Nombre:</label>
                                    <input type="text" class="form-control" id="nom_per" name="nom_per">
                                </div>
                            </div>
                            <div class="offset-col-1 col-4 col-sm-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Apellido Paterno:</label>
                                    <input type="text" class="form-control" id="app_per" name="app_per">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-col-1 col-8 col-sm-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Apellid Materno:</label>
                                    <input type="text" class="form-control" id="apm_per" name="apm_per">
                                </div>
                            </div>
                            <div class="offset-col-1 col-4 col-sm-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Cedula de Identidad:</label>
                                    <input type="text" class="form-control" id="ci_per" name="ci_per">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="offset-col-1 col-8 col-sm-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">N° Telefono:</label>
                                    <input type="text" class="form-control" id="telf_per" name="telf_per">
                                </div>
                            </div>
                            <div class="offset-col-1 col-4 col-sm-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="col-form-label">Correo:</label>
                                    <input type="text" class="form-control" id="email_per" name="email_per">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Clave de Acceso a la App:</label>
                                <input type="text" class="form-control" id="clv_personal" name="clv_personal">
                            </div>
                        </div>

                </div>

                <br>
                <div class=" border border-dark rounded float-none p-3">

                    <div class="row">
                        <div class="col align-self-center">
                            <h3><span class="badge bg-primary">Datos Administrativos</span></h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-col-1 col-8 col-sm-6">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Categoria</label>
                                <Select class="form-control" id="id_cat_per" name="id_cat_per">


                                </Select>
                            </div>
                        </div>
                        <div class="offset-col-1 col-4 col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Cargo:</label>
                                <Select class="form-control" id="id_cargo_per" name="id_cargo_per">


                                </Select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-col-1 col-8 col-sm-6">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Fecha de Ingreso</label>
                                <input type="date" class="form-control" name="fecha_ingreso" id="fecha_ingreso">
                            </div>
                        </div>
                        <div class="offset-col-1 col-4 col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Unidad:</label>
                                <input type="text" class="form-control" name="unidad_per" id="unidad_per">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-col-1 col-8 col-sm-6">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Costo Por Unidad</label>
                                <input type="text" class="form-control" name="cost_per" id="cost_per">
                            </div>
                        </div>
                        <div class="offset-col-1 col-4 col-sm-6">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Ciudad</label>
                                <input type="text" class="form-control" name="ciudad" id="ciudad">
                            </div>
                        </div>

                    </div>
                    <div class="row mt-4">
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="img_per" id="img_per" accept="image/*" enctype="multipart/form-data">
                        </div>
                    </div>
                    <br>

                </div>
                <br>
                <div class="alert alert-danger d-none" id="msg_error" style="text-align: left;">
                    <strong>¡Importante!</strong> Corregir los siguientes Errores.
                    <div id="list-errors">
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enviar Formulario" id="env_form_personal">

                    <button type="button" id="cerrar-modal-rec" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!-- ////////// -->
<!-- Modal Para Reportes -->
<div class="modal fade" id="report_modal" tabindex="-1" aria-labelledby="report_modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Elija la gestion del reporte </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <img src="" id="img_per_pdf" width="230px" height="200px" style="border-radius:10%">
                        <input type="hidden" name="id_per" id="id_per_pdf">

                    </div>
                    <div class="col-md-6">
                        <p id="presetacion_pdf"></p>
                        <select class='form-control' name="year_filt_personal" data-component="date" id="year_filt_personal">
                        </select>

                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" id="g_rep_per">Generar Reporte</button>
            </div>
        </div>
    </div>
</div>