<!-- PILL Control Usuarios -->


<div class="row">
    <div class="offset-md-5">
        <h1 class="mt-4">Control de Usuarios</h1>

    </div>
</div>
<div class="container mt-4">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="cell-border" id="tab_usu" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">
                            Nombre
                        </th>
                        <th class="text-center">
                            Telefono
                        </th>
                        <th class="text-center">
                            Email
                        </th>
                        <th class="text-center">
                            Fecha de Inicio
                        </th>
                        <th class="text-center">
                            Fecha de Finalzación
                        </th>
                        <th class="text-center">
                            Ultima Conexión
                        </th>
                    </tr>
                </thead>
                <tbody class="tbody-usu">

                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- ////////////////////// -->



<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade" id="ModalUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">


                <div class=" border border-dark rounded float-none p-3">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col align-self-center">
                                <h3><span class="badge bg-primary">Datos Personales</span></h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="container">
                                <img id='img_usu_show' alt="Avatar" class="imagen mx-auto d-block">
                            </div>

                            <form id="modif_imagen_usu" action="" method="post">
                                <label id="mod_img_usu" for="img_usu_mod" class=" btn btn-mdb-color btn-rounded float-left back_icon edit_icon">
                                    <svg width="1.6em" height="1.6em" viewBox="0 0 16 16" class="bi bi-pencil-fill icon " fill="White" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z" />
                                    </svg>
                                    <input type="file" style="display: none;" id="img_usu_mod" name='img_usu_mod' enctype="multipart/form-data">

                                </label>
                            </form>


                        </div>
                        <form role="form" method="POST" id="form_usuario">
                            <div class="row">
                                <input type="hidden" class="form-control" id="id_usu" name="id_usu">
                                <div class="offset-col-1 col-8 col-sm-6">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Nombre:</label>
                                        <input type="text" class="form-control" id="nom_usu" name="nom_usu">
                                    </div>
                                </div>
                                <div class="offset-col-1 col-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Apellido Paterno:</label>
                                        <input type="text" class="form-control" id="app_usu" name="app_usu">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="offset-col-1 col-8 col-sm-6">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">N° Telefono:</label>
                                        <input type="text" class="form-control" id="tel_usu" name="tel_usu">
                                    </div>
                                </div>
                                <div class="offset-col-1 col-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Correo:</label>
                                        <input type="text" class="form-control" id="email_usu" name="email_usu">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="offset-col-1 col-8 col-sm-6">
                                    <div class="form-group">
                                        <label for="recipient-name" class="col-form-label">Contraseña:</label>
                                        <input type="text" class="form-control" id="clv_usu" name="clv_usu">
                                    </div>
                                </div>
                            </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Grupo</label>
                                <Select class="form-control" id="id_grp" name="id_grp">


                                </Select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="offset-col-1 col-8 col-sm-6">
                            <div class="form-group">
                                <label for="message-text" class="col-form-label">Fecha de Ingreso</label>
                                <input type="date" class="form-control" name="fec_ini" id="fec_ini">
                            </div>
                        </div>
                        <div class="offset-col-1 col-4 col-sm-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Fecha de Final:</label>
                                <input type="date" class="form-control" name="fec_fin" id="fec_fin">
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-12">
                            <input type="file" class="form-control" name="foto_usu" id="foto_usu" enctype="multipart/form-data" placeholder="Imagen de perfil">
                        </div>
                    </div>
                    <br>
                </div>
                <br>
                <div class="alert alert-danger" id="msg_error_usu" style="text-align: left;">
                    <strong>¡Importante!</strong> Corregir los siguientes Errores.
                    <div id="list_errors_usu">
                    </div>

                </div>

                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Enviar Formulario" id="env_form_personal">

                    <button type="button" id="cerrar-modal-usu" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
                </form>
            </div>
        </div>
    </div>

</div>
<!-- ////////// -->