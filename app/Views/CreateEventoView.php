<!DOCTYPE html>
<html lang="en" id="pdf_content">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- CSS para Data Tables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://use.typekit.net/fad1vtz.css">
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/evento.css">
    <!-- ///// -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Scripts para estilo de Data Tables -->
    <script src="https://unpkg.com/bootstrap-table@1.17.1/dist/bootstrap-table.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.1/js/dataTables.select.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js" integrity="sha512-jzL0FvPiDtXef2o2XZJWgaEpVAihqquZT/tT89qCVaxVuHwJ/1DFcJ+8TBMXplSJXE8gLbVAUv+Lj20qHpGx+A==" crossorigin="anonymous"></script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.22/jspdf.plugin.autotable.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.es.min.js"></script> -->

    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.min.js"></script> -->
    <!-- <script src="https://cdn.datatables.net/1.10.23/js/dataTables.material.min.js"></script> -->
    <!-- <script src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> -->
    <script src="<?= base_url() ?>/assets/js/proforma.js"></script>
    <!-- //////// -->
    <title>Crear Evento</title>

</head>

<body>
    <style>
        .coordinates {
            background: rgba(0, 0, 0, 0.5);
            color: #fff;
            position: absolute;
            bottom: 41px;
            left: 10em;
            padding: 5px 10px;
            margin: 0;
            font-size: 11px;
            line-height: 18px;
            border-radius: 20px;
            display: none;
        }
    </style>



    <div class="position-fixed bottom-0 end-0 p-3 toast_per" style="z-index: 11;" id="liveToast">
        <div class="" role="alert" style="border: 1px; border-style: solid; border-color: black;" aria-live="assertive" aria-atomic="true">
            <div class="toast-header" style="background-color: white;">
                <!-- <img src="..." class="rounded me-2" alt="..."> -->
                <strong class="me-auto">Nombre Personal</strong>

                <button type="button" class="btn-close" onclick="$('#liveToast').addClass('toast_per')" aria-label="Close"></button>
            </div>
            <div class="toast-body" style="background-color: white;" id="desc_toast">

            </div>
        </div>
    </div>
    <div id="logo_url"></div>

    <div class="row  mt-4 ms-1" id="logo_prof">
        <div class="offset-xs-1 col col-sm-3  d-flex justify-content-center " id="logo_1">
            <img src="assets/img/logo2.png" class="mt-3" width="310" height="140">
        </div>
        <div class="container border border-dark col col-sm-8 ms-4 fuente" style="border-radius: 20px; ">
            <div class="row mt-3">
                <div class="col offset-md-2 ">

                    <h3 style="font-size: calc(1.2rem + .6vw); font-weight: 700; color: rgb(19, 27, 85)">

                        NEXT GENERATION COCKTAILS S.R.L.
                    </h3>
                </div>


            </div>
            <div class="text-center lh-lg " style="margin-left: 240px; margin-top: -15px;">


                NIT: 304642024

            </div>
            <div class="row" style="margin-top: -15px;">

                <div class="offset-md-4 col-md-4 align-top" style="font-size: 2.2rem; font-weight: 700; color: rgb(19, 27, 85)">

                    PROFORMA

                </div>
                <div class="col-md-3 text-start" style="font-size: 1.25rem; font-weight: 700; color: red; margin-top: 12px;" id="num_prof_show">

                </div>

            </div>
            <!-- <div class="row"> -->
            <div class="offset-md-5 col-md-4 text-end fs-4" id="fecha_prof_logo" style="margin-top: -10px;">

            </div>
            <!-- </div> -->

        </div>
    </div>
    <input type="hidden" id="is_new" value="1">

    <div class="offset-md-2 col-md-7 fuente " id="" style="font-size: 1rem;">
        Av. Piraí entre 4to y 5to anillo, Edificio Santa Mónica Of. 5A- Telefono:(591) 69420001 - Santa Cruz, Bolivia
    </div>
    <!-- <div class="offset-md-3">
            </div>
        
       <hr class="mt-0"> -->
    <div class="col offset-md-0 col-sm-11 round_event" id="datos" style="margin-left: 40px;">
        <form role="form" method="POST" id="form_reg_event">
            <input type="hidden" name="coordinates_lat">
            <input type="hidden" name="coordinates_lng">
            <input type="hidden" name='id_eve' id="id_eve" value="">
            <div class="container mt-4 ">
                <div class="row">
                    <div class="col-md-auto align-self-center">
                        <h5>
                            <pre for="" class="form-label d-flex align-items-end ">SEÑOR(ES):   </pre>
                        </h5>
                    </div>
                    <div class="col-md-5 d-flex align-items-start ps-0">
                        <div class="mb-1">
                            <input type="text" name="" class='form-control inp_datos' id="search_client" placeholder="CLIENTE" list="list_client" style="width: 170%;">
                            <input type="hidden" name="id_cli" id="event_id_cli">

                        </div>
                        <datalist id="list_client">
                        </datalist>
                    </div>
                    <div class="col-md-1 align-self-center" style="margin-left:1px;">
                        <label for="" class=" d-flex align-items-end ">NIT/CI: </label>
                    </div>
                    <div class="col-md-3 ps-0">
                        <input type="text" id="nit_cli" name="event_nit" style="width: 100%;" class="form-control col-md-1 inp_datos" placeholder="NIT/CI">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-auto align-self-center">
                        <h5>
                            <pre for="" class="form-label d-flex align-items-end ">AUTORIZACIÓN:</pre>
                        </h5>
                    </div>
                    <div class="col-md-5 d-flex align-items-start ps-0">
                        <div class="dropdown">
                            <button class="btn dropdown-toggle inp_datos " style="border-color: rgb(143, 143, 139); width:100%" type="button" id="search_pc" aria-placeholder="CLIENTE" data-bs-toggle="dropdown" aria-expanded="false">
                            </button>
                            <input type="hidden" id="event_id_pc" name="id_per_cont" class="inp_datos">
                            <ul class="dropdown-menu" id='list_pc' aria-labelledby="search_pc">

                            </ul>
                        </div>


                    </div>
                    <div class="col-md-auto align-self-center me-1">
                        <label for="" class=" d-flex align-items-end ">TELEFONO: </label>
                    </div>
                    <div class="col-md-3 ps-0">
                        <input type="text" id="tel_pc" class="form-control col-md-1 inp_datos" placeholder="NUMERO DE TELEFONO" style="width: 100%;">
                    </div>
                </div>
                <div class="row mt-2 mb-4">
                    <div class="col-md-auto align-self-center">
                        <h5>
                            <pre class="form-label d-flex align-items-end ">EVENTO:</pre>
                        </h5>
                    </div>
                    <div class="col-md-5 d-flex align-items-start ps-0">
                        <input type="text" class="form-control inp_datos" style="width: 200%;" name="nom_eve" id="evento_nombre" placeholder="NOMBRE EVENTO" value="">
                        <a class="btn" id="togeolocal"><i class="fas fa-map-marker-alt"></i></a>
                    </div>
                    <div class="col-md-auto align-self-center " style="margin-left:60px">
                        <label for="" class=" d-flex align-items-end ">FECHA: </label>
                    </div>
                    <div class="col-md-3 ps-0" style="margin-left:1.8em;">
                        <input type="date" name="fecha_eve" id="event_date" class="form-control col-md-1 inp_datos" placeholder="FECHA DE EVENTO" value="">
                    </div>
                    <!-- <div class="col-md-1"> -->
                    <!-- </div> -->
                </div>
            </div>


            <div class="row mb-2 mt-4 to_pdf">
                <div class="offset-md-3 col-md-5">
                    <input type="submit" class="form-control btn-success" value="<?php if (isset($is_new)) {
                                                                                        echo 'Crear Proforma';
                                                                                    } else {
                                                                                        echo 'Crear Evento';
                                                                                    } ?>">
                </div>
            </div>
        </form>
    </div>
    <!-- TABLE -->
    <div class="row mx-1 my-4 disabled">
        <div class="col col-md-2 position-relative to_pdf">
            <div class="container text-center position-absolute top-50 start-50 translate-middle">
                <div class=" border border-black rounded-pill">
                    <label for="add_personal" class="form-label me-1">Personal</label>
                    <button id="add_personal" class="btn btn-outline-success" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
                <br>
                <div class=" border rounded-pill">
                    <label for="add_producto" class="form-label">Producto</label>
                    <button id="add_producto" class="btn btn-outline-primary" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
                </div>
                <br>
                <div class=" border rounded-pill">
                    <label for="add_producto" class="form-label">Eliminar Fila</label>
                    <button id="eliminar_producto" class="btn btn-outline-danger" type="button"><i class="fa fa-times" aria-hidden="true"></i></button>
                </div>
            </div>

        </div>
        <div class="col offset-xs-0 ps-0" id="tabla">


            <table id="table_prof" class=" table_prof table-responsive ms-3 me-4">

                <thead class=" text-center" id="thead_table">
                    <tr class=" row-cols-5">
                        <th class="th_prof col col-sm-1 mx-2">
                            Cantidad
                        </th>
                        <th class="th_prof col col-md-3">
                            Detalle
                        </th>
                        <th class="th_prof col col-md-1">
                            Precio Unitario
                        </th>
                        <th class="th_prof col col-md-1">
                            IMPORTE
                        </th>
                        <th class="th_prof col col-md-1">
                            Precio Final
                        </th>
                    </tr>
                    <tr class="text-light">
                        <td class="td_prof_first">z</td>
                        <td class="td_prof_first"> </td>
                        <td class="td_prof_first"> </td>
                        <td class="td_prof_first"> </td>
                        <td class="td_prof_first"> </td>
                    </tr>
                </thead>
                <tbody class="text-center" id="tbody_table">


                </tbody>
                <tfoot id="tfoot_table">
                    <tr class="text-light">
                        <td class="td_prof_end">z</td>
                        <td class="td_prof_end"></td>
                        <td class="td_prof_end"></td>
                        <td class="td_prof_end"></td>
                        <td class="td_prof_end"></td>
                    </tr>


                    </tr>
                    <tr class="text-center">
                        <td class="tfoot-prof fs-4" colspan="2">

                            <div class="row ms-4">
                                <div class="col-md-1 me-2 text-start">SON: </div>
                                <div class="col-md-10 border-bottom border-dark ms-2" id='letra-numero'></div>
                            </div>
                            <div class="row d-flex justify-content-center mb-2">
                                <div class="col-md-8 border-bottom border-dark text-end" id="centavo_num"></div>
                                <div class="col-md-3">
                                    BOLIVIANOS
                                </div>
                            </div>
                        </td>
                        <td class="tfoot-prof degrad_celeste ">SUB - TOTAL: <br> IT - IVA (16%):</td>
                        <td class="tfoot-prof degrad_celeste">
                            <div class="border-bottom border-dark">
                                <input type="text" class="form-control text-center" style="border: none !important; background-color: transparent" name="" id="sub_total_importe" placeholder="SUB TOTAL" disabled>
                            </div>
                            <input type="text" class="form-control text-center" style="border: none !important; background-color: transparent" name="" id="imp_importe" placeholder="IMPUESTOS" disabled>
                        </td>
                        <td class="tfoot-prof degrad_celeste">
                            <div class="border-bottom border-dark">
                                <input type="text" class="form-control text-center" style="border: none !important; background-color: transparent" name="" id="sub_total_precio" placeholder="SUB TOTAL" disabled>
                            </div>
                            <input type="text" class="form-control text-center" style="border: none !important; background-color: transparent" name="" id="imp_precio" placeholder="IMPUESTOS" disabled>
                        </td>
                    </tr>


                    <tr>
                        <td class="tfoot-prof degrad_azul" style="font-size: 14px;" colspan="2">NOTA: Pago con cheque a nombre de la empresa o con deposito bancario</td>
                        <td class="tfoot-prof text-center fs-4 text fw-bold th_prof" rowspan="3">TOTAL Bs:</td>
                        <td class="tfoot-prof " rowspan="3"><input type="text" class='text-center form-control fs-4 text fw-bold' style="border: none !important" placeholder="TOTAL IMP" name="" id="total_importe"></td>
                        <td class="tfoot-prof" rowspan="3"><input type="text" class='text-center form-control fs-4 text fw-bold' style="border: none !important" placeholder="TOTAL FINAL" name="" id="total_precio"></td>
                    </tr>

                    <tr>
                        <td class="tfoot-prof degrad_azul" style="font-size: 13px;" colspan="2" rowspan="2">
                            <div class="row mt-1">
                                <div class="text-start col col-md-5">
                                    Nº CUENTA (EN BOLIVIANOS): 100 - 0275081
                                </div>
                                <div class="col offset-md-1 col-md-4">
                                    BANCO NACIONAL DE BOLVIA
                                </div>
                            </div>

                            <div class="row  my-1">
                                <div class="text-start col col-md-5">
                                    Nº CUENTA (EN DOLARES): 140 - 0618522
                                </div>
                                <div class="col offset-md-1 col-md-5">
                                    NEXT GENERATION COCKTAILS S.R.L.
                                </div>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="row mt-2 ">
            <div class="col-md-3">
                <input type="button" value="Generar Lista de Compras" class="form-control btn-success" id="generar_lista">
            </div>
            <div class="offset-md-2 col-md-4">
                <input type="button" class="form-control btn-danger" value="REGISTRAR PROFORMA" id="insert_proforma_detalle">
            </div>
            <div class="col-md-2 ">
                <!-- <form action="/pdf/event" method="post"> -->
                <input type="submit" class="form-control " value="Generar PDF" id="create_pdf">
                <!-- </form> -->
            </div>
        </div>


    </div>




</body>
<!-- MODAL MODIFICAR RECETA -->
<div class="modal fade bd-example-modal-lg" id="modal_prof" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal_prof_tittle" class="modal-title">Tittle</h5>
                <button type="button" id='butt_det_mod_close' class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id_rec" id="id_rec_mod">

                <div class="row mx-auto">
                    <table class=" my-custom-scrollbar table-wrapper-scroll-y table table-striped table-bordered offset-md-0 col-md-10" id="table_prof_add">
                        <thead id="head_personal" class="hidden">
                            <tr>
                                <th>
                                    Nombre y Apellido
                                </th>
                                <th>
                                    Cargo
                                </th>
                                <th>
                                    Estado
                                </th>
                                <th>
                                    Ciudad
                                </th>
                                <th>
                                    Precio Final
                                </th>

                                <th>

                                </th>
                            </tr>
                        </thead>
                        <thead id="head_producto" class="hidden">
                            <tr>
                                <th>
                                    Producto
                                </th>
                                <th>
                                    Dato ACtualizado
                                </th>
                                <th>
                                    Descripción
                                </th>
                                <th>
                                    Costo Producto
                                </th>
                                <th>
                                    Precio Final
                                </th>
                                <th>

                                </th>
                            </tr>
                        </thead>
                        <tbody id="table-det-rec-mod-body">

                        </tbody>

                    </table>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- MODAL UBICACION GEOGRAFICA -->
<div class="modal fade " id="modal_ug" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal_prof_tittle" class="modal-title">Seleccione la Ubicacion del Evento</h5>
                <button type="button" id='butt_det_mod_close' class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                    <!-- <span aria-hidden="true">&times;</span> -->
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id_rec" id="id_rec_mod">
                <div class="row">
                    <div style="margin-left:30px">
                        <div id='map' style='width: 400px; height: 300px; border-radius: 20px'> </div>
                        <pre id="coordinates" class="coordinates"></pre>
                        <input type="hidden" name="coordinates_lat">
                        <input type="hidden" name="coordinates_lng">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
</div>
<!-- MODAL LISTA DE PRODUCTOS -->
<div class="modal fade bd-example-modal-lg" id="modal_list_comp" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal_prof_tittle" class="modal-title">LISTA DE COMPRAS</h5>
                <button type="button" id='butt_det_mod_close' class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="id_rec" id="id_rec_mod">

                <div class="row mx-auto">
                    <div class="col-md-3 border border-success" style='overflow-y: scroll; height: 20rem'>
                        <h5 class="my-2">Productos en Proforma</h5>
                        <table class=" my-custom-scrollbar table-wrapper-scroll-y table table-striped table-bordered my-2" id="table_list_comp">
                            <thead>
                                <tr>
                                    <th>
                                        Nombre Producto
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>


                                    </th>
                                </tr>
                            </thead>

                            <tbody id="tbody_list_comp">

                            </tbody>

                        </table>
                    </div>
                    <div class="col-md-5  border border-success" style='overflow-y: scroll; height: 20rem'>
                        <h5 class="my-2">Lista de insumos/recetas para añadir a lista de compras</h5>
                        <table class=" my-custom-scrollbar table-wrapper-scroll-y table table-striped table-bordered my-2" id="table_det_prod_comp">
                            <thead>
                                <tr>
                                    <th>
                                        Nombre Producto
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>


                                    </th>
                                </tr>
                            </thead>

                            <tbody id="tbody_det_prod_comp">

                            </tbody>

                        </table>
                    </div>
                    <div class="col-md-4  border border-success" style='overflow-y: scroll; height: 20rem'>
                        <h5 class="my-2">Lista de Insumos en Almacen</h5>
                        <table class=" my-custom-scrollbar table-wrapper-scroll-y table table-striped table-bordered my-2" id="table_ins_almacen">
                            <thead>
                                <tr>
                                    <th>
                                        Nombre Producto
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>


                                    </th>
                                </tr>
                            </thead>

                            <tbody id="tbody_ins_almacen">

                            </tbody>

                        </table>
                    </div>
                </div>
                <div class="row my-2 ">
                    <div class="col-md-7 ms-2 me-4 border border-success">
                        <div class="row my-2">
                            <div class="col-md-4 ">

                                <h5>Tabla de Lista de Compras </h5>
                                <button id="juntar_item" class="form-control btn-info" onclick="juntarLista()">Juntar Item</button>

                            </div>
                            <div class="col-md-4 my-2">
                                <label for="date_to_shop">Cuando se Debe Comprar</label>
                                <input type="date" name="date_to_shop" class='form-control' id="date_to_shop">
                            </div>

                        </div>


                        <div style=" height:400px;  overflow:auto; margin-top: 1rem ">
                            <table class="table table-success table-bordered my-2" id="table_shopList">
                                <thead>
                                    <tr>
                                        <th>
                                            Nombre Producto
                                        </th>
                                        <th>
                                            Cantidad
                                        </th>
                                        <th>
                                            Costo
                                        </th>
                                        <th>
                                            Commentario
                                        </th>

                                        <th>


                                        </th>
                                    </tr>
                                </thead>

                                <tbody id="tbody_shopList">

                                </tbody>

                            </table>
                        </div>
                        <div class="row my-2">
                            <div class="offset-md-3 col-md-4">
                                <button class="form-control btn-success" id="insertar_lista">Guardar Lista</button>

                            </div>
                        </div>

                    </div>
                    <div class="col-md-4 ms-4 border border-success">
                        <div class="row my-2">
                            <div class="col-md-4 ">

                                <h5>A quien Enviar?</h5>
                                <button id="send_notification" class="form-control btn-info">Enviar Lista</button>

                            </div>

                        </div>

                        <div style=" height:400px;  overflow:auto; margin-top: 2.5rem ">
                            <table class=" my-custom-scrollbar table-wrapper-scroll-y table table-striped table-bordered my-2" id="table_shopList_personal">
                                <thead>
                                    <tr>
                                        <th>
                                            Nombre Personal
                                        </th>
                                        <th>
                                            Cargo
                                        </th>
                                        <th>
                                            Ciudad
                                        </th>
                                        <th>


                                        </th>
                                    </tr>
                                </thead>

                                <tbody id="tbody_shopList_personal">

                                </tbody>

                            </table>
                        </div>

                    </div>

                </div>



            </div>
        </div>
    </div>
</div>

</html>