<!-- PILL Personal Fijo -->
<link rel="stylesheet" href="<?= base_url() ?>/assets/css/evento.css">


<div class="row">
    <div class="offset-md-5">
        <h1 class="mt-4">Evento</h1>
    </div>
</div>


<div class="container my-4">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="cell-border" id="tab_eve" cellspacing="0" width="100%" data-toolbar=".toolbar" data-height="400" data-virtual-scroll="true" data-show-columns="true">
                <thead>
                    <tr>
                        <th class="text-center">
                            Nro Factura
                            <br>
                            Fecha
                        </th>
                        <th class="text-center">
                            Nombre Evento
                            <br>
                            Cliente
                        </th>
                        <th class="text-center">
                            Costos de Producción
                        </th>
                        <th class="text-center">
                            IUE
                        </th>
                        <th class="text-center">
                            IT
                        </th>
                        <th class="text-center">
                            IVA
                        </th>
                        <th class="text-center">
                            Total Impuestos
                        </th>
                        <th class="text-center">
                            Total Facturado
                        </th>
                    </tr>
                </thead>
                <tbody class="tbody-eve">

                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- ////////////////////// -->



<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade bd-example-modal-lg" id="ModalModEvento" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modificar Evento</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="modal_mod_event_form">
                    <input type="hidden" name="id_eve" id="id_eve_mod">
                    <div class="row">
                        <div class="form-group ms-3 col-md-5">
                            <label for="recipient-name" class="col-form-label">Nombre Receta:</label>
                            <input type="text" class="form-control" id="nom_eve" name='nom_eve'>
                        </div>
                        <div class="form-group offset-md-1 col-md-5">
                            <label for="message-text" class="col-form-label">Número de Factura</label>
                            <input type="text" class="form-control" id="nro_factura" name="nro_factura">
                        </div>
                    </div>
                    <div class="row my-2 ">
                        <div class="form-group col-md-5">
                            <label for="message-text" class="col-form-label">Fecha del Evento</label>
                            <input type="date" class="form-control" id="fecha_eve" name='fecha_eve'>
                        </div>
                        <div class="form-group offset-md-1 col-md-5">
                            <label for="message-text" class="col-form-label">Total Facturado</label>
                            <input type="text" class="form-control" id="total_facturado" name='total_facturado'>
                        </div>
                    </div>
                    <div class="row ms-3 my-4">
                        <div class="offset-md-4 col-md-3">
                            <input type="submit" value="Modificar Datos" class="btn btn-success">
                        </div>
                    </div>
                </form>
                <!-- <form action="new_proform" method="post"> -->
                <div class="row mt-3 mb-2">
                    <div class="offset-md-8 col-md-2">
                        <input type="hidden" id='id_eve' name='id_eve'>
                        <input type="button" value="Agregar nueva proforma" class="btn btn-primary" id="btn_new_prof">
                    </div>
                </div>
                <!-- </form> -->

                <div class="row mx-auto">
                    <div class="table-responsive">
                        <table class=" my-custom-scrollbar table-wrapper-scroll-y table table-striped table-bordered offset-md-0 col-md-10" id="table-det-rec-mod">
                            <thead>
                                <tr>
                                    <th>
                                        Nro Proforma
                                    </th>
                                    <th>
                                        Fecha de Creacion
                                    </th>
                                    <th>
                                        Sub Total Bs.
                                    </th>
                                    <th>
                                        Iva Prof Bs.
                                    </th>
                                    <th>
                                        Total General Bs.
                                    </th>
                                    <th>
                                        Estado
                                    </th>
                                    <th>

                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody id="table-prof-mod-body">

                            </tbody>

                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
<!-- ////////// -->