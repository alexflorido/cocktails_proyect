<!-- PILL Personal Fijo -->
<div>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/cliente.css">


    <div class="row">
        <div class="offset-md-5">
            <h1 class="mt-4">Menu de Clientes</h1>
        </div>
    </div>

    <div class="container my-4">

        <table class="cell-border" id="tab_cli" width="100%">
            <thead>
                <tr>
                    <th class="text-center">
                        Nombre Cliente
                    </th>
                    <th class="text-center">
                        NIT Cliente
                    </th>
                    <th class="text-center">
                        Número de Contacto
                    </th>
                    <th class="text-center">
                        Correo de Contacto
                    </th>

                </tr>
            </thead>
            <tbody class="tbody-cli">

            </tbody>
        </table>


    </div>

    <!-- ////////////////////// -->



    <!-- MODAL PARA INSERTAR Y EDITAR UN Cliente -->
    <div class="modal fade" id="ModalCliente" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Formulario Cliente</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <div class=" border border-info rounded float-none">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col align-self-center">
                                    <h3><span class="badge badge-info ">Datos Cliente</span></h3>
                                </div>
                            </div>

                            <form role="form" method="POST" id="modal_form_client">
                                <div class="mx-auto row">
                                    <input type="hidden" class="form-control" id="id_cli" name="id_cli">
                                    <div class="offset-col-1 col-8 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Nombre Cliente:</label>
                                            <input type="text" class="form-control" id="nom_cli" name="nom_cli">
                                        </div>
                                    </div>
                                    <div class="offset-col-1 col-4 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">NIT Cliente:</label>
                                            <input type="text" class="form-control" id="nit_cli" name="nit_cli">
                                        </div>
                                    </div>
                                </div>
                                <div class="mx-auto row">
                                    <div class="offset-col-1 col-8 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Número de Contacto:</label>
                                            <input type="text" class="form-control" id="tel_cli" name="tel_cli">
                                        </div>
                                    </div>
                                    <div class="offset-col-1 col-4 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Correo Cliente:</label>
                                            <input type="text" class="form-control" id="email_cli" name="email_cli">
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <br>

                        \
                    </div>

                    <div class="alert alert-danger" id="msg_error_cli" style="text-align: left;">
                        <strong>¡Importante!</strong> Corregir los siguientes Errores.
                        <div id="list-errors_cli">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_client_sub">
                            <span class="spinner-border spinner-border-sm d-none spin_client" role="status" aria-hidden="true"></span>
                            <span class="d-none spin_client">Loading...<br></span>
                            <span class="d-none spin_client"> Please Wait</span>
                            <span class="not_spin_client"> Enviar Formulario</span>
                        </button>
                        <!-- <input type="submit" class="btn btn-primary" value="Enviar Formulario" id="env_form_cliente"> -->

                        <button type="button" id="cerrar-modal-cli" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    </div>
                    </form>
                    <!-- Boddy -->
                </div>
            </div>
        </div>

    </div>
    <!-- ////////// -->
    <!-- MODAL PARA INSERTAR Y EDITAR UN PERSONA DE CONTACTO -->
    <div class="modal fade" id="ModalPersonaContacto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Persona de Contacto</h5>

                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <div class=" border border-info rounded float-none">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col align-self-center">
                                    <h3><span class="badge badge-info ">Datos Persona de Contacto</span></h3>
                                </div>
                            </div>



                            <div class="accordion" id="accordionPersonaContacto">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="registrar_nuevo">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#formulario_PC" aria-expanded="false" aria-controls="formulario_PC">
                                            Registrar Nueva Persona de Contacto
                                        </button>
                                    </h2>
                                    <div id="formulario_PC" class="accordion-collapse collapse show" aria-labelledby="registrar_nuevo" data-bs-parent="#accordionPersonaContacto">
                                        <div class="accordion-body">
                                            <strong>Informacion de la Persona de Contacto </strong>
                                            <form class='form_per_cont' role="form" method="POST">
                                                <div class="mx-auto row">
                                                    <div class="offset-col-1 col-8 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Nombre:</label>
                                                            <input type="hidden" id="id_cli_pc" name="id_cli">
                                                            <input type="text" class="form-control nom_per_cont" name="nom_per_cont">
                                                        </div>
                                                    </div>
                                                    <div class="offset-col-1 col-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Apellido:</label>
                                                            <input type="text" class="form-control app_per_cont" name="app_per_cont">
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="mx-auto row">
                                                    <div class="offset-col-1 col-8 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Telefono:</label>
                                                            <input type="text" class="form-control tel_per_cont" name="tel_per_cont">
                                                        </div>
                                                    </div>
                                                    <div class="offset-col-1 col-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Correo Electronico:</label>
                                                            <input type="text" class="form-control email_per_cont" name="email_per_cont">
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="mx-auto row">
                                                    <div class="offset-col-1 col-8 col-sm-6">
                                                        <input type="submit" class="btn btn-success" value="Enviar Formulario" id="env_form_per_cont">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- copiado hasta aqui -->
                                </div>

                                <div id="accordion-boddy">
                                </div><!-- accordion boddy-->
                            </div>
                        </div>

                    </div>


                </div>
                <div class="modal-footer">


                    <button type="button" id="cerrar-modal-per-cont" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- ////////// -->