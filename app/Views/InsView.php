<!-- PILL INSUMO -->

<div class="row">
  <div class="offset-md-5">
    <h1 class="mt-4">Insumos</h1>
  </div>
</div>
<div class="container mt-4 ">

  <table class="cell-border" id="tab_logic" width="100%" data-toolbar=".toolbar" data-height="400" data-virtual-scroll="true" data-show-columns="true">
    <thead>
      <tr>
        <th class="text-center th-sm">
          Nombre Insumo
        </th>
        <th class="text-center th-sm">
          Unidad de Medida
        </th>
        <th class="text-center th-sm">
          Costo de Insumo
        </th>
        <th class="text-center th-sm">
          Fecha de Compra
        </th>

    </thead>
    <tbody class="tbody-ins">

    </tbody>
  </table>

</div>

<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>

      </div>
      <div class="modal-body">
        <form>
          <input type="hidden" class="form-control" id="id_ins">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre Insumo:</label>
            <input type="text" class="form-control" id="nom_ins">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Unidad de Medida</label>
            <input type="text" class="form-control" id="und_ins">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Costo Insumo:</label>
            <input type="text" class="form-control" id="cost_ins">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Fecha Del Precio</label>
            <input type="date" class="form-control" id="fec_ins">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id='nuevo_ins' type="button" class="btn btn-primary" onclick="insertar_insumo()">Nuevo Insumo</button>
        <button id='modificar_ins' type="button" class="btn btn-primary" onclick="modificar_insumo()">Modificar insumo</button>
        <button type="button" id='cerrar-ins' class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ////////// -->