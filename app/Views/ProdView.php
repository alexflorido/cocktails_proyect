<!-- Tabla de insumos -->



<div class="row">
    <div class="offset-md-5">
        <h1 class="mt-4">Producto</h1>

    </div>
</div>

<div class="container mt-4">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-striped table-bordered table-sm" id="tab_prod" cellspacing="0" width="100%" data-toolbar=".toolbar" data-height="400" data-virtual-scroll="true" data-show-columns="true">
                <thead>
                    <tr>
                        <th class="text-center">
                            Nombre Producto
                        </th>
                        <th class="text-center">
                            Descripcion Producto
                        </th>
                        <th class="text-center">
                            Fecha de Creacion
                        </th>
                        <th class="text-center">
                            Costo
                        </th>
                        <th class="text-center">
                            Precio Total
                        </th>
                    </tr>
                </thead>
                <tbody class="tbody-prod">

                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- ////////////////////// -->



<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade" id="ModalProducto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registro Producto</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <input type="hidden" class="form-control" id="id_prod">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nombre Producto:</label>
                        <input type="text" class="form-control" id="nom_prod">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Descripcion Producto:</label>
                        <input type="text" class="form-control" id="desc_prod">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Fecha De Creacion</label>
                        <input type="date" class="form-control" id="fecha_prod">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id='nuevo_ins' type="button" class="btn btn-primary" onclick="insertar_producto()">Crear Producto</button>
                <!-- <button id='modificar_ins' type="button" class="btn btn-primary" onclick="modificar_insumo()">Modificar insumo</button> -->
                <button type="button" id="cerrar-modal-prod" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- ////////// -->
<!-- MODAL MODIFICAR PRODUCTO -->
<div class="modal fade bd-example-modal-lg" id="modal_mod_prod" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modificar Producto</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </button>
            </div>
            <div class="modal-body">
                <form id="modal_mod_prod_form">
                    <input type="hidden" name="id_prod_mod" id="id_prod_mod">
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="recipient-name" class="col-form-label">Nombre Producto:</label>
                            <input type="text" class="form-control" id="nom_prod_mod" name="nom_prod_mod">
                        </div>
                        <div class="form-group offset-md-1 col-md-5">
                            <label for="message-text" class="col-form-label">Descripcion Producto</label>
                            <textarea cols="40" rows="3" class="form-control" id="desc_prod_mod" name="desc_prod_mod"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="message-text" class="col-form-label">Costo Total Producto:</label>
                            <input type="text" class="form-control" id="cost_total_prod_mod" name="cost_total_prod_mod">
                        </div>
                        <div class="form-group offset-md-1 col-md-5">
                            <label for="message-text" class="col-form-label">Precio Total</label>
                            <input type="text" class="form-control" id="precio_total_prod" name="precio_total_prod">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="message-text" class="col-form-label">Fecha De Creacion</label>
                            <input type="date" class="form-control" id="fecha_prod_mod" name="fecha_prod_mod">
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="offset-md-4 col-md-3">
                            <input type="submit" value="Enviar Formulario" class="btn btn-success">
                        </div>
                    </div>
                </form>
                <br>
                <div class="row mx-auto">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered my-custom-scrollbar">
                            <thead>
                                <tr>
                                    <th>
                                        Insumo
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Unidad
                                    </th>
                                    <th>
                                        Costo Insumo Bs.
                                    </th>
                                    <th>
                                        Precio Final
                                    </th>
                                    <th>

                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody id="table-det-prod-mod-body">

                            </tbody>

                        </table>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>