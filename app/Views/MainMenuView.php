<?php
$session = \Config\Services::session();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/simple-sidebar.css">
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/pill.css">
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/menu.css">
    <link rel="stylesheet" href="<?= base_url() ?>/assets/css/fontawesome-free-5.15.4-web/css/all.css">


    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> -->
    <!-- CSS para Data Tables -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.jqueryui.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.7.1/css/buttons.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.9/css/responsive.dataTables.min.css">
    <!-- ///// -->
    <link href='https://api.mapbox.com/mapbox-gl-js/v2.7.0/mapbox-gl.css' rel='stylesheet' />



</head>

<body>

    <!-- SIDE BAR -->
    <div class="d-flex" id="wrapper">

        <!-- Sidebar -->
        <div class="border-right sticky-top" id="sidebar-wrapper">
            <div id="sidebar_color">
                <div class="sidebar-heading ms-4" id="logo">
                    <img src="<?= base_url() ?>/assets/img/logo2.png" width="190" height="160" id="main_logo">
                </div>
                <div class="list-group mx-3 my-5" style="background-color: rgba(104, 97, 97, 0)!important;">
                    <a class="list-group-item list-group-item-dark" data-toggle="tab" href="#Receta"><i class="fas fa-cocktail"></i> Menu Productos-Recetas</a>
                    <a class="list-group-item list-group-item-dark" data-toggle="tab" href="#Personal"><i class="far fa-address-card"></i></i> Menu Personal</a>
                    <a class="list-group-item list-group-item-dark" data-toggle="tab" href="#Evento"><i class="fas fa-calendar-alt"></i> Menu Eventos</a>
                    <a class="list-group-item list-group-item-dark" data-toggle="tab" href="#Clientes y Proveedores"><i class="fas fa-user-friends"></i> Menu de Clientes y Proveedores</a>
                    <?php if ($id_grp == 1) { ?>
                        <a class="list-group-item list-group-item-dark" data-toggle="tab" href="#usuarios"><i class="fas fa-user"></i> Control de Usuarios</a>
                    <?php } ?>
                </div>
            </div>
            <div class="row text-white" style="margin-top: 12rem">
                <div class="offset-md-2 col-md-9" style='font-size: 0.7rem!important;'>
                    Sistema elaborado para NEXT GENERATION COCKTAILS S.R.L.
                </div>
            </div>
        </div>
        <!-- /#sidebar-wrapper -->

        <!-- Page Content -->
        <div id="page-content-wrapper">

            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <div class="container">
                    <div class="col col-md-1">
                        <button class="btn mx-auto" id="menu-toggle">
                            <span class="navbar-toggler-icon "></span>
                        </button>

                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
                <div class="container">
                    <div class="collapse navbar-collapse " id="navbarSupportedContent">
                        <ul class="navbar-nav container justify-content-md-center">
                            <li class="nav-item active col col-md-1 justify-content-md-center">
                                <a class="nav-link" href="#">Inicio <span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item dropdown offset-md-1 col-md-4 rounded-pill" id="dropdown_perfil">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">


                                    <img src="assets/img/foto_usuario/<?= $session->get('picture') ?>" class="rounded-circle" width="50e" height="40e" id="foto_perfil">

                                    <label>
                                        <?= $session->get('username') ?>
                                    </label>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right container" aria-labelledby="navbarDarkDropdownMenuLink">
                                    <!-- <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a> -->
                                    <!-- <div class="dropdown-divider"></div> -->
                                    <a id='cerrar_sesion' role="button" class='dropdown-item' name='cerrar_sesion'>Cerrar Sesión</a>
                                </div>
                            </li>



                        </ul>
                    </div>
                </div>

            </nav>
            <!-- TAB CONTENT -->
            <div class="tab-content" id="tab-content">
                <!-- RECETA PILL -->
                <div id="Receta" class="container-fluid tab-pane fade">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-ins-tab" data-toggle="pill" href="#pills-ins" role="tab" aria-controls="pills-home" aria-selected="true">Submenu Isumo</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-rec-tab" data-toggle="pill" href="#pills-rec" role="tab" aria-controls="pills-profile" aria-selected="false">Submenu Receta</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-prod-tab" data-toggle="pill" href="#pills-prod" role="tab" aria-controls="pills-prod" aria-selected="false">Submenu Producto</a>
                        </li>
                        <li class="nav-item fade" aria-disabled="false">
                            <a class="nav-link  " id="pills-det-tab" data-toggle="pill" href="#pills-det" role="tab" aria-controls="pills-contact" aria-selected="false">Detalle Receta</a>
                        </li>
                        <li class="nav-item fade" aria-disabled="false">
                            <a class="nav-link " id="pills-detProd-tab" data-toggle="pill" href="#pills-det-prod" role="tab" aria-controls="pills-contact" aria-selected="false">Detalle Producto</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-ins" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getInsPill') ?>
                        </div>
                        <div class="tab-pane fade" id="pills-rec" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getRecPill') ?>
                        </div>
                        <div class="tab-pane fade" id="pills-det" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getDetRecPill') ?>
                        </div>
                        <div class="tab-pane fade" id="pills-prod" role="tabpanel" aria-labelledby="pills-prod-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getProdPill') ?>
                        </div>
                        <div class="tab-pane fade" id="pills-det-prod" role="tabpanel" aria-labelledby="pills-detProd-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getDetProdPill') ?>
                        </div>
                    </div>
                </div>
                <!--Receta PILL END  -->

                <!-- Personal PILL -->
                <div id="Personal" class="container-fluid tab-pane fade">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-fijo" role="tab" aria-controls="pills-home" aria-selected="true">Personal Fijo</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-f" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-x" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
                        </li> -->
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-fijo" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getPersonalFijoPill') ?>
                        </div>
                        <!-- <div class="tab-pane fade" id="pills-f" role="tabpanel" aria-labelledby="pills-profile-tab">f</div>
                        <div class="tab-pane fade" id="pills-x" role="tabpanel" aria-labelledby="pills-contact-tab">x</div> -->
                    </div>
                </div>
                <!-- Personal PILL END -->

                <!-- Evento PILL -->
                <div id="Evento" class="container-fluid tab-pane fade">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-evento-tab" data-toggle="pill" href="#pills-evento" role="tab" aria-controls="pills-evento" aria-selected="true">Eventos</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="pills-e-tab" data-toggle="pill" href="#pills-e" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                        </li> -->
                        <li class="nav-item">
                            <a class="nav-link" id="pills-proforma-tab" data-toggle="pill" href="#pills-proforma" role="tab" aria-controls="pills-contact" aria-selected="false">Proforma</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-reporte-tab" data-toggle="pill" href="#pills-reporte" role="tab" aria-controls="pills-contact" aria-selected="false">Reportes</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-evento" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getEventoPill') ?>
                        </div>
                        <div class="tab-pane fade show" id="pills-proforma" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getProformaPill') ?>
                        </div>
                        <div class="tab-pane fade show" id="pills-reporte" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getReportePill') ?>
                        </div>
                        <!-- <div class="tab-pane fade" id="pills-e" role="tabpanel" aria-labelledby="pills-profile-tab">e</div>
                        <div class="tab-pane fade" id="pills-d" role="tabpanel" aria-labelledby="pills-contact-tab">d</div> -->
                    </div>
                </div>
                <!-- Evento PILL END -->
                <!-- Cliente PILL -->
                <div id="Clientes y Proveedores" class="container-fluid tab-pane fade">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-cliente-tab" type="button" data-bs-toggle="pill" data-bs-target="#pills-cliente" role="tab" aria-controls="pills-home" aria-selected="true">Clientes</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-alex-tab" type="button" data-bs-toggle="pill" data-bs-target="#pills-proveedor" role="tab" aria-controls="pills-profile" aria-selected="false">Proveedores</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="pills-d-tab" type="button" data-bs-toggle="pill" data-bs-target="#pills-v" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
                        </li> -->
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-cliente" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getClientePill') ?>

                        </div>
                        <div class="tab-pane fade" id="pills-proveedor" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getProveedorPill') ?>
                        </div>
                        <!-- <div class="tab-pane fade" id="pills-v" role="tabpanel" aria-labelledby="pills-contact-tab">d</div>s -->
                    </div>
                </div>

                <!-- Cliente PILL END -->
                <!-- Usuarios PILL -->
                <div id="usuarios" class="container-fluid tab-pane fade">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-evento-tab" data-toggle="pill" href="#pills-usuarios" role="tab" aria-controls="pills-home" aria-selected="true">Control de Usuarios</a>
                        </li>
                        <!-- <li class="nav-item">
                            <a class="nav-link" id="pills-e-tab" data-toggle="pill" href="#pills-e" role="tab" aria-controls="pills-profile" aria-selected="false">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-d-tab" data-toggle="pill" href="#pills-d" role="tab" aria-controls="pills-contact" aria-selected="false">Contact</a>
                        </li> -->
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-usuarios" role="tabpanel" aria-labelledby="pills-home-tab">
                            <?= view_cell('App\Libraries\ViewComponents::getControlUsuariosPill') ?>
                        </div>

                    </div>
                </div>
                <!-- Evento PILL END -->






            </div>
            <!-- TAB CONTENT END -->


        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Menu Toggle Script -->
    <script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>


    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <!-- Scripts para Chart -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@3.7.1/dist/chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <!-- Scripts para Mapbox -->
    <script src='https://api.mapbox.com/mapbox-gl-js/v2.7.0/mapbox-gl.js'></script>
    <!-- MY SCRIPTS -->
    <script src="<?= base_url() ?>/assets/js/login.js"></script>
    <script src="<?= base_url() ?>/assets/js/cliente.js"></script>
    <script src="<?= base_url() ?>/assets/js/evento.js"></script>
    <script src="<?= base_url() ?>/assets/js/Pfijo.js"></script>
    <script src="<?= base_url() ?>/assets/js/prod.js"></script>
    <script src="<?= base_url() ?>/assets/js/proveedor.js"></script>
    <script src="<?= base_url() ?>/assets/js/receta.js"></script>
    <script src="<?= base_url() ?>/assets/js/insumo.js"></script>
    <script src="<?= base_url() ?>/assets/js/usuarios.js"></script>
    <script src="<?= base_url() ?>/assets/js/ReporteEvento.js"></script>
    <!-- //////// -->
    <!-- Scripts para estilo de Data Tables -->
    <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.25/js/dataTables.jqueryui.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.7.1/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.10.25/pagination/input.js"></script>
    <script src="https://cdn.datatables.net/select/1.3.3/js/dataTables.select.min.js"></script>
    <!-- //////// -->
    <!-- JSPDF -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/2.5.1/jspdf.es.min.js"></script> -->
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/3.5.22/jspdf.plugin.autotable.min.js"></script>
    <!-- /////////// -->
    <script>
        $(document).ready(function() {
            $("a[href='#Receta']").trigger("click")
        })
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        $(".btn-tab").click(function() {
            $("div.tab-pane.pill").removeClass('active');

        });
    </script>

</body>

</html>