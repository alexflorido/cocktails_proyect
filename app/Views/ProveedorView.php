<!-- PILL Personal Fijo -->
<div>



    <div class="row">
        <div class="offset-md-5">
            <h1 class="mt-4">Proveedor</h1>
        </div>
    </div>

    <div class="container my-4">
        <div class="row clearfix">
            <div class="col-md-12 column">
                <table class="cell-border" id="tab_prov" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center">
                                Nombre Proveedor
                            </th>
                            <th class="text-center">
                                Descripción
                            </th>
                            <th class="text-center">
                                Nit
                            </th>
                            <th class="text-center">
                                Razon Social
                            </th>

                        </tr>
                    </thead>
                    <tbody class="tbody-cli">

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <!-- ////////////////////// -->



    <!-- MODAL PARA INSERTAR Y EDITAR UN Cliente -->
    <div class="modal fade" id="ModalProveedor" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Proveedor</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <div class=" border border-info rounded float-none">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col align-self-center">
                                    <h3><span class="badge badge-info ">Datos Proveedor</span></h3>
                                </div>
                            </div>

                            <form id='form_prov' role="form" method="POST">
                                <div class="mx-auto row">
                                    <input type="hidden" class="form-control" id="id_prov" name="id_prov">
                                    <div class="offset-col-1 col-8 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Nombre Proveedor:</label>
                                            <input type="text" class="form-control" id="nom_prov" name="nom_prov">
                                        </div>
                                    </div>
                                    <div class="offset-col-1 col-4 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Descripcion:</label>
                                            <input type="text" class="form-control" id="desc_prov" name="desc_prov">
                                        </div>
                                    </div>
                                </div>
                                <div class="mx-auto row">
                                    <div class="offset-col-1 col-8 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Nit Proveedor:</label>
                                            <input type="text" class="form-control" id="nit_prov" name="nit_prov">
                                        </div>
                                    </div>
                                    <div class="offset-col-1 col-4 col-sm-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="col-form-label">Razon Social:</label>
                                            <input type="text" class="form-control" id="raz_soc" name="raz_soc">
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <br>

                    </div>

                    <div class="alert alert-danger" id="msg_error_prov" style="text-align: left;">
                        <strong>¡Importante!</strong> Corregir los siguientes Errores.
                        <div id="list-errors_prov">
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary" id="btn_prov_sub">
                            <span class="spinner-border spinner-border-sm d-none spin_prov" role="status" aria-hidden="true"></span>
                            <span class="d-none spin_prov">Loading...<br></span>
                            <span class="d-none spin_prov"> Please Wait</span>
                            <span class="not_spin_prov"> Enviar Formulario</span>
                        </button>

                        <button type="button" id="cerrar-modal-prov" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                    </div>
                    </form>
                    <!-- Boddy -->
                </div>
            </div>
        </div>

    </div>
    <!-- ////////// -->
    <!-- MODAL PARA INSERTAR Y EDITAR UN PERSONA DE CONTACTO -->
    <div class="modal fade" id="ModalPersonaContProv" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Persona de Contacto</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">


                    <div class=" border border-info rounded float-none">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col align-self-center">
                                    <h3><span class="badge badge-info ">Datos Persona de Contacto</span></h3>
                                </div>
                            </div>



                            <div class="accordion" id="accordionPersonaContacto">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="registrar_nuevo">
                                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#formulario_prov" aria-expanded="false" aria-controls="formulario_prov">
                                            Registrar Nueva Persona de Contacto
                                        </button>
                                    </h2>
                                    <div id="formulario_prov" class="accordion-collapse collapse show" aria-labelledby="registrar_nuevo" data-bs-parent="#accordionPersonaContacto">
                                        <div class="accordion-body">
                                            <strong>Informacion de la Persona de Contacto </strong>
                                            <form class='form_cont_prov' role="form" method="POST">
                                                <div class="mx-auto row">
                                                    <div class="offset-col-1 col-8 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Nombre:</label>
                                                            <input type="hidden" id="id_prov_pc" name="id_prov">
                                                            <input type="text" class="form-control nombre_cont" name="nombre_cont">
                                                        </div>
                                                    </div>
                                                    <div class="offset-col-1 col-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Estado:</label>
                                                            <input type="text" class="form-control estado" name="estado">
                                                        </div>
                                                    </div>
                                                </div>



                                                <div class="mx-auto row">
                                                    <div class="offset-col-1 col-8 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Telefono:</label>
                                                            <input type="text" class="form-control telf_cont" name="telf_cont">
                                                        </div>
                                                    </div>
                                                    <div class="offset-col-1 col-4 col-sm-6">
                                                        <div class="form-group">
                                                            <label for="recipient-name" class="col-form-label">Correo Electronico:</label>
                                                            <input type="text" class="form-control email_cont" name="email_cont">
                                                        </div>
                                                    </div>
                                                </div>

                                                <br>
                                                <div class="mx-auto row">
                                                    <div class="offset-col-1 col-8 col-sm-6">
                                                        <input type="submit" class="btn btn-success" value="Enviar Formulario" id="env_form_cont_prov">
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <!-- copiado hasta aqui -->
                                </div>

                                <div id="accordion-boddy-prov">
                                </div><!-- accordion boddy-->
                            </div>
                        </div>

                    </div>


                </div>
                <div class="modal-footer">


                    <button type="button" id="cerrar-modal-per-cont" class="btn btn-secondary" data-bs-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- ////////// -->