<!-- Tabla de insumos -->

<div class="row">
  <div class="ms-4 col-md-5">
    <h1>Registro detalle Receta</h1>
  </div>
  <div class="mt-3 col-md-4">
    <h4>Lista Insumos Agregados</h4>
  </div>

</div>
<div class="row border p-3 m-2">
  <!-- Lista de insumos -->
  <div class="col-md-2 mt-3" style='height: 400px; width: 20%'>
    <div class="row" style='height: 100%;  overflow: auto;'>
      <div class="list-group listado" id="list_ins">
        <li class="list-group-item list-group-item-primary text-center">Insumos <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#reg_ins"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
        <!-- Input para Buscar insumos -->
        <div class="input-group">
          <div class="input-group-prepend">
            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
          <input type="text" class="form-control buscar-insumo" placeholder="Buscar insumo" aria-label="" aria-describedby="basic-addon1">
        </div>
        <!-- ////////////// -->
      </div>
    </div>

  </div>
  <!-- /////////////////////////////// -->
  <!-- Tabla de insumos agregados -->
  <div class="col-md-9 ms-2">
    <div class="row table-responsive">
      <table class="table table-hover" id="tab_rec">
        <thead>
          <tr>
            <th class="text-center">
              Cod Producto
            </th>
            <th class="text-center">
              Insumo
            </th>
            <th class="text-center">
              Cantidad
            </th>
            <th class="text-center">
              Unidad
            </th>
            <th class="text-center">
              Costo Insumo Bs.
            </th>
            <th class="text-center">
              Precio Final
            </th>
            <th class="text-center">

            </th>
          </tr>
        </thead>
        <tbody class="tbody-det text-center align-middle">

        </tbody>
      </table>
    </div>
    <div class="row">
      <div class="offset-md-5">
        <button type="button" id="btn-det-rec" class="btn btn-success btn-block" style='width:auto'>Registrar Insumos</button>
      </div>

    </div>
  </div>
  <!-- <div class="container col-3 offset-md-2">
        <ul class="list-group " id="list_ins_agr">
        </ul>
    </div> -->

</div>

<!-- /////////////////////////////// -->
<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade" id="reg_ins" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Registrar Insumo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <input type="hidden" class="form-control" id="id_insumo">
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre Insumo:</label>
            <input type="text" class="form-control" id="nom_insumo">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Unidad de Medida</label>
            <input type="text" class="form-control" id="und_insumo">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Costo Insumo:</label>
            <input type="text" class="form-control" id="cost_insumo">
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Fecha Del Precio</label>
            <input type="date" class="form-control" id="fec_insumo">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button id='nuevo_ins' type="button" class="btn btn-primary" onclick="insert_insumo()">Nuevo Insumo</button>
        <!-- <button id='modificar_ins' type="button" class="btn btn-primary" onclick="modificar_insumo()">Modificar insumo</button> -->
        <button type="button" id="cerrar-ins-det" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- ////////// -->