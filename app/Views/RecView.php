<!-- PILL RECETA -->

<div class="row">
    <div class="offset-md-5">
        <h1 class="mt-4">Receta</h1>
    </div>
</div>
<div class="container mt-4">
    <div class="row clearfix">
        <div class="col-md-12 column">
            <table class="table table-striped table-bordered table-sm" id="tab_rec" cellspacing="0" width="100%" data-toolbar=".toolbar" data-height="400" data-virtual-scroll="true" data-show-columns="true">
                <thead>
                    <tr>
                        <th class="text-center">
                            Nombre Receta
                        </th>
                        <th class="text-center">
                            Descripcion Receta
                        </th>
                        <th class="text-center">
                            Costo Total Receta
                        </th>
                        <th class="text-center">
                            Fecha de Creacion
                        </th>

                    </tr>
                </thead>
                <tbody class="tbody-rec">

                </tbody>
            </table>

        </div>
    </div>
</div>

<!-- ////////////////////// -->



<!-- MODAL PARA INSERTAR Y EDITAR UN INSUMO -->
<div class="modal fade" id="ModalReceta" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Registrar Receta</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form>
                    <!-- <input type="text" class="form-control" id="id_ins"> -->
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Nombre Receta:</label>
                        <input type="text" class="form-control" id="nom_rec">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Descripcion Receta</label>
                        <input type="text" class="form-control" id="desc_rec">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Unidad</label>
                        <input type="text" class="form-control" id="unidad_rec">
                    </div>
                    <!-- <div class="form-group"> -->
                    <!-- <label for="message-text" class="col-form-label">Costo Total Receta:</label> -->
                    <input type="hidden" class="form-control" id="cost_total_rec">
                    <!-- </div> -->
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Fecha De Creacion</label>
                        <input type="date" class="form-control" id="fecha_rec">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="insertar_receta()">Crear Receta</button>
                <!-- <button id='modificar_ins' type="button" class="btn btn-primary" onclick="modificar_insumo()">Modificar insumo</button> -->
                <button type="button" id="cerrar-modal-rec" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- ////////// -->
<!-- MODAL MODIFICAR RECETA -->
<div class="modal fade bd-example-modal-lg" id="modal_mod_rec" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modificar Receta</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form id="modal_mod_rec_form">
                    <input type="hidden" name="id_rec_mod" id="id_rec_mod">
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="recipient-name" class="col-form-label">Nombre Receta:</label>
                            <input type="text" class="form-control" id="nom_rec_mod" name="nom_rec_mod">
                        </div>
                        <div class="form-group offset-md-1 col-md-5">
                            <label for="message-text" class="col-form-label">Descripcion Receta</label>
                            <input type="text" class="form-control" id="desc_rec_mod" name="desc_rec_mod">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="message-text" class="col-form-label">Costo Total Receta:</label>
                            <input type="text" class="form-control" id="cost_total_rec_mod" name="cost_total_rec_mod">
                        </div>
                        <div class="form-group offset-md-1 col-md-5">
                            <label for="message-text" class="col-form-label">Unidad</label>
                            <input type="text" class="form-control" id="unidad_rec_mod" name="unidad_rec_mod">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-5">
                            <label for="message-text" class="col-form-label">Fecha De Creacion</label>
                            <input type="date" class="form-control" id="fecha_rec_mod" name="fecha_rec_mod">
                        </div>
                    </div>
                    <div class="row my-4">
                        <div class="offset-md-4 col-md-3">
                            <input type="submit" value="Enviar Formulario" class="btn btn-success">
                        </div>
                    </div>
                </form>
                <div class="row mx-auto">
                    <div class="table-responsive">
                        <table class=" my-custom-scrollbar table-wrapper-scroll-y table table-striped table-bordered offset-md-0 col-md-10" id="table-det-rec-mod">
                            <thead>
                                <tr>
                                    <th>
                                        Insumo
                                    </th>
                                    <th>
                                        Cantidad
                                    </th>
                                    <th>
                                        Unidad
                                    </th>
                                    <th>
                                        Costo Insumo Bs.
                                    </th>
                                    <th>
                                        Precio Final
                                    </th>
                                    <th>

                                    </th>
                                    <th>

                                    </th>
                                </tr>
                            </thead>
                            <tbody id="table-det-rec-mod-body">

                            </tbody>

                        </table>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>