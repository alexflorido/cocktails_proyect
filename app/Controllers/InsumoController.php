<?php namespace App\Controllers;

use App\Models\InsumoModel;

class InsumoController extends BaseController
{
	public function get_insumo()
	{
        $InsumoModel = new InsumoModel($db);
        $insumo = $InsumoModel->findAll();
        // $arreglo["data"][]= $insumo;
        echo json_encode($insumo);

		
	}

	//--------------------------------------------------------------------
	public function create_insumo()
	{
		
    }
    public function delete_insumo()
    {
        $request = \Config\Services::request();
        $InsumoModel = new InsumoModel($db);
        $id = $request->getPostGet('id');
        // $db->query('');
        
        // var_dump($id);
        $InsumoModel->where('id_ins', $id)->delete();
        echo json_encode('Se elimino el producto');

    }
    public function insert_insumo()
    {
        $request = \Config\Services::request();
        $InsumoModel = new InsumoModel($db);
        $data = [ 
            'nom_ins'=>$request->getPostGet('nom_ins'),
            'unidad_ins'=>$request->getPostGet('unidad_ins'),
            'cost_ins'=>$request->getPostGet('cost_ins'),
            'fecha_actu_ins'=>$request->getPostGet('fec_ins')

        ];
        $InsumoModel->insert($data);


    }
    public function update_insumo()
    {
        $request = \Config\Services::request();
        $InsumoModel = new InsumoModel($db);
        $id = $request->getPostGet('id_ins');
        $data = [ 
            'nom_ins'=>$request->getPostGet('nom_ins'),
            'unidad_ins'=>$request->getPostGet('unidad_ins'),
            'cost_ins'=>$request->getPostGet('cost_ins'),
            'fecha_actu_ins'=>$request->getPostGet('fec_ins')

        ];
        $InsumoModel->update($id,$data);


    }

}