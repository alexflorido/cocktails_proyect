<?php

namespace App\Controllers;

use App\Models\ClienteModel;


class ClienteController extends BaseController
{
    public function get_cliente()
    {

        $ClienteModel = new ClienteModel();

        $cliente = $ClienteModel->findAll();
        echo json_encode($cliente);
    }
    public function insert_cliente()
    {
        $request = \Config\Services::request();
        $Cliente = new ClienteModel($db);
        $nom_cli = $request->getPostGet('nom_cli');
        $nit_cli = $request->getPostGet('nit_cli');
        $tel_cli = $request->getPostGet('tel_cli');
        $email_cli = $request->getPostGet('email_cli');

        $rules = [
            'nom_cli' => ['label' => 'Nombre Cliete', 'rules' => 'required|alpha_numeric_punct'],
            'nit_cli' =>  ['label' => 'NIT Cliente', 'rules' => 'required|integer'],
            'tel_cli' =>  ['label' => 'Telefono', 'rules' => 'required|integer'],
            'email_cli' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],

        ];

        $data = [
            'nom_cli' => $nom_cli,
            "nit_cli" => $nit_cli,
            "tel_cli" => $tel_cli,
            "email_cli" => $email_cli,

        ];
        if ($this->validate($rules)) {




            $Cliente->insert($data);

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function modificar_cliente()
    {
        $request = \Config\Services::request();
        $Cliente = new ClienteModel($db);
        $id_cli = $request->getPostGet('id_cli');
        $nom_cli = $request->getPostGet('nom_cli');
        $nit_cli = $request->getPostGet('nit_cli');
        $tel_cli = $request->getPostGet('tel_cli');
        $email_cli = $request->getPostGet('email_cli');

        $rules = [
            'nom_cli' => ['label' => 'Nombre Cliete', 'rules' => 'required|alpha_numeric_punct'],
            'nit_cli' =>  ['label' => 'NIT Cliente', 'rules' => 'required|integer'],
            'tel_cli' =>  ['label' => 'Telefono', 'rules' => 'required|integer'],
            'email_cli' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],

        ];


        if ($this->validate($rules)) {;


            $data = [
                'nom_cli' => $nom_cli,
                "nit_cli" => $nit_cli,
                "tel_cli" => $tel_cli,
                "email_cli" => $email_cli,

            ];

            $Cliente->update($id_cli, $data);

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function delete_Cliente()
    {
        $request = \Config\Services::request();
        $Cliente = new ClienteModel($db);
        $id = $request->getPostGet('id_cli');
        // $db->query('');

        // var_dump($id);
        $Cliente->where('id_cli', $id)->delete();
        echo json_encode('Se elimino el Cliente');
    }
}
