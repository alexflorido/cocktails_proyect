<?php

namespace App\Controllers;

use App\Models\PersonaContactoModel;


class PersonaContactoController extends BaseController
{
    public function get_persona_contacto()
    {
        $request = \Config\Services::request();
        $db      = \Config\Database::connect();
        $Persona_contacto = new PersonaContactoModel();
        $id_cli = $request->getPostGet('id_cli');

        // $query = $db->query("Select p.*, c.id_cli from persona_contacto as p INNER JOIN 
        // INNER JOIN cliente as c on p.id_cli = c.id_cli where p.id_cli = ".$id_cli);
        $query = $db->query("Select p.* from persona_contacto as p where p.id_cli = ".$id_cli);
        $result = $query->getResult();
        // print_r($result);
        echo json_encode($result);
    }
    public function insert_persona_contacto()
    {
        $request = \Config\Services::request();
        $Persona_contacto = new PersonaContactoModel($db);
        $id_cli = $request->getPostGet('id_cli');
        $nom_per_cont = $request->getPostGet('nom_per_cont');
        $app_per_cont = $request->getPostGet('app_per_cont');
        $tel_per_cont = $request->getPostGet('tel_per_cont');
        $email_per_cont = $request->getPostGet('email_per_cont');

        $rules = [
            'nom_per_cont' => ['label' => 'Nombre', 'rules' => 'required|alpha_numeric_punct'],
            'app_per_cont' =>  ['label' => 'Apellido', 'rules' => 'required|alpha_numeric_punct'],
            'tel_per_cont' =>  ['label' => 'Telefono', 'rules' => 'required|integer'],
            'email_per_cont' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],

        ];

        $data = [
            'id_cli' => $id_cli,
            'nom_per_cont' => $nom_per_cont,
            "app_per_cont" => $app_per_cont,
            "tel_per_cont" => $tel_per_cont,
            "email_per_cont" => $email_per_cont,

        ];
        if ($this->validate($rules)) {




            $Persona_contacto->insert($data);
            

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function modificar_persona_contacto()
    {
        $request = \Config\Services::request();
        $Persona_contacto = new PersonaContactoModel($db);
        $id_per_cont = $request->getPostGet('id_per_cont');
        $id_cli = $request->getPostGet('id_cli');
        $nom_per_cont = $request->getPostGet('nom_per_cont');
        $app_per_cont = $request->getPostGet('app_per_cont');
        $tel_per_cont = $request->getPostGet('tel_per_cont');
        $email_per_cont = $request->getPostGet('email_per_cont');

        $rules = [
            'nom_per_cont' => ['label' => 'Nombre', 'rules' => 'required|alpha_numeric_punct'],
            'app_per_cont' =>  ['label' => 'Apellido', 'rules' => 'required|alpha_numeric_punct'],
            'tel_per_cont' =>  ['label' => 'Telefono', 'rules' => 'required|integer'],
            'email_per_cont' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],

        ];

        $data = [
            'id_cli' => $id_cli,
            'nom_per_cont' => $nom_per_cont,
            "app_per_cont" => $app_per_cont,
            "tel_per_cont" => $tel_per_cont,
            "email_per_cont" => $email_per_cont,

        ];
        if ($this->validate($rules)) {




            $Persona_contacto->update($id_per_cont,$data);
            

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function delete_eliminar_persona_contacto()
    {
        $request = \Config\Services::request();
        $Persona_contacto = new PersonaContactoModel($db);
        $id_per_cont = $request->getPostGet('id_per_cont');
        // $db->query('');

        // var_dump($id);
        $Persona_contacto->where('id_per_cont', $id_per_cont)->delete();
        echo json_encode('Se elimino el Cliente');
    }
}