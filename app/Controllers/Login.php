<?php

namespace App\Controllers;

use App\Models\PrivGroupEleModel;
use App\Models\PrivGroupModel;
use App\Models\UserModel;
use CodeIgniter\Controller;

class Login extends BaseController
{
    public function index()
    {

        helper('form');

        return view("login_vista");
    }

    //--------------------------------------------------------------------
    public function log_users()
    {
        helper('form');
        $request = \Config\Services::request();
        $UserModel = new UserModel($db);
        $priv_grp_model = new PrivGroupEleModel($db);
        $session = \Config\Services::session();


        $email = $request->getPostGet('email');
        $clv = $request->getPostGet('clv');
        $user = $UserModel->where('email_usu', $email)->where('clv_usu', $clv)->findAll();
        if ($user != null) {

            foreach ($user as $key => $value) {
                $data_session = [
                    'username' => $value['nom_usu'],
                    'email' => $value['email_usu'],
                    'picture' => $value['foto_usu']
                ];
                $session->set($data_session);
                $data = [
                    'id_grp' => $value['id_grp'],
                ];

                setcookie('user', $value['id_usu']);
                setcookie('name', $value['nom_usu']);
                setcookie('id_grp', $value['id_grp']);
                $id_usu = $value['id_usu'];
                $fec_fin = $value['fec_fin'];
            }
            if (strtotime($fec_fin) > strtotime(date('Y-m-d'))) {
                // $privilegios = $priv_grp_model->where('id_grp', $id_grp)->findAll();
                // // print_r($privilegios);
                // $data = [
                //     'id_elem' => [],
                //     'crud_code' => [],
                //     'nom_ele' => []

                // ];
                // foreach ($privilegios as $key => $value) {
                //     $array_code = str_split($value['crud_cod']);
                //     // echo($value['id_ele']);
                //     array_push($data['id_elem'], $value['id_ele']);
                //     array_push($data['crud_code'], $array_code);
                //     array_push($data['nom_ele'], $value['nom_ele']);
                // }
                // print_r($data);
                $ultimacon = [
                    'ult_con' => date("Y-m-d")
                ];

                $UserModel->update($id_usu, $ultimacon);
                // echo (strtotime($fec_fin) . ' ' . strtotime(date('Y-m-d')));
                return view('MainMenuView', $data);
            } else {
                $user['validation'] = "El tiempo de validez del usuario a vencido favor ponerse en contacto con un Administrador";
                return view("login_vista", $user);
            }
        } else {
            $user['validation'] = 'El usuario o contraseña es incorrecto';
            return view("login_vista", $user);
        };
    }
    public function cerrar_sesion()
    {

        $session = \Config\Services::session();
        $session->destroy();
        json_encode('1');
    }
    public function new_evento()
    {
        helper('form');
        return view('CreateEventoView.php');
    }
}
