<?php

namespace App\Controllers;

use App\Models\DetalleProductoModel;

class DetalleProdController extends BaseController
{
    public function insert_detalle()
    {
        $request = \Config\Services::request();
        $DetProdModel = new DetalleProductoModel($db);
        $data = [
            'id_prod' => $request->getPostGet('id_prod'),
            'id_ins' => $request->getPostGet('id_ins'),
            'id_rec' => $request->getPostGet('id_rec'),
            'cantidad_prod' => $request->getPostGet('cantidad_prod'),
            'costo_recurso_prod' => $request->getPostGet('costo_recurso_prod'),
            'costo_total' => $request->getPostGet('costo_total'),
            'precio_total_detProd' => $request->getPostGet('precio_total_detProd')


        ];
        try {
            $DetProdModel->insert($data);
        } catch (\Exception $e) {
            $r = $e->getMessage();
            echo json_encode($r);
        }
    }
    public function update_det_prod()
    {
        $request = \Config\Services::request();
        $DetRecModel = new DetalleProductoModel($db);
        $id_det_prod = $request->getPostGet('id_det_prod');
        $data = [
            'cantidad_prod' => $request->getPostGet('cantidad_prod'),
            'precio_total_detProd' => $request->getPostGet('precio_total_detProd'),
        ];
        $DetRecModel->update($id_det_prod, $data);
    }
    public function get_det_prod()
    {
        $request = \Config\Services::request();
        $db      = \Config\Database::connect();
        $id = $request->getPostGet('id_prod');
        $query = $db->query('SELECT i.* , r.*,dp.* FROM detalle_prod as dp INNER JOIN insumo as i on dp.id_ins = i.id_ins INNER JOIN receta as r on dp.id_rec = r.id_rec  where dp.id_prod =' . $id . ' and dp.deleted_at is NULL');
        $result = $query->getResult();
        echo json_encode($result);
    }
    public function delete_det_prod()
    {
        $request = \Config\Services::request();
        $DetProdModel = new DetalleProductoModel($db);
        $id = $request->getPostGet('id_det_prod');
        $DetProdModel->delete($id);
        echo json_encode('1');
    }
}
