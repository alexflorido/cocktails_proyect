<?php

namespace App\Controllers;

use App\Models\PersonalFijoModel;
use App\Models\CargoModel;
use App\Models\CategoriaModel;
use App\Models\PersonalModel;

class PersonalFijoController extends BaseController
{
    public function get_ev_personal()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $year = $request->getPostGet('year');
        $id_per = $request->getPostGet('id_per');
        $group = $db->query('SELECT dtp.total_detalle,ELT(MONTH(p.fecha_eve_prof), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre") mes,EXTRACT(YEAR FROM p.fecha_eve_prof) year,per.id_per, cp.nom_cargo, CONCAT(per.nom_per," ",per.app_per," ",per.apm_per) nom_per , per.ci_per, e.nom_eve, e.fecha_eve, MONTH(e.fecha_eve) numero_mes FROM detalle_per_prof as dtp INNER JOIN proforma p on p.id_prof = dtp.id_prof INNER JOIN personal per on per.id_per = dtp.id_prod_prof INNER JOIN evento e on e.id_eve=p.id_eve INNER JOIN cargo_personal cp on cp.id_cargo_per = per.id_cargo_per WHERE p.estado_prof = 1 and dtp.tipo_prod_prof = 0 and per.id_per = ' . $id_per . ' ORDER BY numero_mes;')->getResultArray();
        echo json_encode($group);
    }
    public function get_personal()
    {
        $PersonalFijo = new PersonalFijoModel($db);
        $personal = $PersonalFijo->findAll();
        // $arreglo["data"][]= $insumo;
        echo json_encode($personal);
    }
    public function get_personal_activo()
    {
        $PersonalFijo = new PersonalFijoModel($db);
        $personal = $PersonalFijo->where('estado', 'Activo')->where('on_event', 0)->findAll();
        // $arreglo["data"][]= $insumo;
        echo json_encode($personal);
    }
    public function get_cargo()
    {
        $CargoPersonal = new CargoModel($bd);
        $cargo = $CargoPersonal->findAll();
        echo json_encode($cargo);
    }
    public function delete_personal()
    {
        $request = \Config\Services::request();
        $PersonalFijo = new PersonalFijoModel($db);
        $id = $request->getPostGet('id_per');
        // $db->query('');

        // var_dump($id);
        $PersonalFijo->where('id_per', $id)->delete();
        echo json_encode('Se elimino el producto');
    }
    public function cambiar_estado()
    {
        $request = \Config\Services::request();
        $PersonalFijo = new PersonalFijoModel($db);
        $id = $request->getPostGet('id_per');
        $data = [
            'estado' => $request->getPostGet('estado')
        ];
        $PersonalFijo->update($id, $data);
    }
    public function get_categoria()
    {
        $CategoriaPersonal = new CategoriaModel($bd);
        $categoria = $CategoriaPersonal->findAll();
        echo json_encode($categoria);
    }
    public function insert_personal()
    {
        $request = \Config\Services::request();
        $PersonalFijo = new PersonalModel($db);
        $nombre = $request->getPostGet('nom_per');
        $app_per = $request->getPostGet('app_per');
        $apm_per = $request->getPostGet('apm_per');
        $id_cargo_per = $request->getPostGet('id_cargo_per');
        $ci_per = $request->getPostGet('ci_per');
        $id_cat_per = $request->getPostGet('id_cat_per');
        $unidad_per = $request->getPostGet('unidad_per');
        $cost_per = $request->getPostGet('cost_per');
        $telf_per = $request->getPostGet('telf_per');
        $fecha_ingreso = $request->getPostGet('fecha_ingreso');
        $email_per = $request->getPostGet('email_per');
        $clv_personal = $request->getPostGet('clv_personal');
        $estado = "Activo";
        $ciudad = $request->getPostGet('ciudad');



        $rules = [
            'nom_per' => [
                'label' => 'Nombre', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Nombre es requerido', 'alpha_numeric_space' => 'El campo Nombre no puede tener simbolos especiales',],
            ],
            'app_per' =>  ['label' => 'Apellido Paterno', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Apellido Paterno es requerido', 'alpha_numeric_space' => 'El campo Apellido Paterno no puede tener simbolos especiales',]],
            'apm_per' =>  ['label' => 'Apellido Materno', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Apellido Materno es requerido', 'alpha_numeric_space' => 'El campo Apellido Materno no puede tener simbolos especiales',]],
            'ci_per' =>  ['label' => 'Cedula de Identidad', 'rules' => 'required|integer', 'errors' => ['required' => 'El campo Cedula de Identidad es requerido', 'integer' => 'El campo Cedula de Identidad solo puede contener números enteros',]],
            'unidad_per' =>  ['label' => 'Unidad', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Unidad es requerido', 'alpha_numeric_space' => 'El campo Unidad no puede tener simbolos especiales',]],
            'cost_per' =>  ['label' => 'Costo', 'rules' => 'required|numeric', 'errors' => ['required' => 'El campo Costo es requerido', 'numeric' => 'El campo Costo solo puede contener numeros',]],
            'telf_per' =>  ['label' => 'Telefono', 'rules' => 'required|integer|max_length[10]|min_length[7]', 'errors' => ['required' => 'El campo Telefono es requerido', 'integer' => 'El campo Telefono solo puede contener números enteros', 'max_length' => 'El campo Telefono solo puede tener 10 números como maximo', 'min_length' => 'El campo Telefono solo puede tener 7 números como minimo']],
            'email_per' =>  ['label' => 'Correo', 'rules' => 'required|valid_email', 'errors' => ['required' => 'El campo Correo es requerido', 'valid_email' => 'El campo Correo debe contener un correo válido',]],
            'clv_personal' =>  ['label' => 'Clave de Acceso', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Clave de Acceso es requerido', 'alpha_numeric_space' => 'El campo Clave de Acceso no puede tener simbolos especiales',]],
            'ciudad' =>  ['label' => 'Ciudad', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Ciudad es requerido', 'alpha_numeric_space' => 'El campo Ciudad no puede tener simbolos especiales',]],
            'img_per' => ['label' => 'Imagen', 'rules' => 'uploaded[img_per]|is_image[img_per]', 'errors' => ['uploaded' => 'Debe subir una Imagen', 'is_image' => 'El archivo a subir debe ser una Imagen',]]
        ];
        // $message = [
        //     [
        //         'nom_per' => [
        //             'required' => "El Nombre es un Campo Requerido"
        //         ]
        //     ]
        // ];

        if ($this->validate($rules)) {;

            $file = $this->request->getFile('img_per');
            // echo($file->getName());
            $imgName = $file->getRandomName();
            if ($file->isValid()) {
                $file->move('./assets/img/foto_personal', $imgName);
                $data = [
                    'nom_per' => $nombre,
                    "app_per" => $app_per,
                    "apm_per" => $apm_per,
                    "id_cargo_per" => $id_cargo_per,
                    "ci_per" => $ci_per,
                    "id_cat_per" => $id_cat_per,
                    "unidad_per" => $unidad_per,
                    "cost_per" => $cost_per,
                    "telf_per" => $telf_per,
                    "fec_ingreso_personal" => $fecha_ingreso,
                    "email_per" => $email_per,
                    "clv_personal" => $clv_personal,
                    "estado" => $estado,
                    "ciudad" => $ciudad,
                    "img_per" => $imgName
                ];


                $PersonalFijo->insert($data);

                echo json_encode('1');
            }
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function modificar_img_personal()
    {
        $request = \Config\Services::request();
        $PersonalFijo = new PersonalModel($db);
        $id_per = $request->getPostGet('id_per');
        $file = $this->request->getFile('img_per_mod');
        // echo($file->getName());
        $imgName = $file->getRandomName();
        if ($file->isValid()) {
            $file->move('./assets/img/foto_personal', $imgName);
            $data = [

                "img_per" => $imgName
            ];


            $PersonalFijo->update($id_per, $data);

            echo json_encode('1');
        }
    }
    public function modificar_personal()
    {
        $request = \Config\Services::request();
        $PersonalFijo = new PersonalModel($db);
        $id_per = $request->getPostGet('id_per');
        $nombre = $request->getPostGet('nom_per');
        $app_per = $request->getPostGet('app_per');
        $apm_per = $request->getPostGet('apm_per');
        $id_cargo_per = $request->getPostGet('id_cargo_per');
        $ci_per = $request->getPostGet('ci_per');
        $id_cat_per = $request->getPostGet('id_cat_per');
        $unidad_per = $request->getPostGet('unidad_per');
        $cost_per = $request->getPostGet('cost_per');
        $telf_per = $request->getPostGet('telf_per');
        $fecha_ingreso = $request->getPostGet('fecha_ingreso');
        $email_per = $request->getPostGet('email_per');
        $clv_personal = $request->getPostGet('clv_personal');
        $estado = "Activo";
        $ciudad = $request->getPostGet('ciudad');



        // $rules = [
        //     'nom_per' => ['label' => 'Nombre', 'rules' => 'required|alpha_numeric_punct'],
        //     'app_per' =>  ['label' => 'Apellido Paterno', 'rules' => 'required|alpha_numeric_punct'],
        //     'apm_per' =>  ['label' => 'Apellido Materno', 'rules' => 'required|alpha_numeric_punct'],
        //     'ci_per' =>  ['label' => 'Cedula de Identidad', 'rules' => 'required|integer'],
        //     'unidad_per' =>  ['label' => 'Unidad', 'rules' => 'required|alpha_numeric_punct'],
        //     'cost_per' =>  ['label' => 'Costo', 'rules' => 'required|numeric'],
        //     'telf_per' =>  ['label' => 'Telefono', 'rules' => 'required|integer|max_length[10]|min_length[7]'],
        //     'email_per' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],
        //     'clv_personal' =>  ['label' => 'Clave de Acceso', 'rules' => 'required|alpha_numeric_punct'],
        //     'ciudad' =>  ['label' => 'Ciudad', 'rules' => 'required|alpha_numeric_punct']
        // ];
        $rules = [
            'nom_per' => [
                'label' => 'Nombre', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Nombre es requerido', 'alpha_numeric_space' => 'El campo Nombre no puede tener simbolos especiales',],
            ],
            'app_per' =>  ['label' => 'Apellido Paterno', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Apellido Paterno es requerido', 'alpha_numeric_space' => 'El campo Apellido Paterno no puede tener simbolos especiales',]],
            'apm_per' =>  ['label' => 'Apellido Materno', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Apellido Materno es requerido', 'alpha_numeric_space' => 'El campo Apellido Materno no puede tener simbolos especiales',]],
            'ci_per' =>  ['label' => 'Cedula de Identidad', 'rules' => 'required|integer', 'errors' => ['required' => 'El campo Cedula de Identidad es requerido', 'integer' => 'El campo Cedula de Identidad solo puede contener números enteros',]],
            'unidad_per' =>  ['label' => 'Unidad', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Unidad es requerido', 'alpha_numeric_space' => 'El campo Unidad no puede tener simbolos especiales',]],
            'cost_per' =>  ['label' => 'Costo', 'rules' => 'required|numeric', 'errors' => ['required' => 'El campo Costo es requerido', 'numeric' => 'El campo Costo solo puede contener numeros',]],
            'telf_per' =>  ['label' => 'Telefono', 'rules' => 'required|integer|max_length[10]|min_length[7]', 'errors' => ['required' => 'El campo Telefono es requerido', 'integer' => 'El campo Telefono solo puede contener números enteros', 'max_length' => 'El campo Telefono solo puede tener 10 números como maximo', 'min_length' => 'El campo Telefono solo puede tener 7 números como minimo']],
            'email_per' =>  ['label' => 'Correo', 'rules' => 'required|valid_email', 'errors' => ['required' => 'El campo Correo es requerido', 'valid_email' => 'El campo Correo debe contener un correo válido',]],
            'clv_personal' =>  ['label' => 'Clave de Acceso', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Clave de Acceso es requerido', 'alpha_numeric_space' => 'El campo Clave de Acceso no puede tener simbolos especiales',]],
            'ciudad' =>  ['label' => 'Ciudad', 'rules' => 'required|alpha_numeric_space', 'errors' => ['required' => 'El campo Ciudad es requerido', 'alpha_numeric_space' => 'El campo Ciudad no puede tener simbolos especiales',]],
        ];

        if ($this->validate($rules)) {;






            $data = [
                'nom_per' => $nombre,
                "app_per" => $app_per,
                "apm_per" => $apm_per,
                "id_cargo_per" => $id_cargo_per,
                "ci_per" => $ci_per,
                "id_cat_per" => $id_cat_per,
                "unidad_per" => $unidad_per,
                "cost_per" => $cost_per,
                "telf_per" => $telf_per,
                "fec_ingreso_personal" => $fecha_ingreso,
                "email_per" => $email_per,
                "clv_personal" => $clv_personal,
                "estado" => $estado,
                "ciudad" => $ciudad
            ];


            $PersonalFijo->update($id_per, $data);

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
}
