<?php

namespace App\Controllers;

use App\Models\ContactoProveedorModel;


class ContactoProveedorController extends BaseController
{
    public function get_persona_contacto()
    {
        $request = \Config\Services::request();
        $db      = \Config\Database::connect();
        $Persona_contacto = new ContactoProveedorModel();
        $id_prov = $request->getPostGet('id_prov');

        // $query = $db->query("Select p.*, c.id_cli from persona_contacto as p INNER JOIN 
        // INNER JOIN cliente as c on p.id_cli = c.id_cli where p.id_cli = ".$id_cli);
        $query = $db->query("Select p.* from contacto_proveedor as p where p.id_prov = ".$id_prov);
        $result = $query->getResult();
        // print_r($result);
        echo json_encode($result);
    }
    public function insert_contacto_proveedor()
    {
        $request = \Config\Services::request();
        $Persona_contacto = new ContactoProveedorModel();
        $id_prov = $request->getPostGet('id_prov');
        $nombre_cont = $request->getPostGet('nombre_cont');
        $email_cont = $request->getPostGet('email_cont');
        $telf_cont = $request->getPostGet('telf_cont');
        $estado = $request->getPostGet('estado');

        $rules = [
            'nombre_cont' => ['label' => 'Nombre', 'rules' => 'required|alpha_numeric_punct'],
            'estado' =>  ['label' => 'Estado', 'rules' => 'required|alpha_numeric_punct'],
            'telf_cont' =>  ['label' => 'Telefono', 'rules' => 'required|integer'],
            'email_cont' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],

        ];

        $data = [
            'id_prov' => $id_prov,
            'nombre_cont' => $nombre_cont,
            "email_cont" => $email_cont,
            "telf_cont" => $telf_cont,
            "estado" => $estado,

        ];
        if ($this->validate($rules)) {




            $Persona_contacto->insert($data);
            

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function modificar_contacto_proveedor()
    {
        $request = \Config\Services::request();
        $Persona_contacto = new ContactoProveedorModel($db);
        $id_cont_prov = $request->getPostGet('id_cont_prov');
        $id_prov = $request->getPostGet('id_prov');
        $nombre_cont = $request->getPostGet('nombre_cont');
        $email_cont = $request->getPostGet('email_cont');
        $telf_cont = $request->getPostGet('telf_cont');
        $estado = $request->getPostGet('estado');

        $rules = [
            'nombre_cont' => ['label' => 'Nombre', 'rules' => 'required|alpha_numeric_punct'],
            'estado' =>  ['label' => 'Estado', 'rules' => 'required|alpha_numeric_punct'],
            'telf_cont' =>  ['label' => 'Telefono', 'rules' => 'required|integer'],
            'email_cont' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],

        ];

        $data = [
            'id_prov' => $id_prov,
            'nombre_cont' => $nombre_cont,
            "email_cont" => $email_cont,
            "telf_cont" => $telf_cont,
            "estado" => $estado
        ];
        if ($this->validate($rules)) {




            $Persona_contacto->update($id_cont_prov,$data);
            

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function delete_contacto_proveedor()
    {
        $request = \Config\Services::request();
        $Persona_contacto = new ContactoProveedorModel($db);
        $id_cont_prov = $request->getPostGet('id_cont_prov');
        // $db->query('');

        // var_dump($id);
        $Persona_contacto->where('id_cont_prov', $id_cont_prov)->delete();
        echo json_encode('Se elimino el Cliente');
    }
}