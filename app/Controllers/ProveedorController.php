<?php

namespace App\Controllers;

use App\Models\ProveedorModel;


class ProveedorController extends BaseController
{
    public function get_proveedor()
    {

        $ProveedorModel = new ProveedorModel();

        $proveedor = $ProveedorModel->findAll();
        echo json_encode($proveedor);
    }
    public function insert_proveedor()
    {
        $request = \Config\Services::request();
        $Proveedor = new ProveedorModel($db);
        $nom_prov = $request->getPostGet('nom_prov');
        $desc_prov = $request->getPostGet('desc_prov');
        $nit_prov = $request->getPostGet('nit_prov');
        $raz_soc = $request->getPostGet('raz_soc');

        $rules = [
            'nom_prov' => ['label' => 'Nombre Proveedor', 'rules' => 'required|alpha_numeric_punct'],
            'desc_prov' =>  ['label' => 'Descripcion', 'rules' => 'required|alpha_numeric_punct'],
            'nit_prov' =>  ['label' => 'Nit', 'rules' => 'required|integer'],
            'raz_soc' =>  ['label' => 'Razon Social', 'rules' => 'required|alpha_numeric_punct'],

        ];

        $data = [
            'nom_prov' => $nom_prov,
            "desc_prov" => $desc_prov,
            "nit_prov" => $nit_prov,
            "raz_soc" => $raz_soc,

        ];
        if ($this->validate($rules)) {




            $Proveedor->insert($data);

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function modificar_proveedor()
    {
        $request = \Config\Services::request();
        $Proveedor = new ProveedorModel($db);
        $id_prov = $request->getPostGet('id_prov');
        $nom_prov = $request->getPostGet('nom_prov');
        $desc_prov = $request->getPostGet('desc_prov');
        $nit_prov = $request->getPostGet('nit_prov');
        $raz_soc = $request->getPostGet('raz_soc');

        $rules = [
            'nom_prov' => ['label' => 'Nombre Proveedor', 'rules' => 'required|alpha_numeric_punct'],
            'desc_prov' =>  ['label' => 'Descripcion', 'rules' => 'required|alpha_numeric_punct'],
            'nit_prov' =>  ['label' => 'Nit', 'rules' => 'required|integer'],
            'raz_soc' =>  ['label' => 'Razon Social', 'rules' => 'required|alpha_numeric_punct'],
        ];


        if ($this->validate($rules)) {;


            $data = [
                'nom_prov' => $nom_prov,
                "desc_prov" => $desc_prov,
                "nit_prov" => $nit_prov,
                "raz_soc" => $raz_soc,

            ];

            $Proveedor->update($id_prov, $data);

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function delete_proveedor()
    {
        $request = \Config\Services::request();
        $Proveedor = new ProveedorModel($db);
        $id = $request->getPostGet('id_prov');
        // $db->query('');

        // var_dump($id);
        $Proveedor->where('id_prov', $id)->delete();
        echo json_encode('Se elimino el proveedor');
    }
}
