<?php

namespace App\Controllers;

use App\Models\DetalleRecetaModel;

class DetalleRecController extends BaseController
{
    public function insert_detalle()
    {
        $request = \Config\Services::request();
        $DetRecModel = new DetalleRecetaModel($db);
        $data = [
            'id_rec' => $request->getPostGet('id_rec'),
            'id_ins' => $request->getPostGet('id_ins'),
            'cant_det_rec' => $request->getPostGet('cant_det_rec'),
            'cost_ins_det_rec' => $request->getPostGet('cost_ins_det_rec'),
            'prec_final_det_rec' => $request->getPostGet('prec_final_det_rec'),
            'fecha_ins_det_rec' => date("Y-m-d H:i:s")

        ];
        $DetRecModel->insert($data);
    }
    public function update_detalle()
    {
        $request = \Config\Services::request();
        $DetRecModel = new DetalleRecetaModel($db);
        $id_det_rec = $request->getPostGet('id_det_rec');
        $data = [
            'cant_det_rec' => $request->getPostGet('cant_det_rec'),
            'prec_final_det_rec' => $request->getPostGet('prec_final_det_rec'),
        ];
        $DetRecModel->update($id_det_rec, $data);
    }
    public function delete_detalle()
    {
        $request = \Config\Services::request();
        $DetRecModel = new DetalleRecetaModel($db);
        $id_det_rec = $request->getPostGet('id_det_rec');

        $DetRecModel->delete($id_det_rec);
    }
    public function get_det_rec()
    {
        $request = \Config\Services::request();
        $db      = \Config\Database::connect();
        $id = $request->getPostGet('id_rec');
        $query = $db->query('SELECT i.* ,dr.* FROM detalle_receta as dr INNER JOIN insumo as i on dr.id_ins = i.id_ins where dr.id_rec =' . $id);
        $result = $query->getResult();
        echo json_encode($result);
    }
}
