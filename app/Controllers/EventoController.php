<?php

namespace App\Controllers;

use \Mpdf\Mpdf;
use App\Models\EventoViewModel;
use App\Models\EventoModel;
use Mpdf\HTMLParserMode;
use App\Models\ProformaModel;


class EventoController extends BaseController
{
  public function list_to_shop()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $date_to_shop = $request->getPostGet('date_to_shop');
    $id_usu = $request->getPostGet('id_usu');
    $nom_usu = $request->getPostGet('nom_usu');
    $id_prof = $request->getPostGet('id_prof');
    $created_time = date('Y-m-d');
    $db->query("INSERT INTO lista_compras (create_time,created_by,date_to_shop,id_prof,id_user) values('" . $created_time . "', '" . $nom_usu . "','" . $date_to_shop . "'," . $id_prof . "," . $id_usu . ")");
    $lastID = $db->query('SELECT * from lista_compras ORDER BY id_list DESC LIMIT 1')->resultID;
    foreach ($lastID as $key => $value) {
      $id_list = $value['id_list'];
    }
    echo json_encode($id_list);
    $db->close();
  }
  public function insert_detalle_listaCompras()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $cant_item = $request->getPostGet('cant_item');
    $cost_item = $request->getPostGet('cost_item');
    $id_list = $request->getPostGet('id_list');
    $item_list = $request->getPostGet('item_list');
    $description_item = $request->getPostGet('description_item');
    $created_time = date('Y-m-d');
    echo "INSERT INTO detalle_lista_compras (create_time,id_list, item_list, description_item, cant_item, cost_item) values('" . $created_time . "', " . $id_list . ",'" . $item_list . "','" . $description_item . "'," . $cant_item . "," . $cost_item . ")";
    $db->query("INSERT INTO detalle_lista_compras (create_time,id_list, item_list, description_item, cant_item, cost_item) values('" . $created_time . "', " . $id_list . ",'" . $item_list . "','" . $description_item . "'," . $cant_item . "," . $cost_item . ")");
  }
  public function insert_asig_lista_personal()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $id_per = $request->getPostGet('id_per');
    $id_list = $request->getPostGet('id_list');
    // echo "INSERT INTO detalle_lista_compras (create_time,id_list, item_list, description_item, cant_item, cost_item) values('" . $created_time . "', " . $id_list . ",'" . $item_list . "','" . $description_item . "'," . $cant_item . "," . $cost_item . ")";
    $db->query("INSERT INTO personal_listacompras (id_list, id_per, estado_tarea) values(" . $id_list . ", " . $id_per . " ,0)");
  }

  public function get_evento_view()
  {

    $EventoView = new EventoViewModel();

    $evento = $EventoView->findAll();
    echo json_encode($evento);
  }
  public function get_det_list_prod()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $id_prod = $request->getPostGet('id_prod');
    $result = $db->query("SELECT dp.*, i.*, r.* from detalle_prod dp INNER JOIN insumo i on i.id_ins = dp.id_ins INNER JOIN receta r on r.id_rec = dp.id_rec where dp.id_prod = " . $id_prod)->getResultArray();
    echo json_encode($result);
  }
  public function get_insumo_almacen()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $event_date = $request->getPostGet('event_date');

    $result = $db->query("SELECT reg.*, i.*, r.* FROM reg_almacen as reg INNER JOIN insumo i on i.id_ins = reg.id_ins INNER JOIN receta r on r.id_rec = reg.id_rec WHERE STR_TO_DATE('" . $event_date . "', '%Y-%m-%d') BETWEEN STR_TO_DATE(reg.fec_entrada, '%Y-%m-%d') AND STR_TO_DATE(reg.fec_caducidad, '%Y-%m-%d')  ORDER BY reg.fec_caducidad")->getResultArray();
    // echo ("SELECT reg.*, i.*, r.* FROM reg_almacen as reg INNER JOIN insumo i on i.id_ins = reg.id_ins INNER JOIN receta r on r.id_rec = reg.id_rec WHERE STR_TO_DATE(reg.fec_caducidad, '%Y-%m-%d') BETWEEN STR_TO_DATE(reg.fec_entrada, '%Y-%m-%d') AND STR_TO_DATE('" . $event_date . "', '%Y-%m-%d') ORDER BY reg.fec_caducidad");
    echo json_encode($result);
  }
  public function get_proforma_byID()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $id_eve = $request->getPostGet('id_eve');
    $result = $db->query("Select * from proforma where id_eve = " . $id_eve)->getResultArray();
    echo json_encode($result);
  }
  public function create_event()
  {
    return view('CreateEventoView.php');
  }
  public function create_proform()
  {
    $request = \Config\Services::request();
    $EventoView = new EventoModel();

    $id_eve = $request->getPostGet('id_eve');

    $evento = $EventoView->find($id_eve);
    $data['id_eve'] = $evento['id_eve'];
    $data['nom_eve'] = $evento['nom_eve'];
    $data['fecha_eve'] = $evento['fecha_eve'];
    $data['is_new'] = 0;
    // echo $data['id_eve'];

    return view('CreateEventoView.php', $data);
  }
  public function update_total_values_prof()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $proforma = new ProformaModel($db);
    $sub_total_prof = $request->getPostGet('sub_total_prof');
    $iva_prof = $request->getPostGet('iva_prof');
    $total_general_prof = $request->getPostGet('total_general_prof');
    $id_prof = $request->getPostGet('id_prof');
    $data = [
      "sub_total_prof" => $sub_total_prof,
      "iva_prof" => $iva_prof,
      "total_general_prof" => $total_general_prof
    ];

    $proforma->update($id_prof, $data);
    echo json_encode("updatedField");
  }
  public function update_total_values_event()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $Evento = new EventoModel($db);

    $total_facturado = $request->getPostGet('total_facturado');
    $id_eve = $request->getPostGet('id_eve');
    $data = [
      "total_facturado" => $total_facturado
    ];

    $Evento->update($id_eve, $data);
    echo json_encode("Evento Modificado");
  }
  public function insert_event()
  {

    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $Evento = new EventoModel($db);
    $proforma = new ProformaModel($db);
    $nom_eve = $request->getPostGet('nom_eve');
    $id_per_cont = $request->getPostGet('id_per_cont');
    $id_cli = $request->getPostGet('id_cli');
    $fecha_eve = $request->getPostGet('fecha_eve');
    $coordinates_lat = $request->getPostGet('coordinates_lat');
    $coordinates_lng = $request->getPostGet('coordinates_lng');
    $data = [
      'nom_eve' => $nom_eve,
      'nro_factura' => 0,
      "id_per_cont" => $id_per_cont,
      "id_cli" => $id_cli,
      "fecha_eve" => $fecha_eve,
      "coordinates_lat" => $coordinates_lat,
      "coordinates_lng" => $coordinates_lng,

    ];
    $Evento->insert($data);
    $query = "Select LAST_INSERT_ID()";
    $last_event = $db->query('select * from evento ORDER BY id_eve DESC LIMIT 1')->resultID;
    $last_prof = $db->query('select num_prof from proforma ORDER BY id_prof DESC LIMIT 1')->getResultArray();
    foreach ($last_event as $key => $value) {
      $id_eve = $value['id_eve'];
    }
    foreach ($last_prof as $key => $value) {
      $n_proforma = $value['num_prof'] + 1;
    }


    $Pdata = [
      'num_prof' => $n_proforma,
      'fecha_prof' => date("d/m/Y"),
      'fecha_eve_prof' => $fecha_eve,
      'id_eve' => $id_eve,
      'id_per_cont' => $id_per_cont,
      'estado_prof' => 0
    ];
    $proforma->insert($Pdata);
    $last_prof = $db->query('select * from proforma ORDER BY id_prof DESC LIMIT 1')->getResultArray();
    foreach ($last_prof as $key => $value) {
      $id_prof = $value['id_prof'];
    }
    $toJs = [
      'id_prof' => $id_prof,
      'n_prof' => $n_proforma,
      'id_eve' => $id_eve
    ];


    echo json_encode($toJs);
  }
  public function update_event()
  {

    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $Evento = new EventoModel($db);
    $id_eve = $request->getPostGet('id_eve');
    $nom_eve = $request->getPostGet('nom_eve');
    $nro_factura = $request->getPostGet('nro_factura');
    $total_facturado = $request->getPostGet('total_facturado');
    $fecha_eve = $request->getPostGet('fecha_eve');
    $data = [
      'nom_eve' => $nom_eve,
      'nro_factura' => $nro_factura,
      "total_facturado" => $total_facturado,
      "fecha_eve" => $fecha_eve,

    ];
    $Evento->update($id_eve, $data);


    echo json_encode("Modificado");
  }
  public function insert_proform()
  {

    $request = \Config\Services::request();
    $db = \Config\Database::connect();

    $proforma = new ProformaModel($db);
    $nom_eve = $request->getPostGet('nom_eve');
    $id_per_cont = $request->getPostGet('id_per_cont');
    $id_cli = $request->getPostGet('id_cli');
    $fecha_eve = $request->getPostGet('fecha_eve');
    $id_eve = $request->getPostGet('id_eve');

    $last_prof = $db->query('select num_prof from proforma ORDER BY id_prof DESC LIMIT 1')->getResultArray();

    foreach ($last_prof as $key => $value) {
      $n_proforma = $value['num_prof'] + 1;
    }


    $Pdata = [
      'num_prof' => $n_proforma,
      'fecha_prof' => date("d/m/Y"),
      'fecha_eve_prof' => $fecha_eve,
      'id_eve' => $id_eve,
      'id_per_cont' => $id_per_cont,
      'estado_prof' => 0
    ];
    $proforma->insert($Pdata);
    $last_prof = $db->query('select * from proforma ORDER BY id_prof DESC LIMIT 1')->getResultArray();
    foreach ($last_prof as $key => $value) {
      $id_prof = $value['id_prof'];
    }
    $toJs = [
      'id_prof' => $id_prof,
      'n_prof' => $n_proforma,
      'id_eve' => $id_eve
    ];


    echo json_encode($toJs);
  }
  public function update_proforma_estado()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $proforma = new ProformaModel($db);
    $Evento = new EventoModel($db);
    $estado = $request->getPostGet('estado');
    $id_prof = $request->getPostGet('id_prof');
    $total_general_prof = $request->getPostGet('total_general_prof');
    $id_eve = $request->getPostGet('id_eve');
    $data = [
      'estado_prof' => $estado
    ];
    $proforma->update($id_prof, $data);
    if ($estado == 1) {

      $db->query("UPDATE personal set on_event = 1 where id_per in( SELECT det.id_prod_prof from detalle_per_prof as det where det.tipo_prod_prof = 0 and det.id_prof = " . $id_prof . ")");
      $costo_producto = $db->query('SELECT SUM(p.costo_total_prod * d.cantidad) as costo FROM producto as p INNER JOIN detalle_per_prof as d on p.id_prod = d.id_prod_prof where d.tipo_prod_prof = 1 and d.id_prof =' . $id_prof)->getResultArray();
      $costo_empleado = $db->query('SELECT SUM(p.cost_per * d.cantidad) as costo FROM personal as p INNER JOIN detalle_per_prof as d on p.id_per = d.id_prod_prof where d.tipo_prod_prof = 0 and d.id_prof =' . $id_prof)->getResultArray();
      foreach ($costo_empleado as $key => $value) {
        $costo_emp = $value['costo'];
      }
      foreach ($costo_producto as $key => $value) {
        $costo_prod = $value['costo'];
      }
      $costo_total = $costo_emp + $costo_prod;
      $iva = $total_general_prof * 0.13;
      $IT = $total_general_prof * 0.03;
      $IUE = ($total_general_prof - $iva - $IT) * 0.25;
      $total_imp = $iva + $IT + $IUE;
      $data2 = [
        'cost_eve_total' => round($costo_total, 2),
        'IUE' => round($IUE, 2),
        'IT' => round($IT, 2),
        'IVA' => round($iva, 2),
        'total_imp' => round($total_imp, 2),
        'total_facturado' => round($total_general_prof, 2),
      ];
      echo ($costo_total);
      $Evento->update($id_eve, $data2);
    } else {
      $data2 = [
        'cost_eve_total' => 0,
        'IUE' => 0,
        'IT' => 0,
        'IVA' => 0,
        'total_imp' => 0,
        'total_facturado' => 0,
      ];
      $Evento->update($id_eve, $data2);
      echo ("estado NO Aprobado");
    }
  }
  public function delete_event()
  {
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $Evento = new EventoModel($db);
    $id_eve = $request->getPostGet('id_eve');
    $Evento->delete($id_eve);
  }
  public function insert_per_prof()
  {
    echo "llegue a backend";
    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $id_per = $request->getPostGet('id_per');
    $cantidad_dias = $request->getPostGet('cantidad_dias');
    $costo_det = $request->getPostGet('costo_det');
    $total_detalle = $request->getPostGet('total_detalle');
    $id_prof = $request->getPostGet('id_prof');
    $tipo = $request->getPostGet("tipo_prod_prof");
    echo "insert into detalle_per_prof (id_prod_prof,cantidad,costo_det,total_detalle,id_prof,tipo_prod_prof) values(" . $id_per . "," . $cantidad_dias . "," . $costo_det . "," . $total_detalle . "," . $id_prof . "," . $tipo . ")";
    $query = "insert into detalle_per_prof (id_prod_prof,cantidad,costo_det,total_detalle,id_prof,tipo_prod_prof) values(" . $id_per . "," . $cantidad_dias . "," . $costo_det . "," . $total_detalle . "," . $id_prof . "," . $tipo . ")";
    try {
      $db->query($query);
      echo "Detalle insertado";
    } catch (\Throwable $error) {
      echo json_encode("error");
    }
  }
  public function insert_pdf()
  {


    $request = \Config\Services::request();
    $db = \Config\Database::connect();
    $proforma = new ProformaModel($db);
    $id_prof = $request->getPostGet('id_prof');

    if (!empty($_POST['data'])) {
      $data = base64_decode($_POST['data']);
      //$data = $_POST['data'];

      $fname = $id_prof . "-Proforma-" . date("Y-m-d") . ".pdf"; // name the file
      $file = fopen("./assets/img/documents/" . $fname, 'w'); // open the file path
      fwrite($file, $data); //save data
      fclose($file);
      echo "File saved";
    } else {
      echo "No Data Sent";
    };

    $query = "UPDATE proforma SET pdf_prof = '" . $fname . "' WHERE id_prof =" . $id_prof;
    try {
      $db->query($query);
    } catch (\Throwable $error) {
      echo json_encode("Error al cargar PDF");
    }

    // file_put_contents("./assets/img/documents", $data);




    // $query = "update proforma set pdf_prof =" . $name . " where id_prof =".$id_prof;
    // try {
    //   $db->query($query);
    // } catch (\Throwable $error) {
    //   echo json_encode($error);
    // }
  }
}
