<?php

namespace App\Controllers;

use App\Models\RecetaModel;

class RecetaController extends BaseController
{
    public function get_receta()
    {
        $RecetaModel = new RecetaModel($db);
        $receta = $RecetaModel->findAll();
        echo json_encode($receta);
    }

    //--------------------------------------------------------------------

    public function delete_receta()
    {
        $request = \Config\Services::request();
        $RecetaModel = new RecetaModel($db);
        $id = $request->getPostGet('id');
        // $db->query('');

        // var_dump($id);
        $RecetaModel->where('id_rec', $id)->delete();
        echo json_encode('Se elimino el producto');
    }
    public function update_TotalReceta()
    {
        $request = \Config\Services::request();
        $RecetaModel = new RecetaModel($db);
        $id_rec = $request->getPostGet('id_rec');
        $data = [
            'cost_total_rec' => $request->getPostGet('cost_total_rec'),
        ];
        $RecetaModel->update($id_rec, $data);
        echo json_encode(1);
    }
    public function insert_receta()
    {
        $request = \Config\Services::request();
        $RecetaModel = new RecetaModel($db);
        $data = [
            'nom_rec' => $request->getPostGet('nom_rec'),
            'desc_rec' => $request->getPostGet('desc_rec'),
            'unidad_rec' => $request->getPostGet('unidad_rec'),
            'cost_total_rec' => $request->getPostGet('cost_total_rec'),
            'fecha_rec' => $request->getPostGet('fecha_rec')

        ];
        $RecetaModel->insert($data);
        echo json_encode($RecetaModel->insertID());
    }
    public function update_receta()
    {
        $request = \Config\Services::request();
        $RecetaModel = new RecetaModel($db);
        $id_rec = $request->getPostGet('id_rec_mod');
        $data = [
            'nom_rec' => $request->getPostGet('nom_rec_mod'),
            'desc_rec' => $request->getPostGet('desc_rec_mod'),
            'unidad_rec' => $request->getPostGet('unidad_rec_mod'),
            'cost_total_rec' => $request->getPostGet('cost_total_rec_mod'),
            'fecha_rec' => $request->getPostGet('fecha_rec_mod'),

        ];
        $RecetaModel->update($id_rec, $data);
        echo json_encode(1);
    }
    // public function update_insumo()
    // {
    //     $request = \Config\Services::request();
    //     $InsumoModel = new InsumoModel($db);
    //     $id = $request->getPostGet('id_ins');
    //     $data = [ 
    //         'nom_ins'=>$request->getPostGet('nom_ins'),
    //         'unidad_ins'=>$request->getPostGet('unidad_ins'),
    //         'cost_ins'=>$request->getPostGet('cost_ins'),
    //         'fecha_actu_ins'=>$request->getPostGet('fec_ins')

    //     ];
    //     $InsumoModel->update($id,$data);


    // }

}
