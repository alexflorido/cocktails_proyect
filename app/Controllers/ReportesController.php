<?php

namespace App\Controllers;




class ReportesController extends BaseController
{

    public function get_eventoMes()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $year = $request->getPostGet('year');
        $year1 = $request->getPostGet('year1');
        $year2 = $request->getPostGet('year2');
        if ($year1 == 0) {
            $group = $db->query('SELECT ELT(MONTH(e.fecha_eve), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre") mes, COUNT(*) cuenta, MONTH(e.fecha_eve) numero_mes from evento as e where EXTRACT(YEAR FROM e.fecha_eve) = ' . $year . ' GROUP BY mes ORDER BY numero_mes;')->getResultArray();
        } else {
            $group = $db->query('SELECT ELT(MONTH(e.fecha_eve), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre") mes, COUNT(*) cuenta, MONTH(e.fecha_eve) numero_mes , EXTRACT(YEAR FROM e.fecha_eve) year from evento as e where EXTRACT(YEAR FROM e.fecha_eve) BETWEEN ' . $year1 . ' and ' . $year2 . ' GROUP BY mes, year ORDER BY year , numero_mes;')->getResultArray();
        }

        echo json_encode($group);
    }
    public function get_perasing()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $num_prof = $request->getPostGet('num_prof');
        $group = $db->query('SELECT cp.nom_cargo ,per.* , dtp.total_detalle  costo_dtp from personal as per INNER JOIN detalle_per_prof dtp on dtp.id_prod_prof = per.id_per INNER JOIN proforma p on p.id_prof = dtp.id_prof INNER JOIN cargo_personal cp on cp.id_cargo_per = per.id_cargo_per where dtp.tipo_prod_prof = 0 and p.estado_prof =1 and p.num_prof =' . $num_prof)->getResultArray();

        echo json_encode($group);
    }
    public function get_insUti()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $num_prof = $request->getPostGet('num_prof');
        // echo ('select prod.nom_prod,i.nom_ins, SUM(ROUND(dp.cantidad_prod * dpp.cantidad,2 )) cantidad, COUNT() from insumo as i INNER JOIN detalle_prod dp on dp.id_ins = i.id_ins INNER JOIN producto prod on prod.id_prod = dp.id_prod INNER JOIN detalle_per_prof dpp on dpp.id_prod_prof = prod.id_prod INNER JOIN proforma p on p.id_prof = dpp.id_prof where p.num_prof = ' . $num_prof . ' and dp.id_rec =0 GROUP BY i.nom_ins UNION ALL select prod.nom_prod,rec.nom_rec, SUM(ROUND(dp.cantidad_prod * dpp.cantidad,2 )) cantidad , COUNT() from receta as rec INNER JOIN detalle_prod dp on dp.id_rec = rec.id_rec INNER JOIN producto prod on prod.id_prod = dp.id_prod INNER JOIN detalle_per_prof dpp on dpp.id_prod_prof = prod.id_prod INNER JOIN proforma p on p.id_prof = dpp.id_prof where p.num_prof = ' . $num_prof . ' and dp.id_ins =0 GROUP BY rec.nom_rec ORDER BY `nom_prod` ASC;');
        $group = $db->query('select prod.nom_prod,i.nom_ins,i.unidad_ins, SUM(ROUND(dp.cantidad_prod * dpp.cantidad,2 )) cantidad, COUNT(*) from insumo as i INNER JOIN detalle_prod dp on dp.id_ins = i.id_ins INNER JOIN producto prod on prod.id_prod = dp.id_prod INNER JOIN detalle_per_prof dpp on dpp.id_prod_prof = prod.id_prod INNER JOIN proforma p on p.id_prof = dpp.id_prof where p.num_prof = ' . $num_prof . ' and dp.id_rec =0 GROUP BY i.nom_ins UNION ALL select prod.nom_prod,rec.nom_rec, rec.unidad_rec, SUM(ROUND(dp.cantidad_prod * dpp.cantidad,2 )) cantidad , COUNT(*) from receta as rec INNER JOIN detalle_prod dp on dp.id_rec = rec.id_rec INNER JOIN producto prod on prod.id_prod = dp.id_prod INNER JOIN detalle_per_prof dpp on dpp.id_prod_prof = prod.id_prod INNER JOIN proforma p on p.id_prof = dpp.id_prof where p.num_prof = ' . $num_prof . ' and dp.id_ins =0 GROUP BY rec.nom_rec ORDER BY `nom_prod` ASC;')->getResultArray();

        echo json_encode($group);
    }
    public function get_prodasing()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $num_prof = $request->getPostGet('num_prof');
        $group = $db->query('SELECT prod.* , dtp.total_detalle costo_dtp, dtp.cantidad from producto as prod INNER JOIN detalle_per_prof dtp on dtp.id_prod_prof = prod.id_prod INNER JOIN proforma p on p.id_prof = dtp.id_prof where dtp.tipo_prod_prof = 1 and p.estado_prof =1 and p.num_prof =' . $num_prof)->getResultArray();

        echo json_encode($group);
    }
    public function get_eventos()
    {
        $db = \Config\Database::connect();
        // $request = \Config\Services::request();

        $group = $db->query('SELECT p.num_prof,ELT(MONTH(e.fecha_eve), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre") mes, c.nom_cli ,e.*, EXTRACT(YEAR FROM e.fecha_eve) year, MONTH(e.fecha_eve) mesNUM from evento as e INNER JOIN cliente c on c.id_cli = e.id_cli INNER JOIN proforma p on p.id_eve = e.id_eve ORDER BY year DESC, mesNUM DESC ')->getResultArray();

        echo json_encode($group);
    }
    public function get_eventoYRango()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $year1 = $request->getPostGet('year1');
        $year2 = $request->getPostGet('year2');
        $result = $db->query('SELECT EXTRACT(YEAR FROM e.fecha_eve) ye, COUNT(*) cuenta from evento as e where EXTRACT(YEAR FROM e.fecha_eve) BETWEEN ' . $year1 . ' and ' . $year2 . ' GROUP BY ye ORDER BY ye ;')->getResultArray();


        echo json_encode($result);
    }
    public function get_prodYRango()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $year1 = $request->getPostGet('year1');
        $year2 = $request->getPostGet('year2');
        if ($year2 == 0) {
            $group = $db->query('SELECT ELT(MONTH(p.fecha_eve_prof), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre") mes, MONTH(p.fecha_eve_prof) as mesNUM, prod.nom_prod, dtp.cantidad as cant FROM detalle_per_prof as dtp INNER JOIN proforma p on p.id_prof = dtp.id_prof INNER JOIN producto prod on prod.id_prod = dtp.id_prod_prof WHERE p.estado_prof = 1 and dtp.tipo_prod_prof = 1 and EXTRACT(YEAR FROM p.fecha_eve_prof) = ' . $year1 . ' ORDER BY mesNUM;')->getResultArray();
        } else {
            $group = $db->query('SELECT COUNT(*), ELT(MONTH(p.fecha_eve_prof), "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre") mes, MONTH(p.fecha_eve_prof) as mesNUM, prod.nom_prod nom, SUM(dtp.cantidad) as cant, EXTRACT(YEAR FROM p.fecha_eve_prof) year FROM detalle_per_prof as dtp INNER JOIN proforma p on p.id_prof = dtp.id_prof INNER JOIN producto prod on prod.id_prod = dtp.id_prod_prof WHERE p.estado_prof = 1 and dtp.tipo_prod_prof = 1 and EXTRACT(YEAR FROM p.fecha_eve_prof) BETWEEN ' . $year1 . ' and ' . $year2 . ' GROUP BY nom, year, mes ORDER BY year, mesNUM;')->getResultArray();
        }


        echo json_encode($group);
    }
    public function get_prodXYear()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $year1 = $request->getPostGet('year1');
        $year2 = $request->getPostGet('year2');
        // echo 'SELECT prod.nom_prod, SUM(dtp.cantidad) as cant FROM detalle_per_prof as dtp INNER JOIN proforma p on p.id_prof = dtp.id_prof INNER JOIN producto prod on prod.id_prod = dtp.id_prod_prof WHERE p.estado_prof = 1 and dtp.tipo_prod_prof = 1 and EXTRACT(YEAR FROM p.fecha_eve_prof) = ' . $year1 . ' GROUP BY prod.nom_prod ORDER by cant desc;';
        $group = $db->query('SELECT prod.nom_prod, SUM(dtp.cantidad) as cant FROM detalle_per_prof as dtp INNER JOIN proforma p on p.id_prof = dtp.id_prof INNER JOIN producto prod on prod.id_prod = dtp.id_prod_prof WHERE p.estado_prof = 1 and dtp.tipo_prod_prof = 1 and EXTRACT(YEAR FROM p.fecha_eve_prof) BETWEEN ' . $year1 . ' and ' . $year2 . ' GROUP BY prod.nom_prod ORDER by cant desc;')->getResultArray();

        echo json_encode($group);
    }
    public function get_eventoDetalle()
    {
        $db = \Config\Database::connect();
        $request = \Config\Services::request();
        $year1 = $request->getPostGet('year');
        $month = $request->getPostGet('month');
        $group = $db->query('SELECT ev.nom_eve, ev.nro_factura, ev.fecha_eve, ev.evento_terminado from evento as ev where MONTH(ev.fecha_eve) = ' . $month . ' AND EXTRACT(YEAR FROM ev.fecha_eve) = ' . $year1)->getResultArray();


        echo json_encode($group);
    }
}
