<?php

namespace App\Controllers;

use App\Models\DetalleProductoModel;
use CodeIgniter\HTTP\Request;
use App\Models\InsumoModel;


class Home extends BaseController

{

    public function index()
    {
          return view('welcome_message');
    }

    //--------------------------------------------------------------------
    public function test()
    {
        $request = \Config\Services::request();
        $DetProdModel = new DetalleProductoModel($db);
        // print_r($request->getPostGet('id_prod'));
        $data = [ 
            'id_prod'=>$request->getPostGet('id_prod'),
            'id_ins'=>$request->getPostGet('id_ins'),
            'id_rec'=>$request->getPostGet('id_rec'),
            'cantidad_prod'=>$request->getPostGet('cantidad_prod'),
            'costo_recurso_prod'=>$request->getPostGet('costo_recurso_prod'),
            'costo_total'=>$request->getPostGet('costo_total'),
            

        ];
        // print_r($data);
        $DetProdModel->insert($data);
        return view('welcome_message',$data);
    }
}
