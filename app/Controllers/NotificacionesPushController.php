<?php

namespace App\Controllers;

class NotificacionesPushController extends BaseController
{
    public function sendPush()
    {
        $request = \Config\Services::request();
        $db = \Config\Database::connect();
        $to = $request->getPostGet('token_notification');
        $title = "Tienes Asignada Una Nueva Lista de Compras";
        $message = "Mensaje de prueba para el jugador numero 1";
        $id_list = $request->getPostGet('id_list');
        $id_per = $request->getPostGet('id_per');
        $db->query("INSERT INTO personal_listacompras (id_list,id_per) values(" . $id_list . "," . $id_per . ")");
        // sendPush($to, $title, $messagege);
        $registrationIds = array($to);
        $msg = array(
            'message' => $message,
            'title' => $title,
            'vibrate' => 1,
            'sound' => 1
        );

        $fields = array(
            'to' => $to,
            "priority" => "high",
            'notification'   => $msg,
            // 'registrationIds' => $registrationIds,
            'data' => array(
                "notification_foreground" => "true",
                "notification_body" => $message,
                "notification_title" => $title,
                "foo" => "bar",
                "notification_android_channel_id" => "my_channel",
                "notification_android_priority" => "2",
                "notification_android_visibility" => "1",
                "notification_android_color" => "#ff0000",
                "notification_android_icon" => "coffee",
                "notification_android_sound" => "my_sound",
                "notification_android_vibrate" => "500, 200, 500",
                "notification_android_lights" => "#ffff0000, 250, 250"
            )
        );

        $headers = array(
            'Authorization: key=' . 'AAAAbpML8xA:APA91bHPKpjRoAb9tPuckmad0XYECaBVdDs-pP1Msbsdg4d0HfJ-auW_BEq6XOAU7Ti1GJINP5gBTI5BNAqkYsZMG5dPqsewn2tUYc9FiFEhuT-T1KBPKf_Q7qYeMRaP3AZKdvcx5kow',
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }
}
