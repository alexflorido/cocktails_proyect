<?php

namespace App\Controllers;

use App\Models\UserModel;
use App\Models\CargoModel;


class UsuariosController extends BaseController
{
    public function get_usuarios()
    {
        $UserModel = new UserModel();
        $usuarios = $UserModel->findAll();

        echo json_encode($usuarios);
    }
    public function get_grupo()
    {
        $db = \Config\Database::connect();
        $group = $db->query('select * from grupo')->getResultArray();


        echo json_encode($group);
    }
    public function insert_usuarios()
    {
        $request = \Config\Services::request();
        $UserModel = new UserModel($db);
        $nom_usu = $request->getPostGet('nom_usu');
        $app_usu = $request->getPostGet('app_usu');
        $tel_usu = $request->getPostGet('tel_usu');
        $email_usu = $request->getPostGet('email_usu');
        $clv_usu = $request->getPostGet('clv_usu');
        $id_grp = $request->getPostGet('id_grp');
        $fec_ini = $request->getPostGet('fec_ini');
        $fec_fin = $request->getPostGet('fec_fin');





        $rules = [
            'nom_usu' => ['label' => 'Nombre', 'rules' => 'required|alpha_numeric_punct'],
            'app_usu' =>  ['label' => 'Apellido Paterno', 'rules' => 'required|alpha_numeric_punct'],
            'tel_usu' =>  ['label' => 'Telefono', 'rules' => 'required|integer|max_length[10]|min_length[7]'],
            'email_usu' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],
            'clv_usu' =>  ['label' => 'Unidad', 'rules' => 'required|alpha_numeric_punct'],
            'id_grp' =>  ['label' => 'id_grp', 'rules' => 'required|numeric'],
            'fec_ini' =>  ['label' => 'Fecha_Inicio', 'rules' => 'required|alpha_numeric_punct'],
            'fec_fin' =>  ['label' => 'Fecha_Fin', 'rules' => 'required|alpha_numeric_punct'],
            'foto_usu' => ['label' => 'Imagen', 'rules' => 'uploaded[foto_usu]|is_image[foto_usu]']
        ];

        if ($this->validate($rules)) {;

            $file = $this->request->getFile('foto_usu');
            // echo($file->getName());
            $imgName = $file->getRandomName();
            if ($file->isValid()) {
                $file->move('./assets/img/foto_usuario', $imgName);
                $data = [
                    'nom_usu' => $nom_usu,
                    "app_usu" => $app_usu,
                    "tel_usu" => $tel_usu,
                    "email_usu" => $email_usu,
                    "clv_usu" => $clv_usu,
                    "id_grp" => $id_grp,
                    "fec_ini" => $fec_ini,
                    "fec_fin" => $fec_fin,
                    "foto_usu" => $imgName
                ];


                $UserModel->insert($data);

                echo json_encode('1');
            }
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function Update_usuario()
    {
        $request = \Config\Services::request();
        $UserModel = new UserModel($db);
        $id_usu = $request->getPostGet('id_usu');
        $nom_usu = $request->getPostGet('nom_usu');
        $app_usu = $request->getPostGet('app_usu');
        $tel_usu = $request->getPostGet('tel_usu');
        $email_usu = $request->getPostGet('email_usu');
        $clv_usu = $request->getPostGet('clv_usu');
        $id_grp = $request->getPostGet('id_grp');
        $fec_ini = $request->getPostGet('fec_ini');
        $fec_fin = $request->getPostGet('fec_fin');



        $rules = [
            'nom_usu' => ['label' => 'Nombre', 'rules' => 'required|alpha_numeric_punct'],
            'app_usu' =>  ['label' => 'Apellido Paterno', 'rules' => 'required|alpha_numeric_punct'],
            'tel_usu' =>  ['label' => 'Telefono', 'rules' => 'required|integer|max_length[10]|min_length[7]'],
            'email_usu' =>  ['label' => 'Correo', 'rules' => 'required|valid_email'],
            'clv_usu' =>  ['label' => 'Unidad', 'rules' => 'required|alpha_numeric_punct'],
            'id_grp' =>  ['label' => 'id_grp', 'rules' => 'required|numeric'],
            'fec_ini' =>  ['label' => 'Fecha_Inicio', 'rules' => 'required|alpha_numeric_punct'],
            'fec_fin' =>  ['label' => 'Fecha_Fin', 'rules' => 'required|alpha_numeric_punct']
        ];

        if ($this->validate($rules)) {;






            $data = [
                'nom_usu' => $nom_usu,
                "app_usu" => $app_usu,
                "tel_usu" => $tel_usu,
                "email_usu" => $email_usu,
                "clv_usu" => $clv_usu,
                "id_grp" => $id_grp,
                "fec_ini" => $fec_ini,
                "fec_fin" => $fec_fin,
            ];


            $UserModel->update($id_usu, $data);

            echo json_encode('1');
        } else {
            $data['validation'] = $this->validator->listErrors();
            $errores = $this->validator->getErrors();
            echo json_encode($errores);
        }
    }
    public function Update_img_usuario()
    {

        $request = \Config\Services::request();
        $UserModel = new UserModel($db);
        $id_usu = $request->getPostGet('id_usu');
        $file = $this->request->getFile('img_usu_mod');
        // echo($file->getName());
        $imgName = $file->getRandomName();
        if ($file->isValid()) {
            $file->move('./assets/img/foto_usuario', $imgName);
            $data = [

                "foto_usu" => $imgName
            ];


            $UserModel->update($id_usu, $data);

            echo json_encode($imgName);
        }
    }
    public function delete_usuario()
    {
        $request = \Config\Services::request();
        $UserModel = new UserModel($db);
        $id = $request->getPostGet('id_usu');
        // $db->query('');

        // var_dump($id);
        $UserModel->where('id_usu', $id)->delete();
        echo json_encode('Se elimino el usuario');
    }
}
