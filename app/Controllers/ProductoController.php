<?php

namespace App\Controllers;

use App\Models\ProductoModel;

class ProductoController extends BaseController
{
    public function get_producto()
    {

        $ProductoModel = new ProductoModel($db);
        $producto = $ProductoModel->findAll();
        echo json_encode($producto);
    }

    //--------------------------------------------------------------------

    public function delete_producto()
    {
        $request = \Config\Services::request();
        $ProductoModel = new ProductoModel($db);
        $id = $request->getPostGet('id');
        // $db->query('');

        // var_dump($id);
        $ProductoModel->where('id_prod', $id)->delete();
        echo json_encode('Se elimino el producto');
    }
    public function update_producto()
    {
        $request = \Config\Services::request();
        $ProductoModel = new ProductoModel($db);
        $id_prod = $request->getPostGet('id_prod_mod');
        $data = [
            'nom_prod' => $request->getPostGet('nom_prod_mod'),
            'fecha_prod' => $request->getPostGet('fecha_prod_mod'),
            'desc_prod' => $request->getPostGet('desc_prod_mod'),
            'costo_total_prod' => $request->getPostGet('cost_total_prod_mod'),
            'precio_total_prod' => $request->getPostGet('precio_total_prod')
        ];
        $ProductoModel->update($id_prod, $data);
        echo json_encode("Se modifico Correcta");
    }
    public function insert_producto()
    {
        $request = \Config\Services::request();
        $ProductoModel = new ProductoModel($db);
        $data = [
            'nom_prod' => $request->getPostGet('nom_prod'),
            'fecha_prod' => $request->getPostGet('fecha_prod'),
            'desc_prod' => $request->getPostGet('desc_prod'),
            'costo_total_prod' => $request->getPostGet('costo_total_prod'),
            'precio_total_prod' => $request->getPostGet('precio_total_prod')
        ];
        $ProductoModel->insert($data);
        echo json_encode($ProductoModel->insertID());
    }
    public function update_TotalProducto()
    {
        $request = \Config\Services::request();
        $ProductoModel = new ProductoModel($db);
        $id_prod = $request->getPostGet('id_prod');
        $data = [
            'precio_total_prod' => $request->getPostGet('precio_total_prod')
        ];
        $ProductoModel->update($id_prod, $data);
        echo json_encode(1);
    }
    // public function update_insumo()
    // {
    //     $request = \Config\Services::request();
    //     $InsumoModel = new InsumoModel($db);
    //     $id = $request->getPostGet('id_ins');
    //     $data = [ 
    //         'nom_ins'=>$request->getPostGet('nom_ins'),
    //         'unidad_ins'=>$request->getPostGet('unidad_ins'),
    //         'cost_ins'=>$request->getPostGet('cost_ins'),
    //         'fecha_actu_ins'=>$request->getPostGet('fec_ins')

    //     ];
    //     $InsumoModel->update($id,$data);


    // }

}
