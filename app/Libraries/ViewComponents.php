<?php

namespace App\Libraries;

class ViewComponents
{
    public function getInsPill()
    {
        return view('InsView');
    }
    public function getRecPill()
    {
        return view('RecView');
    }
    public function getDetRecPill()
    {
        return view('DetRecView');
    }
    public function getProdPill()
    {
        return view('ProdView');
    }
    public function getDetProdPill()
    {
        return view('DetProdView');
    }
    public function getPersonalFijoPill()
    {
        return view('PersonalFijoView');
    }
    public function getEventoPill()
    {
        return view('EventoView');
    }
    public function getClientePill()
    {
        return view('ClienteView');
    }
    public function getProveedorPill()
    {
        return view('ProveedorView');
    }
    public function getControlUsuariosPill()
    {
        return view('ControlUsuariosView');
    }
    public function getProformaPill()
    {
        return view('CreateEventoView');
    }
    public function getReportePill()
    {
        return view('ReporteEventoView');
    }
}
