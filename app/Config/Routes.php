<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.

$routes->get('/', 'Login::index');
$routes->post('/login', 'Login::log_users');
$routes->get('/close', 'Login::cerrar_sesion');

// Ruta Insumo
$routes->get('/insumo', 'InsumoController::get_insumo');
$routes->post('/insert/insumo', 'InsumoController::insert_insumo');
$routes->post('/update/insumo', 'InsumoController::update_insumo');
$routes->post('/delete/insumo', 'InsumoController::delete_insumo');
// Ruta Receta
$routes->get('/receta', 'RecetaController::get_receta');
$routes->post('/delete/receta', 'RecetaController::delete_receta');
$routes->post('/insert/receta', 'RecetaController::insert_receta');
$routes->post('/update/receta', 'RecetaController::update_receta');
$routes->post('/update/TotalReceta', 'RecetaController::update_TotalReceta');
// Detalle Receta
$routes->post('/detalleReceta', 'DetalleRecController::get_det_rec');
$routes->post('/insert/detalleReceta', 'DetalleRecController::insert_detalle');
$routes->post('/update/detalleReceta', 'DetalleRecController::update_detalle');
$routes->post('/delete/detalleReceta', 'DetalleRecController::delete_detalle');
// Producto
$routes->get('/producto', 'ProductoController::get_producto');
$routes->post('/insert/producto', 'ProductoController::insert_producto');
$routes->post('/delete/producto', 'ProductoController::delete_producto');
$routes->post('/update/producto', 'ProductoController::update_producto');
$routes->post('/update/TotalProducto', 'ProductoController::update_TotalProducto');
// Detalle Producto
$routes->post('/insert/detalleProducto', 'DetalleProdController::insert_detalle');
$routes->post('/detalleProducto', 'DetalleProdController::get_det_prod');
$routes->post('/delete/det_producto', 'DetalleProdController::delete_det_prod');
$routes->post('/update/detalleProducto', 'DetalleProdController::update_det_prod');
// Proveedor
$routes->get('/proveedor', 'ProveedorController::get_proveedor');
$routes->post('/insert/proveedor', 'ProveedorController::insert_proveedor');
$routes->post('/modificar/proveedor', 'ProveedorController::modificar_proveedor');
$routes->post('/delete/proveedor', 'ProveedorController::delete_proveedor');
// Contacto Proveedor
$routes->get('/contacto_proveedor', 'ContactoProveedorController::get_persona_contacto');
$routes->post('/insert/cont_prov', 'ContactoProveedorController::insert_contacto_proveedor');
$routes->post('/modificar/cont_prov', 'ContactoProveedorController::modificar_contacto_proveedor');
$routes->post('/delete/cont_prov', 'ContactoProveedorController::delete_contacto_proveedor');
// Personal
$routes->get('/personalFijo', 'PersonalFijoController::get_personal');
$routes->get('/personalFijo/activo', 'PersonalFijoController::get_personal_activo');
$routes->get('/cargo/personal', 'PersonalFijoController::get_cargo');
$routes->get('/categoria/personal', 'PersonalFijoController::get_categoria');
$routes->post('/insert/personal', 'PersonalFijoController::insert_personal');
$routes->post('/get/ev_personal', 'PersonalFijoController::get_ev_personal');
$routes->post('/modificar/personal', 'PersonalFijoController::modificar_personal');
$routes->post('/modificar/img/personal', 'PersonalFijoController::modificar_img_personal');
$routes->post('/delete/personal', 'PersonalFijoController::delete_personal');
$routes->post('/cambiar/estado/personal', 'PersonalFijoController::cambiar_estado');
// Evento
$routes->get('/evento_view', 'EventoController::get_evento_view');
$routes->post('/insumo_almacen', 'EventoController::get_insumo_almacen');
$routes->post('/get/proforma_byID', 'EventoController::get_proforma_byID');
$routes->get('/new_event', 'EventoController::create_event');
$routes->post('/new_proform', 'EventoController::create_proform');
$routes->post('/get/det_list_prod', 'EventoController::get_det_list_prod');
$routes->post('/update/total_values_proform', 'EventoController::update_total_values_prof');
$routes->post('/update/total_values_event', 'EventoController::update_total_values_event');
$routes->post('/insert/event', 'EventoController::insert_event');
$routes->post('/delete/event', 'EventoController::delete_event');
$routes->post('/update/event', 'EventoController::update_event');
$routes->post('/update/proform', 'EventoController::update_proform');
$routes->post('/insert/proform', 'EventoController::insert_proform');
$routes->post('/update/proforma_estado', 'EventoController::update_proforma_estado');
$routes->post('/insert/detalle_per_prof', 'EventoController::insert_per_prof');
$routes->post('/pdf/event', 'EventoController::to_pdf');
$routes->post('pdf/insertPDF', 'EventoController::insert_pdf');
$routes->post('/insert/list_to_shop', 'EventoController::list_to_shop');
$routes->post('/insert/det_list_to_shop', 'EventoController::insert_detalle_listaCompras');
$routes->post('/insert/asig_lista_personal', 'EventoController::insert_asig_lista_personal');

// Cliente
$routes->get('/cliente', 'ClienteController::get_cliente');
$routes->post('/insert/cliente', 'ClienteController::insert_cliente');
$routes->post('/delete/cliente', 'ClienteController::delete_cliente');
$routes->post('/modificar/cliente', 'ClienteController::modificar_cliente');
// Persona de Contacto
$routes->get('/persona_contacto', 'PersonaContactoController::get_persona_contacto');
$routes->post('insert/per_cont', 'PersonaContactoController::insert_persona_contacto');
$routes->post('modificar/per_cont', 'PersonaContactoController::modificar_persona_contacto');
$routes->post('eliminar/per_cont', 'PersonaContactoController::delete_eliminar_persona_contacto');
// NOTIFICATION
$routes->post('/send/notification', 'NotificacionesPushController::sendPush');
// USUARIOS
$routes->get('usuarios', 'UsuariosController::get_usuarios');
$routes->post('insert/usuarios', 'UsuariosController::insert_usuarios');
$routes->post('modificar/img/usuario', 'UsuariosController::Update_img_usuario');
$routes->post('modificar/usuario', 'UsuariosController::Update_usuario');
$routes->post('delete/usuario', 'UsuariosController::delete_usuario');
$routes->get('grupo', 'UsuariosController::get_grupo');
// REPORTES
$routes->get('/get/eventos', 'ReportesController::get_eventos');
$routes->post('get/eventoMes', 'ReportesController::get_eventoMes');
$routes->post('get/perasing', 'ReportesController::get_perasing');
$routes->post('get/prodasing', 'ReportesController::get_prodasing');
$routes->post('get/insUti', 'ReportesController::get_insUti');
$routes->post('get/eventoYRango', 'ReportesController::get_eventoYRango');
$routes->post('get/prodYRango', 'ReportesController::get_prodYRango');
$routes->post('get/prodXYear', 'ReportesController::get_prodXYear');
$routes->post('get/eventoDetalle', 'ReportesController::get_eventoDetalle');


// $routes->get('/', 'Home::index');
// $routes->post('/test', 'Home::test');
// $routes->get('/inicio', 'Home::index');

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
