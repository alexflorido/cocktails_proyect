<?php namespace App\Models;

use CodeIgniter\Model;

class ContactoProveedorModel extends Model
{
    protected $table      = 'contacto_proveedor';
    protected $primaryKey = 'id_cont_prov';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['nombre_cont', 'email_cont', 'telf_cont', 'estado','id_prov'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}