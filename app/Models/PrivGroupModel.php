<?php namespace App\Models;

use CodeIgniter\Model;

class PrivGroupModel extends Model
{
    protected $table      = 'priv_grp';
    protected $primaryKey = 'id_priv_grp';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['crud_code', 'id_grp', 'id_ele'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}