<?php namespace App\Models;

use CodeIgniter\Model;

class PrivGroupEleModel extends Model
{
    protected $table      = 'priv_grp_ele';
    protected $primaryKey = 'id_priv_grp';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['crud_cod', 'id_grp', 'id_ele', 'nom_grp', 'nom_ele'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}