<?php

namespace App\Models;

use CodeIgniter\Model;

class RecetaModel extends Model
{
    protected $table      = 'receta';
    protected $primaryKey = 'id_rec';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    // protected $deletedField = 'deleted';

    protected $allowedFields = ['nom_rec', 'desc_rec', 'unidad_rec', 'cost_total_rec', 'fecha_rec'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
