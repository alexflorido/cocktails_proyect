<?php

namespace App\Models;

use CodeIgniter\Model;

class PersonalFijoModel extends Model
{
    protected $table      = 'personal_view';
    protected $primaryKey = 'id_per';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['nom_per', 'app_per', 'apm_per', 'id_cargo_per', 'ci_per', 'id_cat_per', 'unidad_per', 'cost_per', 'telf_per', 'email_per', 'estado', 'ciudad', 'nom_cargo', 'nombre_cat', 'fec_ingreso_personal', 'img_per', 'token_notification', 'clv_personal', 'on_event'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
