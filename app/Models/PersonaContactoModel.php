<?php namespace App\Models;

use CodeIgniter\Model;

class PersonaContactoModel extends Model
{
    protected $table      = 'persona_contacto';
    protected $primaryKey = 'id_per_cont';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['nom_per_cont', 'app_per_cont', 'tel_per_cont', 'email_per_cont','id_cli'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}