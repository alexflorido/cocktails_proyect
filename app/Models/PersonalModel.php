<?php

namespace App\Models;

use CodeIgniter\Model;

class PersonalModel extends Model
{
    protected $table      = 'personal';
    protected $primaryKey = 'id_per';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['nom_per', 'app_per', 'apm_per', 'id_cargo_per', 'ci_per', 'id_cat_per', 'unidad_per', 'cost_per', 'telf_per', 'email_per', 'estado', 'ciudad', 'img_per', 'fec_ingreso_personal', 'clv_personal', 'on_event'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
