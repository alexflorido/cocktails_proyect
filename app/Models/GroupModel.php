<?php namespace App\Models;

use CodeIgniter\Model;

class GroupModel extends Model
{
    protected $table      = 'grupo';
    protected $primaryKey = 'id_grp';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['nom_grp', 'desc_grp'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}