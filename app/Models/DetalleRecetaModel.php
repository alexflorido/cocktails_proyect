<?php namespace App\Models;

use CodeIgniter\Model;

class DetalleRecetaModel extends Model
{
    protected $table      = 'detalle_receta';
    protected $primaryKey = 'id_det_rec';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['id_rec', 'id_ins', 'cant_det_rec', 'cost_ins_det_rec', 'prec_final_det_rec', 'fecha_ins_det_rec'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}