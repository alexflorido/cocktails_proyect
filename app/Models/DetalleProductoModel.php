<?php namespace App\Models;

use CodeIgniter\Model;

class DetalleProductoModel extends Model
{
    protected $table      = 'detalle_prod';
    protected $primaryKey = 'id_det_prod';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = ['id_prod', 'id_ins','id_rec', 'cantidad_prod', 'costo_recurso_prod', 'costo_total','precio_total_detProd'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}