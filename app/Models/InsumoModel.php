<?php namespace App\Models;

use CodeIgniter\Model;

class InsumoModel extends Model
{
    protected $table      = 'insumo';
    protected $primaryKey = 'id_ins';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    // protected $deletedField = 'deleted';

    protected $allowedFields = ['nom_ins', 'unidad_ins', 'cost_ins', 'fecha_actu_ins'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}