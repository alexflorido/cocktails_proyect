<?php namespace App\Models;

use CodeIgniter\Model;

class CargoModel extends Model
{
    protected $table      = 'cargo_personal';
    protected $primaryKey = 'id_cargo_per';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['nom_cargo', 'desc_cargo'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}