<?php namespace App\Models;

use CodeIgniter\Model;

class ProductoModel extends Model
{
    protected $table      = 'producto';
    protected $primaryKey = 'id_prod';

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    // protected $deletedField = 'deleted';

    protected $allowedFields = ['nom_prod', 'fecha_prod', 'desc_prod', 'fecha_rec', 'precio_total_prod','costo_total_prod'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}