<?php namespace App\Models;

use CodeIgniter\Model;

class EventoViewModel extends Model
{
    protected $table      = 'evento_view';
    protected $primaryKey = 'id_eve';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['nom_eve', 'fecha_eve', 'id_cli', 'cost_eve_total', 'IUE', 'IT', 'IVA', 'total_imp', 'total_facturado', 'utilidad', 'nro_factura','nom_cli'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}