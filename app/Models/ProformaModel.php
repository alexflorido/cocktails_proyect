<?php namespace App\Models;

use CodeIgniter\Model;

class ProformaModel extends Model
{
    protected $table      = 'proforma';
    protected $primaryKey = 'id_prof';

    protected $returnType     = 'array';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['num_prof', 'fecha_prof', 'auto_nom_prof', 'fecha_eve_prof', 'sub_total_prof', 'iva_prof', 'total_general_prof', 'id_eve', 'id_per_cont', 'estado_prof', 'pdf_prof'];

    protected $useTimestamps = false;
    // protected $createdField  = 'created_at';
    // protected $updatedField  = 'updated_at';
    // protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}