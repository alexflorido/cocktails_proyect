<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, Content-Type");
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
// header("Access-Control-Allow-Headers: Content-Type, Authorization");
$servername = "localhost";
$userName = "root";
$password = "";
$dbName = "coctails_bd";

$conn = mysqli_connect($servername, $userName, $password, $dbName);
$requestMethod = $_SERVER["REQUEST_METHOD"];

if (strtoupper($requestMethod) == 'GET') {
    if ($conn->connect_error) {
        die("Conection Filed " . $conn->connect_error);
    }
    $sql = "Select * from lista_compras";
    $result = mysqli_query($conn, $sql);

    if (mysqli_num_rows($result) > 0) {
        $outp = array();
        $outp = $result->fetch_all(MYSQLI_ASSOC);
        echo json_encode($outp);
    } else {
        echo json_encode("0 Results");
    }
}
if (strtoupper($requestMethod) == 'POST') {
    $data = file_get_contents('php://input');
    $incoming_data = json_decode($data, true);
    if (isset($incoming_data['id_per'])) {
        $query = "Select l.*, pl.*, e.nom_eve from lista_compras as l INNER JOIN personal_listacompras as pl on pl.id_list = l.id_list INNER JOIN proforma as prof on prof.id_prof = l.id_prof INNER JOIN evento as e on e.id_eve = prof.id_eve where l.id_list in (SELECT pl.id_list from personal_listacompras as pl where id_per =" . $incoming_data['id_per'] . ") and pl.estado_tarea = 0";
        // $query = "Select * from usuario where email_usu = '" . $incoming_data['email'] . "' and clv_usu = '" . $incoming_data['password'] . "'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            $outp = array();
            $outp = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($outp);
        } else {
            echo json_encode(0);
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['id_per_comp'])) {
        $query = "Select l.*, pl.*, e.nom_eve from lista_compras as l INNER JOIN personal_listacompras as pl on pl.id_list = l.id_list INNER JOIN proforma as prof on prof.id_prof = l.id_prof INNER JOIN evento as e on e.id_eve = prof.id_eve where l.id_list in (SELECT pl.id_list from personal_listacompras as pl where id_per =" . $incoming_data['id_per_comp'] . ") and pl.estado_tarea = 1";
        // $query = "Select * from usuario where email_usu = '" . $incoming_data['email'] . "' and clv_usu = '" . $incoming_data['password'] . "'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            $outp = array();
            $outp = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($outp);
        } else {
            echo json_encode(0);
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['fec_caducidad'])) {

        $query = "INSERT INTO reg_almacen (fec_entrada, cant_almacen, id_rec, id_ins, id_eve, fec_caducidad) VALUES ('" . $incoming_data['fec_entrada'] . "', " . $incoming_data['cantidad_ex'] . ", " . $incoming_data['id_rec'] . ", " . $incoming_data['id_ins'] . ", " . $incoming_data['id_eve'] . ", '" . $incoming_data['fec_caducidad'] . "')";

        if (mysqli_query($conn, $query) == true) {

            echo 1;
        } else {
            echo "error";
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['eventoterminado'])) {

        $query = "UPDATE evento SET evento_terminado = 1 WHERE id_eve = " . $incoming_data['eventoterminado'];

        if (mysqli_query($conn, $query) == true) {
            $query = "UPDATE personal set on_event = 0 where id_per in (SELECT per.id_per from personal as per INNER JOIN detalle_per_prof dtp on dtp.id_prod_prof = per.id_per INNER JOIN proforma p on p.id_prof = dtp.id_prof  INNER JOIN evento e on e.id_eve = p.id_eve where p.estado_prof =1 and e.id_eve = " . $incoming_data['eventoterminado'] . ")";
            mysqli_query($conn, $query);
            echo json_encode(1);
        } else {
            echo json_encode(0);
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['tabla'])) {
        if ($incoming_data['tabla'] == 'insumo') {

            $query = "Select * from insumo where id_ins != 0 and deleted_at is NULL";
        } else {
            $query = "Select * from receta where id_rec != 0 and deleted_at is NULL";
        }
        // $query = "Select * from usuario where email_usu = '" . $incoming_data['email'] . "' and clv_usu = '" . $incoming_data['password'] . "'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            $outp = array();
            $outp = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($outp);
        } else {
            echo json_encode(0);
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['evento'])) {
        $query = "Select e.coordinates_lng, e.coordinates_lat,e.nom_eve, e.evento_terminado, e.fecha_eve, e.id_eve, e.nro_factura, c.id_cli,c.nom_cli ,c.nit_cli , pc.id_per_cont , pc.nom_per_cont ,pc.app_per_cont, pc.tel_per_cont,pc.email_per_cont from evento as e INNER JOIN cliente as c on c.id_cli = e.id_cli INNER JOIN persona_contacto as pc on pc.id_per_cont = e.id_per_cont;";
        // $query = "Select * from usuario where email_usu = '" . $incoming_data['email'] . "' and clv_usu = '" . $incoming_data['password'] . "'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            $outp = array();
            $outp = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($outp);
        } else {
            echo json_encode(0);
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['token'])) {
        $query = "UPDATE personal SET token_notificatio = '" . $incoming_data['token'] . "' where id_per = " . $incoming_data['id_per_tok'];
        // $query = "Select * from usuario where email_usu = '" . $incoming_data['email'] . "' and clv_usu = '" . $incoming_data['password'] . "'";
        if (mysqli_query($conn, $query) == true) {

            echo $incoming_data['id_per_tok'];
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['id_list'])) {
        $query = "Select * from detalle_lista_compras as dl where dl.id_list = " . $incoming_data['id_list'];
        // $query = "Select * from usuario where email_usu = '" . $incoming_data['email'] . "' and clv_usu = '" . $incoming_data['password'] . "'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            $outp = array();
            $outp = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($outp);
        } else {
            echo json_encode(0);
        }
        // echo json_encode($incoming_data["id_per"]);
    } else if (isset($incoming_data['email'])) {
        // $query = "Select * from detalle_lista_compras where id_list in (SELECT pl.id_list from personal_listacompras as pl where id_per = " . $incoming_data['id_per'] . " and estado_tarea = 0);";
        $query = "Select * from personal where email_per = '" . $incoming_data['email'] . "' and clv_personal = '" . $incoming_data['password'] . "'";
        $result = mysqli_query($conn, $query);
        if (mysqli_num_rows($result) > 0) {
            $outp = array();
            $outp = $result->fetch_all(MYSQLI_ASSOC);
            echo json_encode($outp);
        } else {
            echo json_encode(0);
        }
    } else if (isset($incoming_data['id'])) {
        $query = "UPDATE personal_listacompras SET estado_tarea = 1 where id_plc = " . $incoming_data['id'];
        if (mysqli_query($conn, $query) == true) {

            echo $incoming_data['id'];
        }
    } else {
        return "no funciono";
    }
}



$conn->close();
