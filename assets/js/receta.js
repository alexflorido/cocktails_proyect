var r = 1;
var id_rec;
$(document).ready(function () {
    // get_receta();
    get_receta_ins();
    $(document).on('keyup', '.buscar-insumo', function () {
        var lista = $(this).parents('.listado').find('.search_item');
        var busqueda = $(this).val().toLowerCase();
        if (busqueda.toString() == "") {
            lista.each(function () {
                $(this).removeClass('d-none');
            })

        } else {
            lista.each(function () {

                var fila = $(this).text().toString();
                var cadena = "";
                var estado = false;
                for (let i = 0; i < $(this).text().length; i++) {
                    // console.log(fila.charAt(i));
                    cadena = cadena + fila.charAt(i).toLowerCase();
                    // console.log(cadena);
                    if (cadena == busqueda) {
                        //  console.log(cadena);   
                        i = $(this).text().length;
                        estado = true;
                    }


                }
                if (estado != true) {
                    $(this).addClass('d-none');

                }
                else {
                    estado = false;
                    $(this).removeClass('d-none');
                }
                cadena = "";


            });
        }


    });

    var tabla_rec = $('#tab_rec').DataTable({


        ajax: {
            method: "GET",
            url: "receta",
            dataSrc: ""

        },
        select: {
            style: 'single',
            blurable: true
        },
        dom: 'Bfrtip',
        stateSave: false,
        buttons: [

            {
                text: '<i class="far fa-plus-square"></i> Agregar Receta',
                titleAttr: "Insertar",
                action: function () {
                    var modal = $('#ModalReceta');


                    modal.find('#nom_rec').val('');
                    modal.find('#desc_rec').val('');
                    modal.find('#fecha_rec').val('');
                    modal.modal("show");
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Receta',
                titleAttr: "Modificar",
                action: function () {
                    var modal = $('#modal_mod_rec');
                    var receta = tabla_rec.row({ selected: true }).data();
                    if (receta == null) {
                        alert("Seleccione un receta para modificar");
                    }
                    else {
                        get_det_rec(receta['id_rec']);
                        modal.find('#id_rec_mod').val(receta["id_rec"]);
                        modal.find('#nom_rec_mod').val(receta["nom_rec"])
                        modal.find('#cost_total_rec_mod').val(receta['cost_total_rec']);
                        modal.find('#desc_rec_mod').val(receta['desc_rec']);
                        modal.find('#unidad_rec_mod').val(receta['unidad_rec']);
                        modal.find('#fecha_rec_mod').val(receta['fecha_rec']);
                        modal.modal("show");
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Receta',
                titleAttr: "Eliminar",
                action: function () {
                    var receta = tabla_rec.row({ selected: true }).data();
                    if (receta == null) {
                        alert("Seleccione un receta para eliminar");
                    }
                    else {
                        eliminar_receta(receta["id_rec"], receta["nom_rec"], receta["desc_rec"], receta["cost_total_rec"]);
                    }

                }
            },
        ],
        columns: [

            { data: 'nom_rec' },
            { data: 'desc_rec' },
            { data: 'cost_total_rec' },
            { data: 'fecha_rec' },


        ],

    });
});

// Seleccionar un insumo para receta
$(document).on('dblclick', '#btn-ins-list', function (event) {
    var button = $(this);
    var nom = button.data('nombre');
    var cost = button.data('cost');
    var und = button.data('und');
    var id = button.data('id');
    // console.log(nom);

    $('.tbody-det').append("<tr><td id='id'>" + id + "</td><td id='nom'>" + nom + "</td><td id='cantidad'><input type='text' id='cant' name='cant' placeholder='Cantidad'></td><td id='und'>" + und + "</td><td id='cost'>" + parseFloat(cost).toFixed(4) + "</td><td id='pre_fin'><input type='text' id='pre_fin' name='pre_fin' placeholder='Precio Final'></td><td><button id='btn-r-ins' class='form-control btn-danger input-md'><i class='fas fa-minus-circle'></i></button></td></tr>");
    $(this).remove();
});
// ----------
// Registrar detalle insumo
$(document).on('click', '#btn-det-rec', function (event) {
    // console.log('Reg Detalle');
    // console.log($('#tab_rec tbody tr'));


    $('#tab_rec tbody.tbody-det tr').each(function () {
        var id_receta = id_rec;
        var id_ins = $(this).find('#id').html();
        var cant_det_rec = $(this).find('#cantidad').children('input').val();
        var cost_ins_det_rec = $(this).find('#cost').html();
        var prec_final_det_rec = $(this).find('#pre_fin').children('input').val();
        // console.log('id_rec: ', id_receta,'id_ins: ', id_ins,'cant: ', cant_det_rec,'cost: ', cost_ins_det_rec,'precio: ', prec_final_det_rec);
        setTimeout(function () {

            insert_det_rec(id_receta, id_ins, cant_det_rec, cost_ins_det_rec, prec_final_det_rec);
            // console.log("Se inserto Detalle");
        }, 1000);


    });
    setTimeout(function () {
        $('#tab_rec').DataTable().ajax.reload();
        $('.tbody-det').empty();
        $('#pills-rec-tab').trigger('click');
        alert('Detalle Registrado');

    }, 1100);

});
// ---------
// Calcular el Costo Total
$(document).on('change', '#cant', function () {
    // console.log('change');
    var pre_fin = $(this).parents('tr').find('#pre_fin').children('input');
    var costo_tot = $(this).parents('tr').find('#costo_tot');
    var cant = $(this).val();
    var cost = $(this).parents('tr').find('#cost').html();
    var total = (cost * cant).toFixed(4);
    pre_fin.val(parseFloat(total));
    costo_tot.text(parseFloat(total));
});
// ----------
// Eliminar insumo Seleccionado
$(document).on('click', '#btn-r-ins', function () {
    var id = $(this).parents('tr').find('#id').html();
    var nom = $(this).parents('tr').find('#nom').html();
    var und = $(this).parents('tr').find('#und').html();
    var cost = $(this).parents('tr').find('#cost').html();
    $(this).parents('tr').remove();
    $('#list_ins').append("<button type='button' class='btn-default list-group-item search_item' id='btn-ins-list' data-und='" + und + "' data-nombre='" + nom + "' data-id='" + id + "' data-cost='" + parseFloat(cost).toFixed(4) + "'>" + nom + "</button>");



});
// -----
// Modal Mod Receta
//     $('#modal_mod_rec').on('show.bs.modal', function (event) {
//         $('#table-det-rec-mod-body').empty();

//         var button = $(event.relatedTarget) // Button that triggered the modal
//         var id = button.data('id')
//         var nom = button.data('nom')
//         var cost = button.data('cost')
//         var desc = button.data('desc')
//         var fecha = button.data('fecha')
//         var modal = $(this);
//         get_det_rec(id);
//         modal.find('#id_rec_mod').val(id)
//         modal.find('#nom_rec_mod').val(nom)
//         modal.find('#cost_total_rec_mod').val(cost)
//         modal.find('#desc_rec_mod').val(desc)
//         modal.find('#fecha_rec_mod').val(fecha)
//     });



// });
// Get de detalle receta
function get_det_rec(id) {
    $('#table-det-rec-mod-body').empty();
    $.ajax({
        method: "post",
        url: "detalleReceta",
        data: {
            'id_rec': id
        },
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var det_receta = JSON.parse(result);
            // console.log(det_receta);
            for (const key in det_receta) {
                if (det_receta.hasOwnProperty(key)) {
                    const element = det_receta[key];
                    // console.log(element);
                    $('#table-det-rec-mod-body').append(`<tr>
                    <td><input type='hidden' name='id_det_rec' value='${element['id_det_rec']}'>${element['nom_ins']}</td>
                    <td><input type="text" value='${element['cant_det_rec']}' name='cant_det_rec' style='width: 6rem' disabled></td>
                    <td>${element['unidad_ins']}</td>` +
                        "<td>" + parseFloat(element['cost_ins_det_rec']).toFixed(4) + "</td>" +
                        "<td><input type='text' value='" + parseFloat(element['prec_final_det_rec']).toFixed(4) + "' name='prec_final_det_rec' style='width: 6rem' disabled></td>" +
                        `<td><button name='btn_eliminar_drec' type='button' class='form-control input-md'><i class="far fa-trash-alt"></i></button></td>
                    <td><button name='btn_modificar_drec' type='button' class='form-control input-md'><i class="far fa-edit"></i><i class="fas fa-check d-none"></i></button></td></tr>`);


                }
            }
        }
    });
}
// ------------------
// BTN modificar detalle receta
$(document).on('click', 'button[name="btn_modificar_drec"]', function () {

    if ($(this).find('.fa-check').hasClass('d-none')) {

        $(this).parents('tr').find('input').prop('disabled', false);
        $(this).find('.fa-check').removeClass('d-none');
        $(this).find('.fa-edit').addClass('d-none');
    }
    else {
        var id_rec = $('#modal_mod_rec').find('#id_rec_mod').val()
        var id_det_rec = $(this).parents('tr').find('input[name="id_det_rec"]').val();
        var cant_det_rec = $(this).parents('tr').find('input[name="cant_det_rec"]').val();
        var prec_final_det_rec = $(this).parents('tr').find('input[name="prec_final_det_rec"]').val();
        $.ajax({
            method: "post",
            url: "update/detalleReceta",
            data: {
                'id_det_rec': id_det_rec,
                'cant_det_rec': cant_det_rec,
                'prec_final_det_rec': prec_final_det_rec,
            },
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            success: function (result) {
                $(this).parents('tr').find('input').prop('disabled', true);
                $(this).find('.fa-edit').removeClass('d-none');
                $(this).find('.fa-check').addClass('d-none');
                alert("Se Modifico Correctamente");
            }
        }).done(setTimeout(function () {
            get_det_rec(id_rec);

        }, 1000));

        var cost_total_rec = 0;
        $('#table-det-rec-mod-body tr').each(function () {
            cost_total_rec += parseFloat($(this).find('input[name="prec_final_det_rec"]').val());
        });
        console.log(cost_total_rec);
        $.ajax({
            method: "post",
            url: "update/TotalReceta",
            data: {
                'id_rec': id_rec,
                'cost_total_rec': cost_total_rec,
            },
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            success: function (result) {

            }
        }).done(setTimeout(function () {
            $('#tab_rec').DataTable().ajax.reload()

        }, 1000));
    }
})
// BTN Borrar detalle Rceta
$(document).on('click', 'button[name="btn_eliminar_drec"]', function () {

    if (confirm('Desea Eliminar este Insumo del Detalle de la Receta?')) {
        var id_det_rec = $(this).parents('tr').find('input[name="id_det_rec"]').val();
        $.ajax({
            method: "post",
            url: "delete/detalleReceta",
            data: {
                'id_det_rec': id_det_rec
            },
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            success: function (result) {

                alert("Se Elimino Correctamente");
            }
        }).done(setTimeout(function () {
            get_det_rec($('#modal_mod_rec').find('#id_rec_mod').val());

        }, 1000));
    }

})
// Modificar Receta
$("#modal_mod_rec_form").submit(function (event) {
    event.preventDefault();
    var formData = new FormData($(this)[0]);
    console.log("rec modal");
    $.ajax({
        url: "update/receta",
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {

            alert("Se Modifico Correctamente");
        }
    }).done(setTimeout(function () {
        $('#tab_rec').DataTable().ajax.reload();
        $('#modal_mod_rec').modal('hide');

    }, 1000));

})
// Insertar Detalle Receta
function insert_det_rec(id_receta, id_ins, cant_det_rec, cost_ins_det_rec, prec_final_det_rec) {
    $.ajax({
        method: "POST",
        url: "insert/detalleReceta",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'id_rec': id_receta,
            'id_ins': id_ins,
            'cant_det_rec': cant_det_rec,
            'cost_ins_det_rec': cost_ins_det_rec,
            'prec_final_det_rec': prec_final_det_rec
        },
        success: function (result) {
            var x = JSON.parse(result);
            // console.log(x);
            // id_rec = x;

        }

    });

}
// ---------
// Obtener todas las recetas
function get_receta() {
    // console.log("Get Receta");
    if (r > 1) {
        $(".tbody-rec").html('');
        r = 1;

    }
    $.ajax({
        method: "get",
        url: "receta",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var receta = JSON.parse(result);
            // console.log(receta);
            for (const key in receta) {
                if (receta.hasOwnProperty(key)) {
                    const element = receta[key];
                    // console.log(element);
                    $('#tab_rec').append("<tr><td>" + (r) + "</td><td>" + element['nom_rec'] + " </td><td>" + element['desc_rec'] + "</td><td>" + parseFloat(element['cost_total_rec']).toFixed(4) + "</td><td>" + element['fecha_rec'] + "</td><td><button id='btn_eliminar_rec' name='btn_eliminar_rec' type='button' class='form-control input-md fa fa-trash-o' value='' onClick=\"eliminar_receta( " + element['id_rec'] + ",'" + element['nom_rec'] + "', '" + element['desc_rec'] + "', " + element['cost_total_rec'] + ")\" ></button></td><td><button id='btn_modificar' name='btn_modificar' type='button' class='form-control input-md fa fa-pencil-square-o' data-toggle='modal' data-target='#exampleModal' value='' data-id='" + element['id_ins'] + "' data-nom='" + element['nom_ins'] + "' data-cost='" + element['cost_ins'] + "' data-und='" + element['unidad_ins'] + "' data-fecha='" + element['fecha_actu_ins'] + "' data-mod='mod'></button></td></tr>");

                    r++;
                }
            }
        }
    });
}
// ---------------------------------------------------------------------
// Eliminar un Insumo
function eliminar_receta(id, nom, desc, cost) {
    // console.log('Eliminar insumo',id,nom,cost);

    var r = confirm("Desea Eliminar La Receta: \n Nombre: " + nom + "\n Descripcion: " + desc + "\n Costo de Receta: " + cost);
    if (r == true) {
        $.ajax({
            method: "POST",
            url: "delete/receta",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: { 'id': id },
            success: function (result) {
                // var x = JSON.parse(result);
                // console.log(X);

            }

        }).done(setTimeout(function () { $('#tab_rec').DataTable().ajax.reload() }, 500)).always(function () {
            alert("completado");
        });

    }


}
// -------------------------------------------
// INSERTAR Receta
function insertar_receta() {
    // console.log('insertar receta');




    var nom = $('#nom_rec').val();
    var desc = $('#desc_rec').val();
    var cost = $('#cost_total_rec').val();
    var fec = $('#fecha_rec').val();
    var und = $('#unidad_rec').val();


    $.ajax({
        method: "POST",
        url: "insert/receta",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'nom_rec': nom,
            'desc_rec': desc,
            'cost_total_rec': cost,
            'fecha_rec': fec,
            'unidad_rec': und
        },
        success: function (result) {
            var x = JSON.parse(result);
            // console.log(x);
            id_rec = x;

        }

    }).done(function () {


    }).always(function () {

        $('#cerrar-modal-rec').trigger('click');
        setTimeout(function () { $('#pills-det-tab').trigger('click'); get_receta_ins(); }, 1000);
        setTimeout(function () { $('#tab_rec').DataTable().ajax.reload() }, 1000);

        alert("Receta Completada \n Ahora podra insertar Insumos");
    });
}
// -----------------------------------
// Obtener todos los Insumos
function get_receta_ins() {
    // console.log("Get detalle Receta");
    $('#list_ins').empty();
    $('#list_ins').append(`<li class="list-group-item list-group-item-primary text-center">Insumos <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#reg_ins"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
        <!-- Input para Buscar insumos -->
        <div class="input-group">
          <div class="input-group-prepend">
            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
          <input type="text" class="form-control buscar-insumo" placeholder="Buscar insumo" aria-label="" aria-describedby="basic-addon1">
        </div>`);
    $.ajax({
        method: "get",
        url: "insumo",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var receta = JSON.parse(result);
            // console.log(receta);
            for (const key in receta) {
                if (receta.hasOwnProperty(key)) {
                    const element = receta[key];
                    // console.log(element);

                    $('#list_ins').append("<button type='button' class='btn-default list-group-item search_item' id='btn-ins-list' data-und='" + element['unidad_ins'] + "' data-nombre='" + element['nom_ins'] + "' data-id='" + element['id_ins'] + "' data-cost='" + element['cost_ins'] + "'>" + element['nom_ins'] + "</button>");

                }
            }
        }
    });
}
// ---------------------------------------------------------------------
// INSERTAR INSUMO
function insert_insumo() {
    // console.log('insert insumo');

    var nom = $('#nom_insumo').val();
    var cost = $('#cost_insumo').val();
    var und = $('#und_insumo').val();
    var fec = $('#fec_insumo').val();


    $.ajax({
        method: "POST",
        url: "insert/insumo",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'nom_ins': nom,
            'cost_ins': cost,
            'unidad_ins': und,
            'fec_ins': fec
        },
        success: function (result) {
            var x = JSON.parse(result);
            // console.log(X);

        }

    }).always(function () {
        setTimeout(setTimeout(function () { $('#tab_logic').DataTable().ajax.reload() }, 500));
        setTimeout(get_receta_ins, 500);
        $('#cerrar-ins-det').trigger('click');
        alert("completado");
    });
}
// -----------------------------------
// Modificar detalle Receta
$(document).on('click', '#btn_modificar_det_rec', function () {

    var val = $(this).attr('value');

    var table = $('#tab_logic').DataTable();
    $('#butt_det_mod_close').click();
    $('#pills-ins-tab').click();
    table.search(val).draw();




});