
// Variable para generar la tabla
var i = 1;
$(document).ready(function () {



    var table_insumo = $('#tab_logic').DataTable({

        ajax: {
            method: "GET",
            url: "insumo",
            dataSrc: ""

        },
        select: {
            style: 'single',
            blurable: true
        },
        dom: 'Bfrtip',
        stateSave: false,
        buttons: [

            {
                text: '<i class="far fa-plus-square"></i> Agregar Insumo',
                titleAttr: "Insertar",
                action: function () {
                    var modal = $('#exampleModal');
                    modal.find('.modal-title').text('Registrar Insumo')
                    modal.find('#nuevo_ins').show()
                    modal.find('#modificar_ins').hide()
                    modal.find('#id_ins').val('');
                    modal.find('#nom_ins').val('');
                    modal.find('#und_ins').val('');
                    modal.find('#cost_ins').val('');
                    modal.find('#fec_ins').val('');
                    modal.modal("show");
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Insumo',
                titleAttr: "Modificar",
                action: function () {
                    var modal = $('#exampleModal');
                    var insumo = table_insumo.row({ selected: true }).data();
                    if (insumo == null) {
                        alert("Seleccione un insumo para modificar");
                    }
                    else {
                        console.log(insumo["id_ins"]);
                        modal.find('.modal-title').text('Modificar Insumo')
                        modal.find('#nuevo_ins').hide()
                        modal.find('#modificar_ins').show()

                        modal.find('#id_ins').val(insumo["id_ins"]);
                        modal.find('#nom_ins').val(insumo["nom_ins"]);
                        modal.find('#und_ins').val(insumo["unidad_ins"]);
                        modal.find('#cost_ins').val(insumo["cost_ins"]);
                        modal.find('#fec_ins').val(insumo["fec_ins"]);
                        modal.modal("show");
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Insumo',
                titleAttr: "Eliminar",
                action: function () {
                    var insumo = table_insumo.row({ selected: true }).data();
                    if (insumo == null) {
                        alert("Seleccione un insumo para eliminar");
                    }
                    else {
                        eliminar_insumo(insumo['id_ins'], insumo['nom_ins'], insumo['cost_ins']);
                    }

                }
            },
        ],

        columns: [

            { data: 'nom_ins' },
            { data: 'unidad_ins' },
            { data: 'cost_ins' },
            { data: 'fecha_actu_ins' },


        ],
    });

    // MANEJO DEL MODAL
    // $('#exampleModal').on('show.bs.modal', function (event) {
    //     var button = $(event.relatedTarget) // Button that triggered the modal
    //     var id = button.data('id') // Extract info from data-* attributes
    //     var nom = button.data('nom')
    //     var cost = button.data('cost')
    //     var und = button.data('und')
    //     var fecha = button.data('fecha')
    //     var modal = $(this)
    //     if (button.data('mod')) {
    //         modal.find('.modal-title').text('Modificar Insumo')
    //         modal.find('#nuevo_ins').hide()
    //         modal.find('#modificar_ins').show()

    //     } else {
    //         modal.find('.modal-title').text('Registrar Insumo')
    //         modal.find('#nuevo_ins').show()
    //         modal.find('#modificar_ins').hide()
    //     }
    //     // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    //     // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.

    //     modal.find('#id_ins').val(id)
    //     modal.find('#nom_ins').val(nom)
    //     modal.find('#cost_ins').val(cost)
    //     modal.find('#und_ins').val(und)
    //     modal.find('#fec_ins').val(fecha)
    // });
    // aqui carga los insumos
    // get_insumo();

    // Iterrar todos los insumos
    // setInterval(get_insumo,10000);


});
// Eliminar un Insumo
function eliminar_insumo(id, nom, cost) {
    // console.log('Eliminar insumo',id,nom,cost);

    var r = confirm("Desea Eliminar el Isumo: \n Nombre: " + nom + "\n Costo de Insumo: " + cost);
    if (r == true) {
        $.ajax({
            method: "POST",
            url: "delete/insumo",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: { 'id': id },
            success: function (result) {
                // var x = JSON.parse(result);
                // console.log(X);

            }

        }).done(setTimeout(function () { $('#tab_logic').DataTable().ajax.reload() }, 500)).always(function () {
            alert("completado");
        });

    }


}
// -------------------------------------------


// Obtener todos los Insumos
function get_insumo() {
    console.log("Get insumo");
    if (i > 1) {
        $(".tbody-ins").html('');
        i = 1;
        $('#tab_logic').append('<tr id="addr' + (i) + '"></tr>');
    }

    $.ajax({
        method: "get",
        url: "insumo",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var insumo = JSON.parse(result);
            // console.log(insumo);
            for (const key in insumo) {
                if (insumo.hasOwnProperty(key)) {
                    const element = insumo[key];
                    // console.log(element['nom_ins']);
                    // $('#addr' + i).html("<td>" + (i) + "</td><td>" + element['nom_ins'] + " </td><td>" + element['unidad_ins'] + "</td><td>" + element['cost_ins'] + "</td><td>" + element['fecha_actu_ins'] + "</td><td><button id='btn_eliminar' name='btn_eliminar' type='button' class='form-control input-md fa fa-trash-o' value='' onClick=\"eliminar_insumo( " + element['id_ins'] + ",'" + element['nom_ins'] + "', " + element['cost_ins'] + ")\" ></button></td><td><button id='btn_modificar' name='btn_modificar' type='button' class='form-control input-md fa fa-pencil-square-o' data-toggle='modal' data-target='#exampleModal' value='' data-id='" + element['id_ins'] + "' data-nom='" + element['nom_ins'] + "' data-cost='" + element['cost_ins'] + "' data-und='" + element['unidad_ins'] + "' data-fecha='" + element['fecha_actu_ins'] + "' data-mod='mod'></button></td>");
                    // $('#tab_logic').append('<tr id="addr' + (i + 1) + '"></tr>');
                    $('.tbody-ins').append("<tr><td>" + element['nom_ins'] + " </td><td>" + element['unidad_ins'] + "</td><td>" + element['cost_ins'] + "</td><td>" + element['fecha_actu_ins'] + "</td><td><button id='btn_eliminar' name='btn_eliminar' type='button' class='form-control input-md fa fa-trash-o' value='' onClick=\"eliminar_insumo( " + element['id_ins'] + ",'" + element['nom_ins'] + "', " + element['cost_ins'] + ")\" ></button></td><td><button id='btn_modificar' name='btn_modificar' type='button' class='form-control input-md fa fa-pencil-square-o' data-toggle='modal' data-target='#exampleModal' value='' data-id='" + element['id_ins'] + "' data-nom='" + element['nom_ins'] + "' data-cost='" + element['cost_ins'] + "' data-und='" + element['unidad_ins'] + "' data-fecha='" + element['fecha_actu_ins'] + "' data-mod='mod'></button></td></tr>");
                    i++;
                }
            }
        }
    });

    setTimeout(function () {
        $('#tab_logic').DataTable({
            "scrollY": "40vh",
            "scrollCollapse": true,
        });
    }, 300);


}
// ---------------------------------------------------------------------
// INSERTAR INSUMO
function insertar_insumo() {
    // console.log('Eliminar insumo',id,nom,cost);
    var nom = $('#nom_ins').val();
    var cost = $('#cost_ins').val();
    var und = $('#und_ins').val();
    var fec = $('#fec_ins').val();


    $.ajax({
        method: "POST",
        url: "insert/insumo",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'nom_ins': nom,
            'cost_ins': cost,
            'unidad_ins': und,
            'fec_ins': fec
        },
        success: function (result) {
            var x = JSON.parse(result);
            // console.log(X);

        }

    }).done(setTimeout(function () { $('#tab_logic').DataTable().ajax.reload() }, 500)).always(function () {
        $('#cerrar-ins').trigger('click');
        alert("completado");
    });
}
// -----------------------------------
// MODIFICAR INSUMO
function modificar_insumo() {
    // console.log('Eliminar insumo',id,nom,cost);
    var id = $('#id_ins').val();
    var nom = $('#nom_ins').val();
    var cost = $('#cost_ins').val();
    var und = $('#und_ins').val();
    var fec = $('#fec_ins').val();


    $.ajax({
        method: "POST",
        url: "update/insumo",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'id_ins': id,
            'nom_ins': nom,
            'cost_ins': cost,
            'unidad_ins': und,
            'fec_ins': fec
        },
        success: function (result) {
            var x = JSON.parse(result);
            console.log(X);

        }

    }).done(setTimeout(function () { $('#tab_logic').DataTable().ajax.reload() }, 500)).always(function () {
        $('#cerrar-ins').trigger('click');
        alert("completado");
    });
}
