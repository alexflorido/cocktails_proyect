var table_evento;
var evento
$(document).ready(function () {
    table_evento = $('#tab_eve').DataTable({
        select: {
            style: 'single',
            blurable: true
        },

        stateSave: true,
        // "paging": true,
        // "ordering": true,
        // "info": false,
        dom: 'Bfrtip',
        ajax: {
            method: "GET",
            url: "evento_view",
            dataSrc: ""

        },
        columns: [

            { data: 'nro_factura' },
            { data: 'nom_eve' },
            { data: 'cost_eve_total' },
            { data: 'IUE' },
            { data: 'IT' },
            { data: 'IVA' },
            { data: 'total_imp' },
            { data: 'total_facturado' },
        ],
        columnDefs: [
            {
                targets: [0],
                data: 'nom_eve',
                render: function (data, type, row) {
                    return "<span class='text-center'><i class='fa fa-hashtag'></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + data + "</span><br>" +
                        "<span class='text-center'><i class='fa fa-calendar'></i>" + row.fecha_eve + "</span>"

                }

            },
            {
                targets: [1],
                data: 'nro_factura',
                render: function (data, type, row) {
                    return "<span class='text-center'><i class='fa fa-file-o'></i>&nbsp;" + data + "</span><br>" +
                        "<span class='text-center'><i class='fa fa-calendar'></i>" + row.nom_cli + "</span>"

                }

            },
        ],
        buttons: [

            {
                text: '<i class="far fa-plus-square"></i> Agregar Evento',
                titleAttr: "Insertar",
                action: function () {
                    var r = confirm('Desea Crear un Nuevo evento?');
                    if (r == true) {
                        $('#evento_nombre').val('');
                        $('#is_new').val(1);
                        $('#datos').find('#id_eve').val('');
                        $('#event_date').val('');
                        $('#pills-proforma-tab').trigger('click');
                    }
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Evento',
                titleAttr: "Modificar",
                action: function () {
                    var modal = $('#ModalModEvento');
                    evento = table_evento.row({ selected: true }).data();
                    if (evento == null) {
                        alert("Seleccione un evento para modificar");
                    }
                    else {
                        modal.find('#id_eve_mod').val(evento["id_eve"]);
                        modal.find('#id_eve').val(evento["id_eve"]);
                        modal.find('#nom_eve').val(evento["nom_eve"]);
                        modal.find('#nro_factura').val(evento["nro_factura"]);
                        modal.find('#fecha_eve').val(evento["fecha_eve"]);
                        modal.find('#total_facturado').val(evento["total_facturado"]);
                        get_proformas(evento["id_eve"]);
                        modal.modal("show");
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Evento',
                titleAttr: "Eliminar",
                action: function () {
                    var id_grp = readCookie('id_grp');
                    console.log('grp ' + id_grp);
                    if (id_grp == 1) {
                        var evento = table_evento.row({ selected: true }).data();
                        if (evento == null) {
                            alert("Seleccione un evento para eliminar");
                        }
                        else {
                            eliminar_Evento(evento['id_eve'], evento['nom_eve']);
                        }
                    } else {
                        alert('No se puede realizar esta acción por que no es Administrador');
                    }

                }
            },
            {
                extend: 'excel',
                title: 'Excel_Evento_report',
                text: 'Excel'
            },


        ],
    });






})
function readCookie(name) {

    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;

}
$(document).on('click', '#btn_new_prof', function () {
    $('#evento_nombre').val($("#nom_eve").val());
    $('#is_new').val(0);
    $('#datos').find('#id_eve').val($("#id_eve").val());
    $('#event_date').val($("#fecha_eve").val());
    $('#ModalModEvento').modal('hide');
    $('#pills-proforma-tab').trigger('click');
})
function eliminar_Evento(id_eve, nom_eve) {

    var r = confirm("Desea Eliminar El Evento: \n Nombre: " + nom_eve + "?");
    if (r == true) {
        $.ajax({
            method: "post",
            url: "delete/event",
            data: {
                'id_eve': id_eve,
            },
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            success: function (result) {
                alert("Evento Eliminado")
            }
        }).done(setTimeout(function () {

            $('#tab_eve').DataTable().ajax.reload();

        }, 1000));

    }
}
// Modificar Evento
$("#modal_mod_event_form").submit(function (event) {
    event.preventDefault();
    var formData = new FormData($(this)[0]);
    // console.log("rec modal");
    $.ajax({
        url: "update/event",
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {

        }
    }).done(setTimeout(function () {
        $('#tab_eve').DataTable().ajax.reload();
        alert("Se Modifico Correctamente");
        $('#ModalModEvento').modal('hide');

    }, 1000));

})
// $(document).on('click', '#btn_new_prof', function (e) {

//     $.post('new_proform', { id_eve : evento["id_eve"] })




// })

function get_proformas(id_eve) {
    $('#table-prof-mod-body').empty();
    $.ajax({
        method: "post",
        url: "get/proforma_byID",
        data: {
            'id_eve': id_eve,
        },
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            var proforma = JSON.parse(result);
            var color;
            var texto;
            console.log(proforma);
            for (const key in proforma) {
                if (proforma.hasOwnProperty(key)) {
                    const element = proforma[key];
                    var estado;
                    if (element['estado_prof'] == 0) {
                        estado = "No Aprobado";
                        color = "warning";

                    } else {
                        estado = "Aprobado";
                        color = "success"
                    }
                    // console.log(element);
                    $('#table-prof-mod-body').append(`<tr><input type='hidden' name='id_prof' value='${element['id_prof']}'>
                    <td>${element['num_prof']}</td>
                    <td>${element['fecha_prof']}</td>
                    <td>${element['sub_total_prof']}</td>
                    <td>${element['iva_prof']}</td>
                    <td class='total_general_prof'>${element['total_general_prof']}</td>
                    <td class="estado bg-${color}" style='color: white !important'>${estado}</td>
                    <td><button name='btn_eliminar_prof' type='button' class='form-control input-md'><i class="far fa-trash-alt"></i></button></td>
                    <td><button name='btn_modificar_prof' type='button' class='form-control input-md'><i class="far fa-edit"></i><i class="fas fa-check d-none"></i></button></td>
                    </tr>`);

                }
            }
        }
    })
}
$(document).on('click', 'button[name="btn_modificar_prof"]', function () {
    var estado;
    var id_prof = $(this).parents('tr').find('input[name="id_prof"]').val();
    var total_general_prof = $(this).parents('tr').find('.total_general_prof').html();
    if ($(this).parents('tr').find('.estado').html() == 'No Aprobado') {
        estado = 1;
    } else {
        estado = 0;
    }

    $.ajax({
        method: "post",
        url: "update/proforma_estado",
        data: {
            'estado': estado,
            'id_prof': id_prof,
            'total_general_prof': total_general_prof,
            'id_eve': $('#id_eve').val()
        },
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {

        }
    }).done(setTimeout(function () {
        get_proformas(evento["id_eve"]);
        $('#tab_eve').DataTable().ajax.reload();

    }, 1000));

});

