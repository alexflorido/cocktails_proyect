$(document).ready(function () {

    // DataTable Code
    var table = $('#tab_cli').DataTable({
        select: {
            style: 'single',
            blurable: true
        },

        stateSave: true,
        // "paging": true,
        // "ordering": true,
        // "info": false,
        dom: 'Bfrtip',

        ajax: {
            method: "GET",
            url: "cliente",
            dataSrc: ""

        },
        columns: [
            { data: 'nom_cli' },
            { data: 'nit_cli' },
            { data: 'tel_cli' },
            { data: 'email_cli' },
            // { data: 'id_cli' }
        ],
        columnDefs: [
            {
                targets: [2],
                visible: 'false',
                searchable: 'false'
            }

        ],
        buttons: [

            {
                text: '<i class="far fa-plus-square"></i> Agregar Cliente',
                titleAttr: "Insertar",
                action: function () {
                    var modal = $('#ModalCliente');
                    modal.modal("show");
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Cliente',
                titleAttr: "Modificar",
                action: function () {
                    var modal = $('#ModalCliente');
                    var cliente = table.row({ selected: true }).data();
                    var id_grp = readCookie('id_grp');
                    console.log('grp ' + id_grp);
                    if (id_grp == 1) {
                        if (cliente == null) {
                            alert("Seleccione un cliente para modificar");
                        }
                        else {
                            modal.find('#id_cli').val(cliente["id_cli"]);
                            modal.find('#nom_cli').val(cliente["nom_cli"]);
                            modal.find('#nit_cli').val(cliente["nit_cli"]);
                            modal.find('#tel_cli').val(cliente["tel_cli"]);
                            modal.find('#email_cli').val(cliente["email_cli"]);
                            modal.modal("show");
                        }
                    } else {
                        alert('No se puede realizar esta acción por que no es Administrador');
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Cliente',
                titleAttr: "Eliminar",
                action: function () {
                    var id_grp = readCookie('id_grp');
                    console.log('grp ' + id_grp);
                    if (id_grp == 1) {
                        var cliente = table.row({ selected: true }).data();
                        if (cliente == null) {
                            alert("Seleccione un cliente para eliminar");
                        }
                        else {
                            eliminar_cliente(cliente['id_cli'], cliente['nom_cli'], cliente['tel_cli'], cliente['email_cli']);
                        }
                    } else {
                        alert('No se puede realizar esta acción por que no es Administrador');
                    }

                }
            },
            {
                text: '<i class="far fa-address-card"></i> Ver Persona de Contacto',
                titleAttr: "Persona de Contacto",
                action: function () {
                    console.log("Get persona contacto");
                    var modal = $('#ModalPersonaContacto');
                    var cliente = table.row({ selected: true }).data();
                    if (cliente == null) {
                        alert("Seleccione un cliente para ver sus personas de contacto");
                    }
                    else {
                        $('#id_cli_pc').val(cliente["id_cli"]);
                        get_persona_contacto(cliente["id_cli"]);
                        modal.modal("show");
                    }
                }
            },

        ],

    });
    // ///////////////////////////////////////





    $('#msg_error_cli').hide();
    // Mostrar Personal de Contacto
    function get_persona_contacto(id_cli) {
        $("#accordion-boddy").html('');
        $.ajax({
            method: "GET",
            url: "persona_contacto",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            data: { 'id_cli': id_cli },
            success: function (result) {

                var personaCont = JSON.parse(result);
                // console.log(x);
                for (const key in personaCont) {
                    if (personaCont.hasOwnProperty(key)) {
                        const element = personaCont[key];
                        console.log(element);
                        // console.log(element['nom_per_cont'],"hola");
                        $("#accordion-boddy").append('<div class="accordion-item"><form class="form_per_cont" role="form" method="POST">' +
                            '<h2 class="accordion-header" id="' + element['id_per_cont'] + '">' +
                            '<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#c' + element['id_per_cont'] + '" aria-expanded="false" >' + element['nom_per_cont'] + ' ' + element['app_per_cont'] + '</button></h2>' +
                            '<div id="c' + element['id_per_cont'] + '" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionPersonaContacto">' +
                            '<div class="accordion-body" id="acordeon">' +
                            //  Inicio Primera Fila
                            '<div class="mx-auto row">' +
                            '<input type="hidden" class="form-control id_per_cont" name="id_per_cont" value="' + element['id_per_cont'] + '"><input type="hidden" class="form-control" name="id_cli" value="' + element['id_cli'] + '">' +
                            '<strong>Informacion de la Persona de Contacto &nbsp;&nbsp;<input type="checkbox" class="form-check-input modificar_campos"><label class="form-check-label"> &nbsp;Editar Informacion</label></strong>' +
                            '<div class="offset-col-1 col-8 col-sm-6">' +
                            '<div class="form-group">' +
                            '<label for="recipient-name" class="col-form-label">Nombre:</label>' +
                            '<input type="text" class="form-control nom_per_cont"name="nom_per_cont" disabled value="' + element['nom_per_cont'] + '">' +
                            '</div>' +
                            '</div>' +
                            '<div class="offset-col-1 col-4 col-sm-6">' +
                            '<div class="form-group">' +
                            '<label for="recipient-name" class="col-form-label">Apellido:</label>' +
                            '<input type="text" class="form-control app_per_cont" name="app_per_cont" disabled value="' + element['app_per_cont'] + '">' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            // END primera fila 
                            '<div class="mx-auto row">' +
                            '<div class="offset-col-1 col-8 col-sm-6">' +
                            '<div class="form-group">' +
                            '<label for="recipient-name" class="col-form-label">Telefono:</label>' +
                            '<input type="text" class="form-control tel_per_cont" name="tel_per_cont" disabled value="' + element['tel_per_cont'] + '">' +
                            '</div>' +
                            '</div>' +
                            '<div class="offset-col-1 col-4 col-sm-6">' +
                            '<div class="form-group">' +
                            '<label for="recipient-name" class="col-form-label">Correo Electronico:</label>' +
                            '<input type="text" class="form-control email_per_cont" name="email_per_cont" disabled value="' + element['email_per_cont'] + '">' +
                            '</div>' +
                            '</div>' +
                            '</div>' +
                            // END Segunda fila
                            '<br>' +
                            '<div class="mx-auto row">' +
                            '<div class="offset-col-1 col-8 col-sm-3">' +
                            '<input type="submit" class="btn btn-warning modf_per_cont" value="Modificar" name="modf_per_cont">' +
                            '</div>' +
                            '<div class="offset-col-1 col-8 col-sm-6">' +
                            '<input type="button" class="btn btn-danger elim_per_cont" value="Eliminar" name="elim_per_cont">' +
                            '</div>' +
                            '</form>' +
                            '</div>' +
                            // END Tercera Fila
                            '</div>' +
                            '</div>' +
                            '</div>');

                    }
                }


            }

        })
    }
    // End Mostrar Personal de contacto








    // Cambiar estado CheckBox para modificar informacion de persona de contacto
    $(document).on("change", ".modificar_campos", function () {
        var nombre = $(this).parent().parent().find(".nom_per_cont");
        var apellido = $(this).parent().parent().find(".app_per_cont");
        var telefono = $(this).parent().parent().parent().find(".tel_per_cont");
        var email = $(this).parent().parent().parent().find(".email_per_cont");
        if ($(this).is(':checked')) {
            nombre.removeAttr('disabled');
            apellido.removeAttr('disabled');
            telefono.removeAttr('disabled');
            email.removeAttr('disabled');
        } else {

            nombre.attr('disabled', 'disabled');
            apellido.attr('disabled', 'disabled');
            telefono.attr('disabled', 'disabled');
            email.attr('disabled', 'disabled');

            // console.log($(this).parent().parent().parent().html());

        }


    });
    // ---------------------------





    // Insertar nueva persona de contacto
    $(document).on("submit", ".form_per_cont", function (event) {
        // $('.form_per_cont').submit(function (event) {
        event.preventDefault();
        $('#list-errors_cli').html('');

        var formData = new FormData($(this)[0]);
        // var url = $(this).attr('action');

        // Si el formulario no tiene Id_per debe registrar nuevo usuario
        if (formData.get('id_per_cont') == null) {
            console.log('estoy en el if');
            $.ajax({
                url: "insert/per_cont",
                type: $(this).attr('method'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (respuesta) {
                    // console.log(respuesta,'respuesta');


                    var r = JSON.parse(respuesta);
                    console.log(r);

                    if (r != 1) {
                        $('#msg_error_cli').show();
                        for (const key in r) {
                            if (r.hasOwnProperty(key)) {
                                const element = r[key];
                                // console.log(element);
                                $('#list-errors_cli').append("<li>" + element + "</li>");
                            }
                        }
                    }
                    else {
                        $('#msg_error_cli').hide();
                    }
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + error + ")");
                    alert(err.Message);
                }
            }).done(function () {
                alert("Se Registro Correctamente");
                $('.app_per_cont').val('');
                $('.tel_per_cont').val('');
                $('.nom_per_cont').val('');
                $('.email_per_cont').val('');
                get_persona_contacto(formData.get('id_cli'));

            });
        }
        // Modificar usuario
        // Si el formulario tiene id_per debe enviar los datos para modificar un usuario existente
        else {

            $.ajax({
                url: "modificar/per_cont",
                type: $(this).attr('method'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (respuesta) {
                    // console.log(respuesta,'respuesta');


                    var r = JSON.parse(respuesta);
                    // console.log(r,'json');

                    if (r != 1) {
                        $('#msg_error_cli').show();
                        for (const key in r) {
                            if (r.hasOwnProperty(key)) {
                                const element = r[key];
                                // console.log(element);
                                $('#list-errors_cli').append("<li>" + element + "</li>");


                            }
                        }

                    }
                    else {
                        $('#msg_error_cli').hide();


                    }


                },
                error: function (xhr, status, error) {
                    var err = eval("(" + error + ")");
                    alert(err.Message);
                }


            }).done(function () {
                alert("Se Modifico Correctamente cliente");
                get_persona_contacto(formData.get('id_cli'));
            });
        }
    });
    // END INSERTAR

    // Eliminar persona de contacto
    $(document).on('click', ".elim_per_cont", function (event) {
        event.preventDefault();
        var id = $(this).parent().parent().parent().find('.id_per_cont').val();
        $.ajax({
            method: "POST",
            url: "eliminar/per_cont",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            data: { 'id_per_cont': id },
            success: function (result) {

            }
        }).done(function () {
            alert("Se Borro la persona de contacto");
            get_persona_contacto(formData.get('id_cli'));

        });


    });
    // END eliminar persona
    function readCookie(name) {

        return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;

    }
    function show_spin(button, spin, not_spin) {
        $("." + spin).removeClass("d-none");
        $("." + not_spin).addClass("d-none");
        $("#" + button).attr("disabled", "disabled")
    }
    function hide_spin(button, spin, not_spin) {
        $("." + not_spin).removeClass("d-none");
        $("." + spin).addClass("d-none");
        $("#" + button).attr("disabled", false);
    }



    // Insertar nuevo cliente
    $('#modal_form_client').submit(function (event) {
        event.preventDefault();
        console.log("client modal");
        $('#list-errors_cli').html('');
        var formData = new FormData($(this)[0]);
        // var url = $(this).attr('action');

        // Si el formulario no tiene Id_per debe registrar nuevo usuario
        if (formData.get('id_cli') == "") {
            show_spin("btn_client_sub", "spin_client", "not_spin_client");
            $.ajax({
                url: "insert/cliente",
                type: $(this).attr('method'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (respuesta) {
                    // console.log(respuesta,'respuesta');


                    var r = JSON.parse(respuesta);
                    // console.log(r,'json');

                    if (r != 1) {
                        $('#msg_error_cli').show();
                        for (const key in r) {
                            if (r.hasOwnProperty(key)) {
                                const element = r[key];
                                // console.log(element);
                                $('#list-errors_cli').append("<li>" + element + "</li>");
                            }
                        }
                    }
                    else {
                        $('#msg_error_cli').hide();
                    }
                },
                error: function (xhr, status, error) {
                    var err = eval("(" + error + ")");
                    alert(err.Message);
                }
            }).done(function () {
                hide_spin("btn_client_sub", "spin_client", "not_spin_client");
                alert("Se Registro Correctamente");
                setTimeout(function () { $('#tab_cli').DataTable().ajax.reload() });
            });
        }
        // Modificar usuario
        // Si el formulario tiene id_per debe enviar los datos para modificar un usuario existente
        else {

            show_spin("btn_client_sub", "spin_client", "not_spin_client");
            $.ajax({
                url: "modificar/cliente",
                type: $(this).attr('method'),
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (respuesta) {
                    // console.log(respuesta,'respuesta');


                    var r = JSON.parse(respuesta);
                    // console.log(r,'json');

                    if (r != 1) {
                        $('#msg_error_cli').show();
                        for (const key in r) {
                            if (r.hasOwnProperty(key)) {
                                const element = r[key];
                                // console.log(element);
                                $('#list-errors_cli').append("<li>" + element + "</li>");


                            }
                        }

                    }
                    else {
                        $('#msg_error_cli').hide();
                        hide_spin("btn_client_sub", "spin_client", "not_spin_client");
                        setTimeout(function () { $('#tab_cli').DataTable().ajax.reload() });
                        alert("Se Modifico Cliente Correctamente");

                    }


                },
                error: function (xhr, status, error) {
                    var err = eval("(" + error + ")");
                    alert(err.Message);
                }


            })
        }
    });
    // END INSERTAR
    // ELIMINAR CLIENTE
    function eliminar_cliente(id_cli, nom_cli, tel_cli, email_cli) {
        var r = confirm("Desea Eliminar este Cliente? : \n Nombre: " + nom_cli + "\n Telefono: " + tel_cli + "\n Correo: " + email_cli);
        if (r == true) {
            $.ajax({
                method: "POST",
                url: "delete/cliente",
                headers: { 'X-Requested-With': 'XMLHttpRequest' },
                dataType: 'json',
                data: { 'id_cli': id_cli },
                success: function (result) {
                    // var x = JSON.parse(result);
                    // console.log(X);

                }

            }).done(setTimeout(function () { $('#tab_cli').DataTable().ajax.reload() }, 500)).always(function () {
                alert("completado");
            });

        }


    };
    // END ELIMINAR

});