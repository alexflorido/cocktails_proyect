$(document).ready(function () {
    $('#msg_error_prov').hide();

    // DataTable Code
    var table = $('#tab_prov').DataTable({
        select: {
            style: 'single',
            blurable: true
        },

        stateSave: true,
        dom: 'Bfrtip',

        ajax: {
            method: "GET",
            url: "proveedor",
            dataSrc: ""

        },
        columns: [
            { data: 'nom_prov' },
            { data: 'desc_prov' },
            { data: 'nit_prov' },
            { data: 'raz_soc' },

        ],
        columnDefs: [
            {
                targets: [2],
                visible: 'false',
                searchable: 'false'
            }

        ],
        buttons: [
            {
                text: '<i class="far fa-plus-square"></i> Agregar Proveedor',
                titleAttr: "Insertar",
                className: "btn-success",
                action: function () {
                    var modal = $('#ModalProveedor');
                    modal.modal("show");
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Proveedor',
                titleAttr: "Modificar",
                action: function () {
                    var modal = $('#ModalProveedor');
                    var proveedor = table.row({ selected: true }).data();
                    if (proveedor == null) {
                        alert("Seleccione un cliente para modificar");
                    }
                    else {
                        modal.find('#id_prov').val(proveedor["id_prov"]);
                        modal.find('#nom_prov').val(proveedor["nom_prov"]);
                        modal.find('#desc_prov').val(proveedor["desc_prov"]);
                        modal.find('#nit_prov').val(proveedor["nit_prov"]);
                        modal.find('#raz_soc').val(proveedor["raz_soc"]);
                        modal.modal("show");
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Proveedor',
                titleAttr: "Eliminar",
                action: function () {
                    var id_grp = readCookie('id_grp');
                    console.log('grp ' + id_grp);
                    if (id_grp == 1) {
                        var proveedor = table.row({ selected: true }).data();
                        if (proveedor == null) {
                            alert("Seleccione un cliente para modificar");
                        }
                        else {
                            eliminar_proveedor(proveedor['id_prov'], proveedor['nom_prov'], proveedor['desc_prov'], proveedor['nit_prov']);
                        }
                    } else {
                        alert('No se puede realizar esta acción por que no es Administrador');
                    }
                }
            },
            {
                text: '<i class="far fa-address-card"></i> Ver Persona de Contacto',
                titleAttr: "Persona de Contacto",
                action: function () {
                    console.log("Get persona contacto");
                    var modal = $('#ModalPersonaContProv');

                    var proveedor = table.row({ selected: true }).data();
                    if (proveedor == null) {
                        alert("Seleccione un cliente para modificar");
                    }
                    else {
                        $('#id_prov_pc').val(proveedor["id_prov"]);
                        get_persona_contacto(proveedor["id_prov"]);
                        modal.modal("show");
                    }


                }
            }
        ],

    });
    // ///////////////////////////////////////
});

function readCookie(name) {

    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;

}




// Mostrar Personal de Contacto
function get_persona_contacto(id_prov) {
    $("#accordion-boddy-prov").html('');
    $.ajax({
        method: "GET",
        url: "contacto_proveedor",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        data: { 'id_prov': id_prov },
        success: function (result) {


            var personaCont = JSON.parse(result);
            // console.log(x);
            for (const key in personaCont) {
                if (personaCont.hasOwnProperty(key)) {
                    const element = personaCont[key];
                    console.log(element);

                    $("#accordion-boddy-prov").append('<div class="accordion-item"><form class="form_cont_prov" role="form" method="POST">' +
                        '<h2 class="accordion-header" id="' + element['id_cont_prov'] + '">' +
                        '<button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#c' + element['id_cont_prov'] + '" aria-expanded="false" >' + element['nombre_cont'] + '</button></h2>' +
                        '<div id="c' + element['id_cont_prov'] + '" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionPersonaContacto">' +
                        '<div class="accordion-body" id="acordeon">' +
                        //  Inicio Primera Fila
                        '<div class="mx-auto row">' +
                        '<input type="hidden" class="form-control id_cont_prov" name="id_cont_prov" value="' + element['id_cont_prov'] + '"><input type="hidden" class="form-control" name="id_prov" value="' + element['id_prov'] + '">' +
                        '<strong>Informacion de la Persona de Contacto &nbsp;&nbsp;<input type="checkbox" class="form-check-input modificar_campos_prov"><label class="form-check-label"> &nbsp;Editar Informacion</label></strong>' +
                        '<div class="offset-col-1 col-8 col-sm-6">' +
                        '<div class="form-group">' +
                        '<label for="recipient-name" class="col-form-label">Nombre:</label>' +
                        '<input type="text" class="form-control nombre_cont"name="nombre_cont" disabled value="' + element['nombre_cont'] + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="offset-col-1 col-4 col-sm-6">' +
                        '<div class="form-group">' +
                        '<label for="recipient-name" class="col-form-label">Estado:</label>' +
                        '<input type="text" class="form-control estado" name="estado" disabled value="' + element['estado'] + '">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        // END primera fila 
                        '<div class="mx-auto row">' +
                        '<div class="offset-col-1 col-8 col-sm-6">' +
                        '<div class="form-group">' +
                        '<label for="recipient-name" class="col-form-label">Telefono:</label>' +
                        '<input type="text" class="form-control telf_cont" name="telf_cont" disabled value="' + element['telf_cont'] + '">' +
                        '</div>' +
                        '</div>' +
                        '<div class="offset-col-1 col-4 col-sm-6">' +
                        '<div class="form-group">' +
                        '<label for="recipient-name" class="col-form-label">Correo Electronico:</label>' +
                        '<input type="text" class="form-control email_cont" name="email_cont" disabled value="' + element['email_cont'] + '">' +
                        '</div>' +
                        '</div>' +
                        '</div>' +
                        // END Segunda fila
                        '<br>' +
                        '<div class="mx-auto row">' +
                        '<div class="offset-col-1 col-8 col-sm-3">' +
                        '<input type="submit" class="btn btn-warning modf_cont_prov" value="Modificar" name="modf_cont_prov">' +
                        '</div>' +
                        '<div class="offset-col-1 col-8 col-sm-6">' +
                        '<input type="button" class="btn btn-danger elim_cont_prov" value="Eliminar" name="elim_cont_prov">' +
                        '</div>' +
                        '</form>' +
                        '</div>' +
                        // END Tercera Fila
                        '</div>' +
                        '</div>' +
                        '</div>');

                }
            }


        }

    })
}
// End Mostrar Personal de contacto








// Cambiar estado CheckBox para modificar informacion de persona de contacto
$(document).on("change", ".modificar_campos_prov", function () {
    var nombre = $(this).parent().parent().find(".nombre_cont");
    var apellido = $(this).parent().parent().find(".estado");
    var telefono = $(this).parent().parent().parent().find(".telf_cont");
    var email = $(this).parent().parent().parent().find(".email_cont");
    if ($(this).is(':checked')) {
        nombre.removeAttr('disabled');
        apellido.removeAttr('disabled');
        telefono.removeAttr('disabled');
        email.removeAttr('disabled');
    } else {

        nombre.attr('disabled', 'disabled');
        apellido.attr('disabled', 'disabled');
        telefono.attr('disabled', 'disabled');
        email.attr('disabled', 'disabled');

        // console.log($(this).parent().parent().parent().html());

    }


});
// ---------------------------





// Insertar nueva persona de contacto
$(document).on("submit", ".form_cont_prov", function (event) {
    // $('.form_per_cont').submit(function (event) {
    event.preventDefault();
    $('#list-errors_cli').html('');
    var formData = new FormData($(this)[0]);
    // var url = $(this).attr('action');

    // Si el formulario no tiene Id_per debe registrar nuevo usuario
    if (formData.get('id_cont_prov') == null) {
        console.log('estoy en el if');
        $.ajax({
            url: "insert/cont_prov",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');


                var r = JSON.parse(respuesta);
                console.log(r);

                if (r != 1) {
                    $('#msg_error_cli').show();
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list-errors_cli').append("<li>" + element + "</li>");
                        }
                    }
                }
                else {
                    $('#msg_error_cli').hide();
                }
            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }
        }).done(function () {
            alert("Se Registro Correctamente");
            $('.nombre_cont').val('');
            $('.estado').val('');
            $('.telf_cont').val('');
            $('.email_cont').val('');
            get_persona_contacto(formData.get('id_prov'));

        });
    }
    // Modificar usuario
    // Si el formulario tiene id_per debe enviar los datos para modificar un usuario existente
    else {

        $.ajax({
            url: "modificar/cont_prov",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');


                var r = JSON.parse(respuesta);
                // console.log(r,'json');

                if (r != 1) {
                    $('#msg_error_cli').show();
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list-errors_cli').append("<li>" + element + "</li>");


                        }
                    }

                }
                else {
                    $('#msg_error_cli').hide();


                }


            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }


        }).done(function () {
            alert("Se Modifico Correctamente Proveedor");
            get_persona_contacto(formData.get('id_prov'));
        });
    }
});
// END INSERTAR

// Eliminar persona de contacto
$(document).on('click', ".elim_cont_prov", function (event) {
    event.preventDefault();
    var id = $(this).parent().parent().parent().find('.id_cont_prov').val();
    $.ajax({
        method: "POST",
        url: "delete/cont_prov",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        data: { 'id_cont_prov': id },
        success: function (result) {

        }
    }).done(function () {
        alert("Se Borro el Contacto Correctamente");
        get_persona_contacto(formData.get('id_prov'));
    });


});
// END eliminar persona
function show_spin(button, spin, not_spin) {
    $("." + spin).removeClass("d-none");
    $("." + not_spin).addClass("d-none");
    $("#" + button).attr("disabled", "disabled")
}
function hide_spin(button, spin, not_spin) {
    $("." + not_spin).removeClass("d-none");
    $("." + spin).addClass("d-none");
    $("#" + button).attr("disabled", false);
}

// Insertar nuevo Proveedor
$('#form_prov').submit(function (event) {
    event.preventDefault();
    console.log("estoy aqui");
    $('#list-errors_prov').html('');
    var formData = new FormData($(this)[0]);


    // Si el formulario no tiene Id_per debe registrar nuevo usuario
    if (formData.get('id_prov') == "") {
        show_spin("btn_prov_sub", "spin_prov", "not_spin_prov");
        $.ajax({
            url: "insert/proveedor",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');


                var r = JSON.parse(respuesta);
                // console.log(r,'json');

                if (r != 1) {
                    $('#msg_error_prov').show();
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list-errors_prov').append("<li>" + element + "</li>");
                        }
                    }
                }
                else {
                    $('#msg_error_prov').hide();
                }
            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }
        }).done(function () {
            hide_spin("btn_prov_sub", "spin_prov", "not_spin_prov");
            $("#nom_prov").val('');
            $("#desc_prov").val('');
            $("#nit_prov").val('');
            $("#raz_soc").val('');
            setTimeout(function () { $('#tab_prov').DataTable().ajax.reload() });
        });
    }
    // Modificar usuario
    // Si el formulario tiene id_per debe enviar los datos para modificar un usuario existente
    else {
        show_spin("btn_prov_sub", "spin_prov", "not_spin_prov");
        $.ajax({
            url: "modificar/proveedor",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');


                var r = JSON.parse(respuesta);
                // console.log(r,'json');

                if (r != 1) {
                    $('#msg_error_prov').show();
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list-errors_prov').append("<li>" + element + "</li>");


                        }
                    }

                }
                else {
                    $('#msg_error_prov').hide();
                }


            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }

        }).done(function () {
            hide_spin("btn_prov_sub", "spin_prov", "not_spin_prov");
            $("#nom_prov").val('');
            $("#desc_prov").val('');
            $("#nit_prov").val('');
            $("#raz_soc").val('');
            setTimeout(function () {
                $('#tab_prov').DataTable().ajax.reload(); alert('Se Modifico Correctamente');
                $('#cerrar-modal-prov').click();
            });
        });
    }
});
// END INSERTAR
// ELIMINAR CLIENTE
function eliminar_proveedor(id_prov, nom_prov, raz_soc, nit_prov) {
    var r = confirm("Desea Eliminar este Cliente? : \n Nombre: " + nom_prov + "\n Razon Social: " + raz_soc + "\n Nit: " + nit_prov);
    if (r == true) {
        $.ajax({
            method: "POST",
            url: "delete/proveedor",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: { 'id_prov': id_prov },
            success: function (result) {
                // var x = JSON.parse(result);
                // console.log(X);

            }

        }).done(setTimeout(function () { $('#tab_prov').DataTable().ajax.reload() }, 500)).always(function () {
            alert("Eliminado con exito");
        });

    }


};
    // END ELIMINAR
