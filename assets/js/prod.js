var id_prod;
$(document).ready(function () {
    // PRODUCTO
    var table_prod = $('#tab_prod').DataTable({

        ajax: {
            method: "GET",
            url: "producto",
            dataSrc: ""

        },
        select: {
            style: 'single',
            blurable: true
        },
        dom: 'Bfrtip',
        stateSave: false,
        buttons: [

            {
                text: '<i class="far fa-plus-square"></i> Agregar Producto',
                titleAttr: "Insertar",
                action: function () {
                    var modal = $('#ModalProducto');


                    modal.find('#nom_rec').val('');
                    modal.find('#desc_rec').val('');
                    modal.find('#fecha_rec').val('');
                    modal.modal("show");
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Producto',
                titleAttr: "Modificar",
                action: function () {
                    var modal = $('#modal_mod_prod');
                    var prod = table_prod.row({ selected: true }).data();
                    if (prod == null) {
                        alert("Seleccione un producto para modificar");
                    }
                    else {
                        modificar_prod(prod, modal);
                        modal.modal("show");
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Producto',
                titleAttr: "Eliminar",
                action: function () {
                    var prod = table_prod.row({ selected: true }).data();
                    if (prod == null) {
                        alert("Seleccione un producto para eliminar");
                    }
                    else {
                        eliminar_producto(prod['id_prod'], prod['nom_prod'], prod['desc_prod'], prod['costo_total_prod']);
                    }

                }
            },
        ],
        columns: [

            { data: 'nom_prod' },
            { data: 'desc_prod' },
            { data: 'fecha_prod' },
            { data: 'costo_total_prod' },
            { data: 'precio_total_prod' },
            // {
            //     ordenable: true,
            //     render: function (data, type, row) {
            //         return "<button id='btn_eliminar_prod' name='btn_eliminar_prod' type='button' class='form-control input-md' value='' onClick=\"eliminar_producto( " + row.id_prod + ",'" + row.nom_prod + "', '" + row.desc_prod + "', " + row.costo_total_prod + ")\" ><i class='fas fa-trash-alt'></i></button>"
            //     }
            // },
            // {
            //     ordenable: true,
            //     render: function (data, type, row) {
            //         return "<button id='btn_modificar' name='btn_modificar' type='button' class='form-control input-md fa fa-pencil-square-o' data-toggle='modal' data-target='#modal_mod_prod' value='' data-id='" + row.id_prod + "' data-nom='" + row.nom_prod + "' data-cost='" + row.costo_total_prod + "' data-und='" + row.unidad_ins + "'data-desc ='" + row.desc_prod + "' data-fecha='" + row.fecha_prod + "' data-mod='mod'></button>"
            //     }
            // },

        ],

    });
    // Registrar detalle producto
    $(document).on('click', '#btn-det-prod', function (event) {
        // console.log('Reg Detalle Producto');
        // console.log($('#tab_rec tbody tr'));


        $('#tab_det_prod tbody.tbody-det-prod tr').each(function () {
            // console.log("id_prod",id_prod);
            var id_producto = id_prod;

            var cant_det_prod = $(this).find('#cantidad').children('input').val();
            var cost_recurso_prod = $(this).find('#cost').html();
            var costo_total = $(this).find('#costo_tot').html();
            var precio_total_detProd = $(this).find('#pre_fin').children('input').val();
            if ($(this).data('fila') == 'ins') {
                var id_ins = $(this).find('#id').html();
                var id_rec = '0';


            }
            else {
                var id_rec = $(this).find('#id').html();
                var id_ins = '0';


            }
            // console.log('id_producto: ', id_producto, ' id_rec: ',id_rec,' id_ins: ', id_ins, 'cant: ', cant_det_prod, 'cost unit: ', cost_recurso_prod, 'costo: ', costo_total, 'Precio: ', precio_total_detProd);
            insert_det_prod(id_producto, id_ins, id_rec, cant_det_prod, cost_recurso_prod, costo_total, precio_total_detProd);

        });
        setTimeout(function () {
            $('#tab_prod').DataTable().ajax.reload();
            $('.tbody-det-prod').empty();
            $('#pills-prod-tab').trigger('click');
            alert('Detalle Registrado');

        }, 1000);

    });
    // ---------


    // DETALLE PRODUCTO
    get_prod_ins();
    get_prod_rec();
    $('.input-radio').on('click', function () {
        change_list($(this).children('input').val());
        // console.log($(this).children('input').val());


    });

    // Seleccionar un insumo o receta para producto
    $(document).on('dblclick', '.list-prod', function (event) {
        // console.log('list-prod');

        var button = $(this);
        var nom = button.data('nombre');
        var cost = button.data('cost');
        var und = button.data('und');
        var id = button.data('id');
        var tipo = $(this).data('tipo');
        // console.log(tipo);
        if (tipo == 'ins') {
            $('.tbody-det-prod').append("<tr data-fila='ins'><td id='id'>" + id + "</td><td id='nom'>" + nom + "</td><td id='cantidad'><input type='number' id='cant' name='cant' placeholder='Cantidad'></td><td id='und'>" + und + "</td><td id='cost'>" + parseFloat(cost).toFixed(4) + "</td><td id='costo_tot'>" + parseFloat(cost).toFixed(4) + "</td><td id='pre_fin'><input type='number' id='pre_fin' name='pre_fin' placeholder='Precio Final'></td><td><button id='btn-p-ins' class='form-control btn-danger input-md' data-tipo='ins'><i class='fas fa-minus-circle'></i></button></td></tr>");
            $(this).remove();
        } else if (tipo == 'rec') {
            $('.tbody-det-prod').append("<tr data-fila='rec'><td id='id'>" + id + "</td><td id='nom'>" + nom + "</td><td id='cantidad'><input type='number' id='cant' name='cant' placeholder='Cantidad'></td><td id='und'>" + und + "</td><td id='cost'>" + parseFloat(cost).toFixed(4) + "</td><td id='costo_tot'>" + parseFloat(cost).toFixed(4) + "</td><td id='pre_fin'><input type='number' id='pre_fin' name='pre_fin' placeholder='Precio Final'></td><td><button id='btn-p-ins' class='form-control btn-danger input-md' data-tipo='rec'> <i class='fas fa-minus-circle'></i></button></td></tr>");
            $(this).remove();

        }
    });
    // ----------
    // Eliminar insumo Seleccionado
    $(document).on('click', '#btn-p-ins', function () {
        // console.log('Borrar ins');

        var id = $(this).parents('tr').find('#id').html();
        var nom = $(this).parents('tr').find('#nom').html();
        var und = $(this).parents('tr').find('#und').html();
        var cost = $(this).parents('tr').find('#cost').html();
        var tipo = $(this).data('tipo');
        if (tipo == 'ins') {
            $(this).parents('tr').remove();
            $('#list_ins_prod').append("<button type='button' class='btn-default list-group-item list-prod search_item' id='btn-ins' data-tipo='ins' data-und='" + und + "' data-nombre='" + nom + "' data-id='" + id + "' data-cost='" + cost.toFixed(4) + "'>" + nom + "</button>");
        }
        else if (tipo == 'rec') {
            $(this).parents('tr').remove();
            $('#list_rec_prod').append("<button type='button' class='btn-default list-group-item list-prod search_item' id='btn-rec' data-tipo='rec' data-und='" + und + "' data-nombre='" + nom + "' data-id='" + id + "' data-cost='" + cost.toFixed(4) + "'>" + nom + "</button>");

        }



    });
    // -----
    // Modal Mod Producto
    function modificar_prod(data, modal) {
        // console.log(data);
        $('#table-det-prod-mod').empty();
        var id = data['id_prod'];
        var nom = data['nom_prod'];
        var cost = data['costo_total_prod'];
        var desc = data['desc_prod'];
        var fecha = data['fecha_prod'];
        var precio_total_prod = data['precio_total_prod'];
        get_det_prod(id);
        modal.find('#id_prod_mod').val(id)
        modal.find('#nom_prod_mod').val(nom)
        modal.find('#cost_total_prod_mod').val(cost)
        modal.find('#desc_prod_mod').val(desc)
        modal.find('#fecha_prod_mod').val(fecha)
        modal.find('#precio_total_prod').val(precio_total_prod)
    };







});

// Get de detalle receta
function get_det_prod(id) {

    $('#table-det-prod-mod-body').html('');
    $.ajax({
        method: "post",
        url: "detalleProducto",
        data: {
            'id_prod': id
        },
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var det_receta = JSON.parse(result);
            // console.log(det_receta);
            for (const key in det_receta) {
                if (det_receta.hasOwnProperty(key)) {
                    const element = det_receta[key];
                    // console.log(element);
                    if (element['id_ins'] != 0) {

                        $('#table-det-prod-mod-body').append("<tr><td>" + element['nom_ins'] + " <input type='hidden' name='id_det_prod' value='" + element['id_det_prod'] + "'></td><td><input type='text' value='" + element['cantidad_prod'] + "' name ='cantidad_prod' style='width: 6rem' disabled> </td><td>" + element['unidad_ins'] + "</td><td>" + parseFloat(element['costo_total']).toFixed(4) + "</td><td><input type='text' value='" + parseFloat(element['precio_total_detProd']).toFixed(4) + "' name='precio_total_detProd' style='width: 6rem' disabled></td><td><button name='btn_eliminar_rec' type='button' class='form-control input-md ' ><i class='fas fa-trash-alt'></i></button></td><td><button name='mod_det_prod' type='button' class='ins_prod form-control input-md'><i class='far fa-edit'></i><i class='fas fa-check d-none'></i></button></td></tr>");
                    }
                    else if (element['id_rec'] != 0) {
                        $('#table-det-prod-mod-body').append("<tr><td>" + element['nom_rec'] + "  <input type='hidden' name='id_det_prod' value='" + element['id_det_prod'] + "'> </td><td><input type='text' value='" + element['cantidad_prod'] + "' name ='cantidad_prod' style='width: 6rem' disabled></td><td>" + element['unidad_rec'] + "</td><td>" + parseFloat(element['cost_total_rec']).toFixed(4) + "</td><td><input type='text' value='" + parseFloat(element['precio_total_detProd']).toFixed(4) + "' name='precio_total_detProd' style='width: 6rem' disabled></td><td><button name='btn_eliminar_rec' type='button' class='form-control input-md' ><i class='fas fa-trash-alt'></i></button></td><td><button name='mod_det_prod' type='button' class='rec_prod form-control input-md'><i class='far fa-edit'></i><i class='fas fa-check d-none'></i></button></td></tr>");
                    }


                }
            }
        }
    });
}
// ------------------
// BTN modificar detalle receta
$(document).on('click', 'button[name="mod_det_prod"]', function () {

    if ($(this).find('.fa-check').hasClass('d-none')) {

        $(this).parents('tr').find('input').prop('disabled', false);
        $(this).find('.fa-check').removeClass('d-none');
        $(this).find('.fa-edit').addClass('d-none');
    }
    else {
        var id_prod = $('#modal_mod_prod').find('#id_prod_mod').val();
        var id_det_prod = $(this).parents('tr').find('input[name="id_det_prod"]').val();
        var cantidad_prod = $(this).parents('tr').find('input[name="cantidad_prod"]').val();
        var precio_total_detProd = $(this).parents('tr').find('input[name="precio_total_detProd"]').val();
        $.ajax({
            method: "post",
            url: "update/detalleProducto",
            data: {
                'id_det_prod': id_det_prod,
                'cantidad_prod': cantidad_prod,
                'precio_total_detProd': precio_total_detProd,
            },
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            success: function (result) {
                $(this).parents('tr').find('input').prop('disabled', true);
                $(this).find('.fa-edit').removeClass('d-none');
                $(this).find('.fa-check').addClass('d-none');
                alert("Se Modifico Correctamente");
            }
        }).done(setTimeout(function () {
            get_det_prod(id_prod);

        }, 1000));
        var precio_total_prod = 0;
        $('#table-det-prod-mod-body tr').each(function () {
            precio_total_prod += parseFloat($(this).find('input[name="precio_total_detProd"]').val());
        });
        console.log(cost_total_rec);
        $.ajax({
            method: "post",
            url: "update/TotalProducto",
            data: {
                'id_prod': id_prod,
                'precio_total_prod': precio_total_prod,
            },
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            success: function (result) {

            }
        }).done(setTimeout(function () {
            $('#tab_prod').DataTable().ajax.reload()

        }, 1000));



    }
})
////////////////////////
// Insertar Detalle Producto
function insert_det_prod(id_producto, id_ins, id_rec, cant_det_prod, cost_recurso_prod, costo_total, precio_total_detProd) {
    $.ajax({
        method: "POST",
        url: "insert/detalleProducto",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'id_prod': id_producto,
            'id_ins': id_ins,
            'id_rec': id_rec,
            'cantidad_prod': cant_det_prod,
            'costo_recurso_prod': cost_recurso_prod,
            'costo_total': costo_total,
            'precio_total_detProd': precio_total_detProd
        },
        success: function (result) {
            // console.log(result);
            // var x = JSON.parse(result);
            // console.log(x);
            // id_rec = x;

        }

    });

}
// ---------
// Obtener todos los Insumos
function get_prod_ins() {
    // console.log("Get detalle Receta");
    $('#list_ins_prod').empty();
    $('#list_ins_prod').append(`<li class="list-group-item list-group-item-primary align-content-center text-center">Insumo <button class="btn btn-outline-primary btn-sm" data-toggle="modal" data-target="#reg_ins"><i class="fa fa-plus" aria-hidden="true"></i></button></li>
        <div class="input-group">
          <div class="input-group-prepend">
            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
          <input type="text" class="form-control buscar-insumo" placeholder="Buscar insumo" aria-label="" aria-describedby="basic-addon1">
        </div>`);
    $.ajax({
        method: "get",
        url: "insumo",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var receta = JSON.parse(result);
            // console.log(receta);
            for (const key in receta) {
                if (receta.hasOwnProperty(key)) {
                    const element = receta[key];
                    // console.log(element);

                    $('#list_ins_prod').append("<button type='button' class='btn-default list-group-item list-prod search_item' id='btn-ins' data-und='" + element['unidad_ins'] + "' data-nombre='" + element['nom_ins'] + "' data-id='" + element['id_ins'] + "' data-tipo='ins' data-cost='" + element['cost_ins'] + "'>" + element['nom_ins'] + "</button>");

                }
            }
        }
    });
}
// ---------------------------------------------------------------------
// Obtener todas las recetas
function get_prod_rec() {
    // console.log("Get detalle Receta");
    $('#list_rec_prod').empty();
    $('#list_rec_prod').append(`<li class="list-group-item list-group-item-primary align-content-center">Receta</li>
        <div class="input-group">
          <div class="input-group-prepend">
            <button class="btn btn-outline-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
          </div>
          <input type="text" class="form-control buscar-insumo" placeholder="Buscar Receta" aria-label="" aria-describedby="basic-addon1">
        </div>`);
    $.ajax({
        method: "get",
        url: "receta",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var receta = JSON.parse(result);
            // console.log(receta);
            for (const key in receta) {
                if (receta.hasOwnProperty(key)) {
                    const element = receta[key];
                    // console.log(element);

                    $('#list_rec_prod').append("<button type='button' class='btn-default list-group-item list-prod search_item' id='btn-rec' data-und='" + element['unidad_rec'] + "' data-nombre='" + element['nom_rec'] + "' data-id='" + element['id_rec'] + "' data-tipo='rec' data-cost='" + element['cost_total_rec'] + "'>" + element['nom_rec'] + "</button>");

                }
            }
        }
    });
}
// ---------------------------------------------------------------------
// Cambiar de Lista
function change_list(parametro) {
    // console.log('cambio', parametro);

    if (parametro == 1) {

        $('#list_ins_prod').removeClass('d-none');
        $('#list_rec_prod').addClass('d-none');

    } else {

        $('#list_rec_prod').removeClass('d-none');
        $('#list_ins_prod').addClass('d-none');

    }
}
// Eliminar un Producto
function eliminar_producto(id, nom, desc, cost) {
    // console.log('Eliminar insumo',id,nom,cost);

    var r = confirm("Desea Eliminar El Producto: \n Nombre: " + nom + "\n Descripcion: " + desc + "\n Costo de Producto: " + cost);
    if (r == true) {
        $.ajax({
            method: "POST",
            url: "delete/producto",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: { 'id': id },
            success: function (result) {
                // var x = JSON.parse(result);
                // console.log(X);

            }

        }).done(setTimeout(function () { $('#tab_prod').DataTable().ajax.reload() }, 500)).always(function () {
            alert("completado");
        });

    }


}
// -------------------------------------------
// INSERTAR Producto
function insertar_producto() {
    // console.log('insertar producto');


    var nom = $('#nom_prod').val();
    var desc = $('#desc_prod').val();
    var fec = $('#fecha_prod').val();


    $.ajax({
        method: "POST",
        url: "insert/producto",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'nom_prod': nom,
            'desc_prod': desc,
            'fecha_prod': fec,
            'costo_total_prod': 0,
            'precio_total_prod': 0

        },
        success: function (result) {
            var x = JSON.parse(result);
            // console.log(x);
            id_prod = x;

        }

    }).always(function () {
        $('#cerrar-modal-prod').click();

        setTimeout(function () {
            $('#pills-detProd-tab').trigger('click');
            get_prod_ins();
            get_prod_rec();
        }, 500);
        setTimeout(function () { $('#tab_prod').DataTable().ajax.reload() }, 500);

        alert("Receta Completada \n Ahora podra insertar el detalle de su producto");
    });
}
// -----------------------------------
// Eliminar Detalle Producto
function eliminar_det_prod(id, id_prod, nom, und, cost) {

    var r = confirm("Desea ELIMINAR : \n Nombre: " + nom + "\n Unidad: " + und + "\n Costo de Insumo: " + cost);
    if (r == true) {
        $.ajax({
            method: "POST",
            url: "delete/det_producto",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: { 'id': id },
            success: function (result) {
                // var x = JSON.parse(result);
                // console.log(X);

            }

        }).done(setTimeout(function () { get_det_prod(id_prod) }, 600)).always(function () {
            alert("completado");
        });

    }
}
// END Eliminar detalle producto
// Modificar detalle Producto
$(document).on('click', '#btn_modificar_det_prod', function (event) {
    var name = $(this).attr('name');
    var val = $(this).attr('value');
    if (name == 'mod_rec') {
        var table = $('#tab_rec').DataTable();
        $('#close_modal_mod_det_prod').click();
        $('#pills-rec-tab').click();
        table.search(val).draw();




    } else {
        var table = $('#tab_logic').DataTable();
        $('#close_modal_mod_det_prod').click();
        $('#pills-ins-tab').click();
        table.search(val).draw();


    }

});
// Modificar Receta
$("#modal_mod_prod_form").submit(function (event) {
    event.preventDefault();
    var formData = new FormData($(this)[0]);
    console.log("Prod modal");
    $.ajax({
        url: "update/producto",
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {

            alert("Se Modifico Correctamente");
        }
    }).done(setTimeout(function () {
        $('#tab_prod').DataTable().ajax.reload();
        $('#modal_mod_prod').modal('hide');

    }, 1000));;

})