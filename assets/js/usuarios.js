var table_usuarios;
$(document).ready(function () {
    get_grupo();
    $('#msg_error_usu').hide();
    // Tabla de Usuarios
    table_usuarios = $('#tab_usu').DataTable({
        stateSave: true,
        select: {
            style: 'single',
            blurable: true
        },
        ajax: {
            method: "GET",
            url: "usuarios",
            dataSrc: ""
        },
        dom: 'Bfrtip',
        columns: [
            { data: 'nom_usu' },
            { data: 'tel_usu' },
            { data: 'email_usu' },
            { data: 'fec_ini' },
            { data: 'fec_fin' },
            { data: 'ult_con' }
        ],
        buttons: [
            {
                text: '<i class="far fa-plus-square"></i> Agregar Usuario',
                titleAttr: "Insertar",
                className: "btn-success",
                action: function () {
                    var modal = $('#ModalUsuario');
                    vaciar_modal_usu();

                    modal.modal("show");
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Usuario',
                titleAttr: "Modificar",
                action: function () {

                    var usuario = table_usuarios.row({ selected: true }).data();
                    if (usuario == null) {
                        alert("Seleccione un Usuario para modificar");
                    }
                    else {
                        modificar_usu(usuario['id_usu'], usuario['nom_usu'], usuario['app_usu'], usuario['tel_usu'], usuario['id_grp'], usuario['email_usu'], usuario['clv_usu'], usuario['fec_ini'], usuario['fec_fin'], usuario['foto_usu']);
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Usuario',
                titleAttr: "Eliminar",
                action: function () {
                    var pers = table_usuarios.row({ selected: true }).data();
                    if (pers == null) {
                        alert("Seleccione un usuario para Eliminar");
                    }
                    else {
                        eliminar_Usuario(pers['id_usu'], pers['nom_usu'], pers['id_grp']);
                    }
                }
            },

        ],

        columnDefs: [
            {
                targets: [0],
                data: 'nom_usu',
                render: function (data, type, row) {
                    return "<span><i class='fa fa-user'></i>&nbsp;" + data + " " + row.app_usu + "</span>"


                }

            },
            {
                targets: [1],
                data: 'tel_usu',
                render: function (data, type, row) {
                    return "<span><i class='fa fa-mobile' aria-hidden='true'></i>&nbsp;" + data + "</span>"
                }

            },

            // {
            //     targets: [4],
            //     data: 'fec_fin',
            //     render: function (data, type, row) {
            //         var fecha_actual = new Date().now();
            //         if (data > fecha_actual) {
            //             return "<span class='badge bg-success'>" + data + "</span>"
            //         } else if (data < fecha_actual) {
            //             return "<span class='badge bg-danger'>" + data + "</span>"
            //         }
            //     }

            // },
        ]
    });
});
// Insertar nuevo usuario
$('#form_usuario').submit(function (event) {
    event.preventDefault();
    $('#list-errors').html('');
    var formData = new FormData($(this)[0]);
    // var url = $(this).attr('action');

    // Si el formulario no tiene Id_per debe registrar nuevo usuario
    if (formData.get('id_usu') == "") {
        $.ajax({
            url: "insert/usuarios",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');


                var r = JSON.parse(respuesta);
                console.log(r, 'json');

                if (r != 1) {
                    $('#msg_error_usu').show();
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list_errors_usu').append("<li>" + element + "</li>");
                        }
                    }
                }
                else {
                    $('#msg_error_usu').hide();
                    alert("Se registro Correctamente");
                    $('#ModalUsuario').modal('hide');
                    setTimeout(function () { $('#tab_usu').DataTable().ajax.reload() });
                }
            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }
        })
    }
    // Modificar usuario
    // Si el formulario tiene id_per debe enviar los datos para modificar un usuario existente
    else {
        formData.delete("foto_usu");

        $.ajax({
            url: "modificar/usuario",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');
                var r = JSON.parse(respuesta);
                // console.log(r,'json');

                if (r != 1) {
                    $('msg_error_usu').show();
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list_errors_usu').append("<li>" + element + "</li>");


                        }
                    }

                }
                else {
                    $('#msg_error_usu').hide();
                    $('#ModalUsuario').modal('hide');
                    alert("Se modifico Correctamente");
                    setTimeout(function () { $('#tab_usu').DataTable().ajax.reload() });

                }


            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }


        })
    }






})
// Modificar la foto de personal
$(document).on("change", "#img_usu_mod", function () {
    var r = confirm("Desea cambiar la foto de este usuario?");
    if (r == true) {
        var formData = new FormData($('#modif_imagen_usu')[0]);

        formData.append('id_usu', $('#id_usu').val());
        // console.log(formData.get("id_per"));

        $.ajax({
            url: "modificar/img/usuario",
            type: 'POST',
            dataType: "html",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (resp) {
                // console.log(resp);
                var x = JSON.parse(resp);
                $('#foto_perfil').attr("src", "assets/img/foto_usuario/" + x);

            }
        }).done(function () {
            setTimeout(function () {
                $('#tab_usu').DataTable().ajax.reload();
            });


        }).always(function () {
            $('#cerrar-modal-usu').trigger('click');
            alert("completado");

        });


    }
});
// ---------------------------------
// Eliminar Usuarios
function eliminar_Usuario(id_usu, nom_usu, id_grp) {
    if (id_grp == 1) {
        var grupo = 'Administrador';
    } else if (id_grp == 2) {
        var grupo = 'Personal de Administración';
    }
    var r = confirm("Desea Eliminar este usuario? : \n Nombre: " + nom_usu + "\n Grupo: " + grupo);
    if (r == true) {
        $.ajax({
            method: "POST",
            url: "delete/usuario",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: { 'id_usu': id_usu },
            success: function (result) {
                // var x = JSON.parse(result);
                // console.log(X);

            }

        }).done(setTimeout(function () { $('#tab_usu').DataTable().ajax.reload() }, 500)).always(function () {
            alert("completado");
        });

    }
}
// -------------------------------
// Obtener Grupo
function get_grupo() {
    $.ajax({
        method: "get",
        url: "grupo",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var grupo = JSON.parse(result);
            // console.log(personal);
            for (const key in grupo) {
                if (grupo.hasOwnProperty(key)) {
                    const element = grupo[key];
                    $('#id_grp').append("<option value='" + element['id_grp'] + "'>" + element['nom_grp'] + "</option>")

                }
            }
        }
    });

}
// --------
function vaciar_modal_usu() {
    var modal = $('#ModalUsuario');
    modal.find('.modal-title').text('Registrar Usuario');
    modal.find('#id_usu').val("");
    modal.find('#nom_usu').val("");
    modal.find('#app_usu').val("");
    modal.find('#tel_usu').val("");
    modal.find('#email_usu').val("");
    modal.find('#clv_usu').val("");
    modal.find('#fec_ini').val("");
    modal.find('#fec_fin').val("");
    modal.find('#ult_con').val("");
    modal.find('#img_usu_show').attr("src", "assets/img/avatar_blanco.jpg");
    modal.find('#foto_usu').attr('type', 'file');
    modal.find('#mod_img_usu').attr('style', 'display: none');
}
// Modificar Personal
function modificar_usu(id_usu, nom_usu, app_usu, tel_usu, id_grp, email_usu, clv_usu, fec_ini, fec_fin, foto_usu) {
    var modal = $('#ModalUsuario');
    // console.log(id_per);
    modal.modal("show");
    modal.find('.modal-title').text('Modificar Usuario');
    modal.find('#id_usu').val(id_usu);
    modal.find('#nom_usu').val(nom_usu);
    modal.find('#nom_usu').prop('disabled', false);
    modal.find('#app_usu').val(app_usu);
    modal.find('#app_usu').prop('disabled', false);
    modal.find('#tel_usu').val(tel_usu);
    modal.find('#tel_usu').prop('disabled', false);
    modal.find('#email_usu').val(email_usu);
    modal.find('#email_usu').prop('disabled', false);
    modal.find('#clv_usu').val(clv_usu);
    modal.find('#clv_usu').prop('disabled', false);
    modal.find('#id_grp').val(id_grp);
    modal.find('#id_grp').prop('disabled', false);
    modal.find('#fec_ini').val(fec_ini);
    modal.find('#fec_ini').prop('disabled', false);
    modal.find('#fec_fin').val(fec_fin);
    modal.find('#fec_fin').prop('disabled', false);
    modal.find('#img_usu_show').attr("src", "assets/img/foto_usuario/" + foto_usu);
    modal.find('#foto_usu').attr('type', 'hidden');
    modal.find('#mod_img_usu').attr('style', 'display: inline block');
    modal.find('#env_form_personal').prop('disabled', false);




}