var ev_personal = [];
var table_personal;
var ev_aux;
$(document).ready(function () {
    get_cargo();
    get_cat();


    // Personal Fijo
    table_personal = $('#tab_per').DataTable({
        stateSave: true,
        select: {
            style: 'single',
            blurable: false
        },
        ajax: {
            method: "GET",
            url: "personalFijo",
            dataSrc: ""

        },
        dom: 'Bfrtip',
        columns: [

            { data: 'nom_per' },
            { data: 'ciudad' },
            { data: 'nom_cargo' },
            { data: 'unidad_per' },
            { data: 'telf_per' },
            { data: 'estado' }




        ],
        buttons: [
            {
                text: '<i class="far fa-plus-square"></i> Agregar Personal',
                titleAttr: "Insertar",
                className: "btn-success",
                action: function () {
                    var modal = $('#ModalPersonal');
                    vaciar_modal();
                    modal.modal("show");
                }
            },
            {
                text: '<i class="far fa-edit"></i> Modificar Personal',
                titleAttr: "Modificar",
                action: function () {
                    var modal = $('#ModalPersonal');
                    var pers = table_personal.row({ selected: true }).data();
                    if (pers == null) {
                        alert("Seleccione un Personal para modificar");
                    }
                    else {
                        // console.log(pers);
                        $("#msg_error").addClass("d-none");
                        modificar_personal(pers['clv_personal'], pers['id_per'], pers['nom_per'], pers['ciudad'], pers['unidad_per'], pers['telf_per'], pers['app_per'], pers['apm_per'], pers['ci_per'], pers['id_cat_per'], pers['id_cargo_per'], pers['email_per'], pers['fec_ingreso_personal'], pers['cost_per'], pers['img_per']);
                    }
                }
            },
            {

                text: '<i class="far fa-trash-alt"></i> Eliminar Personal',
                titleAttr: "Eliminar",
                action: function () {
                    var id_grp = readCookie('id_grp');
                    console.log('grp ' + id_grp);
                    if (id_grp == 1) {
                        var pers = table_personal.row({ selected: true }).data();
                        if (pers == null) {
                            alert("Seleccione un personal para modificar");
                        }
                        else {
                            eliminar_personalFijo(pers['id_per'], pers['nom_per'], pers['nom_cargo'], pers['ciudad']);
                        }
                    } else {
                        alert('No se puede realizar esta acción por que no es Administrador');
                    }

                }
            },
            {
                text: '<i class="far fa-address-card"></i> Cambiar estado',
                titleAttr: "Cambiar Estado Personal",
                action: function () {
                    var pers = table_personal.row({ selected: true }).data();
                    if (pers == null) {
                        alert("Seleccione un personal para cambiar de estado");
                    }
                    else {
                        cambiar_estado(pers['id_per'], pers['estado']);
                    }

                }
            },
            {
                text: '<i class="far fa-address-card"></i> Reporte PDF',
                titleAttr: "Reporte Anual",
                action: function () {
                    var pers = table_personal.row({ selected: true }).data();
                    // console.log(pers.id_per);
                    if (pers == null) {
                        alert("Seleccione un personal para el reporte");
                    }
                    else {
                        // ev_personal.length = 0;
                        // ev_personal.length = 0;
                        $("#id_per_pdf").val(pers["id_per"]);
                        $("#img_per_pdf").attr("src", "assets/img/foto_personal/" + pers.img_per);
                        $("#presetacion_pdf").html("Nombre de Empleado: " + pers.nom_per + " " + pers.app_per);
                        $("#report_modal").modal("show");
                        drawSelectYear();
                        // get_ev_per();
                    }

                }
            }
        ],
        // "<div class='dropdown'>" +
        //     "<button type='button' class='btn btn-danger dropdown-toggle' data-bs-toggle='dropdown' aria-expanded='false'>" +
        //     "Menu </button >" +
        //     "<div class='dropdown-menu' aria-labelledby='dropdownMenuButton2'>" +
        //     "<a class='dropdown-item' href='#' OnClick=\"ver_personal(" + row.id_per + ",'" + row.nom_per + "','" + row.ciudad + "','" + row.unidad_per + "','" + row.telf_per + "','" + row.app_per + "','" + row.apm_per + "','" + row.ci_per + "','" + row.id_cat_per + "','" + row.id_cargo_per + "','" + row.email_per + "','" + row.fec_ingreso_personal + "','" + row.cost_per + "','" + row.img_per + "')\">Ver Informacion</a>" +
        //     "<a class='dropdown-item' href='#' onClick=\"modificar_personal(" + row.id_per + ",'" + row.nom_per + "','" + row.ciudad + "','" + row.unidad_per + "','" + row.telf_per + "','" + row.app_per + "','" + row.apm_per + "','" + row.ci_per + "','" + row.id_cat_per + "','" + row.id_cargo_per + "','" + row.email_per + "','" + row.fec_ingreso_personal + "','" + row.cost_per + "','" + row.img_per + "')\">Editar Información</a>" +
        //     "<a class='dropdown-item' href='#' onClick=\"cambiar_estado(" + row.id_per + ",'" + row.estado + "')\">Cambiar de estado</a>" +
        //     "<div class='dropdown-divider'></div>" +
        //     "<a id='btn_eliminar_personalf' name='btn_eliminar_personalf' class='dropdown-item fa fa-trash-o' href='#' onClick=\"eliminar_personalFijo( " + row.id_per + ",'" + row.nom_per + "', '" + row.nom_cargo + "', '" + row.ciudad + "')\" >Eliminar Personal</a></div></div > "
        columnDefs: [
            {
                targets: [0],
                data: 'nombre',
                render: function (data, type, row) {
                    return "<span><i class='fa fa-user'></i>&nbsp;" + data + " " + row.app_per + "</span><br>" +
                        "<span><i class='fa fa-address-card-o'></i>&nbsp;" + row.ci_per + "</span>"

                }

            },
            {
                targets: [2],
                data: 'nom_cargo',
                render: function (data, type, row) {
                    return "<span><i class='fa fa-circle-thin' aria-hidden='true'></i>&nbsp;" + data + "</span><br>" +
                        "<span><i class='fa fa-circle-thin' aria-hidden='true'></i>&nbsp;" + row.nombre_cat + "</span>"

                }

            },
            {
                targets: [1],
                data: 'ciudad',
                render: function (data, type, row) {
                    return "<span><i class='fa fa-globe' aria-hidden='true'></i>&nbsp;" + data + "</span>"
                }

            },
            {
                targets: [3],
                data: 'unidad_per',
                render: function (data, type, row) {
                    return "<span><i class='fa fa-circle-thin' aria-hidden='true'></i>&nbsp;" + row.cost_per + " Bs. " + data + "</span>"
                }

            },
            {
                targets: [4],
                data: 'telf_per ',
                render: function (data, type, row) {
                    return "<span><i class='fa fa-mobile' aria-hidden='true'></i>&nbsp;" + data + "</span>"
                }

            },
            {
                targets: [5],
                data: 'estado',
                render: function (data, type, row) {
                    if (data == 'Activo' && row.on_event == 1) {
                        return "<label>En Empresa :&nbsp</label><span class='badge bg-success'>" + data + "</span><br><label>En Evento :&nbsp&nbsp</label><span class='badge bg-danger'> Ocupado</span>"
                    } else if (data == 'Inactivo' && row.on_event == 1) {
                        return "<label>En Empresa :&nbsp</label><span class='badge bg-danger'>" + data + "</span><br><label>En Evento :&nbsp&nbsp</label><span class='badge bg-danger'> Ocupado</span>"
                    } else if (data == 'Activo' && row.on_event == 0) {
                        return "<label>En Empresa :&nbsp</label><span class='badge bg-success'>" + data + "</span><br><label>En Evento :&nbsp&nbsp</label><span class='badge bg-success'> Disponible</span>"
                    } else if (data == 'Inactivo' && row.on_event == 0) {
                        return "<label>En Empresa :&nbsp</label><span class='badge bg-danger'>" + data + "</span><br><label>En Evento :&nbsp&nbsp</label><span class='badge bg-success'> Disponible</span>"
                    }
                }

            },
        ]
    });
});
function readCookie(name) {

    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;

}

$('#tab_per').on('select.dt', function (e, dt, type, indexes) {
    var data = dt.rows(indexes).data()[0];
    // console.log(data.id_per);
    get_ev_per(data.id_per);
});

// Insertar nuevo personal
$('#form_personal').submit(function (event) {
    event.preventDefault();
    $('#list-errors').html('');
    var formData = new FormData($(this)[0]);
    // var url = $(this).attr('action');

    // Si el formulario no tiene Id_per debe registrar nuevo usuario
    if (formData.get('id_per') == "") {
        $.ajax({
            url: "insert/personal",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');


                var r = JSON.parse(respuesta);
                // console.log(r,'json');

                if (r != 1) {
                    $('#msg_error').removeClass('d-none');
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list-errors').append("<li>" + element + "</li>");
                        }
                    }
                }
                else {
                    $('#msg_error').addClass("d-none");
                    alert("Se registro Correctamente");
                    $('#ModalPersonal').modal('hide');
                    setTimeout(function () { $('#tab_per').DataTable().ajax.reload() });
                }
            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }
        })
    }
    // Modificar usuario
    // Si el formulario tiene id_per debe enviar los datos para modificar un usuario existente
    else {
        formData.delete("img_per");

        $.ajax({
            url: "modificar/personal",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                // console.log(respuesta,'respuesta');
                var r = JSON.parse(respuesta);
                // console.log(r,'json');

                if (r != 1) {
                    $('#msg_error').removeClass('d-none');
                    for (const key in r) {
                        if (r.hasOwnProperty(key)) {
                            const element = r[key];
                            // console.log(element);
                            $('#list-errors').append("<li>" + element + "</li>");


                        }
                    }

                }
                else {
                    $('#msg_error').addClass("d-none");
                    $('#ModalPersonal').modal('hide');
                    // alert("Se modifico Correctamente");
                    setTimeout(function () { $('#tab_per').DataTable().ajax.reload() });

                }


            },
            error: function (xhr, status, error) {
                var err = eval("(" + error + ")");
                alert(err.Message);
            }


        })
    }






})

// Modificar la foto de personal
$(document).on("change", "#img_per_mod", function () {
    var r = confirm("Desea cambiar la foto de este personal?");
    if (r == true) {
        var formData = new FormData($('#modif_imagen')[0]);

        formData.append('id_per', $('#id_per').val());
        // console.log(formData.get("id_per"));

        $.ajax({
            url: "modificar/img/personal",
            type: 'POST',
            dataType: "html",
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (resp) {
                // console.log(resp);

            }
        }).done(function () {
            setTimeout(function () { $('#tab_per').DataTable().ajax.reload(); });


        }).always(function () {
            $('#cerrar-modal-rec').trigger('click');
            alert("completado");

        });


    }
});
// ---------------------------------
function vaciar_modal() {
    var modal = $('#ModalPersonal');
    modal.find('.modal-title').text('Registrar Personal');
    modal.find('#id_per').val("");
    modal.find('#nom_per').val("");
    modal.find('#ci_per').val("");
    modal.find('#app_per').val("");
    modal.find('#apm_per').val("");
    modal.find('#telf_per').val("");
    modal.find('#ciudad').val("");
    modal.find('#unidad_per').val("");
    modal.find('#email_per').val("");
    modal.find('#id_cat_per').val();
    modal.find('#id_cargo_per').val();
    modal.find('#fecha_ingreso').val("");
    modal.find('#cost_per').val("");
    modal.find('#img_per_show').attr("src", "assets/img/avatar_blanco.jpg");
    modal.find('#img_per').attr('type', 'file');
    modal.find('#mod_img_per').attr('style', 'display: none');
}
// Mostrar Informacion personal
function ver_personal(id_per, nom_per, ciudad, unidad_per, telf_per, app_per, apm_per, ci_per, id_cat_per, id_cargo_per, email_per, fec_ingreso_personal, cost_per, img_per) {
    var modal = $('#ModalPersonal');
    modal.modal("show");
    modal.find('.modal-title').text('Información Personal');

    modal.find('#id_per').val(id_per);
    modal.find('#nom_per').val(nom_per);
    modal.find('#nom_per').prop('disabled', true);
    modal.find('#ci_per').val(ci_per);
    modal.find('#ci_per').prop('disabled', true);
    modal.find('#app_per').val(app_per);
    modal.find('#app_per').prop('disabled', true);
    modal.find('#apm_per').val(apm_per);
    modal.find('#apm_per').prop('disabled', true);
    modal.find('#telf_per').val(telf_per);
    modal.find('#telf_per').prop('disabled', true);
    modal.find('#ciudad').val(ciudad);
    modal.find('#ciudad').prop('disabled', true);
    modal.find('#unidad_per').val(unidad_per);
    modal.find('#unidad_per').prop('disabled', true);
    modal.find('#email_per').val(email_per);
    modal.find('#email_per').prop('disabled', true);
    modal.find('#id_cat_per').val(id_cat_per);
    modal.find('#id_cat_per').prop('disabled', true);
    modal.find('#id_cargo_per').val(id_cargo_per);
    modal.find('#id_cargo_per').prop('disabled', true);
    modal.find('#fecha_ingreso').val(fec_ingreso_personal);
    modal.find('#fecha_ingreso').prop('disabled', true);
    modal.find('#cost_per').val(cost_per);
    modal.find('#cost_per').prop('disabled', true);
    modal.find('#img_per_show').attr("src", "assets/img/foto_personal/" + img_per);
    modal.find('#img_per').attr('type', 'hidden');
    modal.find('#mod_img_per').attr('style', 'display: none');
    modal.find('#env_form_personal').prop('disabled', true);
}
// ---------------------------------
// Modificar Personal
function modificar_personal(clv, id_per, nom_per, ciudad, unidad_per, telf_per, app_per, apm_per, ci_per, id_cat_per, id_cargo_per, email_per, fec_ingreso_personal, cost_per, img_per) {
    var modal = $('#ModalPersonal');
    // console.log(id_per);
    modal.modal("show");
    modal.find('.modal-title').text('Modificar Personal');
    modal.find('#id_per').val(id_per);
    modal.find('#nom_per').val(nom_per);
    modal.find('#nom_per').prop('disabled', false);
    modal.find('#clv_personal').val(clv);
    modal.find('#clv_personal').prop('disabled', false);
    modal.find('#ci_per').val(ci_per);
    modal.find('#ci_per').prop('disabled', false);
    modal.find('#app_per').val(app_per);
    modal.find('#app_per').prop('disabled', false);
    modal.find('#apm_per').val(apm_per);
    modal.find('#apm_per').prop('disabled', false);
    modal.find('#telf_per').val(telf_per);
    modal.find('#telf_per').prop('disabled', false);
    modal.find('#ciudad').val(ciudad);
    modal.find('#ciudad').prop('disabled', false);
    modal.find('#unidad_per').val(unidad_per);
    modal.find('#unidad_per').prop('disabled', false);
    modal.find('#email_per').val(email_per);
    modal.find('#email_per').prop('disabled', false);
    modal.find('#id_cat_per').val(id_cat_per);
    modal.find('#id_cat_per').prop('disabled', false);
    modal.find('#id_cargo_per').val(id_cargo_per);
    modal.find('#id_cargo_per').prop('disabled', false);
    modal.find('#fecha_ingreso').val(fec_ingreso_personal);
    modal.find('#fecha_ingreso').prop('disabled', false);
    modal.find('#cost_per').val(cost_per);
    modal.find('#cost_per').prop('disabled', false);
    modal.find('#img_per_show').attr("src", "assets/img/foto_personal/" + img_per);
    modal.find('#img_per').attr('type', 'hidden');
    modal.find('#mod_img_per').attr('style', 'display: inline block');
    modal.find('#env_form_personal').prop('disabled', false);




}
// Cambiar estado
function cambiar_estado(id_per, estado) {
    // console.log(estado);
    var e;
    if (estado == 'Activo') {
        e = "Inactivo";
    } else {
        e = "Activo";
    }
    $.ajax({
        method: "POST",
        url: "cambiar/estado/personal",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'estado': e,
            'id_per': id_per
        },
        success: function (result) {
            // var x = JSON.parse(result);
            // console.log(X);

        }

    }).done(setTimeout(function () { $('#tab_per').DataTable().ajax.reload() }, 200));

}
// Eliminar Personal
function eliminar_personalFijo(id_per, nom_per, nom_cargo, ciudad) {
    var r = confirm("Desea Eliminar este personal? : \n Nombre: " + nom_per + "\n Cargo: " + nom_cargo + "\n Ciudad: " + ciudad);
    if (r == true) {
        $.ajax({
            method: "POST",
            url: "delete/personal",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: { 'id_per': id_per },
            success: function (result) {
                // var x = JSON.parse(result);
                // console.log(X);

            }

        }).done(setTimeout(function () { $('#tab_per').DataTable().ajax.reload() }, 500)).always(function () {
            alert("completado");
        });

    }


}
// Obtener cargo
function get_cargo() {
    $.ajax({
        method: "get",
        url: "cargo/personal",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var personal = JSON.parse(result);
            // console.log(personal);
            for (const key in personal) {
                if (personal.hasOwnProperty(key)) {
                    const element = personal[key];
                    $('#id_cargo_per').append("<option value='" + element['id_cargo_per'] + "'>" + element['nom_cargo'] + "</option>")

                }
            }
        }
    });

}
// --------
// Obtener cargo
function get_cat() {
    $.ajax({
        method: "get",
        url: "categoria/personal",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        success: function (result) {
            // $("#div1").html(result);
            // console.log(result);
            var personal = JSON.parse(result);
            // console.log(personal);
            for (const key in personal) {
                if (personal.hasOwnProperty(key)) {
                    const element = personal[key];
                    $('#id_cat_per').append("<option value='" + element['id_cat_per'] + "'>" + element['nombre_cat'] + "</option>")

                }
            }
        }
    });

}
// --------

$("#g_rep_per").on("click", function () {
    console.log("generar reporte");
    var y = $("#year_filt_personal").val();
    ev_aux = ev_personal.filter(a => a.year == y);
    const doc = new jsPDF('p', 'mm');
    // var logo = document.querySelector("#main_logo").toDataURL("image/png", 1.0);
    var y = 20;
    var mes_anterior = 0;
    var i = 1;
    var sumCosto = 0;
    ev_aux.forEach((element, x) => {
        // sumCosto += parseFloat(element.total_detalle);
        if (element.numero_mes != mes_anterior) {
            if (x != 0) {
                y += 7;
                doc.text(100, y - 6, "TOTAL");
                doc.text(133, y - 6, sumCosto.toString());
                sumCosto = 0;
                doc.line(121, y - 14, 121, y - 1);
                doc.rect(94, y - 14, 61, 13);
                // Linea Borde
                doc.line(26, y + 3, 26, y - 14);
                doc.line(200, y + 3, 200, y - 14);
                doc.line(26, y + 3, 200, y + 3);
                // /////
                y += 10;
            }
            i = 1;
            doc.setFontType("italic");
            doc.setFont("times");
            doc.setFontSize(16);
            // Linea de Borde
            doc.line(26, y - 3, 200, y - 3);
            doc.line(26, y - 3, 26, y + 96);
            doc.line(200, y - 3, 200, y + 96);
            // ///////////////
            doc.addImage(document.querySelector("#main_logo"), "PNG", 30, y, 45, 45);
            var textWidth = doc.getTextWidth("PLANILLA DE PAGO");

            doc.line(80, y + 21, 80 + textWidth, y + 21);
            doc.text(80, y + 20, "PLANILLA DE PAGO");
            textWidth = doc.getTextWidth(element.mes + " " + element.year);
            doc.line(88, y + 27, 88 + textWidth, y + 27);
            doc.text(88, y + 26, element.mes + " " + element.year);
            doc.addImage(document.querySelector("#img_per_pdf"), "PNG", 140, y + 4, 35, 35);
            doc.rect(138, y + 2, 39, 39);
            doc.setFontType("bold");
            y += 48;
            doc.setFontSize(9);
            textWidth = doc.getTextWidth("NEXT GENERATION COCKTAILS S.R.L.");
            doc.text(31, y, "NEXT GENERATION COCKTAILS S.R.L.");
            doc.line(31, y + 1, 30 + textWidth, y + 1);
            doc.setFontType("normal");
            doc.setFontSize(7);
            doc.text(31, y + 5, "Av. Piraí y 4to. Anillo, Edificio Santa Mónica");
            doc.text(31, y + 8, "reacheltorrez@hotmail.com");
            doc.text(31, y + 11, "Cel.: 70749648 - 69420001");
            y += 20;
            doc.setFontSize(10);
            doc.setFontType("bold");
            doc.text(35, y, "Nombre: ");
            doc.text(39, y + 10, "CI.: ");
            doc.text(130, y, "Cargo: ");
            doc.setFontType("normal");
            var split_name = doc.splitTextToSize(element.nom_per, 60);
            doc.text(57, y, split_name);
            doc.text(60, y + 10, element.ci_per);
            doc.text(152, y, element.nom_cargo);
            doc.rect(31, y - 5, 80, 18);
            doc.rect(126, y - 5, 60, 9);
            doc.line(31, y + 5, 111, y + 5);
            doc.line(52, y - 5, 52, y + 13);
            doc.line(147, y - 5, 147, y + 4);
            y += 25;
            doc.setFontType("bold");
            doc.text(35, y, "N°");
            doc.text(63, y, "Evento");
            doc.text(100, y, "Fecha");
            doc.text(125, y, "Total Pago Bs.");
            doc.text(170, y, "Firma");
            doc.rect(31, y - 5, 165, 8);
            y += 10;
        }
        // Linea Borde
        doc.line(26, y - 7, 26, y + 6);
        doc.line(200, y - 7, 200, y + 6);
        // //////
        doc.setFontType("normal");
        doc.text(35, y, i.toString());
        i++;
        var split_name = doc.splitTextToSize(element.nom_eve, 60);
        doc.text(50, y, split_name);
        doc.text(98, y, element.fecha_eve);
        doc.text(135, y, element.total_detalle);

        // Linea Horizontal
        doc.line(31, y + 6, 196, y + 6);
        // Lineas Verticales
        doc.line(31, y - 7, 31, y + 6);
        doc.line(46, y - 7, 46, y + 6);
        doc.line(94, y - 7, 94, y + 6);
        doc.line(121, y - 7, 121, y + 6);
        doc.line(155, y - 7, 155, y + 6);
        doc.line(196, y - 7, 196, y + 6);
        console.log("x ", x, "array ", ev_personal.length);
        sumCosto += parseFloat(element.total_detalle);
        if (x == ev_aux.length - 1) {
            doc.text(100, y + 14, "TOTAL");
            doc.line(121, y + 6, 121, y + 19);
            doc.rect(94, y + 6, 61, 13);
            doc.text(133, y + 14, sumCosto.toString());
            sumCosto = 0;
            // Linea Borde
            doc.line(26, y + 6, 26, y + 22);
            doc.line(200, y + 6, 200, y + 22);
            doc.line(26, y + 22, 200, y + 22);
            // /////
        }
        y += 13;
        mes_anterior = element.numero_mes;

    })
    window.open(doc.output('bloburl'));

})
function drawSelectYear() {
    $("#year_filt_personal").empty();
    var temporal = [];
    var repetidos = [];
    ev_personal.forEach((value, index) => {
        temporal = Object.assign([], ev_personal); //Copiado de elemento
        temporal.splice(index, 1); //Se elimina el elemnto q se compara
        console.log(value.year);
        /**
         * Se busca en temporal el elemento, y en repetido para 
         * ver si esta ingresado al array. indexOf returna
         * -1 si el elemento no se encuetra
         **/
        if (repetidos.indexOf(value.year) == -1) repetidos.push(value.year);
    });
    repetidos.forEach((value, index) => {
        $("#year_filt_personal").append(`<option value='${value}'>${value}</option>`)
    })
}
function get_ev_per(id_per) {
    // ev_personal.length = 0;
    // console.log("evento personal", ev_personal);

    // var id_per = $("#id_per_pdf").val();
    var year = $("#year_filt_personal").val();
    $.ajax({
        method: "POST",
        url: "get/ev_personal",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            // 'year': year,
            'id_per': id_per
        },
        success: function (result) {
            // console.log('Para per Reporte: ', result);
            // var r = JSON.parse(result);
            // console.log(r);
            for (const key in result) {
                if (Object.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    ev_personal.push(element);
                    // console.log("eventos del personal ", ev_personal);
                }

            }


        }
    });
    // console.log(ev_personal);
    // $("#img_per_pdf").attr("src", ev_personal[0].img_per);
}