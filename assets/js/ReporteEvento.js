const labels = [];
const labels2 = [];
const labels3 = [];
const labels4 = [];
const detalleGlobal = [];
const detalle_prodGlobal = [];
var EventData = [];
var indexFinded;
var PersonAsig = [];
var ProdAsig = [];
var insumoUti = [];
const data = {
    labels: labels,
    datasets: [{
        label: 'N° Eventos',
        backgroundColor: ['#AC85FF'],
        borderColor: ['#9A75FF'],
        auxiliar: [],
        years: []
    }]
};
const data2 = {
    labels: labels2,
    datasets: [{
        label: 'N° Eventos',
        backgroundColor: ['rgba(255, 168, 128, 0.494)', 'rgba(232, 137, 116, 0.494)', 'rgba(255, 145, 141, 0.494)', 'rgba(232, 128, 161, 0.494)', 'rgba(255, 128, 240, 0.494)'],
        borderColor: ['#FF935E', '#E87056', '#FF706B', '#E8568E', '#FF5EED']
    }]
};
const data3 = {
    labels: labels3,
    datasets: [

    ]
};
var color = colorRGB();
const data4 = {
    labels: labels4,
    datasets: [{
        label: 'N° Cocteles',
        backgroundColor: [color],
        borderColor: [color],
        data: []
    }]
};
const config = {
    type: 'bar',
    data: data,
    options: {
        scales: {

            y: {

                ticks: {
                    // forces step size to be 50 units
                    stepSize: 1
                }
            }
        },
        // indexAxis: 'y',
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each horizontal bar to be 2px wide
        elements: {
            bar: {
                borderWidth: 2,
            }
        },
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Datos Eventos Por Mes'
            },

        },
        onClick: (e, activeEls) => {
            let datasetIndex = activeEls[0].datasetIndex;
            let dataIndex = activeEls[0].index;
            let datasetLabel = e.chart.data.datasets[datasetIndex].label;
            let value = e.chart.data.datasets[datasetIndex].data[dataIndex];
            let label = e.chart.data.labels[dataIndex];
            let val = e.chart.data.datasets[datasetIndex].auxiliar[dataIndex];
            console.log("In click ", val);
            get_eventoDetalle($('#filt_year').val(), val);
            // $('#filt_year').val(label);
        }

    },
};
const config2 = {
    type: 'bar',
    data: data2,
    options: {
        // indexAxis: 'y',
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each horizontal bar to be 2px wide
        elements: {
            bar: {
                borderWidth: 2,
            }
        },
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Datos Eventos Por Mes'
            },

        },
        onClick: (e, activeEls) => {
            let datasetIndex = activeEls[0].datasetIndex;
            let dataIndex = activeEls[0].index;
            let datasetLabel = e.chart.data.datasets[datasetIndex].label;
            let value = e.chart.data.datasets[datasetIndex].data[dataIndex];
            let label = e.chart.data.labels[dataIndex];
            console.log("In click", datasetLabel, label, value);
            get_eventoMes(label);
            $('#filt_year').val(label);
        }
    },
};
const config3 = {
    type: 'bar',
    data: data3,
    options: {
        // indexAxis: 'y',
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each horizontal bar to be 2px wide
        elements: {
            bar: {
                borderWidth: 2,
            }
        },
        responsive: true,
        plugins: {
            legend: {
                position: 'right',
            },
            title: {
                display: true,
                text: 'Datos Eventos Por Mes'
            },

        },

    },
};
const config4 = {
    type: 'bar',
    data: data4,
    options: {
        // indexAxis: 'y',
        // Elements options apply to all of the options unless overridden in a dataset
        // In this case, we are setting the border of each horizontal bar to be 2px wide
        elements: {
            bar: {
                borderWidth: 2,
            }
        },
        responsive: true,
        plugins: {
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Datos Eventos Por Mes'
            },

        },

    },
};
// Creacion de Graficos
const filter_year = new Chart(
    document.getElementById('filter_year'),
    config
);
const filter_ryear = new Chart(
    document.getElementById('filter_ryear'),
    config2
);
const filter_prod = new Chart(
    document.getElementById('prod_year'),
    config3
);
const sumprod_year = new Chart(
    document.getElementById('sumprod_year'),
    config4
);

// //////////////////////////////
$(document).ready(function () {
    get_eventos();
})
$(document).on('keyup', '#search_events', function () {
    var lista = $('#list_eventosRP').find('.list-group-item');
    // console.log(lista.html());
    var busqueda = $(this).val().toLowerCase();
    if (busqueda.toString() == "") {
        lista.each(function () {
            $(this).removeClass('d-none');
        })

    } else {
        lista.each(function () {

            var fila = $(this).find(".nom_eve").text().toString();
            // console.log(fila);
            var cadena = "";
            var estado = false;
            for (let i = 0; i < fila.length; i++) {
                // console.log(fila.charAt(i));
                cadena = cadena + fila.charAt(i).toLowerCase();
                // console.log(cadena, " ", busqueda);
                if (cadena == busqueda) {
                    //  console.log(cadena);   
                    i = fila.length;
                    estado = true;
                    break
                }


            }
            if (estado != true) {
                // console.log(estado);
                $(this).addClass('d-none');
                // console.log("hola ", $(this).html());

            }
            else {
                // console.log(estado);
                estado = false;
                $(this).removeClass('d-none');
                // console.log("hola ", $(this).html());
            }
            cadena = "";


        });
    }


});
function get_eventos() {
    $.ajax({
        method: "GET",
        url: "get/eventos",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        success: function (result) {
            // console.log(result);

            for (const key in result) {
                if (result.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    EventData.push(element);

                    $("#list_eventosRP").append(` <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                        <input type='hidden' name='indexOfEvent' value='${key}'>
                            <div class="fw-bold fs-5 nom_eve text-wrap" style="width: 9rem;">${element.nom_eve}</div>
                            <div>Fecha: ${element.fecha_eve}</div>
                            <div>Nro. Factura: ${element.nro_factura}</div>
                            <div>Cliente: ${element.nom_cli}</div>
                        </div>
                        <p class="badge bg-success rounded-pill mt-2">${element.mes} ${element.year}</p><br>
                        <button class="btn bg-ligth border-primary rounded-pill ms-2 btn_eventPDF"><i class="far fa-file-alt"></i></button>
                    </li>`);


                }
            }

        }

    });
}
function get_insUtilizados(num_prof) {
    insumoUti.length = 0;
    $.ajax({
        method: "POST",
        url: "get/insUti",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'num_prof': num_prof,
        },
        success: function (result) {
            console.log('Para PDF: ', result);
            // if (result.length == 0) {
            //     $('#personal_asig').append('<div> No hay personal Asignado</div>');
            // } else {
            for (const key in result) {
                if (Object.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    insumoUti.push(element);
                    //     $('#personal_asig').append(` <li class="list-group-item d-flex justify-content-between align-items-start">
                    //     <div class="ms-2 me-auto">
                    //         <div class="fw-bold">${element.nom_cargo}</div>
                    //         ${element.nom_per + " " + element.app_per + " " + element.apm_per}
                    //     </div>
                    //     <span class="badge bg-primary rounded-pill">${element.costo_dtp} Bs</span>
                    // </li>`);

                }
            }
            // }
        }

    })
}
function get_perasing(num_prof) {
    PersonAsig.length = 0;
    $.ajax({
        method: "POST",
        url: "get/perasing",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'num_prof': num_prof,
        },
        success: function (result) {
            // console.log('Para tabla: ', result);
            if (result.length == 0) {
                $('#personal_asig').append('<div> No hay personal Asignado</div>');
            } else {
                for (const key in result) {
                    if (Object.hasOwnProperty.call(result, key)) {
                        const element = result[key];
                        PersonAsig.push(element);
                        $('#personal_asig').append(` <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                            <div class="fw-bold">${element.nom_cargo}</div>
                            ${element.nom_per + " " + element.app_per + " " + element.apm_per}
                        </div>
                        <span class="badge bg-primary rounded-pill">${element.costo_dtp} Bs</span>
                    </li>`);

                    }
                }
            }
        }

    })
}
function get_prodasing(num_prof) {
    ProdAsig.length = 0;
    $.ajax({
        method: "POST",
        url: "get/prodasing",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'num_prof': num_prof,
        },
        success: function (result) {
            // console.log('Para tabla: ', result);
            for (const key in result) {
                if (Object.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    ProdAsig.push(element);
                    $('#prod_asig').append(` <li class="list-group-item d-flex justify-content-between align-items-start">
                        <div class="ms-2 me-auto">
                            <div class="fw-bold">${element.nom_prod}</div>
                            Cantidad: ${element.cantidad}
                        </div>
                        <span class="badge bg-warning rounded-pill">${element.costo_dtp} Bs</span>
                    </li>`);

                }
            }
        }

    })
}
$(document).on("click", ".btn_eventPDF", function () {
    indexFinded = $(this).parent().find("input[name='indexOfEvent']").val();
    $('#personal_asig').empty();
    $('#prod_asig').empty();
    $("#tb_event_rp").empty();
    console.log(EventData[indexFinded]);
    $("input[name='index_dataEv']").val(indexFinded);
    $("#nom_evento_pdf").html("Nombre Evento: " + EventData[indexFinded].nom_eve);
    $("#fec_evento_pdf").html("Fecha Evento: " + EventData[indexFinded].fecha_eve);
    $("#nfac_evento_pdf").html("Numero Factura: " + EventData[indexFinded].nro_factura);
    $("#npro_evento_pdf").html("Numero Proforma: " + EventData[indexFinded].num_prof);
    get_perasing(EventData[indexFinded].num_prof);
    get_prodasing(EventData[indexFinded].num_prof);
    get_insUtilizados(EventData[indexFinded].num_prof);
    $("#tb_event_rp").append(`<tr>
    <td>${EventData[indexFinded].cost_eve_total} Bs.</td>
    <td>${EventData[indexFinded].IT} Bs.</td>
    <td>${EventData[indexFinded].IVA} Bs.</td>
    <td>${EventData[indexFinded].total_imp} Bs.</td>
    <td>${EventData[indexFinded].total_facturado} Bs.</td>
    </tr>`);

})
$("input[name='radio_pdf']").on("change", function () {
    console.log($(this).val());
    var valor = $(this).val()
    if (valor == 1) {
        $(".pr_filter").addClass("d-none");
        $(".pe_filter").addClass("d-none");
        $(".pe_gra").addClass("d-none");
        $(".py_filter").removeClass("d-none");
        $(".py_gra").removeClass("d-none");

    } else if (valor == 2) {
        $(".py_filter").addClass("d-none");
        $(".pe_filter").addClass("d-none");
        $(".pe_gra").addClass("d-none");
        $(".pr_filter").removeClass("d-none");
        $(".py_gra").removeClass("d-none");

    } else {
        $(".pr_filter").addClass("d-none");
        $(".py_filter").addClass("d-none");
        $(".py_gra").addClass("d-none");
        $(".pe_filter").removeClass("d-none");
        $(".pe_gra").removeClass("d-none");

    }
})
// Funciones Para Llamar a los graficos
function get_eventoMes(year) {
    // console.log($("input[name='radio_pdf']").val());
    if ($("#radio_pdf1").is(":checked")) {
        filter_year.data['labels'].length = 0;
        filter_year.data['datasets'][0].data.length = 0;
        filter_year.data['datasets'][0].auxiliar.length = 0;
        $.ajax({
            method: "POST",
            url: "get/eventoMes",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: {
                'year': year,
                'year1': 0,
                'year2': 0,
            },
            success: function (result) {
                console.log('Para Estadistica: ', result);
                for (const key in result) {
                    if (Object.hasOwnProperty.call(result, key)) {
                        const element = result[key];
                        filter_year.data['labels'].push(element.mes);
                        filter_year.data['datasets'][0].data.push(element.cuenta);
                        filter_year.data['datasets'][0].auxiliar.push(element.numero_mes);

                    }
                }
                filter_year.options.plugins.title.text = 'Eventos Realizados en ' + year;
                filter_year.update();
                console.log("myChart mes: ", filter_year.data);


            }

        })
    } else if ($("#radio_pdf2").is(":checked")) {
        filter_year.data['labels'].length = 0;
        filter_year.data['datasets'][0].data.length = 0;
        filter_year.data['datasets'][0].auxiliar.length = 0;
        var year1 = $("#filt_year_range1").val();
        var year2 = $("#filt_year_range2").val();
        $.ajax({
            method: "POST",
            url: "get/eventoMes",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: {
                'year': 0,
                'year1': year1,
                'year2': year2,
            },
            success: function (result) {
                console.log('Para Estadistica: ', result);
                for (const key in result) {
                    if (Object.hasOwnProperty.call(result, key)) {
                        const element = result[key];
                        filter_year.data['labels'].push(element.mes + " " + element.year);
                        filter_year.data['datasets'][0].data.push(element.cuenta);
                        filter_year.data['datasets'][0].auxiliar.push(element.numero_mes);
                        filter_year.data['datasets'][0].years.push(element.year);

                    }
                }
                filter_ryear.options.plugins.title.text = 'Eventos Realizados en ' + year1 + " hasta " + year2;
                filter_year.update();
                console.log("myChart mes: ", filter_year.data);


            }

        })
    }
}
function get_sumprodYear(year1, year2) {
    sumprod_year.data['labels'].length = 0;
    sumprod_year.data['datasets'][0].data.length = 0;
    $.ajax({
        method: "POST",
        url: "get/prodXYear",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'year1': year1,
            'year2': year2,
        },
        success: function (result) {
            // console.log('Para Estadistica: ', result);
            for (const key in result) {
                if (Object.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    detalle_prodGlobal.push(element);
                    sumprod_year.data['labels'].push(element.nom_prod);
                    sumprod_year.data['datasets'][0].data.push(element.cant);
                    // sumprod_year.data['datasets'][0].backgroundColor.push(colorRGB());
                }
            }
            if (year2 == 0) {
                sumprod_year.options.plugins.title.text = 'Nro Cocteles Realizados en ' + year1;
            } else {
                sumprod_year.options.plugins.title.text = 'Nro Cocteles Realizados en ' + year1 + " Hasta: " + year2;

            }
            sumprod_year.update();

            // console.log("myChart: ", sumprod_year.data);


        }

    })

}
function get_eventoDetalle(year, month) {
    $('.list_ev').empty();
    console.log(month, ' ', year);
    $('.list_ev').append(`<li class="list-group-item list-group-item-info">Eventos Realizados en el Mes de ${labels3[month - 1]} del ${year}</li>`);
    // ev_pmonth.data['datasets'][0].data.length = 0;
    $.ajax({
        method: "POST",
        url: "get/eventoDetalle",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'year': year,
            'month': month
        },
        success: function (result) {
            console.log('Para Estadistica: ', result);
            for (const key in result) {
                if (Object.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    $('.list_ev').append(`<button type="button" class="list-group-item list-group-item-action">Nombre Evento: ${element.nom_eve} <br> Nro Factura: ${element.nro_factura} <br> Fecha Evento: ${element.fecha_eve} </button>`)
                }
            }


        }

    })
}
function get_eventoYRango(year1, year2) {
    filter_ryear.data['labels'].length = 0;
    filter_ryear.data['datasets'][0].data.length = 0;
    $.ajax({
        method: "POST",
        url: "get/eventoYRango",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'year1': year1,
            'year2': year2

        },
        success: function (result) {
            // console.log('Para Estadistica: ', result);
            for (const key in result) {
                if (Object.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    filter_ryear.data['labels'].push(element.ye);
                    filter_ryear.data['datasets'][0].data.push(element.cuenta);

                }
            }
            if (year1 == year2) {

                filter_ryear.options.plugins.title.text = 'Eventos Realizados en ' + year1;
            } else {
                filter_ryear.options.plugins.title.text = 'Eventos Realizados en ' + year1 + " hasta " + year2;
            }
            filter_ryear.update();
            // console.log("myChart: ", filter_ryear.data);


        }

    })
}
function get_productos(year1) {
    filter_prod.data['labels'].length = 0;
    // filter_prod.data.datasets.forEach(dataset => {
    //     console.log(dataset);
    //     dataset.le;
    // });
    filter_prod.data.datasets.length = 0;
    // filter_prod.data['datasets'][0].data.length = 0;
    if ($("#radio_pdf1").is(":checked")) {
        $.ajax({
            method: "POST",
            url: "get/prodYRango",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: {
                'year1': year1,
                'year2': 0,
            },
            success: function (result) {
                // console.log('Para Estadistica: ', result);
                var index = 0;
                for (const key in result) {
                    if (Object.hasOwnProperty.call(result, key)) {
                        const element = result[key];
                        var dataNEW = [];
                        for (let index = 0; index <= filter_prod.data['labels'].length; index++) {
                            if (filter_prod.data['labels'][index] == element.mes || filter_prod.data['labels'].length == 0 || index == filter_prod.data['labels'].length) {
                                dataNEW.push(element.cant);
                                break;
                            } else {
                                dataNEW.push(0);
                            }

                        }
                        if (filter_prod.data['labels'].includes(element.mes)) {
                            const newDataset = {
                                label: element.nom_prod,
                                backgroundColor: colorRGB(),
                                borderWidth: 1,
                                data: dataNEW
                            }
                            // console.log("Dataset: ", newDataset.data);
                            filter_prod.data.datasets.push(newDataset);
                        } else {
                            filter_prod.data['labels'].push(element.mes);
                            const newDataset = {
                                label: element.nom_prod,
                                backgroundColor: colorRGB(),
                                borderWidth: 1,
                                data: dataNEW
                            }
                            filter_prod.data.datasets.push(newDataset);
                            // console.log("Dataset: ", newDataset.data);
                        }
                    }
                }
                filter_prod.options.plugins.title.text = 'Productos vendidos en ' + year1;
                filter_prod.update();
                console.log("myChart: ", filter_prod.data);


            }

        })
    } else if ($("#radio_pdf2").is(":checked")) {
        var year1 = $("#filt_year_range1").val();
        var year2 = $("#filt_year_range2").val();
        $.ajax({
            method: "POST",
            url: "get/prodYRango",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: {
                'year1': year1,
                'year2': year2,
            },
            success: function (result) {
                // console.log('Para Estadistica: ', result);
                var index = 0;
                for (const key in result) {
                    if (Object.hasOwnProperty.call(result, key)) {
                        const element = result[key];
                        var dataNEW = [];
                        for (let index = 0; index <= filter_prod.data['labels'].length; index++) {
                            if (filter_prod.data['labels'][index] == (element.mes + " " + element.year) || filter_prod.data['labels'].length == 0 || index == filter_prod.data['labels'].length) {
                                dataNEW.push(element.cant);
                                break;
                            } else {
                                dataNEW.push(0);
                            }

                        }
                        if (filter_prod.data['labels'].includes(element.mes + " " + element.year)) {
                            const newDataset = {
                                label: element.nom,
                                backgroundColor: colorRGB(),
                                borderWidth: 1,
                                data: dataNEW
                            }
                            // console.log("Dataset: ", newDataset.data);
                            filter_prod.data.datasets.push(newDataset);
                        } else {
                            filter_prod.data['labels'].push(element.mes + " " + element.year);
                            const newDataset = {
                                label: element.nom,
                                backgroundColor: colorRGB(),
                                borderWidth: 1,
                                data: dataNEW
                            }
                            filter_prod.data.datasets.push(newDataset);
                            // console.log("Dataset: ", newDataset.data);
                        }
                    }
                }
                filter_prod.options.plugins.title.text = 'Productos vendidos en ' + year1 + " Hasta: " + year2 + " Por mes";
                filter_prod.update();
                console.log("myChart: ", filter_prod.data);


            }

        })

    }
}
// Funcion Para generar Colores aleatorios
function generarNumero(numero) {
    return (Math.random() * numero).toFixed(0);
}
function colorRGB() {
    var coolor = "(" + generarNumero(255) + "," + generarNumero(255) + "," + generarNumero(255) + ",0.494)";
    return "rgb" + coolor;
}
///////////////////////////////////////////
// Funciones para cambiar parametros dde busquda de graficos
$("#filt_year").on('change', function () {
    detalleGlobal.length = 0;
    detalle_prodGlobal.length = 0;
    console.log('Change');
    var year = $(this).val();
    get_eventoMes(year);
    get_sumprodYear(year, year);
    get_eventoYRango(year, year);
    get_productos(year);
    // setTimeout(function () {
    //     get_det_topdf(year);
    // }, 700)


})
$("#filt_yearP").on('change', function () {
    console.log('Change');
    get_productos($(this).val());

})
$(".year_range").on('change', function () {
    console.log('Change');
    var year1 = $("#filt_year_range1").val();
    var year2 = $("#filt_year_range2").val();
    detalleGlobal.length = 0;
    detalle_prodGlobal.length = 0;
    get_eventoYRango(year1, year2);
    get_eventoMes(year1);
    get_productos(year1);
    get_sumprodYear(year1, year2);
});
// //////////////////////////////////////
// Change Para mostrar los graficos
$("#ev_pyear").on('change', function () {
    if (this.checked) {
        $(".filt_year").removeClass("d-none");
        console.log("checked");
    } else {
        $(".filt_year").addClass("d-none");
        console.log("not checked");
    }
})
$("#ev_ryear").on('change', function () {
    if (this.checked) {
        $(".filt_ryear").removeClass("d-none");
        console.log("checked");
    } else {
        $(".filt_ryear").addClass("d-none");
        console.log("not checked");
    }
})
/////////////////////
$('#pdf_button').on('click', function () {
    detalleGlobal.length = 0;
    var year = $("#filt_year").val();
    get_det_topdf(year);
    var nEventosxy = document.querySelector("#filter_ryear");
    var img = nEventosxy.toDataURL("image/png", 1.0);
    var nEventos = filter_ryear.data['datasets'][0].data[0];
    // detalleGlobal.length = 0;
    // detalle_prodGlobal.length = 0;
    // get_prodXYear(year);
    if ($("#radio_pdf1").is(":checked")) {
        const doc = new jsPDF('p', 'mm');
        var pageHeight = doc.internal.pageSize.height;
        console.log(pageHeight);
        doc.setFont("Times-Roman");
        doc.setFontStyle("bold");
        doc.setFontSize(20);
        doc.text(45, 15, "NEXT GENERATION COCKTAILS S.R.L.");
        doc.setFontSize(20);
        doc.setFontStyle("normal");
        doc.text(70, 30, "Reporte de la gestion " + year);
        doc.setFontSize(12);
        doc.text(15, 55, "En el año " + year + " se realizaron #" + nEventos + " eventos, estos se pueden ver reflejados en la siguiente grafica: ");
        doc.addImage(img, "PNG", 60, 60, 100, 60);
        doc.text(15, 130, "Los cuales fueron ejecutados de la siguiente manera: ");
        var evento_mes = document.querySelector("#filter_year").toDataURL("image/png", 1.0);
        doc.addImage(evento_mes, "PNG", 60, 135, 100, 60);
        doc.text(15, 205, "Y a continuacion se muestra el detalle de cada mes de eventos realizados: ");
        setTimeout(function () {
            var yInicio = 215;
            detalleGlobal.forEach((element, i) => {
                // console.log(filter_year.data['labels'][i]);
                if (yInicio + 30 > pageHeight) {
                    yInicio = 15;
                    doc.addPage();
                }
                doc.setFontStyle("bold");
                doc.text(20, yInicio, filter_year.data['labels'][i] + ": ");
                doc.setFontStyle("normal");
                yInicio = yInicio + 10;
                for (const key in element) {
                    if (Object.hasOwnProperty.call(element, key)) {
                        const x = element[key];

                        doc.text(30, yInicio, "Nombre Evento: " + x.nom_eve);
                        yInicio = yInicio + 5;
                        doc.text(30, yInicio, "Nro Factura: " + x.fecha_eve);
                        yInicio = yInicio + 5;
                        doc.text(30, yInicio, "Fecha Evento: " + x.nro_factura);
                        yInicio = yInicio + 10;
                        if (yInicio + 10 > pageHeight) {
                            yInicio = 15;
                            doc.addPage();
                        }



                    }
                }
            })
            if (yInicio + 85 > pageHeight) {
                yInicio = 15;
                doc.addPage();
            }
            doc.text(15, yInicio, "En este periodo se vendieron los siguientes productos: ");
            yInicio = yInicio + 20;
            var prod_year = document.querySelector("#sumprod_year").toDataURL("image/png", 1.0);
            doc.addImage(prod_year, "PNG", 85, yInicio - 10, 120, 60);
            detalle_prodGlobal.forEach((element, i) => {
                // console.log(element);
                // console.log("Nombre: ", element.nom_prod);
                doc.setFontStyle("bold");
                doc.text(15, yInicio, "#" + (i + 1));
                doc.setFontStyle("normal");
                doc.text(25, yInicio, element.nom_prod + " : " + element.cant);
                yInicio = yInicio + 7;
            });
            yInicio = yInicio + 30;
            if (yInicio + 95 > pageHeight) {
                yInicio = 15;
                doc.addPage();
            }
            doc.text(15, yInicio, "Distribuidos en los siguientes meses: ");
            yInicio = yInicio + 10;
            var prod_month = document.querySelector("#prod_year").toDataURL("image/png", 1.0);
            doc.addImage(prod_month, "PNG", 40, yInicio, 140, 120);




            window.open(doc.output('bloburl'));
        }, 700);
    } else if ($("#radio_pdf2").is(":checked")) {
        var year1 = $("#filt_year_range1").val();
        var year2 = $("#filt_year_range2").val();
        const doc = new jsPDF('p', 'mm');
        var pageHeight = doc.internal.pageSize.height;
        console.log(pageHeight);
        doc.setFont("Times-Roman");
        doc.setFontStyle("bold");
        doc.setFontSize(20);
        doc.text(45, 15, "NEXT GENERATION COCKTAILS S.R.L.");
        doc.setFontSize(20);
        doc.setFontStyle("normal");
        doc.text(50, 30, "Reporte de la gestión " + year1 + " a la gestión " + year2);
        doc.setFontSize(12);
        doc.text(10, 55, "En este periodo se realizaron eventos, estos se pueden ver reflejados de manera cuantitativa en la siguiente grafica: ");
        doc.addImage(img, "PNG", 60, 60, 100, 60);
        doc.text(15, 130, "Los cuales fueron ejecutados de la siguiente manera: ");
        var evento_mes = document.querySelector("#filter_year").toDataURL("image/png", 1.0);
        doc.addImage(evento_mes, "PNG", 60, 135, 100, 60);
        doc.text(15, 205, "Y a continuacion se muestra el detalle de cada mes de eventos realizados: ");
        setTimeout(function () {
            var yInicio = 215;
            detalleGlobal.forEach((element, i) => {
                // console.log(filter_year.data['labels'][i]);
                if (yInicio + 30 > pageHeight) {
                    yInicio = 15;
                    doc.addPage();
                }
                doc.setFontStyle("bold");
                doc.text(20, yInicio, filter_year.data['labels'][i] + ": ");
                doc.setFontStyle("normal");
                yInicio = yInicio + 10;
                for (const key in element) {
                    if (Object.hasOwnProperty.call(element, key)) {
                        const x = element[key];

                        doc.text(30, yInicio, "Nombre Evento: " + x.nom_eve);
                        yInicio = yInicio + 5;
                        doc.text(30, yInicio, "Nro Factura: " + x.nro_factura);
                        yInicio = yInicio + 5;
                        doc.text(30, yInicio, "Fecha Evento: " + x.fecha_eve);
                        yInicio = yInicio + 10;
                        if (yInicio + 10 > pageHeight) {
                            yInicio = 15;
                            doc.addPage();
                        }



                    }
                }
            })
            if (yInicio + 85 > pageHeight) {
                yInicio = 15;
                doc.addPage();
            }
            doc.text(15, yInicio, "En este periodo se vendieron los siguientes productos: ");
            yInicio = yInicio + 20;
            var prod_year = document.querySelector("#sumprod_year").toDataURL("image/png", 1.0);
            doc.addImage(prod_year, "PNG", 75, yInicio - 10, 120, 60);
            detalle_prodGlobal.forEach((element, i) => {
                // console.log(element);
                // console.log("Nombre: ", element.nom_prod);
                doc.setFontStyle("bold");
                doc.text(15, yInicio, "#" + (i + 1));
                doc.setFontStyle("normal");
                doc.text(25, yInicio, element.nom_prod + " : " + element.cant);
                yInicio = yInicio + 7;
            });
            yInicio = yInicio + 30;
            if (yInicio + 90 > pageHeight) {
                yInicio = 15;
                doc.addPage();
            }
            doc.text(15, yInicio, "Distribuidos en los siguientes meses: ");
            yInicio = yInicio + 10;
            var prod_month = document.querySelector("#prod_year").toDataURL("image/png", 1.0);
            doc.addImage(prod_month, "PNG", 30, yInicio, 160, 95);




            window.open(doc.output('bloburl'));
        }, 700);
    }
})

function get_det_topdf(year) {

    console.log("filterYear");
    if ($("#radio_pdf1").is(":checked")) {
        filter_year.data['datasets'][0].auxiliar.forEach((element, i) => {
            console.log("elemento: " + element);
            $.ajax({
                method: "POST",
                url: "get/eventoDetalle",
                headers: { 'X-Requested-With': 'XMLHttpRequest' },
                dataType: 'json',
                data: {
                    'year': year,
                    'month': element
                },
                success: function (result) {
                    console.log('Para Prueba Reporte: ', result, " mes: ", element, "iteracion: ", i);
                    // var x = JSON.parse(result);
                    detalleGlobal.push(result);


                }
            }).done(function () {

                console.log("detalle Global: ", detalleGlobal);
            });
        })
    } else if ($("#radio_pdf2").is(":checked")) {
        console.log(filter_year.data['datasets'][0].auxiliar);
        filter_year.data['datasets'][0].auxiliar.forEach((element, i) => {
            var y = filter_year.data['datasets'][0].years[i];
            // console.log("elemento: " + element + " " + y);
            // console.log(y);
            $.ajax({
                method: "POST",
                url: "get/eventoDetalle",
                headers: { 'X-Requested-With': 'XMLHttpRequest' },
                dataType: 'json',
                data: {
                    'year': y,
                    'month': element
                },
                success: function (result) {
                    // console.log('Para Prueba Reporte: ', result, " mes: ", element, "iteracion: ", i);
                    // var x = JSON.parse(result);
                    detalleGlobal.push(result);


                }
            }).done(function () {

                console.log("detalle Global: ", detalleGlobal);
            });
        })
    }

}
function get_prodXYear(year) {

    $.ajax({
        method: "POST",
        url: "get/prodXYear",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'year': year
        },
        success: function (result) {
            // console.log('Para Estadistica Reporte: ', result);
            for (const key in result) {
                if (Object.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    detalle_prodGlobal.push(element);
                }

            }


        }
    });
}
$(document).on("click", ".btn_pdf_eventos_det", function () {
    var index = $("input[name='index_dataEv']").val();
    const doc = new jsPDF('p', 'mm');
    doc.setFont("Times-Roman");
    doc.setFontStyle("bold");
    doc.setFontSize(20);
    var textWidth = doc.getTextWidth("DETALLE DE EVENTO");
    var pageHeight = doc.internal.pageSize.height;
    doc.line(65, 16, 65 + textWidth, 16);
    doc.text(65, 15, "DETALLE DE EVENTO");
    doc.setFontSize(14);
    doc.text(15, 45, "Nombre de Evento: ");
    doc.text(15, 55, "Fecha de Evento: ");
    doc.text(120, 45, "Número de Factura: ");
    doc.text(120, 55, "Número de Proforma: ");
    doc.setFontStyle("normal");
    doc.setFontSize(12);
    doc.text(57, 45, EventData[index].nom_eve);
    doc.text(57, 55, EventData[index].fecha_eve);
    textWidth = doc.getTextWidth(EventData[index].nom_eve.toString());
    doc.text(130 + textWidth, 45, EventData[index].nro_factura);
    doc.text(130 + textWidth, 55, EventData[index].num_prof);
    doc.setFontSize(18);
    doc.setFontStyle("bold");
    doc.text(15, 70, "Personal Asignado del Evento");
    var y = 85;
    doc.setFontSize(12);
    PersonAsig.forEach((element, i) => {
        doc.setTextColor(0, 0, 0, 0);
        doc.line(15, y - 8, 120, y - 8);
        doc.line(15, y - 8, 15, y + 20);
        doc.line(120, y - 8, 120, y + 20);
        doc.setFontStyle("bold");
        doc.text(20, y, (i + 1).toString() + '.');
        doc.text(25, y, element.nom_cargo);
        doc.setFontStyle("normal");
        doc.text(25, y + 10, element.nom_per + " " + element.app_per + " " + element.apm_per);
        textWidth = doc.getTextWidth((element.nom_per + " " + element.app_per + " " + element.apm_per).toString());
        // doc.setTextColor(255, 255, 255, 100);
        doc.setFillColor(158, 205, 255);
        var cost = doc.getTextWidth(element.costo_dtp + " Bs.");
        doc.roundedRect(85, y - 1, 25, 10, 5, 5, 'DF');
        doc.text(90, y + 5, element.costo_dtp + " Bs.");
        y += 25;

    });
    doc.line(15, y - 5, 120, y - 5);
    doc.setFontSize(18);
    doc.setFontStyle("bold");
    doc.text(15, y + 10, "Productos Vendidos");
    y += 25;
    doc.setFontSize(12);
    ProdAsig.forEach((element, i) => {
        // console.log(element);
        doc.line(15, y - 8, 120, y - 8);
        doc.line(15, y - 8, 15, y + 20);
        doc.line(120, y - 8, 120, y + 20);
        doc.setFontStyle("bold");
        doc.text(20, y, (i + 1).toString() + '.');
        doc.text(25, y, element.nom_prod.toString());
        doc.setFontStyle("normal");
        doc.text(25, y + 10, "Cantidad: " + element.cantidad.toString());
        doc.setFillColor(254, 255, 152);
        doc.roundedRect(85, y - 1, 25, 10, 5, 5, 'DF');
        doc.text(90, y + 5, element.costo_dtp + " Bs.");
        y += 25;
    })
    if (y + 30 > pageHeight) {
        y = 15;
        doc.addPage();
    }
    doc.line(15, y - 5, 120, y - 5);
    doc.setFontStyle("bold");
    doc.setFontSize(18);
    doc.text(15, y + 10, "Insumo Utilizados");
    doc.setFontSize(12);
    y += 22
    insumoUti.forEach(element => {
        doc.setFontStyle("bold");
        doc.text(25, y, element.nom_ins + ":");
        textWidth = doc.getTextWidth(element.nom_ins.toString());
        doc.setFontStyle("Normal");
        doc.text(30 + textWidth, y, element.cantidad + " " + element.unidad_ins);
        y += 10;
        if (y + 20 > pageHeight) {
            y = 15;
            doc.addPage();
        }

    })
    y += 15;
    var res = doc.autoTableHtmlToJson(document.getElementById("t-detEv"));
    doc.autoTable(res.columns, res.data, {
        startY: y,
        margin: { horizontal: 10 },
        tableLineColor: 200,
        tableLineWidth: 0,
        height: 'auto',
        bodyStyles: {
            valign: 'top',
            minCellHeight: 8,
        },

        styles: {
            overflow: 'linebreak',
            fontSize: 10,
            cellPadding: 2,
        },


        columnStyles: {

            10: { cellWidth: 39 },

            text: { cellWidth: 'auto' },
            nil: { halign: 'right' },
            tgl: { halign: 'right' }
        },
        theme: 'grid',
        // tableWidth: 'auto',
    });
    window.open(doc.output('bloburl'));
})