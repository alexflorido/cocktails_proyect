var contador = 0;
var auxiliar = 0;
var cd = new Date();
var id_proforma;
var n_prof;
var productos = [];
var det_prod;
var cantidad_prod;
var id_list;
var id_eve;
var insAlmacen;
$(document).ready(function () {
    mapboxgl.accessToken = 'pk.eyJ1IjoidGludG9yIiwiYSI6ImNqd2ltcXZpbjA1am80YW5jcGFhZXNuYmcifQ.RUG_jZrBQmKP5NtQj2FhPg';
    const map = new mapboxgl.Map({
        container: 'map', // container ID
        style: 'mapbox://styles/mapbox/streets-v11', // style URL
        center: [-63.20302845977794, -17.76644364828489], // starting position [lng, lat]
        zoom: 14 // starting zoom
    });
    const marker = new mapboxgl.Marker({
        draggable: true
    }).setLngLat([-63.20302845977794, -17.76644364828489]).addTo(map);
    function onDragEnd() {
        const lngLat = marker.getLngLat();
        coordinates.style.display = 'block';
        coordinates.innerHTML = `Longitude: ${lngLat.lng}<br />Latitude: ${lngLat.lat}`;
        $("input[name='coordinates_lat']").val(lngLat.lat);
        $("input[name='coordinates_lng']").val(lngLat.lng);
    }
    marker.on('dragend', onDragEnd);
    var meses = new Array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    document.getElementById("fecha_prof_logo").innerHTML = cd.getDate() + " de " + meses[cd.getMonth()] + " de " + cd.getFullYear();
    $.ajax({
        method: "GET",
        url: "cliente",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        success: function (result) {
            // console.log(result);


            for (const key in result) {
                if (result.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    $("#list_client").append("<option class='" + element['nom_cli'] + "' id='" + element['id_cli'] + "' value='" + element['nom_cli'] + "' data-nit='" + element['nit_cli'] + "'></option>");

                }
            }

        }

    });


});
$(document).on("change", "#search_client", function (event) {
    var s = $(this).val();
    var x = 0;
    $('#list_client option').each(function () {
        if ($(this).attr("class") == s) {
            // console.log($(this).attr('id'));
            // console.log($(this).data("nit"));
            // var pc = get_PC_proforma($(this).attr('id'));
            $("#nit_cli").val($(this).data("nit"));
            $('#search_pc').html('');
            $('#tel_pc').val('');
            $('#event_id_cli').val($(this).attr('id'));
            get_PC_proforma($(this).attr('id'));
            // console.log(pc);


        }


    });
});

$(document).on("click", "#togeolocal", function (event) {
    event.preventDefault();
    console.log("click");
    $("#modal_ug").modal("show");
})

$(document).on('click', '.pc', function () {
    var tel = $(this).data('tel');
    // console.log($(this).data('tel'));
    $("#search_pc").html($(this).html());
    $('#tel_pc').val(tel);
    $('#event_id_pc').val($(this).attr('id'));

});
function get_PC_proforma(id) {
    $("#list_pc").html("");
    $.ajax({
        method: "GET",
        url: "persona_contacto",
        data: { "id_cli": id },
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        success: function (result) {
            // console.log(result);
            for (const key in result) {
                if (result.hasOwnProperty.call(result, key)) {
                    const element = result[key];
                    // console.log(element['nom_per_cont']);
                    // $("#list_pc").append("<option class='pc " + element['nom_per_cont'] + "' id='" + element['id_per_cont'] + "' value='" + element['nom_per_cont'] + " " + element['app_per_cont'] + "'></option>");
                    $("#list_pc").append("<li ><a   id='" + element['id_per_cont'] + "' class='pc dropdown-item' href='#' data-tel='" + element['tel_per_cont'] + "'>" + element['nom_per_cont'] + " " + element['app_per_cont'] + "</a></li>");

                }
            }

        }
    });

}
$(document).on('click', "#add_personal", function () {
    $('#table_prof_add').DataTable().destroy();
    $('#table_prof_add').DataTable({

        ajax: {
            method: "GET",
            url: "personalFijo/activo",
            dataSrc: ""

        },
        columns: [

            { data: 'nom_per' },
            { data: 'nom_cargo' },
            { data: 'estado' },
            { data: 'ciudad' },
            { data: 'cost_per' },
            {
                ordenable: true,
                render: function (data, type, row) {
                    return "<button id='btn_modificar' name='btn_modificar' type='button' class='btn rounded-circle input-md fa fa-chevron-circle-right' value='' data-mod='mod' onclick=" + `add_to_proform('${row.nom_cargo.replace(/ /g, "")}',${row.cost_per},'${row.nom_per.replace(/ /g, "")}','${row.app_per.replace(/ /g, "")}',${row.id_per},0)></button>`
                }
            },

        ],
        columnDefs: [
            {
                targets: [0],
                data: 'nom_per',
                render: function (data, type, row) {
                    return data + " " + row.app_per;

                }

            },
        ],
        stateSave: false,
    });

    $("#head_producto").addClass("d-none");
    $("#head_personal").removeClass("d-none");
    $("#modal_prof").modal("show");
    $("#modal_prof_tittle").html("Lista de Personal");
});
$(document).on('click', "#add_producto", function () {
    $('#table_prof_add').DataTable().destroy();
    $("#head_personal").addClass("d-none");
    $("#head_producto").removeClass("d-none");


    $('#table_prof_add').DataTable({
        ajax: {
            method: "GET",
            url: "producto",
            dataSrc: ""

        },
        columns: [

            { data: 'nom_prod' },
            { data: 'fecha_prod' },
            { data: 'desc_prod' },
            { data: 'costo_total_prod' },
            { data: 'precio_total_prod' },

            {
                ordenable: true,
                render: function (data, type, row) {
                    return "<button id='btn_modificar' name='btn_modificar' type='button' class='btn rounded-circle input-md fa fa-chevron-circle-right' value='' data-id='" + row.id_ins + "' data-nom='" + row.nom_ins + "' data-cost='" + row.cost_ins + "' data-und='" + row.unidad_ins + "' data-fecha='" + row.fecha_actu_ins + "'  onclick=\"add_to_proform('" + row.nom_prod + "'," + row.precio_total_prod + ",'',''," + row.id_prod + ",1)\"></button>"
                }
            },

        ],
        stateSave: false,
    });

    $("#modal_prof").modal("show");
    $("#modal_prof_tittle").html("Lista de Productos");
});

function add_to_proform(detalle, cost_unitario, nom_per, app_per, id, type) {

    contador++;
    // console.log(contador);
    // console.log("auxiliar", auxiliar);

    if (contador <= 6) {
        auxiliar = contador * 8;
    }
    if (contador == 7) {
        auxiliar = contador * 9;
    }
    // if (contador==5 || contador==4) {
    //     auxiliar = 30;
    // }
    // if(contador==6){
    //     auxiliar = 40;
    // }
    // if(contador==7){
    //     auxiliar = 50;
    // }
    // if(contador==8){
    //     auxiliar = 60;
    // }
    console.log("auxiliar", auxiliar);

    $("#tbody_table").append("<tr data-isactive=''>" +
        "<td style='border: 2px solid rgba(40, 60, 209, 0.645); '><input id='cantidad' class='cantidad-prof form-control text-center col-md-5' style='border:0' type='text' value=1><input id='id' type='hidden' value='" + id + "'></td>" +
        "<td style='border: 2px solid rgba(40, 60, 209, 0.645);' class='td_prof_body '><input id='detalle' class='inp_prof form-control text-center desc_table' style='border:0' type='text' value='" + detalle + "' data-nom ='" + nom_per + " " + app_per + "' data-type=" + type + "></td>" +
        "<td style='border: 2px solid rgba(40, 60, 209, 0.645); ' class='td_prof_body precio_unitario-a '>" + cost_unitario + "</td>" +
        "<td style='border: 2px solid rgba(40, 60, 209, 0.645); ' class='td_prof_body importe-a'><input class='inp_prof form-control importe text-center col-md-9' type='number' style='border:0' value='" + cost_unitario + "'></td>" +
        "<td style='border: 2px solid rgba(40, 60, 209, 0.645);' class='td_prof_body precio_fin-a '><input id='precio_fin' class='inp_prof precio_fin form-control text-center col-md-9' type='number' style='border:0' value='" + cost_unitario + "'></td>" +
        "</tr>");

    get_total();

}
$(document).on('mouseover', '.desc_table', function (event) {


    if ($(this).data("type") == 0) {

        $('#liveToast').removeClass("toast_per");
        $('#desc_toast').html($(this).data('nom'))


        setTimeout(function () {
            $('#liveToast').addClass("toast_per");

        }, 7000);
    }


});

$(document).on('change', '.inp_prof', function () {
    get_total();
});

$(document).on('change', '.cantidad-prof', function () {

    var precio_fin = $(this).parents('tr').find('.precio_fin-a').children("input");
    var importe = $(this).parents('tr').find('.importe-a').children("input");
    var precio_unitario = $(this).parents('tr').find('.precio_unitario-a').html();
    var canti = $(this).val();
    precio_fin.val(parseFloat(canti * precio_unitario).toFixed(2));
    importe.val(parseFloat(canti * precio_unitario).toFixed(2));
    get_total();

});

$(document).on('click', '#create_pdf', function () {
    $(".to_pdf").addClass("d-none");

    const imgs = [document.querySelector("#logo_prof"), document.querySelector("#datos"), document.querySelector("#thead_table"), document.querySelector("#tbody_table"), document.querySelector("#tfoot_table")];
    const doc = new jsPDF('p', 'mm');
    imgs.forEach((a, i) => {
        html2canvas(a)
            .then(canvas => {
                const img = canvas.toDataURL('image/png');

                // doc.addImage(img,'PNG', 45, 45, 100, 100);
                if (i == 0) {
                    console.log(i);
                    doc.addImage(img, 'PNG', 8, 10, 205, 40);
                    doc.setFontSize(7);
                    doc.setTextColor(28, 40, 131);
                    doc.text(30, 54, "Av. Piraí entre 4to y 5to anillo, Edificio Santa Mónica Of. 5A- Telefono:(591) 69420001 - Santa Cruz, Bolivia");

                }
                if (i == 1) {
                    console.log(i);
                    doc.addImage(img, 'PNG', 10, 55, 190, 40);
                }
                if (i == 2) {
                    console.log(i);
                    doc.addImage(img, 'PNG', 10, 97, 190, 15);
                }
                if (i == 3) {
                    console.log(i);
                    doc.addImage(img, 'PNG', 10, 112, 190, auxiliar);
                }
                if (i == 4) {
                    console.log(i);
                    var y = 112 + auxiliar;
                    doc.addImage(img, 'PNG', 10, y, 190, 50);
                }



            })
            .then(() => {
                if ((i + 1) === imgs.length) {
                    window.open(doc.output('bloburl'));
                    var pdf = btoa(doc.output());
                    var data = new FormData();
                    data.append("data", pdf);
                    data.append("id_prof", id_proforma);
                    var xhr = new XMLHttpRequest();
                    xhr.open('post', 'pdf/insertPDF', true);
                    // alert(data);
                    xhr.send(data);
                    // doc.save('MyPdf.pdf');
                }
            })


    })




});
function get_total() {
    console.log("estoy en la funcion");
    var total_importe = parseFloat(0);
    var total_precio = 0;
    var imp_importe = 0;
    var imp_precio = 0;
    $('#tbody_table tr').each(function () {

        var precio_fin = parseFloat($(this).find('.importe-a').children("input").val()).toFixed(2);
        var importe = parseFloat($(this).find('.precio_fin-a').children("input").val()).toFixed(2);
        console.log(importe);
        total_importe = parseFloat(total_importe) + parseFloat(importe);
        total_precio = parseFloat(total_precio) + parseFloat(precio_fin);
        console.log(total_importe);
    });
    imp_importe = (total_importe * 0.16).toFixed(2);
    imp_precio = (total_precio * 0.16).toFixed(2);
    var precio_total = parseFloat(imp_importe) + parseFloat(total_importe);
    var importe_total = parseFloat(imp_precio) + parseFloat(total_precio);
    $('#sub_total_importe').val(total_precio.toFixed(2));
    $('#sub_total_precio').val(total_importe.toFixed(2));
    $('#imp_importe').val(imp_precio);
    $('#imp_precio').val(imp_importe);
    $('#total_importe').val(importe_total.toFixed(2));
    $('#total_precio').val(precio_total.toFixed(2));
    $('#letra-numero').html(NumeroALetras(importe_total.toFixed(2)));
    // var x = document.createElement("div");

    // x.textContent = "SON: " + NumeroALetras(importe_total.toFixed(2));
    // console.log(x);
    // document.getElementById("letra-numero").appendChild(x);
    // console.log(NumeroALetras(importe_total.toFixed(2)));
    cent(precio_total.toFixed(2));

}
// Sacar centavos
function cent(num) {
    var cent = (((Math.round(num * 100)) - (Math.floor(num) * 100)));
    var cadena = "";
    cadena += "CON " + cent + "/100";
    // console.log(cadena);
    document.getElementById("centavo_num").innerHTML = cadena;

}
// DE NUMERO A LETRAS
function Unidades(num) {

    switch (num) {
        case 1: return "UN";
        case 2: return "DOS";
        case 3: return "TRES";
        case 4: return "CUATRO";
        case 5: return "CINCO";
        case 6: return "SEIS";
        case 7: return "SIETE";
        case 8: return "OCHO";
        case 9: return "NUEVE";
    }

    return "";
}

function Decenas(num) {

    decena = Math.floor(num / 10);
    unidad = num - (decena * 10);

    switch (decena) {
        case 1:
            switch (unidad) {
                case 0: return "DIEZ";
                case 1: return "ONCE";
                case 2: return "DOCE";
                case 3: return "TRECE";
                case 4: return "CATORCE";
                case 5: return "QUINCE";
                default: return "DIECI" + Unidades(unidad);
            }
        case 2:
            switch (unidad) {
                case 0: return "VEINTE";
                default: return "VEINTI" + Unidades(unidad);
            }
        case 3: return DecenasY("TREINTA", unidad);
        case 4: return DecenasY("CUARENTA", unidad);
        case 5: return DecenasY("CINCUENTA", unidad);
        case 6: return DecenasY("SESENTA", unidad);
        case 7: return DecenasY("SETENTA", unidad);
        case 8: return DecenasY("OCHENTA", unidad);
        case 9: return DecenasY("NOVENTA", unidad);
        case 0: return Unidades(unidad);
    }
}//Unidades()

function DecenasY(strSin, numUnidades) {
    if (numUnidades > 0)
        return strSin + " Y " + Unidades(numUnidades)

    return strSin;
}//DecenasY()

function Centenas(num) {

    centenas = Math.floor(num / 100);
    decenas = num - (centenas * 100);

    switch (centenas) {
        case 1:
            if (decenas > 0)
                return "CIENTO " + Decenas(decenas);
            return "CIEN";
        case 2: return "DOSCIENTOS " + Decenas(decenas);
        case 3: return "TRESCIENTOS " + Decenas(decenas);
        case 4: return "CUATROCIENTOS " + Decenas(decenas);
        case 5: return "QUINIENTOS " + Decenas(decenas);
        case 6: return "SEISCIENTOS " + Decenas(decenas);
        case 7: return "SETECIENTOS " + Decenas(decenas);
        case 8: return "OCHOCIENTOS " + Decenas(decenas);
        case 9: return "NOVECIENTOS " + Decenas(decenas);
    }

    return Decenas(decenas);
}//Centenas()

function Seccion(num, divisor, strSingular, strPlural) {
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    letras = "";

    if (cientos > 0)
        if (cientos > 1)
            letras = Centenas(cientos) + " " + strPlural;
        else
            letras = strSingular;

    if (resto > 0)
        letras += "";

    return letras;
}//Seccion()

function Miles(num) {
    divisor = 1000;
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    strMiles = Seccion(num, divisor, "MIL", "MIL");
    strCentenas = Centenas(resto);

    if (strMiles == "")
        return strCentenas;

    return strMiles + " " + strCentenas;

    //return Seccion(num, divisor, "UN MIL", "MIL") + " " + Centenas(resto);
}//Miles()

function Millones(num) {
    divisor = 1000000;
    cientos = Math.floor(num / divisor)
    resto = num - (cientos * divisor)

    strMillones = Seccion(num, divisor, "UN MILLON", "MILLONES");
    strMiles = Miles(resto);

    if (strMillones == "")
        return strMiles;

    return strMillones + " " + strMiles;

    //return Seccion(num, divisor, "UN MILLON", "MILLONES") + " " + Miles(resto);
}//Millones()

function NumeroALetras(num, centavos) {
    var data = {
        numero: num,
        enteros: Math.floor(num),
        centavos: (((Math.round(num * 100)) - (Math.floor(num) * 100))),
        letrasCentavos: "",
    };
    if (centavos == undefined || centavos == false) {
        data.letrasMonedaPlural = "";
        data.letrasMonedaSingular = "";
    } else {
        data.letrasMonedaPlural = "";
        data.letrasMonedaSingular = "";
    }

    if (data.centavos > 0)
        //   data.letrasCentavos = "CON " + NumeroALetras(data.centavos,true);

        if (data.enteros == 0)
            return "CERO " + data.letrasMonedaPlural + " " + data.letrasCentavos;
    if (data.enteros == 1)
        return Millones(data.enteros) + " " + data.letrasMonedaSingular + " " + data.letrasCentavos;
    else
        return Millones(data.enteros) + " " + data.letrasMonedaPlural + " " + data.letrasCentavos;
}//NumeroALetras()
$(document).on("submit", "#form_reg_event", function (event) {

    event.preventDefault();
    // $('#list-errors_cli').html('');
    var formData = new FormData($(this)[0]);
    // var url = $(this).attr('action');

    // Si el formulario no tiene Id_per debe registrar nuevo usuario
    if ($('#is_new').val() == 1) {

        $.ajax({
            url: "insert/event",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                var data = JSON.parse(respuesta)
                console.log(data);
                id_proforma = data['id_prof'];
                n_prof = data['n_prof'];
                id_eve = data['id_eve'];
                $('#num_prof_show').append('N° ' + n_prof);
            }
        }).done(function () {
            alert("Se Registro El Evento \n Puede Llenar la Proforma");
            $('#add_personal').removeAttr("disabled");
            $('#add_producto').removeAttr("disabled");
            $('#insert_proforma').removeAttr("disabled");
            $('#insert_proforma').addClass('btn-success');
            $('#insert_proforma').removeClass('btn-danger');
            $('#tab_eve').DataTable().ajax.reload();
        });
    }
    else {
        $.ajax({
            url: "insert/proform",
            type: $(this).attr('method'),
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (respuesta) {
                var data = JSON.parse(respuesta)
                console.log(data);
                id_proforma = data['id_prof'];
                n_prof = data['n_prof'];
                id_eve = data['id_eve'];
                $('#num_prof_show').append('N° ' + n_prof);
            }
        }).done(function () {
            alert("Se Registro El Evento \n Puede Llenar la Proforma");
            $('#add_personal').removeAttr("disabled");
            $('#add_producto').removeAttr("disabled");
            $('#insert_proforma').removeAttr("disabled");
            $('#insert_proforma').addClass('btn-success');
            $('#insert_proforma').removeClass('btn-danger');
        });
    }



});
$(document).on("click", "#tbody_table tr", function () {
    if ($(this).data("isactive") == "active") {

        $(this).css("background-color", "white");
        $(this).data("isactive", "");
    } else {
        $(this).data("isactive", "active");
        $(this).css("background-color", "#acbad4");
    }
});
$(document).on("click", "#eliminar_producto", function () {
    console.log("eliminar");
    $("#tbody_table tr").each(function () {
        if ($(this).data("isactive") == "active") {
            $(this).remove();
            get_total();
        }

    });
})

$(document).on("click", "#insert_proforma_detalle", function () {
    var i = 1;
    productos.length = 0;
    update_totals_proform();
    $("#tbody_table tr").each(function () {

        var tipo_d;
        if ($(this).find('#detalle').data('type') == 0) {

            tipo_d = 0;
            var id = $(this).find('#id').val();
            var cant = $(this).find('#cantidad').val();
            var cost_uni = $(this).find('.precio_unitario-a').html();
            var total_det = $(this).find('#precio_fin').val();
            setTimeout(function () { insert_per_prof(id, cant, cost_uni, total_det, tipo_d); }, 500);


        }
        else if ($(this).find('#detalle').data('type') == 1) {
            tipo_d = 1;
            var id = $(this).find('#id').val();
            var product_name = $(this).find('#detalle').val();
            var cant = $(this).find('#cantidad').val();
            var product = {
                "id_prod": id,
                "name": product_name,
                "cant": cant
            };

            productos.push(product);

            var cost_uni = $(this).find('.precio_unitario-a').html();
            var total_det = $(this).find('#precio_fin').val();
            setTimeout(function () { insert_per_prof(id, cant, cost_uni, total_det, tipo_d); }, 500);

        }


    });
    alert("Proforma Registrada");;
});
// //////////////////
var personalToNotificate;
function personal_toList() {
    $('#table_shopList_personal').DataTable().destroy();
    personalToNotificate = $('#table_shopList_personal').DataTable({

        ajax: {
            method: "GET",
            url: "personalFijo",
            dataSrc: ""

        },
        select: 'single',
        columns: [

            { data: 'nom_per' },
            { data: 'nom_cargo' },
            { data: 'ciudad' },
            {
                ordenable: true,
                render: function (data, type, row) {
                    return "<button name='quitarLista' type='button' class='btn rounded-circle input-md fa fa-chevron-circle-right' value=''></button>"
                }
            },

        ],
        columnDefs: [
            {
                targets: [0],
                data: 'nom_per',
                render: function (data, type, row) {
                    return data + " " + row.app_per;

                }

            },
        ],
        stateSave: false,
    });

    $("#head_producto").addClass("d-none");
    $("#head_personal").removeClass("d-none");
    // $("#modal_prof").modal("show");
    // $("#modal_prof_tittle").html("Lista de Personal");
};






// /////////////////////////////




$(document).on("click", "#generar_lista", function () {
    personal_toList();
    // console.log("lista de productos", productos);
    $("#tbody_list_comp").empty();
    for (const key in productos) {
        if (Object.hasOwnProperty.call(productos, key)) {
            const element = productos[key];
            $("#tbody_list_comp").append("<tr>" +
                `<td>${element['name']}<input type="hidden" name="id_prod" value = "${element['id_prod']}" /></td>
                <td class="td_cant">${element["cant"]}</td>
                <td class="text-center"><button class="btn btn-outline-secondary rounded-circle generar_lideprod"><i class="fa fa-arrow-circle-right"></i></button></td>`
            );


        }
    }
    get_insumos_almacen();
    $("#modal_list_comp").modal("show");

});
function get_insumos_almacen() {
    $("#tbody_ins_almacen").empty();
    $.ajax({
        method: "POST",
        url: "insumo_almacen",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'event_date': $("#event_date").val(),
        },
        success: function (respuesta) {
            console.log(respuesta);
            insAlmacen = respuesta;
            for (const key in respuesta) {
                if (Object.hasOwnProperty.call(respuesta, key)) {
                    const element = respuesta[key];
                    if (element['id_ins'] == 0) {
                        // console.log("es Receta");
                        $("#tbody_ins_almacen").append(`<tr data-prod="${element['id_rec']}" data-type="receta">
                        <td class='nom_rec'>${element['nom_rec']}</td>
                        <td class="cantidad_prod" data-cantidad="${element['cant_almacen']}"> ${element['cant_almacen']}</td>
                        <td class="text-center"><button class="btn btn-outline-danger rounded-circle rest_insAlmacen_receta"><i class="fas fa-minus-square"></i></button></td>
                        </tr>`);
                    }
                    else {
                        // console.log("es Insumo");
                        $("#tbody_ins_almacen").append(`<tr data-prod="${element['id_ins']}" class="insumo">
                        <td>${element['nom_ins']}</td>
                        <td class="cantidad_prod" data-cantidad="${element['cant_almacen']}"> ${element['cant_almacen']}</td>
                        <td class="text-center"><button class="btn btn-outline-danger rounded-circle rest_insAlmacen"><i class="fas fa-minus-square"></i></button></td>
                        </tr>`);
                    }

                }
            }


        }

    });
}
$(document).on('click', ".rest_insAlmacen", function () {
    var cant = $(this).parents("tr").find(".cantidad_prod");

    var id = $(this).parents("tr").data("prod");
    if (cant.html() == 0) {
        alert("Cantidad Insuficiente");
    } else {
        $("#tbody_shopList tr").each(function () {
            if ($(this).find('input[name="id_ins"]').val() == id) {
                var costoUnitario = $(this).find("input[name='prec_fin']").val() / $(this).find('input[name="cantidad_ins"]').val();

                console.log("origin: ", $(this).find('input[name="id_ins"]').val());
                if ($(this).find('input[name="cantidad_ins"]').val() > parseFloat(cant.html())) {
                    var cantTotal = $(this).find('input[name="cantidad_ins"]').val() - parseFloat(cant.data("cantidad"));
                    $(this).find('input[name="cantidad_ins"]').val(cantTotal.toFixed(4));
                    $(this).find("input[name='prec_fin']").val((costoUnitario * cantTotal).toFixed(4));
                    cant.html(0);
                    console.log("IF: ", cant.html());
                } else {
                    $(this).find('input[name="cantidad_ins"]').val(0);
                    var sum = parseFloat(cant.html()) - $(this).find('input[name="cantidad_ins"]').val();
                    cant.html(sum.toFixed(4));
                    console.log("Else ", $(this).find('input[name="cantidad_ins"]').val());
                }
            }
            else {
                alert("Insumo no esta en la lista de compras")
            }
        });
    }
});
$(document).on('click', ".rest_insAlmacen_receta", function () {
    var cant = $(this).parents("tr").find(".cantidad_prod");

    var nom = $(this).parents("tr").find(".nom_rec").html();
    if (cant.html() == 0) {
        alert("Cantidad Insuficiente");
    } else {
        $("#tbody_det_prod_comp tr").each(function () {
            if ($(this).find('.nom_rec').html() == nom) {
                console.log($(this).find('.nom_rec').html());
                // var costoUnitario = $(this).find("input[name='prec_fin']").val() / $(this).find('input[name="cantidad_ins"]').val();

                // console.log("origin: ", $(this).find('input[name="id_ins"]').val());
                if ($(this).find('.cantidad_prod').html() > parseFloat(cant.html())) {

                    var cantTotal = parseFloat($(this).find('.cantidad_prod').html()) - parseFloat(cant.data("cantidad"));
                    $(this).find('.cantidad_prod').html(cantTotal.toFixed(4));
                    // $(this).find("input[name='prec_fin']").val((costoUnitario * cantTotal).toFixed(4));
                    cant.html(0);
                    console.log("IF: ", cant.html());
                } else {
                    var sum = parseFloat(cant.html()) - $(this).find('.cantidad_prod').html();
                    cant.html(sum.toFixed(4));
                    $(this).find('.cantidad_prod').html(0);
                    console.log("Else ", $(this).find('input[name="cantidad_ins"]').val());
                }
            }

        });
    }
});
$(document).on("click", ".generar_lideprod", function () {
    var id_prod = $(this).parents('tr').find('input').val();
    var cant = $(this).parents('tr').find(".td_cant").html();
    $("#tbody_det_prod_comp").empty();

    $.ajax({
        method: "POST",
        url: "get/det_list_prod",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'id_prod': id_prod,
        },
        success: function (respuesta) {
            det_prod = respuesta;
            for (const key in respuesta) {
                if (Object.hasOwnProperty.call(respuesta, key)) {
                    const element = respuesta[key];
                    if (element['id_ins'] == 0) {
                        // console.log("es Receta");
                        $("#tbody_det_prod_comp").append(`<tr data-prod="${element['id_det_prod']}" data-type="receta">
                        <td class='nom_rec'>${element['nom_rec']}</td>
                        <td class="cantidad_prod" data-cantidad="${(element['cantidad_prod'] * cant).toFixed(4)}"> ${(element['cantidad_prod'] * cant).toFixed(4)}</td>
                        <td class="text-center"><button class="btn btn-outline-success rounded-circle add_to_shoplist"><i class="fa fa-plus-square"></i></button></td>
                        </tr>`);
                    }
                    else {
                        // console.log("es Insumo");
                        $("#tbody_det_prod_comp").append(`<tr data-prod="${element['id_det_prod']}" data-type="insumo">
                        <td>${element['nom_ins']}</td>
                        <td class="cantidad_prod" data-cantidad="${(element['cantidad_prod'] * cant).toFixed(4)}"> ${(element['cantidad_prod'] * cant).toFixed(4)}</td>
                        <td class="text-center"><button class="btn btn-outline-success rounded-circle add_to_shoplist"><i class="fa fa-plus-square"></i></button></td>
                        </tr>`);
                    }

                }
            }


        }

    });
})


$(document).on("click", ".add_to_shoplist", function () {


    var id_det_prod = $(this).parents('tr').data("prod");
    var type = $(this).parents('tr').data("type");
    var cantidad = parseFloat($(this).parents('tr').find('.cantidad_prod').html());

    // console.log(cantidad);
    for (const key in det_prod) {
        if (Object.hasOwnProperty.call(det_prod, key)) {
            const element = det_prod[key];
            if (element["id_det_prod"] == id_det_prod) {
                if (type == "receta") {

                    $.ajax({
                        method: "POST",
                        url: "detalleReceta",
                        headers: { 'X-Requested-With': 'XMLHttpRequest' },
                        dataType: 'json',
                        data: {
                            'id_rec': element['id_rec'],
                        },
                        success: function (respuesta) {
                            // console.log(respuesta);


                            for (const key in respuesta) {
                                if (Object.hasOwnProperty.call(respuesta, key)) {

                                    const element = respuesta[key];

                                    $("#tbody_shopList").append(`<tr class='tr_tshop'>
                                            <td><input type="text" value='${element["nom_ins"]}' class="form-control" name='nom_ins'><input type='hidden' value='${element['id_ins']}' name='id_ins'></td>
                                            <td><input type="text" value='${(cantidad * element["cant_det_rec"]).toFixed(4)}' class="form-control" name='cantidad_ins'></td>
                                            <td><input type="text" value='${(element["prec_final_det_rec"] * cantidad).toFixed(4)}' class="form-control" name='prec_fin'></td>
                                            <td><textarea type="text" placeholder="Comentarios/Instruciones" class="form-control" name="description"></textarea></td>
                                            <td class="text-center"><button class="btn btn-outline-danger rounded-circle delete_fromshoplist"><i class="fa fa-trash"></i></button></td>
                                            </tr>`);

                                }
                            }


                        }

                    });


                }
                else {
                    $("#tbody_shopList").append(`<tr class='tr_tshop'>
                    <td><input type="text" value="${element["nom_ins"]}" class="form-control" name='nom_ins'><input type='hidden' value='${element['id_ins']}' name='id_ins'></td>
                    <td><input type="text" value=${cantidad} class="form-control" name='cantidad_ins'></td>
                    <td><input type="text" value=${(element["costo_total"] * cantidad).toFixed(4)} class="form-control" name='prec_fin'></td>
                    <td><textarea type="text" placeholder="Comentarios/Instruciones" class="form-control" name='description'></textarea></td>
                    <td class="text-center"><button class="btn btn-outline-danger rounded-circle delete_fromshoplist" ><i class="fa fa-trash"></i></button></td>
                    </tr>`);
                }

            }

        }
    }



});
function juntarLista() {
    let item = $("#tbody_shopList").find(".table-active");
    var aux = [];
    $("#tbody_shopList tr").each(function () {
        let findedItem = $(this).find("input[name='id_ins']").val();
        console.log($(this).find("input[name='nom_ins']").val());
        console.log("iteracion");
        if (aux.length === 0) {
            aux.push({
                id_ins: $(this).find("input[name='id_ins']").val(),
                nom_ins: $(this).find("input[name='nom_ins']").val(),
                cant_ins: parseFloat($(this).find("input[name='cantidad_ins']").val()),
                prec_fin: parseFloat($(this).find("input[name='prec_fin']").val())
            });
            console.log("primer Log");
            console.log('Vacio: ', aux);

        } else {
            console.log(aux.find(x => x.id_ins === $(this).find("input[name='id_ins']").val()));
            if (aux.find(x => x.id_ins === $(this).find("input[name='id_ins']").val()) != undefined) {
                var AuxIndex = aux.findIndex(x => x.id_ins === $(this).find("input[name='id_ins']").val());
                aux[AuxIndex].cant_ins = parseFloat(aux[AuxIndex].cant_ins) + parseFloat($(this).find("input[name='cantidad_ins']").val());
                aux[AuxIndex].prec_fin = parseFloat(aux[AuxIndex].prec_fin) + parseFloat($(this).find("input[name='prec_fin']").val());
            }
            else {
                aux.push({
                    id_ins: $(this).find("input[name='id_ins']").val(),
                    nom_ins: $(this).find("input[name='nom_ins']").val(),
                    cant_ins: parseFloat($(this).find("input[name='cantidad_ins']").val()),
                    prec_fin: parseFloat($(this).find("input[name='prec_fin']").val())
                });
                console.log('Else: ', aux);

            }
        }


    })
    $("#tbody_shopList").empty();
    aux.forEach(element => {
        $("#tbody_shopList").append(`<tr class='tr_tshop'>
                    <td><input type="text" value="${element.nom_ins}" class="form-control" name='nom_ins'><input type='hidden' value='${element.id_ins}' name='id_ins'></td>
                    <td><input type="text" value=${(element.cant_ins).toFixed(4)} class="form-control" name='cantidad_ins'></td>
                    <td><input type="text" value=${(element.prec_fin).toFixed(4)} class="form-control" name='prec_fin'></td>
                    <td><textarea type="text" placeholder="Comentarios/Instruciones" class="form-control" name='description'></textarea></td>
                    <td class="text-center"><button class="btn btn-outline-danger rounded-circle delete_fromshoplist" ><i class="fa fa-trash"></i></button></td>
                    </tr>`);
    });
    console.log(aux);
}
$(document).on("click", ".tr_tshop", function () {
    if ($(this).hasClass("table-active")) {

        $(this).removeClass("table-active");

    } else {
        $(this).parents().parents().find(".table-active").removeClass('table-active');
        $(this).addClass("table-active");

    }
});
$(document).on("click", ".delete_fromshoplist", function () {
    $(this).parents("tr").remove();
})


function update_totals_proform() {
    var sub_total_prof = $("#sub_total_precio").val();
    var iva_prof = $("#imp_precio").val();
    var total_general_prof = $("#total_precio").val();
    $.ajax({
        method: "POST",
        url: "update/total_values_proform",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'sub_total_prof': sub_total_prof,
            'iva_prof': iva_prof,
            'total_general_prof': total_general_prof,
            'id_prof': id_proforma,
        },
        success: function (respuesta) {
            console.log("insertado");
            // var x = JSON.parse(respuesta);
            // console.log(x);
            // id_rec = x;

        }

    });
    console.log("id_evento", id_eve);
    $.ajax({
        method: "POST",
        url: "update/total_values_event",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'id_eve': id_eve,
            'total_facturado': total_general_prof,
            'id_prof': id_proforma,
        },
        success: function (respuesta) {
            console.log(respuesta);
            // var x = JSON.parse(respuesta);
            // console.log(x);
            // id_rec = x;

        }

    });

}
// Insertar Detalle Proforma
function insert_per_prof(id, cant, cost_uni, total_det, tipo_d) {
    console.log(tipo_d);
    $.ajax({
        method: "POST",
        url: "insert/detalle_per_prof",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'id_per': id,
            'cantidad_dias': cant,
            'costo_det': cost_uni,
            'total_detalle': total_det,
            'id_prof': id_proforma,
            'tipo_prod_prof': tipo_d
        },
        success: function (respuesta) {
            console.log("insertado");
            // var x = JSON.parse(respuesta);
            // console.log(x);
            // id_rec = x;

        }

    })

}
// ---------
function readCookie(name) {

    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + name.replace(/[\-\.\+\*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1")) || null;

}
// Boton Guardar Lista de Compra
$(document).on('click', "#insertar_lista", function () {

    let date_to_shop = $("#date_to_shop").val();
    // console.log(date_to_shop);
    misCookies = document.cookie;
    listaCookies = misCookies.split(";");
    let id_usu = readCookie("user");
    let nom_usu = readCookie("name");
    $.ajax({
        method: "POST",
        url: "insert/list_to_shop",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'date_to_shop': date_to_shop,
            'id_usu': id_usu,
            'nom_usu': nom_usu,
            'id_prof': id_proforma

        },
        success: function (respuesta) {
            id_list = respuesta;
            console.log(respuesta);
            insert_detalle_listaCompras(respuesta);


        }

    }).done(function () {
        alert("Se Guardo La Lista de Compras");
        // setTimeout(function () { asig_lista_personal(); }, 700);


    });

})
// Asignacion de lista al personal
function asig_lista_personal(id_per) {
    console.log("asginar personal");

    $.ajax({
        method: "POST",
        url: "insert/asig_lista_personal",
        headers: { 'X-Requested-With': 'XMLHttpRequest' },
        dataType: 'json',
        data: {
            'id_per': id_per,
            'id_list': id_list

        },
        success: function (respuesta) {
            // id_list = respuesta;
            // console.log(respuesta);
            // insert_detalle_listaCompras(respuesta);


        }

    })
}

// Detalle Lista de Compras para
function insert_detalle_listaCompras(id_list) {
    console.log(id_list);
    $("#tbody_shopList tr").each(function () {
        let item = $(this).find("input[name='nom_ins']").val();
        let cant = $(this).find("input[name='cantidad_ins']").val();
        let prec = $(this).find("input[name='prec_fin']").val();
        let description = $(this).find("textarea[name='description']").val();

        $.ajax({
            method: "POST",
            url: "insert/det_list_to_shop",
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            data: {
                'item_list': item,
                'cant_item': cant,
                'cost_item': prec,
                'description_item': description,
                'id_list': id_list

            },
            success: function (respuesta) {
                console.log("insertado el detalle");

            }

        });

    })

}
// Enviar Notificacion
$(document).on('click', '#send_notification', function () {
    let data = personalToNotificate.row({ selected: true }).data();
    // console.log("send notification");
    console.log();
    if (data['token_notification'] != null) {

        $.ajax({
            method: "POST",
            url: "send/notification",
            data: {
                "id_list": id_list,
                'token_notification': data['token_notification'],
                "id_per": data['id_per']

            },
            headers: { 'X-Requested-With': 'XMLHttpRequest' },
            dataType: 'json',
            success: function (result) {
                console.log(result);



            }
        }).done(function () {
            alert("La Lista fue eviada a : " + data['nom_per']);
            setTimeout(function () { asig_lista_personal(data['id_per']); }, 700);
        });
    } else {
        alert("El personal : " + data['nom_per'] + " No tiene habilitada la opcion de recibir notificaciones");
    }

})
